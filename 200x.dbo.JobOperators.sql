﻿IF OBJECT_ID('dbo.JobOperators','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[JobOperators]
GO


/******************************************************************************
* Name
	[dbo].[JobOperators]

* Author
	Adam Bean
	
* Date
	2010.01.12
	
* Synopsis
	Procedure to add job operators to specified job(s)
	
* Description
	Allows you to enable operators on specified (or all) jobs. Operators can be:
	- Email, Page or NetSend
	Optionally, can also use this procedure to enable file system notifications into the application event log

* Examples
	EXEC [dbo].[JobOperators] @Email = '2', @EventLog = 2, @EmailOperator = 'SQLDBA' -- Will email the SQLDBA operator and write to application log on failure

* Dependencies
	dbo.Split_fn
	If specifying an operator, said operator must be valid

* Parameters
	@JobName = Job name(s) to add operators for (CSV supported) [leave NULL for all]
	@JobNameExclude = Job name(s) to exclude operators for (CSV supported) [leave NULL for all]
	@ExcludeExisting = Do not update any job operators that already have an operators
	@ExcludeOperator = Do not update any job operators that have these names (CSV supported) [leave NULL for no exclusions]
	@EventLog = Log to event log (0 = Never, 1 = On success, 2 = On failure, 3 = Always)
	@Email = Send email (0 = Never, 1 = On success, 2 = On failure, 3 = Always)
	@Page = Send page (0 = Never, 1 = On success, 2 = On failure, 3 = Always)
	@NetSend = Send netsend (0 = Never, 1 = On success, 2 = On failure, 3 = Always)
	@EmailOperator = Operator to email
	@PageOperator = Operator to page
	@NetSendOperator = Operator to netsend
	@Debug = Show work to be done but don't execute procedure
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20100205	Adam Bean		Added checks to make sure @Email, @Page or @NetSend was passed
								Fixed Changed column to properly display that a job was updated
	20100330	Adam Bean		Fixed logic for @ExcludeExisting
	20100614	Adam Bean		Fixed issue if job had single quotes in name
	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
	20170111	Srujana Gorge	Removed ORDER BY ordinal column number
	20170405	Adam Bean		Added @ExcludeOperator to support omitting any existing operators matching these names
	20170412	Adam Bean		Expanded paramater data types 
******************************************************************************/

CREATE PROCEDURE [dbo].[JobOperators]
(
	@JobName					NVARCHAR(512)	= NULL
	,@JobNameExclude			NVARCHAR(512)	= NULL
	,@ExcludeExisting			BIT				= 0
	,@ExcludeOperator			NVARCHAR(256)	= NULL
	,@EventLog					CHAR(1)			= 0
	,@Email						CHAR(1)			= 0
	,@Page						CHAR(1)			= 0
	,@NetSend					CHAR(1)			= 0
	,@EmailOperator				VARCHAR(128)	= NULL
	,@PageOperator				VARCHAR(128)	= NULL
	,@NetSendOperator			VARCHAR(128)	= NULL
	,@Debug						BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@SQL						VARCHAR(MAX)
	,@Current_Email_Operator	VARCHAR(128)
	,@Current_Page_Operator		VARCHAR(128)
	,@Current_NetSend_Operator	VARCHAR(128)

-- Make sure at least one operator type was passed
IF 
	(
		@EmailOperator IS NULL 
		AND @PageOperator IS NULL 
		AND @PageOperator IS NULL
	)
BEGIN
	PRINT 'Pass in at least one value for any of the operator parameters: @EmailOperator, @PageOperator, @PageOperator'
	RETURN 2
END

-- If an operator was passed, make sure it's enabled
IF 
	(
		(@EmailOperator IS NOT NULL AND @Email = 0)
		OR
		(@PageOperator IS NOT NULL AND @Page = 0)
		OR
		(@NetSendOperator IS NOT NULL AND @NetSend  = 0)
	)
BEGIN
	PRINT 'When passing @EmailOperator, @PageOperator, @PageOperator, the cooresponding @Email, @Page or @NetSend must be passed (0 = Never, 1 = On success, 2 = On failure, 3 = Always)'
	RETURN 2
END


-- Setup a temp working table to show before and after log operators
IF OBJECT_ID('tempdb.dbo.#operators') IS NOT NULL
	DROP TABLE #operators
	
-- Populate working table with data based on parameters
SELECT 
	sj.[name]						AS [JobName]
	,CASE sj.[notify_level_email]
		WHEN 0 THEN 'Never'
		WHEN 1 THEN 'On Success'
		WHEN 2 THEN 'On Failure'
		WHEN 3 THEN 'Always'
	END								AS [notify_level_email]
	,eo.[name]						AS [Current_Email_Operator]
	,CAST(NULL AS VARCHAR(128))		AS [New_Email_Operator]
	,CASE sj.[notify_level_page]
		WHEN 0 THEN 'Never'
		WHEN 1 THEN 'On Success'
		WHEN 2 THEN 'On Failure'
		WHEN 3 THEN 'Always'
	END								AS [notify_level_page]
    ,po.[name]						AS [Current_Page_Operator]
    ,CAST(NULL AS VARCHAR(128))		AS [New_Page_Operator]
	,CASE sj.[notify_level_netsend]
		WHEN 0 THEN 'Never'
		WHEN 1 THEN 'On Success'
		WHEN 2 THEN 'On Failure'
		WHEN 3 THEN 'Always'
	END								AS [notify_level_netsend]
	,ns.[name]						AS [Current_NetSend_Operator]
	,CAST(NULL AS NVARCHAR(128))	AS [New_NetSend_Operator]
	,CASE sj.[notify_level_eventlog]
		WHEN 0 THEN 'Never'
		WHEN 1 THEN 'On Success'
		WHEN 2 THEN 'On Failure'
		WHEN 3 THEN 'Always'
	END	AS [notify_level_eventlog]
	,CAST(0 AS VARCHAR(48))				AS [Changed]
INTO #operators
FROM [msdb].[dbo].[sysjobs] sj
LEFT JOIN [msdb].[dbo].[sysoperators] eo
      ON sj.[notify_email_operator_id] = eo.[id]
LEFT JOIN [msdb].[dbo].[sysoperators] po
      ON sj.[notify_page_operator_id] = po.[id]
LEFT JOIN [msdb].[dbo].[sysoperators] ns
      ON sj.[notify_netsend_operator_id] = po.[id]
LEFT JOIN [dbo].[Split_fn](@JobName,',') ji
	ON sj.[name] = ji.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@JobNameExclude,',') je
	ON sj.[name] = je.[item] COLLATE DATABASE_DEFAULT
WHERE ((ji.[item] IS NOT NULL AND @JobName IS NOT NULL) OR @JobName IS NULL) -- Specified job(s), or all
AND je.[item] IS NULL -- All but excluded job(s)
ORDER BY [JobName]


-- Add operators to specified job(s)
DECLARE #JobList CURSOR FOR
SELECT
	REPLACE([JobName],'''','''''')
	,[Current_Email_Operator]
	,[Current_Page_Operator]
	,[Current_NetSend_Operator]
FROM #operators

OPEN #JobList
FETCH NEXT FROM #JobList INTO @JobName, @Current_Email_Operator, @Current_Page_Operator, @Current_NetSend_Operator
WHILE @@FETCH_STATUS = 0
BEGIN
	
	-- Populate working table with new operators
	UPDATE #operators
	SET [New_Email_Operator] = @EmailOperator
		,[New_Page_Operator] = @PageOperator
		,[New_NetSend_Operator] = @NetSendOperator
	WHERE REPLACE([JobName],'''','''''') = @JobName
	
	-- If @ExcludeExisting was passed, do not change
	IF @ExcludeExisting = 1 AND (@Current_Email_Operator IS NOT NULL OR @Current_Page_Operator IS NOT NULL OR @Current_NetSend_Operator IS NOT NULL)
	BEGIN
		UPDATE #operators
		SET [New_Email_Operator] = '@ExcludeExisting was passed, operator will not change.'
			,[New_Page_Operator] = '@ExcludeExisting was passed, operator will not change.'
			,[New_NetSend_Operator] = '@ExcludeExisting was passed, operator will not change.'
		WHERE [JobName] = @JobName
	END
	-- If @ExcludeOperator was passed, do not change
	ELSE IF 
		(
			@Current_Email_Operator IN (SELECT [item] FROM dbo.Split_fn(@ExcludeOperator,','))
			OR @PageOperator IN (SELECT [item] FROM dbo.Split_fn(@ExcludeOperator,','))
			OR @NetSendOperator IN (SELECT [item] FROM dbo.Split_fn(@ExcludeOperator,','))
		)
	BEGIN
		UPDATE #operators
		SET [New_Email_Operator] = '@ExcludeOperator was passed, operator will not change.'
			,[New_Page_Operator] = '@ExcludeOperator was passed, operator will not change.'
			,[New_NetSend_Operator] = '@ExcludeOperator was passed, operator will not change.'
		WHERE [JobName] = @JobName		
	END
	-- Update the job
	ELSE 
	BEGIN
		SET @SQL = 
		'
		EXEC [msdb].[dbo].[sp_update_job] 
			@job_name = ''' + @JobName + '''
			,@notify_level_eventlog = ' + @EventLog + '
			,@notify_level_email = ' + @Email + '
			,@notify_level_page = ' + @Page + '
			,@notify_level_netsend = ' + @NetSend + '
			,@notify_email_operator_name = ''' + ISNULL(@EmailOperator,'') + '''
			,@notify_page_operator_name = ''' + ISNULL(@PageOperator, '') + '''
			,@notify_netsend_operator_name = ''' + ISNULL(@NetSendOperator,'') + '''
		'
	END
	
	IF @Debug = 1
	BEGIN
		PRINT (@SQL)
		UPDATE #operators
		SET [Changed] = '@Debug is enabled, see messages tab for syntax'
	END
	ELSE
	BEGIN
		EXEC (@SQL)
		-- Update working table to show that log operator was changed
		UPDATE #operators
		SET [Changed] = 1
		WHERE [JobName] = @JobName
		AND ISNULL([New_Email_Operator],'') NOT LIKE '%@ExcludeExisting%'
		AND ISNULL([New_Page_Operator],'') NOT LIKE '%@ExcludeExisting%'
		AND ISNULL([New_NetSend_Operator],'') NOT LIKE '%@ExcludeExisting%'
	END

FETCH NEXT FROM #JobList INTO @JobName, @Current_Email_Operator, @Current_Page_Operator, @Current_NetSend_Operator
END
CLOSE #JobList
DEALLOCATE #JobList

-- Return work done
SELECT * FROM #operators
ORDER BY [JobName]

SET NOCOUNT OFF



