IF OBJECT_ID('[dbo].[Split_fn]') IS NOT NULL
	DROP FUNCTION [dbo].[Split_fn]
GO

/*******************************************************************************************************
**	Name:			dbo.Split_fn
**	Desc:			Split up a delimited list and return a recordset
**	Auth:			Unknown
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090728	Adam Bean		General cleanup
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE FUNCTION [dbo].[Split_fn]
(
	@sInputList VARCHAR(8000)			-- List of delimited items
	,@sDelimiter VARCHAR(8)		= ','	-- delimiter that separates items
) 
RETURNS @List TABLE ([item] VARCHAR(8000)) 

AS

BEGIN
DECLARE @sItem VARCHAR(8000) 
WHILE CHARINDEX(@sDelimiter,@sInputList,0) <> 0
BEGIN
	SELECT
		@sItem=RTRIM(LTRIM(SUBSTRING(@sInputList,1,CHARINDEX(@sDelimiter,@sInputList,0)-1)))
		,@sInputList=RTRIM(LTRIM(SUBSTRING(@sInputList,CHARINDEX(@sDelimiter,@sInputList,0)+LEN(@sDelimiter),LEN(@sInputList))))
	
	IF LEN(@sItem) > 0
		INSERT INTO @List SELECT @sItem
	END

	IF LEN(@sInputList) > 0
		INSERT INTO @List SELECT @sInputList-- Put the last item in
RETURN 
END
GO