﻿IF OBJECT_ID('[dbo].[BackUpDB]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[BackUpDB]
GO

/******************************************************************************
* Name
	[dbo].[BackUpDB]

* Author
	Adam Bean
	
* Date
	2008.10.08
	
* Synopsis
	Runs SQL Server database backups
	
* Description
	Runs SQL Server database backups

* Examples
	EXEC [dbo].[BackUpDB] @BackupType = 'D', @BackupLocation = '\\prodsqlbackup\SQLBU', @UseBackupPrefix = 1, @UseDateSuffix = 1

* Dependencies
	dbo.Split_fn
	dbo.BackupDB_Native
	dbo.BackupDB_LiteSpeed
	dbo.BackupDB_Idera
	dbo.BackupDB_DDBoost
	dbo.sysdatabases_vw
	dbo.sysmasterfiles_vw

* Parameters
	@BackupType					- D = Full, I = Differential, F = Individual File, G = Individual File Group, L = Transaction Log
	@BackupProduct				- 'Native','LiteSpeed','RedGate','Idera','DDBoost','DYNAMIC', Leaving DYNAMIC will use whatever the instance has
	@DBName						- Database name(s) to query, CSV supported, null for all
	@DBNameExclude				- Database name(s) to exclude, CSV supported
	@BackupPrefix				- Prefix to the backup name
	@BackupSuffix				- Suffix to the backup name
	@BackupLocation				- Location to backup, defaults to instance default
	@UseFolderStructure			- Pre determined folder structure to use based on type (\TLOG, \FULL, etc.)
	@AltInstanceFolders			- Instead of using the SERVER\INSTANCE from @UseFolderStructure
	@UseBackupPrefix			- Pre determined backup suffix based on type (TLOG_, FULL_, etc.)
	@UseDateSuffix				- Add a time stamp to the backup suffix
	@FileCount					- Number of files to backup to
	@SkipRecentBackups			- To skip any backups taken within the minutes of @SkipRecentBackupsMinutes
	@SkipRecentBackupsMinutes	- Used for @SkipRecentBackups
	@SkipRecentRestoreMinutes	- If a database was recently restored skip doing a backup.  This is the number of minutes.  Default won't skip any.
	@CleanupBackups				- Remove backups from the file system based on @CleanupBackupsMinutes
	@CleanupBackupsMinutes		- Used for @CleanupBackups
	@Init						- Controls whether the backup operation appends to or overwrites the existing backup sets on the backup media
	@Verify						- Verifies the backup set
	@Password					- Sets the password for the backup set
	@Compression				- Bit value if native, compression level if vendor
	@CopyOnly					- Specifies that the backup is a copy-only backup, which does not affect the normal sequence of backups
	@Stats						- Displays a message each time another percentage completes, and is used to gauge progress
	@RetainDays					- Specifies when the backup set for this backup can be overwritten
	@File						- The logical file to backup
	@FileGroup					- The file group to backup
	@Description				- Description to add to backup set
	@CleanupHistory				- Clean up backup history
	@UseAGBackupPreferences		- Use Availability Group backup preferences to determine which databases to backup
	@Debug						- Bit value, do the work or just print it
	@Threads					- Vendor specific
	@EncryptionLevel			- Vendor specific
	@EncryptionKey				- Vendor specific
	@Throttle					- Vendor specific
	@Affinity					- Vendor specific
	@Priority					- Vendor specific
	@Domain						- Vendor specific
	@DDHost						- Vendor specific
	@DDUser						- Vendor specific
	@DDDevicePath				- Vendor specific

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20090521	Adam Bean		Added support for transaction log backups to all be backed up without specifying name
	20090611	Matt Stanford	Converted sys.databases and sys.master_files to use the views.
	20090629	Adam Bean		Fixed bug after added support for all transaction log backups
								Switched to SERVERPROPERTY to retrieve servername
								Fixed some minor issues to support Latin1_General_BIN
								Removed master from tlog backups 
	20090929	Adam Bean		Global header formatting & general clean up
	20091129	Adam Bean		Added support for RedGate
	20100104	Adam Bean		Removed model from tlog backups
	20100427	Adam Bean		Added @BackupPrefix, @UseBackupPrefix, @SkipRecentBackups, @SkipRecentBackupsMinutes
								Changed @FolderStructure to @UseFolderStructure and @DateSuffix to @UseDateSuffix
								Removed multi_user requirement
	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
	20110216    John DelNostro  Added @CopyOnly Option
	20110419	Matt Stanford	Removed the model tlog restriction, added @SkipRecentRestoreMinutes	& reformatted header
	20110912	Matt Stanford	Updated the cleanup backup logic to check only the most recent backup time
	20140227	John DelNostro	Changed @CleanupBackupHours from TINYINT to INT
	20150211	Mike Wolfe		Updated tests for @Compression, since 2008 R2, 2012, and 2014 Standard now support it
	20150803	Jeff Gogel		Updated to support @Compression = 0 passing to BackupDB_Native to use NO_COMPRESSION argument
	20150804	Jeff Gogel		Updated to Availability Group backup preferences
	20150807	Jeff Gogel		Fix for secondary read only replicas requiring ReadOnly backups
	20151006	Jeff Gogel		Added logic to exclude tlog backups for databases in full recovery without an initial full
	20151016	Jeff Gogel		Added support for DDBoost
	20160307	Jeff Gogel		Added @FileCount support for backup to multiple files using Native backups
	20170314	Adam Bean		Added @AltInstanceFolders to switch from instance being a sub folder to being SERVER_INSTANCE
								Added DIFF backups to the removal of master from the backup set
								Fixed Idera file naming conventions
******************************************************************************/
CREATE PROCEDURE [dbo].[BackUpDB] 
(
	@BackupType					CHAR(1)			= 'D'
	,@BackupProduct				CHAR(16)		= 'DYNAMIC'
	,@DBName					VARCHAR(4000)	= NULL
	,@DBNameExclude				VARCHAR(4000)	= NULL
	,@BackupPrefix				VARCHAR(128)	= NULL
	,@BackupSuffix				VARCHAR(128)	= NULL
	,@BackupLocation			VARCHAR(512)	= NULL
	,@UseFolderStructure		BIT				= 0
	,@AltInstanceFolders		BIT				= 0
	,@UseBackupPrefix			BIT				= 0
	,@UseDateSuffix				BIT				= 0
	,@FileCount					INT				= 1
	,@SkipRecentBackups			BIT				= 0 
	,@SkipRecentBackupsMinutes	SMALLINT		= 30
	,@SkipRecentRestoreMinutes	SMALLINT		= -1
	,@CleanupBackups			BIT				= 0
	,@CleanupBackupHours		INT				= 0
	-- Native and shared options --
	,@Init						BIT				= 0
	,@Verify					BIT				= NULL
	,@Password					VARCHAR(4000)	= NULL
	,@Compression				TINYINT			= NULL
	,@CopyOnly					BIT				= 0
	,@Stats						TINYINT			= NULL
	,@RetainDays				TINYINT			= NULL
	,@File						VARCHAR(128)	= NULL
	,@FileGroup					VARCHAR(128)	= NULL
	,@Description				VARCHAR(512)	= NULL
	,@CleanupHistory			BIT				= NULL
	,@UseAGBackupPreferences	BIT				= 1
	,@Debug						BIT				= 0
	-- Vendor specific options --
	,@Threads					TINYINT			= NULL -- LiteSpeed / RedGate
	,@EncryptionLevel			TINYINT			= NULL -- LiteSpeed
	,@EncryptionKey				VARCHAR(4000)	= NULL -- LiteSpeed
	,@Throttle					TINYINT			= NULL -- LiteSpeed
	,@Affinity					TINYINT			= NULL -- LiteSpeed
	,@Priority					TINYINT			= NULL -- LiteSpeed
	,@Domain					VARCHAR(64)		= NULL
	,@DDHost					VARCHAR(256)	= NULL -- DDBoost
	,@DDUser					VARCHAR(64)		= NULL -- DDBoost
	,@DDDevicePath				VARCHAR(64)		= NULL -- DDBoost
)	

AS

SET NOCOUNT ON

DECLARE 
	@ServerName				VARCHAR(128)
	,@InstanceName			VARCHAR(128)
	,@DBBackupPath			VARCHAR(512)
	,@BackupName			VARCHAR(256)
	,@ReturnCode			TINYINT
	,@BackupTimeStamp		VARCHAR(16)
	,@NativeFileExt			VARCHAR(256)
	,@LiteSpeedFileExt		VARCHAR(256)
	,@RedGateFileExt		VARCHAR(256)
	,@IderaFileExt			VARCHAR(256)
	,@DBBackupFileExt		VARCHAR(256)
	,@ProductionVersion		TINYINT
	,@ProductionSubVersion	TINYINT
	,@ProductEdition		VARCHAR(16)
	,@CleanupBUHist			DATETIME
	,@CleanupBackupsSQL		VARCHAR(512)
	,@CleanupFileName		VARCHAR(768)
	,@FileExists			TINYINT
	,@HasLiteSpeed			BIT
	,@HasRedGate			BIT
	,@HasIdera				BIT
	,@HasDDBoost			BIT
	,@TakeCopyOnly			BIT

IF @Debug = 1
	PRINT '/**** DEBUG ENABLED ****/'

-- Find out what's installed	
SET @HasLiteSpeed = 0
SET @HasRedGate = 0
SET @HasIdera = 0
SET @HasDDBoost = 0

IF OBJECT_ID('master.dbo.xp_backup_database','X') IS NOT NULL
	SET @HasLiteSpeed = 1
ELSE IF OBJECT_ID('master.dbo.sqlbackup','X') IS NOT NULL
	SET @HasRedGate = 1
ELSE IF OBJECT_ID('master.dbo.xp_ss_backup','X') IS NOT NULL
	SET @HasIdera = 1
ELSE IF OBJECT_ID('master.dbo.emc_run_backup','PC') IS NOT NULL
	SET @HasDDBoost = 1

-- Populate some variables
SET @ReturnCode = 1
SET @ServerName = (SELECT CAST(SERVERPROPERTY('SERVERNAME') AS VARCHAR(128)))
SET @ProductEdition = CAST(SERVERPROPERTY('Edition') AS VARCHAR(16)) 
SET @CleanupBUHist = (SELECT DATEADD(dd, -30, GETDATE()))

-- Populate version variables. There are newer, more concise ways to extract the information, but
-- they aren't supported by SQL 2000 and SQL 2005. Instead, we take the version/"sub version" and 
-- extract them using SUBSTRING() and CHARINDEX()
--
-- @ProductVersion is the major release family: 10,11,12 (2008/2012/2014)
-- @ProductSubVersion is used to detect major releases (2008 R2 is the only release designated by a sub version -- @ProductSubVersion="50" (@ProductVersion="10"))
--
DECLARE @sVersion VARCHAR(16)																			 -- Temporary variable used when parsing below to make it more readable.
SET @sVersion = CAST(SERVERPROPERTY('ProductVersion') AS VARCHAR(16))									 -- Set @sVersion with the ProductVersion, cast as a string
SELECT	@ProductionVersion = SUBSTRING(@sVersion,0,CHARINDEX('.',@sVersion,0))							 -- Set @ProductionVersion to the main release family: 8,9,10,11,12
		, @ProductionSubVersion = SUBSTRING(@sVersion
			,CHARINDEX('.',@sVersion,0) + 1 -- start at the first period
			,CHARINDEX('.',@sVersion, CHARINDEX('.',@sVersion,0) + 1) - (CHARINDEX('.',@sVersion,0) + 1) -- Extract from the first period up to the second
		)

-- Create a temp table to be used for checking file existance on cleanup
IF @CleanupBackups = 1
BEGIN
	IF OBJECT_ID('tempdb.dbo.#File_Results') IS NOT NULL
	DROP TABLE #File_Results
		CREATE TABLE #File_Results
		(
			[File_Exists]				TINYINT
			,[File_is_a_Directory]		TINYINT
			,[Parent_Directory_Exists]	TINYINT
		)
END

-- Test our inputs
-- If no backup location was passed, use the default
IF @BackupLocation IS NULL
	SELECT @BackupLocation = [dbo].[SQLServerBackupDir_fn]()

-- Backup Product
IF @BackupProduct NOT IN ('Native','LiteSpeed','RedGate','Idera','DDBoost','DYNAMIC')
BEGIN
	PRINT '@BackupProduct must be either ''Native'',''LiteSpeed'',''RedGate'', ''Idera'', ''DDBoost'' or ''DYNAMIC'' which will determine which product your system can use'
	RETURN
END

-- If backup product is set to dynamic, figure out which product the system has
IF @BackupProduct = 'DYNAMIC'
BEGIN
	IF @HasLiteSpeed = 1
		SET @BackupProduct = 'LiteSpeed'
	ELSE IF @HasRedGate = 1
		SET @BackupProduct = 'RedGate'
	ELSE IF @HasIdera = 1
		SET @BackupProduct = 'Idera'
	ELSE IF @HasDDBoost = 1
		SET @BackupProduct = 'DDBoost'
	ELSE SET @BackupProduct = 'Native'
END

-- Test to see if system has backup product specified, if not, revert to Native
IF @BackupProduct = 'LiteSpeed'
BEGIN
	IF @HasLiteSpeed = 0
	BEGIN
		PRINT '@BackupProduct was specified as LiteSpeed, but your system does not have LiteSpeed. @BackupProduct has been changed to ''Native'''
		SET @BackupProduct = 'Native'
	END
END
ELSE IF @BackupProduct = 'RedGate'
BEGIN
	IF @HasRedGate = 0
	BEGIN
		PRINT '@BackupProduct was specified as RedGate, but your system does not have RedGate. @BackupProduct has been changed to ''Native'''
		SET @BackupProduct = 'Native'
	END
END
ELSE IF @BackupProduct = 'Idera'
BEGIN
	IF @HasIdera = 0
	BEGIN
		PRINT '@BackupProduct was specified as Idera, but your system does not have Idera. @BackupProduct has been changed to ''Nativ%'''
		SET @BackupProduct = 'Native'
	END
END
ELSE IF @BackupProduct = 'DDBoost'
BEGIN
	IF @HasDDBoost = 0
	BEGIN
		PRINT '@BackupProduct was specified as DDBoost, but your system does not have DDBoost. @BackupProduct has been changed to ''Nativ%'''
		SET @BackupProduct = 'Native'
	END
END

-- Test compression
-- Compression is enterprise only on SQL 2008, but is available in 
-- Enterprise AND Standard Edition for SQL 2008 R2, 2012, and 2014
IF @Compression IS NOT NULL AND @BackupProduct = 'Native'
BEGIN
	IF @ProductionVersion IN ('8','9') -- Compression is only available in 2008+
	BEGIN
		PRINT 'Compression can only be used in SQL Server 2008 (or later), @Compression has been changed to NULL'
		SET @Compression = NULL
	END
	ELSE IF @ProductionVersion = '10' AND @ProductionSubVersion = '00' AND @ProductEdition NOT LIKE 'Enterprise%' -- Compression is only available for 2008 EE
	BEGIN
		PRINT 'Compression can only be used in SQL Server 2008 Enterprise Edition, @Compression has been changed to NULL'
		SET @Compression = NULL
	END
	ELSE IF @ProductEdition NOT LIKE 'Standard%' AND @ProductEdition NOT LIKE 'Enterprise%' -- Must be Standard or Enterprise for 2008R2 +
	BEGIN
		PRINT 'Compression can only be used in SQL Server 2008 R2+ Standard or Enterprise Edition, @Compression has been changed to NULL'
		SET @Compression = NULL
	END
END


-- Test the file
IF @BackupType = 'F' AND @File IS NULL
BEGIN
	PRINT 'You must pass a valid @File to process a file backup'
	RETURN
END
ELSE IF @BackupType = 'F'
BEGIN
	IF NOT EXISTS	(
						SELECT [name] 
						FROM [dbo].[sysmasterfiles_vw]
						WHERE [database_name] = @DBName
						AND [name] = @File
					)
	BEGIN					
		PRINT '' + @File + ' is not a valid file for database ' + @DBName + ''
		RETURN	
	END
END

-- Test the filegroup
IF @BackupType = 'G'
BEGIN
	IF OBJECT_ID('tempdb.dbo.#filegroup') IS NOT NULL
		DROP TABLE #filegroup
	CREATE TABLE #filegroup
	(
		[groupname] sysname
	) 
	INSERT INTO #filegroup
	EXEC('SELECT [name] FROM [' + @DBName + '].[sys].[filegroups] WHERE [name] = ''' + @FileGroup + '''')

	IF (SELECT COUNT (*) FROM #filegroup) = 0
	BEGIN					
		PRINT '' + @FileGroup + ' is not a valid file for database ' + @DBName + ''
		RETURN	
	END
END

-- If backup type is transaction log, check recovery model
IF @BackupType = 'L'
BEGIN
	IF 
		(
			SELECT [recovery_model_desc]
			FROM [dbo].[sysdatabases_vw]
			WHERE [name] = @DBName
		) = 'SIMPLE'
	BEGIN
		PRINT '' + @DBName + '''s recovery model is set to SIMPLE, you can not run a transaction log back on this database.'
		RETURN	
	END
END

-- Create the file prefix based on type
IF @UseBackupPrefix = 1
BEGIN
	IF @BackupType = 'D'
		SET @BackupPrefix = 'FULL_'
	ELSE IF @BackupType = 'I'
		SET @BackupPrefix = 'DIFF_'
	ELSE IF @BackupType = 'F'
		SET @BackupPrefix = 'FILE_'
	ELSE IF @BackupType = 'G'
		SET @BackupPrefix = 'FILEGROUP_'
	ELSE IF @BackupType = 'L'
		SET @BackupPrefix = 'LOG_'
END
ELSE
	SET @BackupPrefix = ''
	
-- Add a time stamp to the extension (YYYMMDD_HHMM)
IF 	@UseDateSuffix = 1
BEGIN
	SET @BackupTimeStamp = '_'
							+ CAST(DATEPART(yy, GETDATE()) AS VARCHAR) 
							+ RIGHT('0' + CAST(DATEPART(mm, GETDATE()) AS VARCHAR),2)
							+ RIGHT('0' + CAST(DATEPART(dd, GETDATE()) AS VARCHAR),2)
							+ '_' 
							+ RIGHT('0' + CAST(DATEPART(hh, GETDATE()) AS VARCHAR),2)
							+ RIGHT('0' + CAST(DATEPART(mi, GETDATE()) AS VARCHAR),2)
END
ELSE
	SET @BackupTimeStamp = ''

-- Create the file extension based on product
SET @NativeFileExt		= @BackupTimeStamp + '.bak'
SET @LiteSpeedFileExt	= @BackupTimeStamp + '.bkp'
SET @RedGateFileExt		= @BackupTimeStamp + '.sqb'
SET @IderaFileExt		= @BackupTimeStamp + '.safe'

IF @BackupProduct = 'Native'
	SET @DBBackupFileExt = @NativeFileExt
ELSE IF @BackupProduct = 'LiteSpeed'
	SET @DBBackupFileExt = @LiteSpeedFileExt
ELSE IF @BackupProduct = 'RedGate'
	SET @DBBackupFileExt = @RedGateFileExt
ELSE IF @BackupProduct = 'Idera'
	SET @DBBackupFileExt = @IderaFileExt

-- If a user suffix was passed, prepend to extension
IF @BackupSuffix IS NOT NULL
	SET @DBBackupFileExt = '_' + @BackupSuffix + '_' + @DBBackupFileExt

-- If @BackupLocation was passed without a trailing backslash, add it
IF RIGHT(@BackupLocation,1) != '\'
	SET @BackupLocation = @BackupLocation + '\'

-- Determine backup path based on servername if desired
-- @BackupLocation \ ServerName \ InstanceName (if applicable)
IF @UseFolderStructure = 1
BEGIN
	IF @AltInstanceFolders = 1
	BEGIN
		IF CHARINDEX('\',@ServerName) > 1
			BEGIN
				SET @ServerName = LEFT(@ServerName,CHARINDEX('\',@ServerName) - 1)
				SELECT @InstanceName = CAST(SERVERPROPERTY('InstanceName') AS VARCHAR(96))
				SET @DBBackupPath = @BackupLocation + @ServerName + '_' + @InstanceName + '\'
			END
		ELSE
			BEGIN
				SET @ServerName = @ServerName
				SET @DBBackupPath = @BackupLocation + @ServerName + '\'
			END		
	END
	ELSE
	BEGIN
		IF CHARINDEX('\',@ServerName) > 1
			BEGIN
				SET @ServerName = LEFT(@ServerName,CHARINDEX('\',@ServerName) - 1)
				SELECT @InstanceName = CAST(SERVERPROPERTY('InstanceName') AS VARCHAR(96))
				SET @DBBackupPath = @BackupLocation + @ServerName + '\' + @InstanceName + '\'
			END
		ELSE
			BEGIN
				SET @ServerName = @ServerName
				SET @DBBackupPath = @BackupLocation + @ServerName + '\DEFAULT\'
			END
	END
	IF @BackupType = 'D'
		SET @DBBackupPath = @DBBackupPath + 'FULL\'
	IF @BackupType = 'I'
		SET @DBBackupPath = @DBBackupPath + 'DIFF\'
	IF @BackupType = 'F'
		SET @DBBackupPath = @DBBackupPath + 'FILE\'
	IF @BackupType = 'G'
		SET @DBBackupPath = @DBBackupPath + 'FILEGROUP\'
	IF @BackupType = 'L'
		SET @DBBackupPath = @DBBackupPath + 'TLOG\'
END
ELSE
	SET @DBBackupPath = @BackupLocation

-- Setup table to hold database names
DECLARE @DBNames TABLE
	(
		[name]				sysname
		,[TakeCopyOnly]		BIT
	)
	
-- Populate @DBNames with database names to be backed up and if it should be a copy only backup for a secondary replica
INSERT INTO @DBNames
SELECT [name]
	  ,CASE WHEN @BackupType = 'L' THEN 0														-- Cannot take copy_only TLog backups
			WHEN ((SELECT COUNT(0)																-- Check if it's a secondary replica
						FROM dbo.sysdatabases_vw sdb
						 JOIN sys.dm_hadr_availability_replica_states rs
							ON sdb.replica_id = rs.replica_id
						 WHERE sdb.name = s.name AND rs.role_desc = 'SECONDARY'
				  ) > 0
				  AND (sys.fn_hadr_backup_is_preferred_replica(s.[name]) = 1))					-- Check for backup preference
			THEN 1																				-- Must take copy_only on secondaries
			ELSE @CopyOnly
		END AS [TakeCopyOnly]
FROM [dbo].[sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE [name] != 'tempdb'
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database(s), or all
AND de.[item] IS NULL -- All but excluded databases
AND NOT (-- Either backup the specified database(s) transaction logs or all
		[name] IN (SELECT [name] 
					FROM [dbo].[sysdatabases_vw] sdb
					JOIN [sys].[database_recovery_status] rs
						ON sdb.database_id = rs.database_id
					WHERE [recovery_model_desc] = 'SIMPLE'
						  OR (sdb.[recovery_model_desc] = 'FULL' AND rs.last_log_backup_lsn IS NULL)  -- Skip DB's that have no full backup
					)
		AND @BackupType = 'L'
	)
AND ((@UseAGBackupPreferences = 1 AND sys.fn_hadr_backup_is_preferred_replica(s.[name]) = 1) -- Preferred AG to backup 
		OR @UseAGBackupPreferences = 0) -- Or disregard AG preference
AND [source_database_id] IS NULL
AND [state_desc] = 'ONLINE'
AND [is_in_standby] = 0
ORDER BY [name]

-- Remove master/model if running diffs or transaction log backups
IF @BackupType IN ('I','L')
BEGIN
	IF EXISTS(SELECT [name] FROM @DBNames WHERE [name] = 'master')
		DELETE FROM @DBNames WHERE [name] = 'master'
END

-- Skip databases that were recently backed up
IF @SkipRecentBackups = 1
BEGIN
	DELETE d
	FROM @DBNames d
	INNER JOIN [msdb].[dbo].[backupset] b
		ON d.[name] = b.[database_name] COLLATE DATABASE_DEFAULT
	WHERE b.[backup_finish_date] > DATEADD(mi,-@SkipRecentBackupsMinutes,GETDATE())
	AND b.[type] = @BackupType
END

-- Skip databases that were recently restored here
IF @SkipRecentRestoreMinutes > 0
BEGIN
	DELETE d
	FROM @DBNames d
	INNER JOIN [msdb].[dbo].[restorehistory] b
		ON d.[name] = b.[destination_database_name] COLLATE DATABASE_DEFAULT
	WHERE b.[restore_date] > DATEADD(mi,-@SkipRecentRestoreMinutes,GETDATE())
END

-- Cleanup file system backups
-- If @UseDateSuffix is not used, this has to run prior to backups running as it will backup and then remove as the names are the same
IF @CleanupBackups = 1
BEGIN
	IF @BackupProduct = 'Native'
	BEGIN				
		DECLARE #BackupCleanup CURSOR LOCAL STATIC FOR 
		-- Find all backups that occured outside of our retention threshold (@CleanupBackupDays) 
		SELECT
			x.[physical_device_name]
		FROM (
			SELECT 
				bm.[physical_device_name]
				,MAX(bs.[backup_finish_date]) AS [backup_finish_date]
			FROM [msdb].[dbo].[backupset] bs
			INNER JOIN [msdb].[dbo].[backupmediafamily] bm
				ON bs.[media_set_id] = bm.[media_set_id]
			WHERE bs.[type] = @BackupType
			GROUP BY bm.[physical_device_name]
		) x
		WHERE DATEDIFF(hh,x.[backup_finish_date],GETDATE()) >= @CleanupBackupHours
		
		OPEN #BackupCleanup
		FETCH NEXT FROM #BackupCleanup INTO @CleanupFileName
		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Check to see if the file exists
			TRUNCATE TABLE #File_Results
			INSERT INTO #File_Results
			EXEC [master].[dbo].[xp_fileexist] @CleanupFileName
			
			SET @FileExists = (SELECT [File_Exists] FROM #File_Results)
			
			IF @FileExists = 1
			BEGIN
				-- Remove the file
				SET @CleanupBackupsSQL = 'EXEC [master].[dbo].[xp_delete_file] 0,''' + @CleanupFileName + ''''
					IF @Debug = 1
						PRINT (@CleanupBackupsSQL)
					ELSE
						EXEC (@CleanupBackupsSQL)		
			END

		FETCH NEXT FROM #BackupCleanup INTO @CleanupFileName
		END
		CLOSE #BackupCleanup
		DEALLOCATE #BackupCleanup
	END
	ELSE IF @BackupProduct = 'LiteSpeed' 
	BEGIN
		-- Unable to target specific files with LiteSpeed cleanup, have to use target folder
		SET @CleanupBackupsSQL = 'EXEC [master].[dbo].[xp_slssqlmaint] ''-MAINTDEL -DELFOLDER "' + @DBBackupPath + '" -DELEXTENSION "' + RIGHT(@LiteSpeedFileExt,4) + '" -DELUNIT "' + CAST(@CleanupBackupHours AS VARCHAR) + '" -DELUNITTYPE "Hours" -DELUSEAGE'''
			IF @Debug = 1
				PRINT (@CleanupBackupsSQL)
			ELSE
				EXEC (@CleanupBackupsSQL)
	END
END 

-- Cursor through databases and run the backup(s) per @BackupType and @BackupProduct
DECLARE #DatabaseBackups CURSOR FAST_FORWARD FOR 
SELECT	[name]
		,[TakeCopyOnly]
FROM @DBNames

OPEN #DatabaseBackups
FETCH NEXT FROM #DatabaseBackups INTO @DBName, @TakeCopyOnly
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Create our full backup name based on path, backup type, database name and file extension
	IF @BackupType = 'F'
		SET @BackupName = @DBBackupPath + @BackupPrefix + @DBName + '_(' + @File + ')' + @DBBackupFileExt
	ELSE IF @BackupType = 'G'
		SET @BackupName = @DBBackupPath + @BackupPrefix + @DBName + '_(' + @FileGroup + ')' + @DBBackupFileExt
	ELSE SET @BackupName = @DBBackupPath + @BackupPrefix + @DBName + @DBBackupFileExt

	-- Native
	IF @BackupProduct = 'Native'
	BEGIN
		EXEC [dbo].[BackUpDB_Native]
			@DBName				= @DBName
			,@BackupType		= @BackupType
			,@BackupLocation	= @BackupLocation
			,@BackupName		= @BackupName
			,@File				= @File
			,@FileGroup			= @FileGroup
			,@Init				= @Init
			,@RetainDays		= @RetainDays
			,@Stats				= @Stats
			,@Description		= @Description
			,@Verify			= @Verify
			,@Password			= @Password
			,@Compression		= @Compression
			,@CopyOnly          = @TakeCopyOnly
			,@FileCount			= @FileCount
			,@Debug				= @Debug
	END
	-- LiteSpeed
	ELSE IF @BackupProduct = 'LiteSpeed'
	BEGIN
		EXEC [dbo].[BackUpDB_LiteSpeed]
			@DBName				= @DBName
			,@BackupType		= @BackupType
			,@BackupLocation	= @BackupLocation
			,@BackupName		= @BackupName
			,@File				= @File
			,@FileGroup			= @FileGroup
			,@Init				= @Init
			,@RetainDays		= @RetainDays
			,@Stats				= @Stats
			,@Description		= @Description
			,@Verify			= @Verify
			,@Compression		= @Compression
			,@Threads			= @Threads
			,@EncryptionLevel	= @EncryptionLevel
			,@EncryptionKey		= @EncryptionKey
			,@Throttle			= @Throttle
			,@Affinity			= @Affinity
			,@Priority			= @Priority
			,@Debug				= @Debug
	END
	-- RedGate
	ELSE IF @BackupProduct = 'RedGate'
	BEGIN
		EXEC [dbo].[BackUpDB_RedGate]
			@DBName				= @DBName
			,@BackupType		= @BackupType
			,@BackupLocation	= @BackupLocation
			,@BackupName		= @BackupName
			,@File				= @File
			,@FileGroup			= @FileGroup
			,@Init				= @Init
			,@RetainDays		= @RetainDays
			,@Stats				= @Stats
			,@Description		= @Description
			,@Verify			= @Verify
			,@Password			= @Password
			,@Compression		= @Compression
			,@Threads			= @Threads
			,@Debug				= @Debug
	END
	-- DDBoost
	ELSE IF (@BackupProduct = 'DDBoost')
	BEGIN
		EXEC [dbo].[BackUpDB_DDBoost]
			@DBName					= @DBName
		    ,@BackupType			= @BackupType
		    ,@BackupName			= @BackupName
		    ,@RetainDays			= @RetainDays
		    ,@Description			= @Description
		    ,@Threads				= @Threads
			,@Domain				= @Domain
			,@DDHost				= @DDHost
			,@DDUser				= @DDUser
			,@DDDevicePath			= @DDDevicePath
		    ,@Debug					= @Debug
	END
	
FETCH NEXT FROM #DatabaseBackups INTO @DBName,@TakeCopyOnly
END
CLOSE #DatabaseBackups
DEALLOCATE #DatabaseBackups

-- Cleanup backup/restore history
IF @CleanupHistory = 1
BEGIN
	IF @Debug = 1
		PRINT 'EXEC [msdb].[dbo].[sp_delete_backuphistory] ''' + CAST(@CleanupBUHist AS VARCHAR) + ''''
	ELSE
		EXEC [msdb].[dbo].[sp_delete_backuphistory] @CleanupBUHist
END

-- Return status
IF @@ERROR <> 0
	BEGIN
		SET @ReturnCode = @@ERROR
		RETURN @ReturnCode
	END
ELSE
	BEGIN
		RETURN @ReturnCode
	END	

SET NOCOUNT OFF