IF OBJECT_ID('[dbo].[ShrinkAllLogs]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[ShrinkAllLogs]
GO

/*******************************************************************************************************
**	Name:			dbo.ShrinkAllLogs
**	Desc:			Sets all databases to SIMPLE recovery and shrinks their log files.
**	Auth:			Jeff Gogel (SQLSlayer.com)
**  Dependencies:	dbo.ForEachDB
**  Parameters:		@DBName = Database name(s) to apply permissions to (CSV supported, NULL for all)
					@DBNameExclude = Database name(s) to exclude permissions to (CSV supported)
					@Debug = Show, but don't execute work to be done
**	Date:			2013.06.05
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
**  20130605	Jeff Gogel		Initial creation.
**  20131029	Jeff Gogel		Changed to use DB_ID() and added brackets to db name.
********************************************************************************************************/

CREATE PROCEDURE [dbo].[ShrinkAllLogs]
(
	@Debug				BIT				= 0
)

AS

SET NOCOUNT ON

EXEC [admin].[dbo].[ForEachDB] @SQL = 'ALTER DATABASE [?] SET RECOVERY SIMPLE', @DBNameExclude = 'tempdb', @Debug = @Debug

EXEC [admin].[dbo].[ForEachDB] @SQL = 'DECLARE @LogFileName VARCHAR(512) = (SELECT TOP 1 name FROM sys.master_files WHERE type_desc = ''LOG'' AND database_id = DB_ID(N''?''))
DBCC SHRINKFILE (@LogFileName, 0, TRUNCATEONLY)', @DBNameExclude = 'tempdb', @SwitchContext =1, @Debug = @Debug

SET NOCOUNT OFF


GO