IF OBJECT_ID('dbo.DBExtProps','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[DBExtProps]
GO

/******************************************************************************
**	Name:			[dbo].[DBExtProps]
**	Desc:			Retrieve/insert/update/delete extended properites to a database
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.GetErrorInfo_fn
**  Parameters:		@DBName				= Database name(s) to query, CSV supported, null for all
					@DBNameExclude		= Database name(s) to exclude, CSV supported
					@Action				= I = insert, U = update, D = delete | Leave NULL for SELECT
					@Name				= Extended property name
					@Value				= Value for specified extended property
					@IncludeUserAndDate	= Add a user and timestamp to the inserted value
					@Debug				= Show but don't execute
**  Usage:			EXEC [dbo].[DBExtProps] @DBName = 'AdventureWorks'
					EXEC [dbo].[DBExtProps] @DBName = 'AdventureWorks', @Action = 'I', @Name = 'Owner', @Value = 'Microsoft'
					EXEC [dbo].[DBExtProps] @DBName = 'AdventureWorks', @Action = 'U', @Name = 'Owner', @Value = 'Joe Smith'
					EXEC [dbo].[DBExtProps] @DBName = 'AdventureWorks', @Action = 'D', @Name = 'Owner'
**  Notes:			All values will have a suffixed user name and date
**	Date:			2009.10.05
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
**	20130605	Adam Bean		Added @IncludeUserAndDate 
********************************************************************************************************/

CREATE PROCEDURE [dbo].[DBExtProps]
(
	@DBName					NVARCHAR(512)	= NULL
	,@DBNameExclude			NVARCHAR(512)	= NULL	
	,@Action				CHAR(1)			= NULL
	,@Name					NVARCHAR(128)	= NULL
	,@Value					VARCHAR(8000)	= NULL
	,@IncludeUserAndDate	BIT				= 0
	,@Debug					BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE
	@SQL			VARCHAR(512)
	,@PropName		NVARCHAR(128)
	,@Date			VARCHAR(24)
	,@User			VARCHAR(64)

IF @Debug = 1 
	PRINT ('/* Debug Mode */')
	
-- Set variables
SET @Date = (SELECT CONVERT(VARCHAR(24), GETDATE(), 120))
SET @User = (SELECT SUSER_SNAME())

-- Test parameters
IF @Action NOT IN ('D','I','U')
BEGIN
	PRINT '@Action must be one of the following single character values: ''S'' (SELECT), ''I'' (INSERT), ''U'' (UPDATE), ''D'' (DELETE)'
	RETURN
END
IF @Action IN ('I','U') AND (@Name IS NULL OR @Value IS NULL) 
BEGIN
	PRINT 'When inserting or updating, both @Name and @Value must be passed.'
	RETURN
END
IF @Action = 'D' AND @Name IS NULL
BEGIN
	PRINT 'When deleting, @Name must be passed.'
	RETURN
END

-- Create temp table to store extended properties
IF OBJECT_ID('tempdb.dbo.#DBExtProps') IS NOT NULL
	DROP TABLE #DBExtProps
CREATE TABLE #DBExtProps
(
	[PropertyStatus]	CHAR(16)
	,[DBName]			NVARCHAR(128)	
	,[name]				NVARCHAR(128)	NULL
	,[value]			SQL_VARIANT		NULL
	,[Processed]		BIT
)

-- Cursor through databases and retrieve extended properties
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name] AS [DBName]
FROM [sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database(s), or all
AND de.[item] IS NULL -- All but excluded databases

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Populate temp table with extended properties from specified database(s)	
	INSERT INTO #DBExtProps
	EXEC
	(
		'
		SELECT 
			''No Change''
			,''' + @DBName + '''
			,[name]
			,[value]
			,0
		FROM [' + @DBName + '].[sys].[fn_listextendedproperty](DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
		'
	)

	-- Add new extended properties for specified database(s)
	IF @Action = 'I'
	BEGIN
		IF (SELECT [name] FROM #DBExtProps WHERE [name] = @Name AND DBName = @DBName) IS NOT NULL
		BEGIN
			PRINT '''' + @Name + ''' is already an extended property in use on ' + @DBName + '.'
			RETURN
		END
		ELSE
		BEGIN
			INSERT INTO #DBExtProps
			VALUES
			('To be inserted', @DBName, @Name, @Value, 0)

			IF @IncludeUserAndDate = 1
				SET @SQL = 'USE [' + @DBName + '] EXEC [sys].[sp_addextendedproperty] @name = ''' + @Name + ''', @value = ''' + @Date + ' - ' + @User + ': ' + @Value + ''''
			ELSE
				SET @SQL = 'USE [' + @DBName + '] EXEC [sys].[sp_addextendedproperty] @name = ''' + @Name + ''', @value = ''' + @Value + ''''
		END
	END
	
	-- Update existing extended properties for specified database(s)
	IF @Action = 'U'
	BEGIN
		IF (SELECT [name] FROM #DBExtProps WHERE [name] = @Name AND DBName = @DBName) IS NULL
		BEGIN
			PRINT '''' + @Name + ''' is not a valid extended property on ' + @DBName + '.'
			RETURN
		END
		ELSE
		BEGIN
			-- Update working table
			UPDATE #DBExtProps
			SET [PropertyStatus] = 'To be updated'
			WHERE [DBName] = @DBName
			AND [name] = @Name
			
			IF @IncludeUserAndDate = 1
				SET @SQL = 'USE [' + @DBName + '] EXEC [sys].[sp_addextendedproperty] @name = ''' + @Name + ''', @value = ''' + @Date + ' - ' + @User + ': ' + @Value + ''''
			ELSE
				SET @SQL = 'USE [' + @DBName + '] EXEC [sys].[sp_addextendedproperty] @name = ''' + @Name + ''', @value = ''' + @Value + ''''

		END
	END

	-- Delete existing extended properties for specified database(s)
	IF @Action = 'D'
	BEGIN
		IF (SELECT [name] FROM #DBExtProps WHERE [name] = @Name AND DBName = @DBName) IS NULL
		BEGIN
			PRINT '''' + @Name + ''' is not a valid extended property on ' + @DBName + '.'
			RETURN
		END
		ELSE
		BEGIN
        
			-- Update working table
			UPDATE #DBExtProps
			SET [PropertyStatus] = 'To be deleted'
			WHERE [DBName] = @DBName
			AND [name] = @Name
			
			SET @SQL = 'USE [' + @DBName + '] EXEC [sys].[sp_dropextendedproperty] @name = ''' + @Name + '''
			'
		END
	END

	-- Print or execute
	IF @Debug = 1
	BEGIN
		PRINT (@SQL)
	END
	ELSE
	BEGIN TRY
		-- Execute changes
		EXEC(@SQL)
		-- Update working table
		UPDATE #DBExtProps
		SET [Processed] = 1
		,[PropertyStatus] = CASE [PropertyStatus]
			WHEN 'To be inserted'	THEN 'Inserted'
			WHEN 'To be deleted'	THEN 'Deleted'
			WHEN 'To be updated'	THEN 'Updated'
		END
		WHERE [DBName] = @DBName
		AND [name] = @Name
	END TRY
	BEGIN CATCH
		SELECT * FROM [dbo].[GetErrorInfo_fn]()
	END CATCH

FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs

-- Return results
SELECT 
	[PropertyStatus]
	,[DBName]
	,[name]		AS [PropertyName]
	,[value]	AS [ValueName]
	,[Processed]
FROM #DBExtProps

SET NOCOUNT OFF