IF OBJECT_ID('[dbo].[DeployConfigAuditNotification]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[DeployConfigAuditNotification]
GO

/******************************************************************************
* Name
	[dbo].[DeployConfigAuditNotification]

* Author
	Jeff Gogel
	
* Date
	2015.03.03
	
* Synopsis
	Wrapper procedure to deploy the ConfigAuditNotification procedure
	
* Description
	Wrapper procedure is required as the procedure can not accept email recipients as a parameter because
	a queue can not call a procedure with a parameter. 

* Examples
	EXEC [dbo].[DeployConfigAuditNotification] @Recipients = 'you@domain.com'	-- Deploy notification (this does not actually enable it)
	EXEC [dbo].[DeployConfigAuditNotification] @Drop = 1						-- Remove all deployed objects
	EXEC [dbo].[DeployConfigAuditNotification] @Enable = 1						-- Enable queue
	EXEC [dbo].[DeployConfigAuditNotification] @Disable = 1						-- Disable queue
* Dependencies
	n/a

* Parameters
	@Recipients			- Email recipients of the notification (CSV string supported)
	@Drop				- Remove Event Notification, associated objects (queue, service, etc.), and activated stored proc but leaves logging table.
	@Enable				- Enables the queue to allow notifications to be sent
	@Disable			- Disables the queue to stop notifications from being sent
	@Debug				- Show the procedure to be deployed but do not execute
	
* Notes
	This procedure will also setup the queue, service and event notification when deploying.
	This procedure should always be ran in the context of the admin database.

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------

******************************************************************************/

CREATE PROCEDURE [dbo].[DeployConfigAuditNotification]
(
	@Recipients				VARCHAR(MAX) = NULL
	,@Drop					BIT = 0
	,@Enable				BIT = 0
	,@Disable				BIT = 0
	,@Debug					BIT	= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@SQL		VARCHAR(MAX)
	,@DBName	NVARCHAR(128)
	,@DropStatement	VARCHAR(MAX)


-- Replace commas with semi-colons to work properly for database mail
SET @Recipients = REPLACE(@Recipients, ',', ';')

-- Find our current database
SET @DBName = (SELECT DB_NAME())

-- Just a comment - will be included even if debug is off
SET @SQL = '/*** DEBUG ENABLED ****/
'


-- Dropping all notification objects
IF (@Drop = 1)
BEGIN
	-- Check for Event Notification
	IF EXISTS (SELECT [name] FROM [sys].[server_event_notifications] WHERE [name] = 'ConfigAuditNotification')
	BEGIN
		SET @SQL = @SQL + '
DROP EVENT NOTIFICATION ConfigAuditNotification ON SERVER'
	END


	-- Check for Service
	IF EXISTS (SELECT [name] FROM [sys].[services] WHERE [name] = 'ConfigAuditNotificationService')
	BEGIN
		SET @SQL = @SQL + '
DROP SERVICE ConfigAuditNotificationService'
	END
	
	-- Check for Queue
	IF EXISTS (SELECT [name] FROM [sys].[service_queues] WHERE [name] = 'ConfigAuditNotificationQueue')
	BEGIN
		SET @SQL = @SQL + '
DROP QUEUE ConfigAuditNotificationQueue'
	END

	-- Check for Notification Stored Procedure
	IF OBJECT_ID (N'dbo.ConfigAuditNotification', N'P') IS NOT NULL
	BEGIN
		SET @SQL = @SQL + '
DROP PROCEDURE dbo.ConfigAuditNotification'
	END

	-- Print or run
	IF (@Debug = 1)
		PRINT (@SQL)
	ELSE
		EXEC(@SQL)
END
ELSE IF (@Enable = 1) -- Enable notification queue
BEGIN
	SET @SQL = '
ALTER QUEUE ConfigAuditNotificationQueue WITH ACTIVATION (STATUS = ON)
ALTER QUEUE ConfigAuditNotificationQueue WITH STATUS = ON'

	IF (@Debug = 1)
		PRINT (@SQL)
	ELSE
		EXEC(@SQL)
END
ELSE IF (@Disable = 1) -- Disable notification queue
BEGIN
	SET @SQL = '
ALTER QUEUE ConfigAuditNotificationQueue WITH ACTIVATION (STATUS = OFF)
ALTER QUEUE ConfigAuditNotificationQueue WITH STATUS = OFF'

	IF (@Debug = 1)
		PRINT (@SQL)
	ELSE
		EXEC(@SQL)
END
ELSE  -- Code to deploy everything...
BEGIN

	-- Create our logging table
	IF OBJECT_ID (N'dbo.AuditNotifications_Config', N'U') IS NULL
	BEGIN
		SET @SQL = N'		
CREATE TABLE [dbo].[AuditNotifications_Config]
(   
	[EventPostTime]		DATETIME
	,[DatabaseName]		NVARCHAR(256)
	,[LoginName]		NVARCHAR(100)
	,[NTUserName]		NVARCHAR(100)
	,[NTDomainName]		NVARCHAR(100)
	,[SessionLoginName]	NVARCHAR(100)
	,[ApplicationName]	NVARCHAR(250)
	,[HostName]			NVARCHAR(50)
	,[SPID]				INT
	,[ClientProcessID]	INT
	,[Message]			NVARCHAR(MAX)
CONSTRAINT [PK_AuditNotifications_Config] PRIMARY KEY CLUSTERED 
(
	[EventPostTime] ASC, [SPID] ASC
));'
	
		IF (@Debug = 1)
			PRINT (@SQL)
		ELSE
			EXEC(@SQL)
	END

	-- Code to create activated stored proc
	SET @SQL =
(N'
EXEC
(''
	/******************************************************************************
	* Name
		[dbo].[ConfigAuditNotification]

	* Author
		Jeff Gogel
	
	* Date
		2015.03.03
	
	* Synopsis
		Procedure to notify recipients of server level SQL configuration Audits.
	
	* Description
		Procedure which will send an HTML formatted email showing the configuration option that was changed and the before/after value.
		The procedure is fired via service broker.

	* Examples
		EXEC [dbo].[ConfigAuditNotification]

	* Dependencies
		[dbo].[DeployConfigAuditNotification]

	* Parameters
		n/a
	
	* Notes
		To only be created from the wrapper procedure: [dbo].[DeployConfigAuditNotification]

	*******************************************************************************
	* License
	*******************************************************************************
		Copyright ? SQLSlayer.com. All rights reserved.

		All objects published by SQLSlayer.com are licensed and goverened by 
		Creative Commons Attribution-Share Alike 3.0
		http://creativecommons.org/licenses/by-sa/3.0/

		For more scripts and sample code, go to http://www.SQLSlayer.com

		You may alter this code for your own *non-commercial* purposes. You may
		republish altered code as long as you give due credit.

		THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
		ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
		TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
		PARTICULAR PURPOSE.
	*******************************************************************************
	* Audit History
	*******************************************************************************
		Date:		Author:			Description:
		--------	--------		---------------------------------------
	
	******************************************************************************/

	CREATE PROCEDURE [dbo].[ConfigAuditNotification]
	WITH EXECUTE AS OWNER
	AS 

	SET NOCOUNT ON

	DECLARE 
		@message_body		XML
		,@email_message		NVARCHAR(MAX)
		,@Subject			VARCHAR(150)
		,@EmailHeader		VARCHAR(2048)
		,@EmailBody			VARCHAR(MAX)
		,@EmailCaption		VARCHAR(128)
		,@EmailFooter		VARCHAR(2048)
		,@Recipients		VARCHAR(MAX)
		,@Count				INT
		,@html				NVARCHAR(MAX)
		,@ErrorMessage		NVARCHAR(MAX)


	SET @Recipients	= ''''' + @Recipients + N'''''
	SET @Subject	= ''''SQL Server Configuration Audit Notification - Configuration Audited On '''' + @@SERVERNAME 

	-- Parse XML Into Temp Table
	DECLARE @EventTable TABLE
	(   
		[EventPostTime]	DATETIME
		,[DatabaseName]		NVARCHAR(256)
		,[LoginName]		NVARCHAR(100)
		,[NTUserName]		NVARCHAR(100)
		,[NTDomainName]		NVARCHAR(100)
		,[SessionLoginName]	NVARCHAR(100)
		,[ApplicationName]	NVARCHAR(250)
		,[HostName]			NVARCHAR(50)
		,[SPID]				INT
		,[ClientProcessID]	INT
		,[Message]			NVARCHAR(MAX)
	)


	WHILE (1 = 1)
	BEGIN
		BEGIN TRANSACTION

		WAITFOR 
		(
			RECEIVE TOP (1) CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/PostTime)[1]'''',''''DATETIME'''')					AS [EventPostTime]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/DatabaseName)[1]'''',''''NVARCHAR(256)'''')		AS [DatabaseName]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/LoginName)[1]'''',''''NVARCHAR(100)'''')			AS [LoginName]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/NTUserName)[1]'''',''''NVARCHAR(100)'''')			AS [NTUserName]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/NTDomainName)[1]'''',''''NVARCHAR(100)'''')		AS [NTDomainName]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/SessionLoginName)[1]'''',''''NVARCHAR(100)'''')	AS [SessionLoginName]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/ApplicationName)[1]'''',''''NVARCHAR(250)'''')		AS [ApplicationName]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/HostName)[1]'''',''''NVARCHAR(50)'''')				AS [HostName]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/SPID)[1]'''',''''INT'''')							AS [SPID]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/ClientProcessID)[1]'''',''''INT'''')				AS [ClientProcessID]
						   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/TextData)[1]'''',''''NVARCHAR(MAX)'''')			AS [Message]
			FROM [dbo].[ConfigAuditNotificationQueue]
			INTO @EventTable
		), TIMEOUT 5000
		-- If we didn''''t get anything, bail out
		IF (@@ROWCOUNT = 0)
		BEGIN
			ROLLBACK TRANSACTION
			BREAK
		END
		ELSE
		BEGIN
			COMMIT TRANSACTION
		END
	END	

	-- Clean up event table to only include config Audits
	DELETE FROM @EventTable WHERE [Message] NOT LIKE ''''%Configuration option%''''	-- Only config option message
								OR [Message] LIKE ''''%Agent XPs%''''				-- Ignore SQL Agent restarts
								OR [Message] LIKE ''''%from 1 to 1%''''				-- Ignore changes that aren''''t changes
								OR [Message] LIKE ''''%from 0 to 0%''''
	-- Clean up Message data
	UPDATE @EventTable SET [Message] = RIGHT([Message],(LEN([Message])-CHARINDEX(''''Configuration'''' ,[Message],0)) + 1)
	-- Insert in to logging table
	INSERT INTO [dbo].[AuditNotifications_Config] SELECT * FROM @EventTable

	-- Build the email
	SET @EmailCaption = ''''Configuration Audit Notification For '''' + @@SERVERNAME 
	-- Setup table to build body
	DECLARE @EmailBodyContents TABLE 
	(
		[html]			VARCHAR(MAX)
	)

	-- Setup table to build email
	DECLARE @Email TABLE 
	(
		[ID]			INT IDENTITY
	   ,[html]			VARCHAR(MAX)
	)

	-- Build the email header
	SELECT @EmailHeader = 
	''''
	<HTML>
	<DIV ALIGN="CENTER">
	<TABLE BORDER="1" CELLSPACING="5" CELLPADDING="1" ALIGN="CENTER">
	<CAPTION>
		<H2>'''' + @EmailCaption + ''''</H2>
		<EM><i>The following Configuration Changes have occured on  :'''' + @@SERVERNAME + ''''</i></EM>
	</CAPTION>
	<TR BGCOLOR="#C0C0C0">
		<TH>EventPostTime</TH>
		<TH>DatabaseName</TH>
		<TH>LoginName</TH>
		<TH>NTUserName</TH>
		<TH>NTDomainName</TH>
		<TH>SessionLoginName</TH>
		<TH>ApplicationName</TH>
		<TH>HostName</TH>
		<TH>SPID</TH>
		<TH>ClientProcessID</TH>
		<TH>Message</TH>
	</TR>
	''''

	-- Build the email footer
	SELECT @EmailFooter =
	''''
	</TABLE>
	</DIV>
	</HTML>
	''''

	-- Build the email body
	INSERT INTO @EmailBodyContents
	SELECT
		''''<TR>
			<TD>'''' + E.[EventPostTime]		+ ''''</TD>
			<TD>'''' + E.[DatabaseName]		+ ''''</TD>
			<TD>'''' + E.[LoginName]			+ ''''</TD>	
			<TD>'''' + E.[NTUserName]			+ ''''</TD>	
			<TD>'''' + E.[NTDomainName]		+ ''''</TD>	
			<TD>'''' + E.[SessionLoginName]	+ ''''</TD>	
			<TD>'''' + E.[ApplicationName]		+ ''''</TD>	
			<TD>'''' + E.[HostName]			+ ''''</TD>	
			<TD>'''' + CAST(E.[SPID] AS VARCHAR)				+ ''''</TD>
			<TD>'''' + CAST(E.[ClientProcessID] AS VARCHAR)		+ ''''</TD>	
			<TD>'''' + E.[Message]				+ ''''</TD>	

		</TR>
		'''' 
	FROM ( 
			SELECT      
				REPLACE(REPLACE(A.[EventPostTime],''''T'''','''' ''''),''''.'''','''''''')		AS [EventPostTime]
				,A.[DatabaseName]
				,A.[LoginName]
				,A.[NTUserName]
				,A.[NTDomainName]
				,A.[SessionLoginName]
				,A.[ApplicationName]
				,A.[HostName]
				,A.[SPID]
				,A.[ClientProcessID]
				,A.[Message]
				,ROW_NUMBER() OVER (PARTITION BY [LoginName] ORDER BY [EventPostTime] DESC)	AS Row 
			FROM @EventTable A
		  ) E

	-- Build the email
	INSERT INTO @Email
	SELECT @EmailHeader
	UNION ALL
	SELECT *
	FROM @EmailBodyContents
	UNION ALL
	SELECT @EmailFooter

	-- Put it all together
	SET @Count = 1
	SET @EmailBody = ''''''''
	WHILE @Count <= (SELECT COUNT(*) FROM @Email)
	BEGIN
		SET @EmailBody = @EmailBody + (SELECT [html] FROM @Email WHERE [ID] = @Count)
		SET @Count = @Count + 1
	END

	-- Only send email if there is data
	IF @Count > 3
	BEGIN
		-- Send mail
		EXEC [msdb].[dbo].[sp_send_dbmail]
			@Recipients		= @Recipients
			,@Subject		= @Subject
			,@Body			= @EmailBody
			,@Body_format	= ''''HTML''''
	END

'')
')


	-- Check to see if the stored proc exists, if not create it (print for debug)
	IF OBJECT_ID (N'dbo.ConfigAuditNotification', N'P') IS NULL
	BEGIN
		IF @Debug = 1
		BEGIN
			PRINT (@SQL)
		END
		ELSE
		BEGIN
			EXEC(@SQL)
			IF EXISTS (SELECT [name] FROM [sys].[objects] WHERE [name] = '[ConfigAuditNotification]')
			BEGIN
				PRINT 'Successfully deployed the [dbo].[ConfigAuditNotification] stored procedure with ''' + @Recipients + ''' as the recipient(s).'
			END
		END
	END

	-- Enable service broker and set up notification queue
	IF @Debug = 0
	BEGIN

		-- Trustworthy must be on to send emails through this process
		IF ((SELECT is_trustworthy_on from sys.databases WHERE name = DB_NAME()) = 0)
		BEGIN
			EXEC('ALTER DATABASE [' + @DBName + '] SET TRUSTWORTHY ON WITH ROLLBACK IMMEDIATE')
		END

		-- Setup service broker
		IF NOT EXISTS
		(
			SELECT 
				[name] 
			FROM [dbo].[sysdatabases_vw] 
			WHERE [is_broker_enabled] = 1
			AND [name] = @DBName
		)
		BEGIN
			EXEC('ALTER DATABASE [' + @DBName + '] SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE')
		END

		-- Setup queue
		IF NOT EXISTS (SELECT [name] FROM [sys].[service_queues] WHERE [name] = N'ConfigAuditNotificationQueue')
		CREATE QUEUE [dbo].[ConfigAuditNotificationQueue] 
			WITH STATUS = OFF
			,RETENTION = OFF
			,ACTIVATION (STATUS = OFF, PROCEDURE_NAME = [dbo].[ConfigAuditNotification]
			,MAX_QUEUE_READERS = 5
			,EXECUTE AS OWNER)
		ON [PRIMARY] 

		-- Setup service
		IF NOT EXISTS (SELECT [name] FROM [sys].[services] WHERE [name] = N'ConfigAuditNotificationService')
		CREATE SERVICE [ConfigAuditNotificationService] AUTHORIZATION [dbo] 
		ON QUEUE [dbo].[ConfigAuditNotificationQueue] ([http://schemas.microsoft.com/SQL/Notifications/PostEventNotification])

		-- Setup event notification
		IF NOT EXISTS (SELECT [name] FROM [sys].[server_event_notifications] WHERE [name] = 'ConfigAuditNotification')
		CREATE EVENT NOTIFICATION ConfigAuditNotification
			ON SERVER
			FOR ERRORLOG
			TO SERVICE 'ConfigAuditNotificationService', 'current database'
	END
END

SET NOCOUNT OFF