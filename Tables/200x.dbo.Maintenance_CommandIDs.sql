IF OBJECT_ID('[dbo].[Maintenance_CommandIDs]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_CommandIDs]
	(
		[CommandID]		INT IDENTITY(1,1) NOT NULL
		,[Command]		NVARCHAR(1024)
	 CONSTRAINT [PK__Maintenance_Commands_CommandID] PRIMARY KEY CLUSTERED 
	(
		[CommandID] ASC
	)
	)
END