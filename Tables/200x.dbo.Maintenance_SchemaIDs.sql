IF OBJECT_ID('[dbo].[Maintenance_SchemaIDs]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_SchemaIDs]
	(
		[SchemaID]		INT IDENTITY(1,1) NOT NULL
		,[SchemaName]	NVARCHAR(128)
	 CONSTRAINT [PK__Maintenance_SchemaIDs__SchemaID] PRIMARY KEY CLUSTERED 
	(
		[SchemaID] ASC
	)
	)
END