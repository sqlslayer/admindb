IF OBJECT_ID('[dbo].[Maintenance_RunIDs]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_RunIDs]
	(
		[RunID]			INT IDENTITY(1,1) NOT NULL
		,[DateStart]	SMALLDATETIME
		,[DateEnd]		SMALLDATETIME
	 CONSTRAINT [PK__Maintenance_RunIDs__RunID] PRIMARY KEY CLUSTERED 
	(
		[RunID] ASC
	)
	)
	ALTER TABLE [dbo].[Maintenance_RunIDs] ADD CONSTRAINT [DF_Maintenance_RunIDs__DateStart] DEFAULT (GETDATE()) FOR [DateStart]
END