IF OBJECT_ID('[dbo].[Maintenance_Reindex]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_Reindex]
	(
		[ID]				INT IDENTITY(1,1) NOT NULL
		,[DatabaseID]		INT NOT NULL
		,[SchemaID]			INT NOT NULL
		,[TableID]			INT NOT NULL
		,[IndexID]			INT NOT NULL
		,[IndexFillFactor]	TINYINT NOT NULL
		,[PageSpaceUsed]	FLOAT NOT NULL
		,[RecordSize]		FLOAT NOT NULL
		,[AvgFrag]			FLOAT NOT NULL
		,[CommandID]		INT	NOT NULL
		,[IsOverride]		BIT NOT NULL
		,[RunID]			INT NOT NULL
		,[DateStart]		DATETIME NULL
		,[DateEnd]			DATETIME NULL
	CONSTRAINT [PK__Maintenance_Reindex__ID] PRIMARY KEY CLUSTERED 
	(
		[ID]
	)
	)
END

