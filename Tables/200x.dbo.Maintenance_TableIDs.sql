IF OBJECT_ID('[dbo].[Maintenance_TableIDs]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_TableIDs]
	(
		[TableID]		INT IDENTITY(1,1) NOT NULL
		,[TableName]	NVARCHAR(128)
	 CONSTRAINT [PK__Maintenance_TableIDs__TableID] PRIMARY KEY CLUSTERED 
	(
		[TableID] ASC
	)
	)
END