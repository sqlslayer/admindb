IF OBJECT_ID('[dbo].[Maintenance_CheckDB]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_CheckDB]
	(
		[ID]			INT IDENTITY(1,1) NOT NULL
		,[DatabaseID]	INT NOT NULL
		,[CommandID]	INT	NOT NULL
		,[IsOverride]	BIT NOT NULL
		,[RunID]		INT NOT NULL
		,[DateStart]	DATETIME NULL
		,[DateEnd]		DATETIME NULL
	CONSTRAINT [PK__Maintenance_CheckDB__ID] PRIMARY KEY CLUSTERED 
	(
		[ID]
	)
	)
END

