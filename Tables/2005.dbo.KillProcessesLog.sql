IF OBJECT_ID('dbo.KillProcessesLog','U') IS NULL
BEGIN
	CREATE TABLE [dbo].[KillProcessesLog]
	(
		[SPID]			INT				NULL
		,[DBName]		NVARCHAR(128)	NULL
		,[Login]		NCHAR(128)		NULL
		,[HostName]		NCHAR(128)		NULL
		,[ProgramName]	NCHAR(128)		NULL
		,[Command]		NCHAR(16)		NULL
		,[Status]		NCHAR(30)		NULL
		,[LoginTime]	DATETIME		NULL
		,[LastBatch]	DATETIME		NULL
		,[Query]		VARCHAR(MAX)	NULL
		,[DateCreated]	SMALLDATETIME CONSTRAINT DF_KillProcessesLog_DateCreated DEFAULT GETDATE()
	) ON [PRIMARY]
END