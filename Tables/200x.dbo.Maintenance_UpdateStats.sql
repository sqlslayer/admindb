IF OBJECT_ID('[dbo].[Maintenance_UpdateStats]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_UpdateStats]
	(
		[ID]				INT IDENTITY(1,1) NOT NULL
		,[DatabaseID]		INT NOT NULL
		,[SchemaID]			INT NOT NULL
		,[TableID]			INT NOT NULL
		,[StatID]			INT NOT NULL
		,[PctChange]		DECIMAL NULL
		,[RowCount]			BIGINT NULL
		,[RowsChanged]		BIGINT NULL
		,[LastStatsUpdate]	DATETIME NULL
		,[CommandID]		INT	NOT NULL
		,[IsOverride]		BIT NOT NULL
		,[RunID]			INT NOT NULL
		,[DateStart]		DATETIME NULL
		,[DateEnd]			DATETIME NULL
	CONSTRAINT [PK__Maintenance_UpdateStats__ID] PRIMARY KEY CLUSTERED 
	(
		[ID]
	)
	)
END

