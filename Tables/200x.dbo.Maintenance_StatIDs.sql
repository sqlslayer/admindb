IF OBJECT_ID('[dbo].[Maintenance_StatIDs]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_StatIDs]
	(
		[StatID]		INT IDENTITY(1,1) NOT NULL
		,[StatName]		NVARCHAR(128)
	 CONSTRAINT [PK__Maintenance_StatIDs__StatID] PRIMARY KEY CLUSTERED 
	(
		[StatID] ASC
	)
	)
END