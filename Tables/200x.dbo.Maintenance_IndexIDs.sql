IF OBJECT_ID('[dbo].[Maintenance_IndexIDs]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_IndexIDs]
	(
		[IndexID]		INT IDENTITY(1,1) NOT NULL
		,[IndexName]	NVARCHAR(128)
	 CONSTRAINT [PK__Maintenance_IndexIDs__IndexID] PRIMARY KEY CLUSTERED 
	(
		[IndexID] ASC
	)
	)
END