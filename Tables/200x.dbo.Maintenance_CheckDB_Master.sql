IF OBJECT_ID('[dbo].[Maintenance_CheckDB_Master]','U') IS NULL	
BEGIN
	CREATE TABLE [dbo].[Maintenance_CheckDB_Master]
		(
			[Error]			INT
			,[Level]		INT
			,[State]		INT
			,[MessageText]	VARCHAR(7500)
			,[RepairLevel]	VARCHAR(128)
			,[Status]		INT
			,[DbId]			INT
			,[NewDbId]		INT
			,[DbFragId]		INT          
			,[ObjectID]		INT
			,[IndexID]		INT
			,[PartitionID]	BIGINT
			,[AllocUnitID]	BIGINT
			,[RidDbId]		INT          
			,[RidPruId]		INT
			,[File]			INT
			,[Page]			INT
			,[Slot]			INT
			,[RefDbId]		INT          
			,[RefPruId]		INT          
			,[RefFile]		INT
			,[RefPage]		INT
			,[RefSlot]		INT
			,[Allocation]	INT
			,[RunID]		INT
		)
END