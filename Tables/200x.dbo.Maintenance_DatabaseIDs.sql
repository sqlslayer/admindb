IF OBJECT_ID('[dbo].[Maintenance_DatabaseIDs]','U') IS NULL 
BEGIN
	CREATE TABLE [dbo].[Maintenance_DatabaseIDs]
	(
		[DatabaseID]	INT IDENTITY(1,1) NOT NULL
		,[DBName]		NVARCHAR(128)
	 CONSTRAINT [PK__Maintenance_DatabaseIDs__DatabaseID] PRIMARY KEY CLUSTERED 
	(
		[DatabaseID] ASC
	)
	)
END