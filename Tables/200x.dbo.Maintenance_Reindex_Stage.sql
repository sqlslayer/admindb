IF OBJECT_ID('[dbo].[Maintenance_Reindex_Stage]','U') IS NULL 
BEGIN
	CREATE TABLE dbo.[Maintenance_Reindex_Stage]
	(
		[database_id]			SMALLINT 
		,[DBName]				NVARCHAR(128)
		,[TableName]			NVARCHAR(128)
		,[SchemaName]			NVARCHAR(128)
		,[idx_object_id]		INT
		,[IndexName]			NVARCHAR(128)
		,[IndexType]			NVARCHAR(64)
		,[AllowPageLocks]		BIT
		,[AllocationType]		NVARCHAR(64)
		,[PartitionCount]		INT
		,[PartitionNumber]		INT
		,[IndexFillFactor]		TINYINT
		,[AvgFrag]				FLOAT
		,[PageSpaceUsed]		FLOAT
		,[page_count]			BIGINT
		,[record_count]			BIGINT
		,[RecordSize]			FLOAT
		,[Type]					VARCHAR(16)
		,[ONLINE]				BIT
		,[TypeOptionOverride]	NVARCHAR(128)
		,[WithOptionsOverride]	NVARCHAR(128)
		,[Command]				NVARCHAR(4000)
		,[DateCreated]			DATETIME
		,[Processed]			BIT
		,[Error]				NVARCHAR(1024)
	)
END