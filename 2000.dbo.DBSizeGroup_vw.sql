IF OBJECT_ID('[dbo].[DBSizeGroup_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[DBSizeGroup_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.DBSizeGroup_vw
**	Desc:			View to split databases into multiple groups based on size (SQL2000)
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2008.06.10
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
**	20100425	Adam Bean		Fixed collation issues
********************************************************************************************************/

CREATE VIEW [dbo].[DBSizeGroup_vw]

AS

SELECT 
	CASE WHEN 
		(
			SELECT COUNT(t.[dbid]) 
			FROM [master].[dbo].[sysaltfiles] t
			WHERE t.[dbid] >= t1.[dbid]
			AND ([status] & 64 = 0)
		) % 2 = 1 
		THEN 1
		ELSE 2
	END AS [DBGroup]
	,[dbid]
	,DB_NAME([dbid])		AS [DBName]
	,SUM([size]) * 8 / 1024 AS [Size]
FROM [master].[dbo].[sysaltfiles] t1
WHERE ([status] & 64 = 0)
GROUP BY [dbid], DB_NAME([dbid])