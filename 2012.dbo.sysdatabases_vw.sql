IF OBJECT_ID('dbo.sysdatabases_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[sysdatabases_vw]
GO

/******************************************************************************
* Name
	[dbo].[sysdatabases_vw]

* Author
	Matt Stanford (SQLSlayer.com)
	
* Date
	2009.05.19
	
* Synopsis
	View to give you a common view of sysdatabases amongst all versions
	
* Description
	n/a

* Examples
	n/a

* Dependencies
	n/a

* Parameters
	n/a
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20090929	Adam Bean		Global header formatting & general clean up
	20140511	Adam Bean		New version for 2012
******************************************************************************/

CREATE VIEW [dbo].[sysdatabases_vw]

AS

SELECT
	[name]
	,[database_id]
	,[source_database_id]
	,[owner_sid]
	,[create_date]
	,[compatibility_level]
	,[collation_name]
	,[user_access]
	,[user_access_desc]
	,[is_read_only]
	,[is_auto_close_on]
	,[is_auto_shrink_on]
	,[state]
	,[state_desc]
	,[is_in_standby]
	,[is_cleanly_shutdown]
	,[is_supplemental_logging_enabled]
	,[snapshot_isolation_state]
	,[snapshot_isolation_state_desc]
	,[is_read_committed_snapshot_on]
	,[recovery_model]
	,[recovery_model_desc]
	,[page_verify_option]
	,[page_verify_option_desc]
	,[is_auto_create_stats_on]
	,[is_auto_update_stats_on]
	,[is_auto_update_stats_async_on]
	,[is_ansi_null_default_on]
	,[is_ansi_nulls_on]
	,[is_ansi_padding_on]
	,[is_ansi_warnings_on]
	,[is_arithabort_on]
	,[is_concat_null_yields_null_on]
	,[is_numeric_roundabort_on]
	,[is_quoted_identifier_on]
	,[is_recursive_triggers_on]
	,[is_cursor_close_on_commit_on]
	,[is_local_cursor_default]
	,[is_fulltext_enabled]
	,[is_trustworthy_on]
	,[is_db_chaining_on]
	,[is_parameterization_forced]
	,[is_master_key_encrypted_by_server]
	,[is_published]
	,[is_subscribed]
	,[is_merge_published]
	,[is_distributor]
	,[is_sync_with_backup]
	,[service_broker_guid]
	,[is_broker_enabled]
	,[log_reuse_wait]
	,[log_reuse_wait_desc]
	,[is_date_correlation_on]
	,[is_cdc_enabled]
	,[is_encrypted]
	,[is_honor_broker_priority_on]
	,[replica_id]
	,[group_database_id]
	,[default_language_lcid]
	,[default_language_name]
	,[default_fulltext_language_lcid]
	,[default_fulltext_language_name]
	,[is_nested_triggers_on]
	,[is_transform_noise_words_on]
	,[two_digit_year_cutoff]
	,[containment]
	,[containment_desc]
	,[target_recovery_time_in_seconds]
FROM [master].[sys].[databases]