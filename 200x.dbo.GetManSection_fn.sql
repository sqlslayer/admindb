IF OBJECT_ID('dbo.GetManSection_fn','FN') IS NOT NULL 
	DROP FUNCTION [dbo].[GetManSection_fn]
GO

/******************************************************************************
* Name
	[dbo].[GetManSection_fn]

* Author
	Matt Stanford (SQLSlayer.com)
	
* Date
	2010.11.29
	
* Synopsis
	Inline function to shred a standard header into sections, for use by man
	
* Description
	Helper function to shred a standard SS header into separate man sections.
	
	This function typically won't ever be used on its own, but consumed by [dbo].[man].

* Examples
	SELECT [dbo].[GetManSection_fn](@header,'* Name','* ')

* Dependencies
	NONE

* Parameters
	@SQL				- T-SQL (comments) to search in
	@KeyStart			- The start key of the man section
	@KeyEnd				- The end key of the man section
	@Remove				- Any characters that should be removed
	
* Notes
	NONE

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE FUNCTION dbo.GetManSection_fn(
	@SQL		NVARCHAR(4000)
	,@KeyStart	NVARCHAR(128)
	,@KeyEnd	NVARCHAR(128)
	,@Remove	VARCHAR(50)
)
RETURNS NVARCHAR(4000)
AS
BEGIN
	DECLARE
		@ManSection		NVARCHAR(4000)
		,@I1			INT
		,@I2			INT
		
	SET @I1 = CHARINDEX(@KeyStart,@SQL)
	SET @I2 = CHARINDEX(@KeyEnd,@SQL,@I1+1)
	
	IF @I1 > 0 AND @I2 > 0		
		SET @ManSection = SUBSTRING(@SQL,@I1,@I2 - @I1)
	ELSE
		SET @ManSection = ''
		
	SET @ManSection = REPLACE(@ManSection, @KeyStart, '')
	SET @ManSection = REPLACE(@ManSection, @KeyEnd, '')
	SET @ManSection = REPLACE(@ManSection, @Remove, '')

	RETURN @ManSection
END