IF OBJECT_ID('dbo.DatabaseAttributes_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[DatabaseAttributes_vw]
GO

/******************************************************************************
* Name
	[dbo].[DatabaseAttributes_vw]

* Author
	Matt Stanford
	
* Date
	2011.04.05
	
* Synopsis
	Attribute style view of all database properties
	
* Description
	An unpivoted view of all database attributes along with some file attributes.
	It is in this format for ease of query as well as ease of storage (for consumption by 
	a DBACentral collector)

* Examples
	SELECT * FROM dbo.DatabaseAttributes_vw

* Dependencies
	dbo.sysdatabases_vw
	dbo.sysmasterfiles_vw

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20110411	Matt Stanford	Added two missing columns in sysmasterfiles	
	20110418	Adam Bean		Added COLLATE DATABASE_DEFAULT to resolve collation issues
	20120607	Adam Bean		Added new columns for tlog shipping and database mirroring
******************************************************************************/

CREATE VIEW [dbo].[DatabaseAttributes_vw]
AS

SELECT
	[name]				AS [Database_Name]
	,NULL				AS [file_id]
	,[Key]				AS [AttributeName]
	,[Value]			AS [AttributeValue]
FROM
	(
SELECT  
	d.[name]
	,CAST(d.[database_id] AS SQL_VARIANT) AS [database_id]
	,CAST([source_database_id] AS SQL_VARIANT) AS [source_database_id]
	,CAST([owner_sid] AS SQL_VARIANT) AS [owner_sid]
	,CAST([create_date] AS SQL_VARIANT) AS [create_date]
	,CAST([compatibility_level] AS SQL_VARIANT) AS [compatibility_level]
	,CAST([collation_name] AS SQL_VARIANT) AS [collation_name]
	,CAST([user_access_desc] AS SQL_VARIANT) AS [user_access_desc]
	,CAST(d.[is_read_only] AS SQL_VARIANT) AS [is_read_only]
	,CAST([is_auto_close_on] AS SQL_VARIANT) AS [is_auto_close_on]
	,CAST([is_auto_shrink_on] AS SQL_VARIANT) AS [is_auto_shrink_on]
	,CAST(d.[state_desc] AS SQL_VARIANT) AS [state_desc]
	,CAST([is_in_standby] AS SQL_VARIANT) AS [is_in_standby]
	,CAST([is_cleanly_shutdown] AS SQL_VARIANT) AS [is_cleanly_shutdown]
	,CAST([is_supplemental_logging_enabled] AS SQL_VARIANT) AS [is_supplemental_logging_enabled]
	,CAST([snapshot_isolation_state_desc] AS SQL_VARIANT) AS [snapshot_isolation_state_desc]
	,CAST([is_read_committed_snapshot_on] AS SQL_VARIANT) AS [is_read_committed_snapshot_on]
	,CAST([recovery_model_desc] AS SQL_VARIANT) AS [recovery_model_desc]
	,CAST([page_verify_option_desc] AS SQL_VARIANT) AS [page_verify_option_desc]
	,CAST([is_auto_create_stats_on] AS SQL_VARIANT) AS [is_auto_create_stats_on]
	,CAST([is_auto_update_stats_on] AS SQL_VARIANT) AS [is_auto_update_stats_on]
	,CAST([is_auto_update_stats_async_on] AS SQL_VARIANT) AS [is_auto_update_stats_async_on]
	,CAST([is_ansi_null_default_on] AS SQL_VARIANT) AS [is_ansi_null_default_on]
	,CAST([is_ansi_nulls_on] AS SQL_VARIANT) AS [is_ansi_nulls_on]
	,CAST([is_ansi_padding_on] AS SQL_VARIANT) AS [is_ansi_padding_on]
	,CAST([is_ansi_warnings_on] AS SQL_VARIANT) AS [is_ansi_warnings_on]
	,CAST([is_arithabort_on] AS SQL_VARIANT) AS [is_arithabort_on]
	,CAST([is_concat_null_yields_null_on] AS SQL_VARIANT) AS [is_concat_null_yields_null_on]
	,CAST([is_numeric_roundabort_on] AS SQL_VARIANT) AS [is_numeric_roundabort_on]
	,CAST([is_quoted_identifier_on] AS SQL_VARIANT) AS [is_quoted_identifier_on]
	,CAST([is_recursive_triggers_on] AS SQL_VARIANT) AS [is_recursive_triggers_on]
	,CAST([is_cursor_close_on_commit_on] AS SQL_VARIANT) AS [is_cursor_close_on_commit_on]
	,CAST([is_local_cursor_default] AS SQL_VARIANT) AS [is_local_cursor_default]
	,CAST([is_fulltext_enabled] AS SQL_VARIANT) AS [is_fulltext_enabled]
	,CAST([is_trustworthy_on] AS SQL_VARIANT) AS [is_trustworthy_on]
	,CAST([is_db_chaining_on] AS SQL_VARIANT) AS [is_db_chaining_on]
	,CAST([is_parameterization_forced] AS SQL_VARIANT) AS [is_parameterization_forced]
	,CAST([is_master_key_encrypted_by_server] AS SQL_VARIANT) AS [is_master_key_encrypted_by_server]
	,CAST([is_published] AS SQL_VARIANT) AS [is_published]
	,CAST([is_subscribed] AS SQL_VARIANT) AS [is_subscribed]
	,CAST([is_merge_published] AS SQL_VARIANT) AS [is_merge_published]
	,CAST([is_distributor] AS SQL_VARIANT) AS [is_distributor]
	,CAST([is_sync_with_backup] AS SQL_VARIANT) AS [is_sync_with_backup]
	,CAST([service_broker_guid] AS SQL_VARIANT) AS [service_broker_guid]
	,CAST([is_broker_enabled] AS SQL_VARIANT) AS [is_broker_enabled]
	,CAST([log_reuse_wait_desc] AS SQL_VARIANT) AS [log_reuse_wait_desc]
	,CAST([is_date_correlation_on] AS SQL_VARIANT) AS [is_date_correlation_on]
	,CASE
		WHEN s.[primary_database] IS NOT NULL THEN CAST(1 AS SQL_VARIANT)
		ELSE CAST(0 AS SQL_VARIANT)
	END AS [is_tlog_shipped]
	,CASE
		WHEN m.[mirroring_guid] IS NOT NULL THEN CAST(1 AS SQL_VARIANT)
		ELSE CAST(0 AS SQL_VARIANT)
	END AS [is_db_mirrored]
FROM [dbo].[sysdatabases_vw] d
LEFT JOIN [msdb].[dbo].[log_shipping_primary_databases] s
	ON d.[name] = s.[primary_database]
LEFT JOIN [sys].[database_mirroring] m
	ON d.[database_id] = m.[database_id]
) p
UNPIVOT
	([Value] FOR [Key] IN (
		[p].[create_date]
		,p.[database_id]
		,p.[source_database_id]
		,p.[owner_sid]
		,p.[compatibility_level]
		,p.[collation_name]
		,p.[user_access_desc]
		,p.[is_read_only]
		,p.[is_auto_close_on]
		,p.[is_auto_shrink_on]
		,p.[state_desc]
		,p.[is_in_standby]
		,p.[is_cleanly_shutdown]
		,p.[is_supplemental_logging_enabled]
		,p.[snapshot_isolation_state_desc]
		,p.[is_read_committed_snapshot_on]
		,p.[recovery_model_desc]
		,p.[page_verify_option_desc]
		,p.[is_auto_create_stats_on]
		,p.[is_auto_update_stats_on]
		,p.[is_auto_update_stats_async_on]
		,p.[is_ansi_null_default_on]
		,p.[is_ansi_nulls_on]
		,p.[is_ansi_padding_on]
		,p.[is_ansi_warnings_on]
		,p.[is_arithabort_on]
		,p.[is_concat_null_yields_null_on]
		,p.[is_numeric_roundabort_on]
		,p.[is_quoted_identifier_on]
		,p.[is_recursive_triggers_on]
		,p.[is_cursor_close_on_commit_on]
		,p.[is_local_cursor_default]
		,p.[is_fulltext_enabled]
		,p.[is_trustworthy_on]
		,p.[is_db_chaining_on]
		,p.[is_parameterization_forced]
		,p.[is_master_key_encrypted_by_server]
		,p.[is_published]
		,p.[is_subscribed]
		,p.[is_merge_published]
		,p.[is_distributor]
		,p.[is_sync_with_backup]
		,p.[service_broker_guid]
		,p.[is_broker_enabled]
		,p.[log_reuse_wait_desc]
		,p.[is_date_correlation_on]
		,p.[is_tlog_shipped]
		,p.[is_db_mirrored]
		)
) AS unpvt

UNION ALL

SELECT
	[database_name] COLLATE DATABASE_DEFAULT
	,[file_id]
	,[Key]
	,[Value]
FROM (
SELECT 
	f.[database_name]
	,f.[file_id]
	,CAST(f.[physical_name] AS SQL_VARIANT) AS [physical_name]
	,CAST(f.[growth] AS SQL_VARIANT) AS [growth]
	,CAST(f.[name] AS SQL_VARIANT) AS [name]
	,CAST(f.[is_percent_growth] AS SQL_VARIANT) AS [is_percent_growth]
	,CAST(f.[is_read_only] AS SQL_VARIANT) AS [is_read_only]
	,CAST(f.[type_desc] AS SQL_VARIANT) AS [type_desc]
	,CAST(f.[size] AS SQL_VARIANT) AS [size]
	,CAST(f.[state_desc] AS SQL_VARIANT) AS [state_desc]
	,CAST(f.[max_size] AS SQL_VARIANT) AS [max_size]
	,CAST(f.[is_sparse] AS SQL_VARIANT) AS [is_sparse]
	,CAST(f.[is_name_reserved] AS SQL_VARIANT) AS [is_name_reserved]
	,CAST(f.[data_space_id] AS SQL_VARIANT) AS [data_space_id]
	,CAST(f.[is_media_read_only] AS SQL_VARIANT) AS [is_media_read_only]
FROM [dbo].[sysmasterfiles_vw] f
) p
UNPIVOT
	([Value] FOR [Key] IN (
		p.[physical_name]
		,p.[growth]
		,p.[name]
		,p.[is_percent_growth]
		,p.[is_read_only]
		,p.[type_desc]
		,p.[size]
		,p.[state_desc]
		,p.[max_size]
		,p.[is_sparse]
		,p.[is_name_reserved]
		,p.[data_space_id]
		,p.[is_media_read_only]
	)
) AS unpvt