IF OBJECT_ID('[dbo].[Maintenance_Reindex_Logger]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[Maintenance_Reindex_Logger]
GO

/******************************************************************************
* Name
	[dbo].[Maintenance_Reindex_Logger]

* Author
	Adam Bean
	
* Date
	2011.09.20
	
* Synopsis
	Logging procedure which supports Reindex
	
* Description
	Procedure to normalize and log all appropriate data surrounding the Reindex maintenance procedure.

* Examples
	Do not call this procedure directly, it will be utilized within the maintenance routines.

* Dependencies
	[dbo].[Maintenance_GetDatabaseID] 
	[dbo].[Maintenance_GetSchemaID]
	[dbo].[Maintenance_GetTableID]]
	[dbo].[Maintenance_GetIndexID] 
	[dbo].[Maintenance_GetCommandID]
	[dbo].[Maintenance_GetRunID] 
	[dbo].[Maintenance_Reindex]
	
* Parameters
	@DBName = Database that had Reindex run on it
	@SchemaName = Schema that had Reindex run on it
	@TableName = Table that had Reindex run on it
	@IndexName = Index that had Reindex run on it
	@IndexFillFactor = The fillfactor of the index
	@PageSpaceUsed = The page space used per index
	@RecordSize = The record size of the index
	@AvgFrag = The fragmentation of the index
	@PctChange = The percent of change since the last stats update
	@RowCount = The amount of rows per table
	@Command = The CHECK and WITH options run with CHECKDB
	@Override = Specifies that an override via extended properties was utilized

* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20140222	Adam Bean		Added @DateStart and @DateEnd
								Removed @RunID logic from this procedure as it's now done in the calling Reindex procedure
******************************************************************************/

CREATE PROCEDURE [dbo].[Maintenance_Reindex_Logger]
(
	@DBName				NVARCHAR(128)
	,@TableName			NVARCHAR(128)
	,@SchemaName		NVARCHAR(128)
	,@IndexName			NVARCHAR(128)
	,@IndexFillFactor	TINYINT
	,@PageSpaceUsed		FLOAT
	,@RecordSize		FLOAT
	,@AvgFrag			FLOAT
	,@Command			VARCHAR(3072)
	,@Override			BIT
	,@DateStart			DATETIME
	,@DateEnd			DATETIME
	,@RunID				INT
)

AS

SET NOCOUNT ON

DECLARE 	
	@DatabaseID			INT
	,@SchemaID			INT
	,@TableID			INT
	,@IndexID			INT
	,@CommandID			INT


-- Get the database ID
EXEC [dbo].[Maintenance_GetDatabaseID] 
	@DBName = @DBName
	,@DatabaseID = @DatabaseID OUTPUT

-- Get the schema ID
EXEC [dbo].[Maintenance_GetSchemaID] 
	@SchemaName = @SchemaName
	,@SchemaID = @SchemaID OUTPUT
	
-- Get the table ID
EXEC [dbo].[Maintenance_GetTableID] 
	@TableName = @TableName
	,@TableID = @TableID OUTPUT

-- Get the index ID
EXEC [dbo].[Maintenance_GetIndexID] 
	@IndexName = @IndexName
	,@IndexID = @IndexID OUTPUT

-- Get the command ID
EXEC [dbo].[Maintenance_GetCommandID] 
	@Command = @Command
	,@CommandID = @CommandID OUTPUT
	
-- Insert the values
INSERT INTO [dbo].[Maintenance_Reindex]
([DatabaseID], [SchemaID], [TableID], [IndexID], [IndexFillFactor], [PageSpaceUsed], [RecordSize], [AvgFrag], [CommandID], [IsOverride], [RunID], [DateStart], [DateEnd])
SELECT
	@DatabaseID
	,@SchemaID
	,@TableID
	,@IndexID
	,@IndexFillFactor
	,@PageSpaceUsed
	,@RecordSize
	,@AvgFrag
	,@CommandID
	,@Override
	,@RunID
	,@DateStart
	,@DateEnd

SET NOCOUNT OFF