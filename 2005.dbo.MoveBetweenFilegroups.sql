IF OBJECT_ID('dbo.MoveBetweenFilegroups','P') IS NOT NULL
	DROP PROCEDURE [dbo].[MoveBetweenFilegroups]
	
GO

/******************************************************************************
**	Name:			[dbo].[MoveBetweenFilegroups]
**	Desc:			Procedure to accurately and fully move tables and indexes to a specific filegroup
**	Auth:			Matt Stanford
**  Dependancies:	None
**  Parameters:		@DBName = the database in which you want to move tables/indexes
					@TablesToMove = the name of a single table that you want to move (all indexes will be moved).  
							Future versions may support multiple table names/pattern matching.
					@TargetFileGroup = The filgroup that you want to move the table and indexes to.
					@Debug = if set to 0, changes will be made to the system.
**  Usage:			EXEC dbo.MoveBetweenFilegroups 'admin', 'sampleTable', 'Secondary', 1
**  Notes:			May work your server.  Use with caution.  Also will acquire heavy locks on your tables.
**	Date:			2009.09.28
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:				Description:
**	--------	--------			---------------------------------------	
**	20110705	Adam Bean			Fixed @TargetFileGroup for case sensitive collations
********************************************************************************************************/

CREATE PROCEDURE [dbo].[MoveBetweenFilegroups]
(
	@DBName						NVARCHAR(128)
	,@TablesToMove				NVARCHAR(4000)
	,@TargetFileGroup			NVARCHAR(128)
	,@Debug						BIT				= 0
)
AS

SET NOCOUNT ON

DECLARE
	@DBID						INT
	,@FGID						INT
	,@DropCmd					NVARCHAR(4000)
	,@CreateCmd					NVARCHAR(4000)
	,@object_id					INT
	,@index_id					INT
	,@index_name				sysname
	,@type						TINYINT
	,@type_desc					NVARCHAR(60)
	,@is_unique					BIT
	,@ignore_dup_key			BIT
	,@is_primary_key			BIT
	,@is_unique_constraint		BIT
	,@fill_factor				TINYINT
	,@is_padded					BIT
	,@is_disabled				BIT
	,@is_hypothetical			BIT
	,@allow_row_locks			BIT
	,@allow_page_locks			BIT
	,@SchemaName				NVARCHAR(128)
	,@ObjectName				NVARCHAR(128)
	,@KeyCols					NVARCHAR(4000)
	,@NonKeyCols				NVARCHAR(4000)
	,@WithClause				NVARCHAR(4000)
	
-----------------
--DECLARE
--	@DBName						NVARCHAR(128)
--	,@TablesToMove				NVARCHAR(4000)
--	,@TargetFileGroup			NVARCHAR(128)
--	,@Debug						BIT
	
--SET @DBName = 'DWH'
--SET @TargetFileGroup = 'fact_inventory_2008Q1'
--SET @TablesToMove = 'fact_Inventory_Ford_200801'
--SET @Debug = 1
 --EXEC [dbo].[MoveBetweenFilegroups] 'DWH', 'fact_inventory_FordPDC_200405', 'fact_inventory_2009Q3', 1

IF OBJECT_ID('tempdb.dbo.#sysindexes') IS NOT NULL
   DROP TABLE #sysindexes
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#syspartitions') IS NOT NULL
   DROP TABLE #syspartitions
IF OBJECT_ID('tempdb.dbo.#sysschemas') IS NOT NULL
   DROP TABLE #sysschemas
IF OBJECT_ID('tempdb.dbo.#syscolumns') IS NOT NULL
   DROP TABLE #syscolumns
IF OBJECT_ID('tempdb.dbo.#sysindex_columns') IS NOT NULL
   DROP TABLE #sysindex_columns
IF OBJECT_ID('tempdb.dbo.#sysfilegroups') IS NOT NULL
   DROP TABLE #sysfilegroups
IF OBJECT_ID('tempdb.dbo.#sysallocation_units') IS NOT NULL
   DROP TABLE #sysallocation_units
IF OBJECT_ID('tempdb.dbo.#IndexDefs') IS NOT NULL
	DROP TABLE #IndexDefs

-- Create the tables
SELECT TOP 0 *, [statsdate] = CAST(NULL AS DATETIME), [database_id] = CONVERT(INT,NULL) INTO #sysindexes FROM [sys].[indexes]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysobjects FROM [sys].[objects]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #syspartitions FROM [sys].[partitions]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysschemas FROM [sys].[schemas]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #syscolumns FROM [sys].[columns]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysindex_columns  FROM [sys].[index_columns]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysallocation_units FROM [sys].[allocation_units]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysfilegroups  FROM [sys].[filegroups]


CREATE TABLE #IndexDefs (
	[database_id]			INT
	,[object_id]			INT
	,[index_id]				INT
	,[type]					TINYINT
	,[dropstatement]		NVARCHAR(4000)
	,[createstatement]		NVARCHAR(4000)
	,PRIMARY KEY CLUSTERED (
		[database_id]
		,[object_id]
		,[index_id]
	)
)

-- Get the object information for the specific database
EXEC
('
	USE [' + @DBName + '] 
		
	-- sys.indexes
	INSERT INTO #sysindexes
	EXEC(''SELECT i.*, STATS_DATE(i.[object_id], i.[index_id]), DB_ID() FROM [sys].[indexes] i JOIN [sys].[tables] t ON t.[object_id] = i.[object_id]'')

	-- sys.objects
	INSERT INTO #sysobjects
	EXEC(''SELECT *, DB_ID() FROM [sys].[objects]'')

	-- sys.partitions 
	INSERT INTO #syspartitions
	EXEC(''SELECT *, DB_ID() FROM [sys].[partitions]'')

	-- sys.schemas 
	INSERT INTO #sysschemas
	EXEC(''SELECT *, DB_ID() FROM [sys].[schemas]'')

	-- sys.columns 
	INSERT INTO #syscolumns
	EXEC(''SELECT *, DB_ID() FROM [sys].[columns]'')

	-- sys.index_columns 
	INSERT INTO #sysindex_columns 
	EXEC(''SELECT *, DB_ID() FROM [sys].[index_columns]'')

	-- sys.filegroups
	INSERT INTO #sysfilegroups 
	EXEC(''SELECT *, DB_ID() FROM [sys].[filegroups]'')

	-- sys.allocation_units
	INSERT INTO #sysallocation_units
	EXEC(''SELECT *, DB_ID() FROM [sys].[allocation_units]'')
')

SELECT @FGID = [data_space_id]
FROM #sysfilegroups
WHERE [name] = @TargetFileGroup
AND [database_id] = DB_ID(@DBName)

DECLARE #idx CURSOR LOCAL STATIC FOR
SELECT 
	i.[database_id]
	,i.[object_id]
	,i.[index_id]
	,i.[name]
	,i.[type]
	,i.[type_desc]
	,i.[is_unique]
	,i.[ignore_dup_key]
	,i.[is_primary_key]
	,i.[is_unique_constraint]
	,i.[fill_factor]
	,i.[is_padded]
	,i.[is_disabled]
	,i.[is_hypothetical]
	,i.[allow_row_locks]
	,i.[allow_page_locks]
FROM #sysindexes i
INNER JOIN #sysobjects o
	ON i.[object_id] = o.[object_id]
	AND i.[database_id] = o.[database_id]
WHERE i.[type] IN (0,1,2)
AND i.[data_space_id] <> @FGID
AND (o.[name] LIKE @TablesToMove
	OR o.[name] = @TablesToMove)

OPEN #idx

FETCH NEXT FROM #idx INTO @DBID, @object_id, @index_id, @index_name, @type, @type_desc, @is_unique, @ignore_dup_key, @is_primary_key, @is_unique_constraint, @fill_factor, @is_padded, @is_disabled, @is_hypothetical, @allow_row_locks, @allow_page_locks

-- Generate the drop/create statements
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Reset variables for commands
	SET @DropCmd = ''
	SET @CreateCmd = ''
	SET @KeyCols = ''
	SET @NonKeyCols = ''
	SET @WithClause = ''
	
	-- Lookup the schema/object names
	SELECT
		@SchemaName = s.[name]
		,@ObjectName = o.[name]
	FROM #sysobjects o
	INNER JOIN #sysschemas s
		ON o.[schema_id] = s.[schema_id]
		AND o.[database_id] = s.[database_id]
	WHERE o.[object_id] = @object_id
	AND o.[database_id] = @DBID
	
	-- Build a list of keyed (indexed) columns.  It will have a trailing comma
	SELECT 
		@KeyCols = @KeyCols + '[' + c.[name] + ']' + CASE WHEN ic.[is_descending_key] = 1 THEN ' DESC' ELSE ' ASC' END + ','
	FROM #sysindex_columns ic
	INNER JOIN #syscolumns c
		ON c.[object_id]			= ic.[object_id]
		AND c.[column_id]			= ic.[column_id]
		AND c.[database_id]			= ic.[database_id]
	WHERE c.[object_id]				= @object_id
		AND ic.[index_id]			= @index_id
		AND c.[database_id]			= @DBID
		AND ic.[is_included_column] = 0
	ORDER BY ic.[key_ordinal]
	
	-- If there were indexed columns, remove the trailing comma
	IF LEN(@KeyCols) > 0
		SET @KeyCols = LEFT(@KeyCols,LEN(@KeyCols) - 1)
	
	-- Build a list of non-keyed (included) columns.  It will have a trailing comma.
	SELECT 
		@NonKeyCols = @NonKeyCols + '[' + c.[name] + ']' + CASE WHEN ic.[is_descending_key] = 1 THEN ' DESC' ELSE ' ASC' END + ','
	FROM #sysindex_columns ic
	INNER JOIN #syscolumns c
		ON c.[object_id]			= ic.[object_id]
		AND c.[column_id]			= ic.[column_id]
		AND c.[database_id]			= ic.[database_id]
	WHERE c.[object_id]				= @object_id
		AND ic.[index_id]			= @index_id
		AND c.[database_id]			= @DBID
		AND ic.[is_included_column] = 1
	ORDER BY ic.[key_ordinal]
	
	-- If there were included columns, remove the trailing comma
	IF LEN(@NonKeyCols) > 0
		SET @NonKeyCols = 'INCLUDE (' + LEFT(@NonKeyCols,LEN(@NonKeyCols) - 1) + ')'
		
	-- Build the "WITH" clause for clustered and non-clustered indexes
	IF @type IN (1,2)
	BEGIN
		SET @WithClause = ' WITH ('

		IF @is_padded = 1
			SET @WithClause = @WithClause + 'PAD_INDEX = ON,'
		ELSE
			SET @WithClause = @WithClause + 'PAD_INDEX = OFF,'
		
		IF @fill_factor > 0
			SET @WithClause = @WithClause + 'FILLFACTOR = ' + CAST(@fill_factor AS VARCHAR(3)) + ','

		IF @ignore_dup_key = 1
			SET @WithClause = @WithClause + 'IGNORE_DUP_KEY = ON,'
		ELSE
			SET @WithClause = @WithClause + 'IGNORE_DUP_KEY = OFF,'			

		IF @allow_row_locks = 1
			SET @WithClause = @WithClause + 'ALLOW_ROW_LOCKS = ON,'
		ELSE
			SET @WithClause = @WithClause + 'ALLOW_ROW_LOCKS = OFF,'
			
		IF @allow_page_locks = 1
			SET @WithClause = @WithClause + 'ALLOW_PAGE_LOCKS = ON)'
		ELSE
			SET @WithClause = @WithClause + 'ALLOW_PAGE_LOCKS = OFF)'		

	END

	-- Build the drop/create statements
	IF @type = 0 -- HEAP
	BEGIN
	
		-- Heaps don't have key/non-key columns.  I'm going to re-use the @KeyCols variable here, because it should be empty.
		SELECT 
			TOP 1 @KeyCols = [name]
		FROM #syscolumns sc
		WHERE sc.[system_type_id] NOT IN (34,35,98,99,104,173,189,241)
		AND sc.[object_id] = @object_id
		AND sc.[database_id] = @DBID
		
		-- The create/drops will be backwards here, only for heaps.  This is because the standard "drop" will be to create the clustered index.
		SET @DropCmd = 'CREATE CLUSTERED INDEX [CIX_Temp_' + CAST(@object_id AS NVARCHAR(128)) + '] ON [' + @SchemaName + '].[' + @ObjectName + '] ([' + @KeyCols + ']) ON [' + @TargetFileGroup + ']'	
		SET @CreateCmd = 'DROP INDEX [' + @SchemaName + '].[' + @ObjectName + '].[CIX_Temp_' + CAST(@object_id AS NVARCHAR(128)) + ']'

	END
	ELSE IF @type = 1 -- CLUSTERED INDEX
	BEGIN
	
		IF @is_primary_key = 1
		BEGIN
			SET @DropCmd = 'ALTER TABLE [' + @SchemaName + '].[' + @ObjectName + '] DROP CONSTRAINT [' + @index_name + ']'
			SET @CreateCmd = 'ALTER TABLE [' + @SchemaName + '].[' + @ObjectName + '] ADD CONSTRAINT [' + @index_name + '] PRIMARY KEY CLUSTERED (' + @KeyCols + ') ' + @NonKeyCols + @WithClause + ' ON [' + @TargetFileGroup + ']'
		END
		ELSE	
		BEGIN -- Regular clustered index
			SET @DropCmd = 'DROP INDEX [' + @SchemaName + '].[' + @ObjectName + '].[' + @index_name + ']'
			SET @CreateCmd = 'CREATE '
			
			-- If Unique
			IF @is_unique = 1
				SET @CreateCmd = @CreateCmd + 'UNIQUE ' 
			
			SET @CreateCmd = @CreateCmd + 'CLUSTERED INDEX [' + @index_name + '] ON [' + @SchemaName + '].[' + @ObjectName + '] (' + @KeyCols + ') ' + @NonKeyCols + @WithClause
			
			SET @CreateCmd = @CreateCmd + ' ON [' + @TargetFileGroup + ']'
			
		END
	
	END
	ELSE IF @type = 2 -- NONCLUSTERED INDEX
	BEGIN
		IF @is_primary_key = 1
		BEGIN
			SET @DropCmd = 'ALTER TABLE [' + @SchemaName + '].[' + @ObjectName + '] DROP CONSTRAINT [' + @index_name + ']'
			SET @CreateCmd = 'ALTER TABLE [' + @SchemaName + '].[' + @ObjectName + '] ADD CONSTRAINT [' + @index_name + '] PRIMARY KEY NONCLUSTERED (' + @KeyCols + ') ' + @NonKeyCols + @WithClause + ' ON [' + @TargetFileGroup + ']'
		END
		ELSE	
		BEGIN -- Regular nonclustered index
			SET @DropCmd = 'DROP INDEX [' + @SchemaName + '].[' + @ObjectName + '].[' + @index_name + ']'
			
			SET @CreateCmd = 'CREATE '
			
			-- If Unique
			IF @is_unique = 1
				SET @CreateCmd = @CreateCmd + 'UNIQUE ' 
			
			SET @CreateCmd = @CreateCmd + 'INDEX [' + @index_name + '] ON [' + @SchemaName + '].[' + @ObjectName + '] (' + @KeyCols + ') ' + @NonKeyCols + @WithClause
			
			SET @CreateCmd = @CreateCmd + ' ON [' + @TargetFileGroup + ']'
		END
	END


	-- Insert
	INSERT INTO #IndexDefs ([database_id],[object_id],[index_id],[type],[dropstatement],[createstatement])
	VALUES (@DBID,@object_id,@index_id,@type,@DropCmd,@CreateCmd)

	FETCH NEXT FROM #idx INTO @DBID, @object_id, @index_id, @index_name, @type, @type_desc, @is_unique, @ignore_dup_key, @is_primary_key, @is_unique_constraint, @fill_factor, @is_padded, @is_disabled, @is_hypothetical, @allow_row_locks, @allow_page_locks

END
CLOSE #idx
DEALLOCATE #idx

-- Drop Nonclustered
-- HEAP - Create Clustered
-- HEAP - Drop Clustered
-- Drop Clustered
-- Create Clustered
-- Create Nonclustered

-- Add Use statement
UPDATE i
	SET [dropstatement] = 'USE [' + @DBName + ']; ' + [dropstatement]
	,[createstatement] = 'USE [' + @DBName + ']; ' + [createstatement]
FROM #IndexDefs i

-- Do this one object at a time, for performance sake
DECLARE #objects CURSOR LOCAL STATIC FOR
SELECT
	DISTINCT [database_id]
	,[object_id]
FROM #IndexDefs

OPEN #objects
FETCH NEXT FROM #objects INTO @DBID, @object_id

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @DropCmd = NULL
	SET @CreateCmd = NULL
	
	-- May have multiple nonclustered indexes, so remove them first with a cursor
	DECLARE #NCRemover CURSOR LOCAL STATIC FOR
	SELECT
		[dropstatement]
	FROM #IndexDefs
	WHERE [database_id] = @DBID
	AND [object_id] = @object_id
	AND [type] = 2 -- nonclustered only
	
	OPEN #NCRemover
	FETCH NEXT FROM #NCRemover INTO @DropCmd
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT('-- Nonclustered indexes - Dropping')
		IF @Debug = 0
			EXEC(@DropCmd)
		ELSE
			PRINT(@DropCmd)
	
		FETCH NEXT FROM #NCRemover INTO @DropCmd
	END
	
	CLOSE #NCRemover
	DEALLOCATE #NCRemover
	
	SET @DropCmd = NULL
	
	-- Heaps
	-- Create clustered index
	SELECT
		@CreateCmd = [dropstatement]
	FROM #IndexDefs
	WHERE [database_id] = @DBID
	AND [object_id] = @object_id
	AND [type] = 0
	
	IF @CreateCmd IS NOT NULL
	BEGIN
		PRINT('-- Heap - Creating temp clustered index')
		IF @Debug = 0
			EXEC(@CreateCmd)
		ELSE
			PRINT(@CreateCmd)
	END
	SET @CreateCmd = NULL
	
	-- Drop clustered index
	SELECT
		@DropCmd = [createstatement]
	FROM #IndexDefs
	WHERE [database_id] = @DBID
	AND [object_id] = @object_id
	AND [type] = 0
	
	IF @DropCmd IS NOT NULL
	BEGIN
		PRINT('-- Heap - Dropping temp clustered index')
		IF @Debug = 0
			EXEC(@DropCmd)
		ELSE
			PRINT(@DropCmd)
	END
	SET @DropCmd = NULL
	
	-- Clustered indexes --
	-- Drop clustered index
	SELECT
		@DropCmd = [dropstatement]
	FROM #IndexDefs
	WHERE [database_id] = @DBID
	AND [object_id] = @object_id
	AND [type] = 1
	
	IF @DropCmd IS NOT NULL
	BEGIN
		PRINT('-- Clustered Index - Dropping')
		IF @Debug = 0
			EXEC(@DropCmd)
		ELSE
			PRINT(@DropCmd)
	END
	SET @DropCmd = NULL
	
	-- Create clustered index
	SELECT
		@CreateCmd = [createstatement]
	FROM #IndexDefs
	WHERE [database_id] = @DBID
	AND [object_id] = @object_id
	AND [type] = 1
	
	IF @CreateCmd IS NOT NULL
	BEGIN
		PRINT('-- Clustered Index - Re-Create')
		IF @Debug = 0
			EXEC(@CreateCmd)
		ELSE
			PRINT(@CreateCmd)
	END
	SET @CreateCmd = NULL
	
	-- May have multiple nonclustered indexes, so add them last with a cursor
	DECLARE #NCAdder CURSOR LOCAL STATIC FOR
	SELECT
		[createstatement]
	FROM #IndexDefs
	WHERE [database_id] = @DBID
	AND [object_id] = @object_id
	AND [type] = 2 -- nonclustered only
	
	OPEN #NCAdder
	FETCH NEXT FROM #NCAdder INTO @CreateCmd
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT('-- Nonclustered Index - Re-Create')
		IF @Debug = 0
			EXEC(@CreateCmd)
		ELSE
			PRINT(@CreateCmd)
	
		FETCH NEXT FROM #NCAdder INTO @CreateCmd
	END
	
	CLOSE #NCAdder
	DEALLOCATE #NCAdder
	
	SET @CreateCmd = NULL
	--

	FETCH NEXT FROM #objects INTO @DBID, @object_id
END

CLOSE #objects
DEALLOCATE #objects

--SELECT * FROM #IndexDefs
