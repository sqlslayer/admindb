IF OBJECT_ID('dbo.CleanupMSDB','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[CleanupMSDB]
GO

/******************************************************************************
* Name
	[dbo].[CleanupMSDB]

* Author
	Adam Bean
	
* Date
	2014.01.07
	
* Synopsis
	Procedure to cleanup system data stored in msdb.
	
* Description
	Expanding upon sp_delete_backuphistory & sysmail_delete_mailitems_sp, this procedure will cleanup all tables as they pertain to database mail, backups and restores;
	however instead of a single transaction that the builtin system objects use, this procedure will delete in batches to avoid very lenghty runs and filled transaction logs.

* Examples
	EXEC [dbo].[CleanupMSDB] @DeleteOlderThan = 14, @BatchSize = 30, @Debug = 1

* Dependencies
	n/a

* Parameters
	@Backups			- Perform backup history cleanup
	@Mail				- Perform mail cleanup
	@DeleteOlderThan	- Numbers of days subtracted from today to start purge from [defaulted to six months]
	@BatchSize			- Numbers of days to delete as a batch [defaulted to one week]
	@Shrink				- Perform a DBCC SHRINKDATABASE on msdb (should not be scheduled)
	@Debug				- Display queries to be run but don't execute
	
* Notes
	msdb.dbo.sp_delete_backuphistory msdb.dbo.sysmail_delete_mailitems_sp

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [dbo].[CleanupMSDB]
(
	@Backups			BIT = 1
	,@Mail				BIT = 1
	,@DeleteOlderThan	INT = 180 
	,@BatchSize			INT = 7
	,@Shrink			BIT = 0
	,@Debug				BIT = 0
)

AS

DECLARE 
	@Count				INT
	,@SQL				VARCHAR(MAX)  
	,@DateEnd			SMALLDATETIME
	,@DeletePriorToDate	SMALLDATETIME  

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'

-- Cleanup backups
IF @Backups = 1
BEGIN

	IF @Debug = 1
		PRINT '-- Cleanup backups'

	-- Retrieve the cutoff date to delete everything prior to this date
	SELECT @DateEnd = DATEADD(DAY, -@DeleteOlderThan, MAX([backup_start_date])) FROM [msdb].[dbo].[backupset]
	
	-- Set the @DeletePriorToDate based on the @BatchSize. This will give us the starting point to delete from.
	SELECT @DeletePriorToDate = DATEADD(DAY, @BatchSize, (SELECT MIN([backup_start_date]) FROM [msdb].[dbo].[backupset]))

	-- Index msdb
	IF @Debug = 0
	BEGIN
		IF NOT EXISTS (SELECT [name] FROM [msdb].[sys].[indexes] WHERE OBJECT_ID = OBJECT_ID('[dbo].[backupset]') AND [name] = 'IX__backupset__backup_finish_date__WITH__Includes')
			CREATE INDEX [IX__backupset__backup_finish_date__WITH__Includes] ON [msdb].[dbo].[backupset]([backup_finish_date]) INCLUDE ([media_set_id])
 
		IF NOT EXISTS (SELECT [name] FROM [msdb].[sys].[indexes] WHERE OBJECT_ID = OBJECT_ID('[dbo].[backupset]') AND name = 'IX__backupset__media_set_id')
			CREATE INDEX [IX__backupset__media_set_id] ON [msdb].[dbo].[backupset] ([media_set_id])
	END

	-- Delete in batches
	WHILE @DeletePriorToDate < @DateEnd
	BEGIN		
		IF @Debug = 1
			PRINT 'EXEC [msdb].[dbo].[sp_delete_backuphistory] @oldest_date = ''' + CAST(@DeletePriorToDate AS VARCHAR) + ''''
		ELSE  
			EXEC [msdb].[dbo].[sp_delete_backuphistory] @oldest_date = @DeletePriorToDate

		-- Reset the date to delete from
		SELECT @DeletePriorToDate = DATEADD(DAY, @BatchSize, @DeletePriorToDate)
	END

	-- Remove our indexing
	IF EXISTS (SELECT [name] FROM [msdb].[sys].[indexes] WHERE [name] = 'IX__backupset__backup_finish_date__WITH__Includes')
		EXEC ('USE [msdb] DROP INDEX [backupset].[IX__backupset__backup_finish_date__WITH__Includes]')
 
	IF EXISTS (SELECT [name] FROM [msdb].[sys].[indexes] WHERE [name] = 'IX__backupset__media_set_id')
		EXEC ('USE [msdb] DROP INDEX [backupset].[IX__backupset__media_set_id]')

END -- Cleanup backups done


-- Cleanup database mail
IF @Mail = 1
BEGIN

	IF @Debug = 1
		PRINT '-- Cleanup database mail'

	-- Retrieve the cutoff date to delete everything prior to this date
	SELECT @DateEnd = DATEADD(DAY, -@DeleteOlderThan, MAX([send_request_date])) FROM [msdb].[dbo].[sysmail_allitems]
	
	-- Set the @DeletePriorToDate based on the @BatchSize. This will give us the starting point to delete from.
	SELECT @DeletePriorToDate = DATEADD(DAY, @BatchSize, (SELECT MIN([send_request_date]) FROM [msdb].[dbo].[sysmail_allitems]))
	
	-- Delete in batches
	WHILE @DeletePriorToDate < @DateEnd
	BEGIN		
		IF @Debug = 1
			PRINT 'EXEC [msdb].[dbo].[sysmail_delete_mailitems_sp] @sent_before = ''' + CAST(@DeletePriorToDate AS VARCHAR) + ''''
		ELSE  
			EXEC [msdb].[dbo].[sysmail_delete_mailitems_sp] @sent_before = @DeletePriorToDate

		-- Reset the date to delete from
		SELECT @DeletePriorToDate = DATEADD(DAY, @BatchSize, @DeletePriorToDate)
	END
    
END -- Cleanup mail done

-- Shrink MSDB
IF @Shrink = 1
BEGIN

	IF @Debug = 1
		PRINT '-- Shrink msdb'

	IF @Debug = 1
		PRINT 'DBCC SHRINKDATABASE (''msdb'')'
	ELSE  
		DBCC SHRINKDATABASE ('msdb')
END
	