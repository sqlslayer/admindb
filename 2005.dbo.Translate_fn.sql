IF OBJECT_ID('dbo.Translate_fn','FN') IS NOT NULL 
	DROP FUNCTION [dbo].[Translate_fn]
GO

/******************************************************************************
* Name
       [dbo].[Translate_fn]

* Author
       Philip (Dale) Campbell
       
* Date
       2013.01.24
       
* Synopsis
       Implementation of Oracle 'Translate' function
       
* Description
       A given string is returned in a modified state, based on the contents
       of two provided control strings. For each character in @Match which
       appears in @Source, it is replaced with the character in the corresponding
       position in @Replacement. If @Match is longer than @Replacement, any
       additional characters in @Match are removed from @Source.

* Examples
       ROT13 (output in all uppercase):
         SELECT dbo.Translate( upper('Fancr xvyyrq Qhzoyrqber!')
                             , 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                             , 'NOPQRSTUVWXYZABCDEFGHIJKLM')

       ROT13 (case-preserved):
         SELECT dbo.Translate( 'Fancr xvyyrq Qhzoyrqber!'
                             , 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
                             , 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm')

       Remove all non-digit characters from a phone #:
         DECLARE @Phone VARCHAR(24) = '214/304-2856';
         SELECT dbo.Translate( @Phone
                             , dbo.Translate( @Phone
                                            , '0123456789'
                                            , '')
                             , '')


* Dependencies

* Parameters
       @Source                   - String to modify
       @Match                    - Characters in @Source to be changed
       @Replacement              - Characters to be substituted into @Source


*******************************************************************************
* License
*******************************************************************************
       Copyright � SQLSlayer.com. All rights reserved.

       All objects published by SQLSlayer.com are licensed and goverened by 
       Creative Commons Attribution-Share Alike 3.0
       http://creativecommons.org/licenses/by-sa/3.0/

       For more scripts and sample code, go to http://www.SQLSlayer.com

       You may alter this code for your own *non-commercial* purposes. You may
       republish altered code as long as you give due credit.

       THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
       ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
       TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
       PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
       Date:         Author:                    Description:
       --------      --------                   -------------------------------
       20130124      Philip (Dale) Campbell     Initial creation.
******************************************************************************/
CREATE FUNCTION dbo.Translate_fn (
    @Source   NVARCHAR(MAX)
  , @Match    NVARCHAR(MAX)
  , @Replacement   NVARCHAR(MAX)
  )
  RETURNS NVARCHAR(MAX)
AS
  BEGIN
    DECLARE @ndx    INT
          , @Str    NVARCHAR(MAX)
          , @hit  NCHAR
          , @new    INT;

	SET @ndx = 0
	SET @Str = ''

    IF @Source IS NULL
        RETURN NULL;
    IF isnull( len(@Match), 0) < 1
        RETURN @Source;

    WHILE @ndx < len(@Source)
      BEGIN
        SET @ndx = @ndx + 1;
        SET @hit= substring( @Source, @ndx, 1);
        SET @new= charindex( @hit, @Match COLLATE Latin1_General_CS_AS);
        IF @new > 0
            SET @Str = @Str + substring( @Replacement, @new, 1);
        ELSE
            SET @Str = @Str + @hit;
    END

    RETURN @Str;
  END
