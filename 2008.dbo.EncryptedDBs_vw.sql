﻿IF OBJECT_ID('dbo.EncryptedDBs_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[EncryptedDBs_vw]
GO

/******************************************************************************
**	Name:			[dbo].[EncryptedDBs_vw]
**	Desc:			View to query encryption state, keys, certificates
**	Auth:			Jeff Gogel
**	Date:			2013.09.18
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20131016	Adam Bean		Added check for existance header
********************************************************************************************************/

CREATE VIEW [dbo].[EncryptedDBs_vw]

AS

SELECT 
	DB_NAME(ek.[database_id]) AS [DBName],
	CASE
		WHEN ek.[encryption_state]	 = 0 THEN 'No database encryption key present, no encryption'
		WHEN ek.[encryption_state] = 1 THEN 'Unencrypted'
		WHEN ek.[encryption_state] = 2 THEN 'Encryption in progress'
		WHEN ek.[encryption_state] = 3 THEN 'Encrypted'
		WHEN ek.[encryption_state] = 4 THEN 'Key change in progress'
		WHEN ek.[encryption_state] = 5 THEN 'Decryption in progress'
		WHEN ek.[encryption_state] = 6 THEN 'Protection change in progress'
		ELSE 'Unknown' 
	END AS [EncryptionState],
	CASE 
		WHEN ek.[encryption_state] IN (0,1,3) THEN NULL
		ELSE ek.[percent_complete]
	END AS [PercentComplete],
	ek.[key_algorithm] AS [DEKAlgorithm],
	ek.[key_length] AS [DEKLength],
	ek.[create_date] AS [DEKCreateDate],
	c.[name] AS [CertName],
	p.[name] AS [CertOwner],
	c.[start_date] AS [CertStartDate],
	c.[subject] AS [CertSubject],
	c.[pvt_key_encryption_type_desc] AS [CertPKEncryptionType],
	c.[pvt_key_last_backup_date] AS [CertPKBackupDate]
FROM [master].[sys].[dm_database_encryption_keys] ek
JOIN [master].[sys].[certificates] c
	ON ek.encryptor_thumbprint = c.[thumbprint]
JOIN [master].[sys].[server_principals] p
	ON c.[principal_id] = p.[principal_id]
GO