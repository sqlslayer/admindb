IF OBJECT_ID('dbo.ForeignKeys','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[ForeignKeys]
GO

/******************************************************************************
* Name
	[dbo].[ForeignKeys]

* Author
	Matt Stanford
	
* Date
	2011.08.26
	
* Synopsis
	Displays and optionally creates foreign keys based only on column names and column types.
	
* Description
	This procedure can be used to identify "missing" foreign keys in a database.

* Examples
	[dbo].[ForeignKeys]

* Dependencies
	dbo.sysdatabases_vw
	dbo.Split_fn

* Parameters
	@DBName					- Database names (CSV supported), NULL for all
	@DBNameExclude			- Database names to exclude (CSV supported), NULL for all
	@MissingOnly			- Show only missing keys (default will show all)
	
* Notes
	Does not support compound keys at this time

*******************************************************************************
* License
*******************************************************************************
	Copyright ? SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20110923	Matt Stanford	Added @MissingOnly parameter
	20170111	Srujana Gorge	Removed ORDER BY ordinal column number
******************************************************************************/
CREATE PROCEDURE [dbo].[ForeignKeys]
(
	@DBName					NVARCHAR(4000)	= NULL
	,@DBNameExclude			NVARCHAR(4000)	= NULL
	,@MissingOnly			BIT = 0
)

AS
SET NOCOUNT ON

DECLARE
	@DBID				INT
	
IF OBJECT_ID('tempdb.dbo.#sysindexes') IS NOT NULL
   DROP TABLE #sysindexes
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#sysschemas') IS NOT NULL
   DROP TABLE #sysschemas
IF OBJECT_ID('tempdb.dbo.#syscolumns') IS NOT NULL
   DROP TABLE #syscolumns
IF OBJECT_ID('tempdb.dbo.#sysindex_columns') IS NOT NULL
   DROP TABLE #sysindex_columns
IF OBJECT_ID('tempdb.dbo.#sysforeign_keys') IS NOT NULL
   DROP TABLE #sysforeign_keys
IF OBJECT_ID('tempdb.dbo.#sysforeign_key_columns') IS NOT NULL
   DROP TABLE #sysforeign_key_columns
IF OBJECT_ID('tempdb.dbo.#AllTablesAndColumns') IS NOT NULL
   DROP TABLE #AllTablesAndColumns

SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysindexes FROM [sys].[indexes]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysobjects FROM [sys].[objects]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysschemas FROM [sys].[schemas]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #syscolumns FROM [sys].[columns]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysindex_columns  FROM [sys].[index_columns]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysforeign_keys  FROM [sys].[foreign_keys]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysforeign_key_columns  FROM [sys].[foreign_key_columns]

-- Populate the tables
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
	,[database_id]
FROM [dbo].[sysdatabases_vw] s
LEFT OUTER JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT OUTER JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName, @DBID
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 
			
		-- sys.indexes
		INSERT INTO #sysindexes
		SELECT *, ' + @DBID + ' FROM [sys].[indexes]

		-- sys.objects
		INSERT INTO #sysobjects
		SELECT *, ' + @DBID + ' FROM [sys].[objects]

		-- sys.schemas 
		INSERT INTO #sysschemas
		SELECT *, ' + @DBID + ' FROM [sys].[schemas]

		-- sys.columns 
		INSERT INTO #syscolumns
		SELECT *, ' + @DBID + ' FROM [sys].[columns]

		-- sys.index_columns 
		INSERT INTO #sysindex_columns 
		SELECT *, ' + @DBID + ' FROM [sys].[index_columns]
		
		-- sys.foreign_keys 
		INSERT INTO #sysforeign_keys
		SELECT *, ' + @DBID + ' FROM [sys].[foreign_keys]
		
		-- sys.foreign_key_columns 
		INSERT INTO #sysforeign_key_columns
		SELECT *, ' + @DBID + ' FROM [sys].[foreign_key_columns]

	')
	
FETCH NEXT FROM #dbs INTO @DBName, @DBID
END
CLOSE #dbs
DEALLOCATE #dbs

SELECT
	o.[database_id]			AS DatabaseID
	,s.[schema_id]			AS SchemaID
	,s.[name]				AS SchemaName
	,o.[object_id]			AS ObjectID
	,o.[name]				AS ObjectName
	,c.[column_id]			AS ColumnID
	,c.[name]				AS ColumnName
	,c.[system_type_id]		AS ColumnType
INTO #AllTablesAndColumns
FROM #sysobjects o
INNER JOIN #sysschemas s
	ON o.[schema_id] = s.[schema_id]
	AND o.[database_id] = s.[database_id]
INNER JOIN #syscolumns c
	ON o.[object_id] = c.[object_id]
	AND o.[database_id] = c.[database_id]
WHERE o.[type] = 'U'

;WITH PrimaryKeyColumns AS (
	SELECT
		t.[DatabaseID]
		,t.[SchemaName]
		,t.[ObjectName]
		,i.[name]				AS PKeyName
		,t.[ColumnName]
		,t.[ColumnType]
	FROM #AllTablesAndColumns t
	INNER JOIN #sysindexes i
		ON t.[ObjectID] = i.[object_id]
		AND t.[DatabaseID] = i.[database_id]
	INNER JOIN #sysindex_columns ic
		ON t.[ObjectID] = ic.[object_id]
		AND i.[index_id] = ic.[index_id]
		AND t.[ColumnID] = ic.[column_id]
		AND t.[DatabaseID] = ic.[database_id]
	WHERE i.[is_primary_key] = 1
), PossibleForeignKeys AS (
	SELECT 
		t.[DatabaseID]
		,t.[SchemaName]			AS ParentSchemaName
		,t.[ObjectName]			AS ParentObjectName
		,t.[ColumnName]			AS ParentColumnName
		,pk.[SchemaName]		AS ReferencedSchemaName
		,pk.[ObjectName]		AS ReferencedObjectName
		,pk.[PKeyName]
		,pk.[ColumnName]		AS ReferencedColumnName
		,'FK__' + t.SchemaName + '.' + t.ObjectName + '.' + t.ColumnName + '__' + pk.SchemaName + '.' + pk.ObjectName + '.' + pk.ColumnName AS FKeyName
	FROM #AllTablesAndColumns t
	INNER JOIN PrimaryKeyColumns pk
		ON t.[ColumnName] = pk.[ColumnName]
		AND NOT (
			t.[ObjectName] = pk.[ObjectName]
			AND t.[SchemaName] = pk.[SchemaName]
			AND t.[DatabaseID] = pk.[DatabaseID]
		)
		AND t.[DatabaseID] = pk.[DatabaseID]
		AND t.[ColumnType] = pk.[ColumnType]
), ExistingForeignKeys AS (
	SELECT
		fk.[database_id]		AS DatabaseID
		,p_s.[name]				AS ParentSchemaName
		,p_o.[name]				AS ParentObjectName
		,p_c.[name]				AS ParentColumnName
		,r_s.[name]				AS ReferencedSchemaName
		,r_o.[name]				AS ReferencedObjectName
		,r_c.[name]				AS ReferencedColumnName
		,fk.[name]				AS FKeyName
		,i.[name]				AS PKeyName
	FROM #sysforeign_keys fk
	INNER JOIN #sysobjects p_o
		ON fk.[parent_object_id] = p_o.[object_id]
		AND fk.[database_id] = p_o.[database_id]
		AND p_o.[type] = 'U'
	INNER JOIN #sysschemas p_s
		ON p_o.[schema_id] = p_s.[schema_id]
		AND p_o.[database_id] = p_s.[database_id]
	INNER JOIN #sysobjects r_o
		ON fk.[referenced_object_id] = r_o.[object_id]
		AND fk.[database_id] = r_o.[database_id]
	INNER JOIN #sysschemas r_s
		ON r_o.[schema_id] = r_s.[schema_id]
		AND r_o.[database_id] = r_s.[database_id]
	INNER JOIN #sysforeign_key_columns fkc
		ON fk.[object_id] = fkc.[constraint_object_id]
		AND p_o.[object_id] = fkc.[parent_object_id]
		AND r_o.[object_id] = fkc.[referenced_object_id]
		AND r_o.[database_id] = fkc.[database_id]
	INNER JOIN #syscolumns p_c
		ON fkc.[parent_column_id] = p_c.[column_id]
		AND p_c.[object_id] = p_o.[object_id]
		AND fkc.[database_id] = p_c.[database_id]
	INNER JOIN #syscolumns r_c
		ON fkc.[referenced_column_id] = r_c.[column_id]
		AND r_c.[object_id] = r_o.[object_id]
		AND r_c.[database_id] = r_o.[database_id]
	LEFT OUTER JOIN (
		SELECT
			*
		FROM #sysindexes 
		WHERE [is_primary_key] = 1
	) i
		ON i.[object_id] = fk.[referenced_object_id]
		AND i.[database_id] = fk.[database_id]
)
SELECT						
	db.[name]				AS [database_name]
	,COALESCE(pfk.ParentSchemaName,efk.ParentSchemaName) AS ParentSchemaName
	,COALESCE(pfk.ParentObjectName,efk.ParentObjectName) AS ParentObjectName
	,COALESCE(pfk.ParentColumnName,efk.ParentColumnName) AS ParentColumnName
	,COALESCE(pfk.PKeyName,efk.PKeyName) AS PKeyName
	,COALESCE(pfk.ReferencedSchemaName,efk.ReferencedSchemaName) AS ReferencedSchemaName
	,COALESCE(pfk.ReferencedObjectName,efk.ReferencedObjectName) AS ReferencedObjectName
	,COALESCE(pfk.ReferencedColumnName,efk.ReferencedColumnName) AS ReferencedColumnName
	,pfk.FKeyName			AS NewFKeyName
	,efk.FKeyName			AS ExistingFKeyName
	,'ALTER TABLE [' + pfk.ParentSchemaName + '].[' + pfk.ParentObjectName + '] ADD CONSTRAINT [' + pfk.FKeyName + '] FOREIGN KEY ([' + pfk.ParentColumnName 
	+ ']) REFERENCES [' + pfk.ReferencedSchemaName + '].[' + pfk.ReferencedObjectName + '] ([' + pfk.ReferencedColumnName + '])' AS CreateStatement
FROM PossibleForeignKeys pfk
FULL OUTER JOIN ExistingForeignKeys efk
	ON pfk.DatabaseID = efk.DatabaseID
	AND pfk.ParentSchemaName = efk.ParentSchemaName
	AND pfk.ParentObjectName = efk.ParentObjectName
	AND pfk.ParentColumnName = efk.ParentColumnName
	AND pfk.ReferencedSchemaName = efk.ReferencedSchemaName
	AND pfk.ReferencedObjectName = efk.ReferencedObjectName
	AND pfk.ReferencedColumnName = efk.ReferencedColumnName
INNER JOIN [dbo].[sysdatabases_vw] db
	ON db.[database_id] = COALESCE(pfk.DatabaseID, efk.DatabaseID)
WHERE (
	@MissingOnly = 0
	OR
	(@MissingOnly = 1 AND efk.FKeyName IS NULL)
)
ORDER BY [database_name],ParentSchemaName,ParentObjectName,ParentColumnName


