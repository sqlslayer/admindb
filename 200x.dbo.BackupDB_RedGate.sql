IF OBJECT_ID('[dbo].[BackUpDB_RedGate]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[BackUpDB_RedGate]
GO

/*******************************************************************************************************
**	Name:			dbo.BackUpDB_RedGate
**	Desc:			Runs full SQL Server database backups with RedGate support
**	Auth:			Adam Bean (SQLSlayer.com)
**  Parameters:		Inherited from BackupDB
**	Notes:			Should not be run as a stand alone procedure. Use BackupDB.
**	Date:			2008.11.29
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/

CREATE PROCEDURE [dbo].[BackUpDB_RedGate] 
(
	@DBName					sysname
	,@BackupType			CHAR(1)
	,@BackupLocation		VARCHAR(512)
	,@BackupName			VARCHAR(256)
	,@File					VARCHAR(4000)	
	,@FileGroup				VARCHAR(4000)
	,@Init					BIT				
	,@RetainDays			TINYINT
	,@Stats					TINYINT
	,@Description			VARCHAR(512)		
	,@Verify				BIT
	,@Password				VARCHAR(4000)
	,@Compression			TINYINT
	,@Threads				TINYINT	
	,@Debug					BIT				
)

AS

SET NOCOUNT ON

DECLARE
	@Comma					VARCHAR(2)
	,@Tab					VARCHAR(8)
	,@NewLine				VARCHAR(8)
	,@SQL					VARCHAR(4000)

-- Populate some variables
SET @Comma = CHAR(44)
SET @Tab = CHAR(9)	
SET @NewLine = CHAR(10)

-- Build our backup command
SET @SQL = @NewLine
SET @SQL = @SQL + 'EXEC [master].[dbo].[sqlbackup]' + @NewLine 
SET @SQL = @SQL + @Tab + '''-SQL "BACKUP'
IF @BackupType = 'L' 
	SET @SQL = @SQL + ' LOG'
ELSE 
	SET @SQL = @SQL + ' DATABASE'
SET @SQL = @SQL + ' [' + @DBName + ']'
IF @BackupType = 'F'
	SET @SQL = @SQL + ' FILE = ''' + @File + ''''
ELSE IF @BackupType = 'G'
	SET @SQL = @SQL + ' FILEGROUP = ''' + @FileGroup + '''  '
SET @SQL = @SQL + ' TO DISK = ''''' + @BackupName + '''''' + @NewLine
SET @SQL = @SQL + @Tab + 'WITH COMPRESSION = ' + CAST(ISNULL(@Compression,0) AS VARCHAR) + '' + @NewLine
IF @BackupType = 'I'
	SET @SQL = @SQL + @Tab + @Comma + 'DIFFERENTIAL' + @NewLine
IF @Init IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + 'INIT' + @NewLine
IF @Threads IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + 'THREADS = ' + CAST(@Threads AS VARCHAR) + '' + @NewLine
IF @Password IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + 'PASSWORD = ''<ENCRYPTEDPASSWORD>''' + @Password + '''</ENCRYPTEDPASSWORD>''' + @NewLine
IF @RetainDays IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + 'ERASEFILES = ' + CAST(@RetainDays AS VARCHAR) + '' + @NewLine
IF @Verify = 1
	SET @SQL = @SQL + @Tab + @Comma + 'VERIFY'
SET @SQL = @SQL + @Tab + '"'''

IF @Debug = 1
	PRINT (@SQL)
ELSE
	EXEC (@SQL)
	
SET NOCOUNT OFF


