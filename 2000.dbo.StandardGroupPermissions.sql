IF OBJECT_ID('[dbo].[StandardGroupPermissions]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[StandardGroupPermissions]
GO

/******************************************************************************
* Name
	[dbo].[StandardGroupPermissions]

* Author
	Adam Bean
	
* Date
	2009.11.01
	
* Synopsis
	Creates standardized NT group permissions
	
* Description
	Based on domain, instance, server and optional parameters, this procedure will dynamically create and apply applicable permissions as define

* Examples
	EXEC [admin].[dbo].[StandardGroupPermissions]
		@Domain = 'UH'
		
	EXEC [admin].[dbo].[StandardGroupPermissions]
		@Domain = 'ETC'
		,@Reader = 'Readers'
		,@PwrUser = 'PwrUsers'
		,@Captain = 'Captains'
		,@Admin = 'Admins'
		,@Prefix = 'SQL'
		,@Sufix = 'sg'
		,@debug = 1

* Dependencies
	dbo.Split_fn
	dbo.sysdatabases_vw

* Parameters
	@DBName			= Database name(s) to apply permissions to (CSV supported, NULL for all)
	@DBNameExclude	= Database name(s) to exclude permissions to (CSV supported)
	@Domain			= Domain name where NT groups resides
	@Reader			= Login name for reader group, defaulted to SQLReaders
	@PwrUser		= Login name for reader group, defaulted to SQLPwrUsers		
	@Captain		= Login name for reader group, defaulted to SQLCaptains
	@Admin			= Login name for reader group, defaulted to SQLAdmins
	@ReaderRole		= Role name for reader group, defaulted to db_nt_reader
	@PwrUserRole	= Role name for reader group, defaulted to db_nt_pwruser	
	@IncludeSystem	= Apply group permissions to system databses, off by default
	@Prefix			= Add an additional prefix name before ths standard name
	@Sufix			= Add an additional sufix name before ths standard name
	@NameOverride	= Override login name to use this string instead of Server_InstanceName convention
	@Debug			= Show, but don't execute work to be done
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20091124	Adam Bean		Added DTS roles, fixed how @ServerName is built
	20091125	Adam Bean		Added select permissions on DTS tables
	20091201	Adam Bean		Added Captain role
	20100407	Adam Bean		Fixed issue where reader group wasn't being creating
	20100628	Adam Bean		Fixed issue with DTS roles when replication enabled
	20120130	Adam Bean		Added db_datareader to Reader and PwrUser groups, changed @IncludeSystem to 1 by default
	20170425	Adam Bean		Added @Prefix to allow for a prefixed name to be added to the naming standard & new standard header
	20170426	Adam Bean		Added @Suffix to allow for a suffixed name to be added to the naming standard
	20170512	Adam Bean		Removed execute permissions from Readers, fixed adding role to user based on parameter
	20170519	Adam Bean		Removed read only databases
********************************************************************************************************/

CREATE PROCEDURE [dbo].[StandardGroupPermissions]
(
	@DBName				NVARCHAR(512)	= NULL
	,@DBNameExclude		NVARCHAR(512)	= NULL
	,@Domain			VARCHAR(16)
	,@Reader			VARCHAR(32)		= 'SQLReaders'
	,@PwrUser			VARCHAR(32)		= 'SQLPwrUsers'
	,@Captain			VARCHAR(32)		= 'SQLCaptains'
	,@Admin				VARCHAR(32)		= 'SQLAdmins'
	,@ReaderRole		VARCHAR(16)		= 'db_nt_reader'
	,@PwrUserRole		VARCHAR(16)		= 'db_nt_pwruser'
	,@CaptainRole		VARCHAR(16)		= 'db_nt_captain'
	,@IncludeSystem		BIT				= 1
	,@Prefix			VARCHAR(256)	= NULL
	,@Sufix				VARCHAR(16)		= NULL
	,@Debug				BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE
	@ServerName			NVARCHAR(128)
	,@InstanceName		NVARCHAR(128)
	,@DatabaseName		NVARCHAR(128)
	,@SQL				VARCHAR(8000)
	,@ReaderLogin		VARCHAR(272)
	,@PwrUserLogin		VARCHAR(272)
	,@CaptainLogin		VARCHAR(272)
	,@AdminLogin		VARCHAR(272)
	,@LF				CHAR(1)

-- Populate some working variables
SET @ServerName = (SELECT CAST(SERVERPROPERTY('MachineName') AS VARCHAR(128)))
SET @InstanceName = (SELECT CAST(SERVERPROPERTY('InstanceName') AS VARCHAR(128)))
SET @LF = CHAR(10)

-- Make sure @Domain has a backslash
IF RIGHT(@Domain,1) != '\'
	SET @Domain = @Domain + '\'
	
-- Build login names based on domain, server, instance names and optionally if @Prefix was passed
IF @InstanceName IS NOT NULL
	SET @ServerName = UPPER(@Domain) + ISNULL(@Prefix + '_' + UPPER(@ServerName), UPPER(@ServerName)) + '_' + UPPER(@InstanceName) + '_'
ELSE
	SET @ServerName = UPPER(@Domain) + ISNULL(@Prefix + '_' + UPPER(@ServerName), UPPER(@ServerName)) + '_DEFAULT_' 

-- Build login name based on servername + instancename + login type and optionally if @Sufix was passed
SET @ReaderLogin	= @ServerName + ISNULL(@Reader + '_' + @Sufix, @Reader)
SET @PwrUserLogin	= @ServerName + ISNULL(@PwrUser + '_' + @Sufix, @PwrUser)
SET @CaptainLogin	= @ServerName + ISNULL(@Captain + '_' + @Sufix, @Captain)
SET @AdminLogin		= @ServerName + ISNULL(@Admin + '_' + @Sufix, @Admin)

-- Get all of the catalog views
IF OBJECT_ID('tempdb.dbo.#sysdatabase_principals') IS NOT NULL
   DROP TABLE #sysdatabase_principals
SELECT TOP 0 * INTO #sysdatabase_principals FROM [dbo].[sysusers]

IF @Debug = 1
	SET @SQL = '-- *** Debug Enabled ***'
ELSE SET @SQL = ''

-- Create logins
SET @SQL = @SQL + @LF + '-- Create logins'
IF EXISTS(SELECT [name] FROM [master].[dbo].[syslogins] WHERE [name] = @ReaderLogin)
	SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_revokelogin] @loginame = [' + @ReaderLogin + ']' 
SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_grantlogin] @loginame = [' + @ReaderLogin + ']' 
SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_defaultdb] @loginame = [' + @ReaderLogin + '], @defdb = ''master'''
IF EXISTS(SELECT [name] FROM [master].[dbo].[syslogins] WHERE [name] = @PwrUserLogin)
	SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_revokelogin] @loginame = [' + @PwrUserLogin + ']' 
SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_grantlogin] @loginame = [' + @PwrUserLogin + ']' 
SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_defaultdb] @loginame = [' + @PwrUserLogin + '], @defdb = ''master'''
IF EXISTS(SELECT [name] FROM [master].[dbo].[syslogins] WHERE [name] = @CaptainLogin)
	SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_revokelogin] @loginame = [' + @CaptainLogin + ']' 
SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_grantlogin] @loginame = [' + @CaptainLogin + ']' 
SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_defaultdb] @loginame = [' + @CaptainLogin + '], @defdb = ''master'''
IF EXISTS(SELECT [name] FROM [master].[dbo].[syslogins] WHERE [name] = @AdminLogin)
	SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_revokelogin] @loginame = [' + @AdminLogin + ']' 
SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_grantlogin] @loginame = [' + @AdminLogin + ']' 
SET @SQL = @SQL + @LF + 'EXEC [dbo].[sp_defaultdb] @loginame = [' + @AdminLogin + '], @defdb = ''master'''

IF @Debug = 1
	PRINT (@SQL)
ELSE
	EXEC (@SQL)

-- Server roles
SET @SQL = @LF + '-- Server roles' +
'
EXEC [dbo].[sp_addsrvrolemember] ''' + @PwrUserLogin + ''', ''dbcreator''
EXEC [dbo].[sp_addsrvrolemember] ''' + @PwrUserLogin + ''', ''bulkadmin''
EXEC [dbo].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''dbcreator''
EXEC [dbo].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''bulkadmin''
EXEC [dbo].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''diskadmin''
EXEC [dbo].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''processadmin''
EXEC [dbo].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''setupadmin''
EXEC [dbo].[sp_addsrvrolemember] ''' + @AdminLogin + ''', ''sysadmin''
'

IF @Debug = 1
	PRINT (@SQL)
ELSE
	EXEC (@SQL)

-- Create users, roles & grant permissions
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item]
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item]
WHERE s.[state_desc] = 'ONLINE'
AND s.[is_read_only] = 0
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND s.[name] != 'tempdb'
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database(s), or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY 1

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DatabaseName
WHILE @@FETCH_STATUS = 0
BEGIN

	SET @SQL = @LF + '-- Database permissions for: ' + @DatabaseName + ''

	-- Gather database users
	TRUNCATE TABLE #sysdatabase_principals
	INSERT INTO #sysdatabase_principals
	EXEC('SELECT * FROM [' + @DatabaseName + '].[dbo].[sysusers]')

	-- Create database users
	SET @SQL = @SQL + @LF + '-- Create users'
	
	-- Reader
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @Reader)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_revokedbaccess] [' + @Reader + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_grantdbaccess] @loginame = ''' + @ReaderLogin + ''', @name_in_db = ''' + @Reader + ''''

	-- PowerUser
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @PwrUser)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_revokedbaccess] [' + @PwrUser + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_grantdbaccess] @loginame = ''' + @PwrUserLogin + ''', @name_in_db = ''' + @PwrUser + ''''
	
	-- Captain
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @Captain)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_revokedbaccess] [' + @Captain + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_grantdbaccess] @loginame = ''' + @CaptainLogin + ''', @name_in_db = ''' + @Captain + ''''


	-- Create database roles
	SET @SQL = @SQL + @LF + @LF + '-- Create roles'
	
	-- Reader
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @ReaderRole AND [issqlrole] = 1)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_droprole] ''' + @ReaderRole + ''''
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_addrole] ''' + @ReaderRole + ''''

	-- PowerUser
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @PwrUserRole AND [issqlrole] = 1)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_droprole] ''' + @PwrUserRole + ''''
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_addrole] ''' + @PwrUserRole + ''''

	-- Captain
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @CaptainRole AND [issqlrole] = 1)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_droprole] ''' + @CaptainRole + ''''
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC [dbo].[sp_addrole] ''' + @CaptainRole + ''''
	
	-- Only grant permissions to system databses if specified (@IncludeSystem)
	IF DB_ID(@DatabaseName) > 4 OR @IncludeSystem = 1
	BEGIN	
		-- Database and role permissions
		-- Reader
		SET @SQL = @SQL + @LF + @LF + '-- Roles & permissions'
		SET @SQL = @SQL +
		'
		-- Reader
		USE [' + @DatabaseName + '] 
		EXEC [dbo].[sp_addrolemember] ''db_datareader'', ''' + @Reader + '''
		EXEC [dbo].[sp_addrolemember] ''db_nt_reader'', ''' + @Reader + '''
		'
			
		-- PwrUser
		SET @SQL = @SQL +
		'
		-- PwrUser
		USE [' + @DatabaseName + ']
		EXEC [dbo].[sp_addrolemember] ''db_datareader'', ''' + @PwrUser + '''
		EXEC [dbo].[sp_addrolemember] ''db_datawriter'', ''' + @PwrUser + '''
		EXEC [dbo].[sp_addrolemember] ''db_ddladmin'', ''' + @PwrUser + '''
		EXEC [dbo].[sp_addrolemember] ''db_backupoperator'', ''' + @PwrUser + '''
		EXEC [dbo].[sp_addrolemember] ''db_nt_pwruser'', ''' + @PwrUser + '''
		'

		-- Captain
		SET @SQL = @SQL +
		'
		-- Captain
		USE [' + @DatabaseName + '] 
		EXEC sp_addrolemember ''db_owner'', ''' + @Captain + '''
		'

		-- Execute for power user
		SET @SQL = @SQL + @LF + 
		('
		DECLARE 
			@ObjName		NVARCHAR(128)
			,@SchemaName	NVARCHAR(128)
			,@SQL			VARCHAR(512)

		USE [' + @DatabaseName + ']
		DECLARE #objs CURSOR STATIC LOCAL FOR 
		SELECT 
			[name]
			,USER_NAME([uid]) AS [schemaname] 
		FROM [sysobjects]
		WHERE [xtype] IN (''P'', ''X'', ''FN'') 
		AND LEFT([name], 2) <> ''dt''
		AND USER_NAME([uid]) != ''system_function_schema''

		OPEN #objs 
		FETCH NEXT from #objs into @ObjName, @SchemaName
		WHILE @@FETCH_STATUS = 0 
		BEGIN 

			SET @SQL = ''GRANT EXECUTE ON ['' + @SchemaName + ''].['' + @ObjName + ''] TO [' + @PwrUserRole + ']''
				EXEC (@SQL) 

		FETCH NEXT FROM #objs INTO @ObjName, @SchemaName
		END 
		CLOSE #objs 
		DEALLOCATE #objs
		')
	END
	
	IF @Debug = 1
		PRINT (@SQL)
	ELSE
		EXEC (@SQL)
		
FETCH NEXT FROM #dbs INTO @DatabaseName
END
CLOSE #dbs
DEALLOCATE #dbs


-- System database permissions
-- Only apply if no database was specified as the users have to be in the system databases
IF @DBName IS NULL
BEGIN
	SET @SQL = @LF + @LF + '-- System permissions'
	-- Database roles
	SET @SQL = @SQL + @LF + 
	'
	-- Reader
	USE [master]
	EXEC [sys].[sp_addrolemember] ''db_datareader'', ''' + @Reader + '''
	EXEC [sys].[sp_addrolemember] ''db_datareader'', ''' + @PwrUser + '''
	
	USE [msdb]
	EXEC [sys].[sp_addrolemember] ''db_datareader'', ''' + @Reader + '''
	EXEC [sys].[sp_addrolemember] ''db_datareader'', ''' + @PwrUser + '''
	'

	-- DTS
	SET @SQL = @SQL + @LF + 
	'
	-- DTS 
	USE [msdb]
	IF EXISTS(SELECT [name] FROM [dbo].[sysusers] WHERE [name] = ''DTS_Reader'' AND [issqlrole] = 1)
		 EXEC [dbo].[sp_droprole] [DTS_Reader]
	EXEC [dbo].[sp_addrole] ''DTS_Reader''
	IF EXISTS(SELECT [name] FROM [dbo].[sysusers] WHERE [name] = ''DTS_Operator'' AND [issqlrole] = 1)
		 EXEC [dbo].[sp_droprole] [DTS_Operator]
	EXEC [dbo].[sp_addrole] ''DTS_Operator''
	
	GRANT EXECUTE ON [dbo].[sp_enum_dtspackagelog] TO [DTS_Reader], [DTS_Operator]
	GRANT EXECUTE ON [dbo].[sp_enum_dtspackages] TO [DTS_Reader], [DTS_Operator]
	GRANT EXECUTE ON [dbo].[sp_enum_dtssteplog] TO [DTS_Reader], [DTS_Operator]
	GRANT EXECUTE ON [dbo].[sp_enum_dtstasklog] TO [DTS_Reader], [DTS_Operator]
	GRANT EXECUTE ON [dbo].[sp_get_dtspackage] TO [DTS_Reader], [DTS_Operator]
	GRANT EXECUTE ON [dbo].[sp_get_dtsversion] TO [DTS_Reader], [DTS_Operator]
	GRANT EXECUTE ON [dbo].[sp_drop_dtspackage] TO [DTS_Operator]
	GRANT EXECUTE ON [dbo].[sp_add_dtspackage] TO [DTS_Operator]

	GRANT SELECT ON [dbo].[sysdtspackages] TO [DTS_Reader], [DTS_Operator]
	GRANT SELECT ON [dbo].[sysdtspackagelog] TO [DTS_Reader], [DTS_Operator]
	GRANT SELECT ON [dbo].[sysdtssteplog] TO [DTS_Reader], [DTS_Operator]
	
	EXEC [dbo].[sp_addrolemember] ''DTS_Reader'', ''' + @Reader + '''
	EXEC [dbo].[sp_addrolemember] ''DTS_Operator'', ''' + @PwrUser + '''
	EXEC [dbo].[sp_addrolemember] ''DTS_Operator'', ''' + @Captain + '''
	'
	
	-- SQLMail, Extended/stored procedures & trace
	SET @SQL = @SQL + @LF + 
	'
	USE [master]
	GRANT EXECUTE ON [sp_execute] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [sp_executesql] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [xp_readerrorlog] TO [' + @ReaderRole + '], [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [xp_sendmail] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	'

	-- Misc.
	-- LiteSpeed	
	IF OBJECT_ID('[master].[dbo].[xp_backup_database]','X') IS NOT NULL 
	SET @SQL = @SQL + @LF + 
	'
	USE [master]
	GRANT EXECUTE ON [dbo].[xp_backup_database] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_restore_database] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_backup_log] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_restore_log] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	'

	-- SQLBackup	
	IF OBJECT_ID('[master].[dbo].[sqlbackup]','X') IS NOT NULL 
	SET @SQL = @SQL + @LF + 
	'
	USE [master]
	GRANT EXECUTE ON [dbo].[sqlbackup] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbdata] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbdir] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbmemory] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbstatus] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbtest] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbtestcancel] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbteststatus] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbutility] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	'
	
	IF @Debug = 1
		PRINT (@SQL)
	ELSE
		EXEC (@SQL)

	-- Replication
	IF (SELECT MAX(CAST([is_distributor] AS TINYINT)) FROM [dbo].[sysdatabases_vw]) > 0
	BEGIN
		DECLARE #dbs CURSOR LOCAL STATIC FOR
		SELECT 
			[name]
		FROM [sysdatabases_vw]
		WHERE [is_distributor] = 1
		ORDER BY 1

		OPEN #dbs
		FETCH NEXT FROM #dbs INTO @DatabaseName
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		SET @SQL = 
		'
		USE [' + @DatabaseName + '] 
		EXEC [dbo].[sp_addrolemember] ''replmonitor'', ''' + @Reader + '''
		EXEC [dbo].[sp_addrolemember] ''replmonitor'', ''' + @PwrUser + '''
		EXEC [sys].[sp_addrolemember] ''replmonitor'', ''' + @Captain + '''
		'	
		
		IF @Debug = 1
			PRINT (@SQL)
		ELSE
			EXEC (@SQL)
				
		FETCH NEXT FROM #dbs INTO @DatabaseName
		END
		CLOSE #dbs
		DEALLOCATE #dbs
	END

END
SET NOCOUNT OFF