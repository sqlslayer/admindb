IF OBJECT_ID('[dbo].[CheckDB_vw]','V') IS NOT NULL 
	DROP VIEW [dbo].[CheckDB_vw]
GO

/******************************************************************************
* Name
	[dbo].[CheckDB_vw]

* Author
	Adam Bean
	
* Date
	2011.09.13
	
* Synopsis
	View to bring together the maintenance data for DBCC CHECKDB.
	
* Description
	View which joins data from all normalized tables which are logged to as part of the CheckDB procedure

* Examples
	SELECT * FROM [dbo].[CheckDB_vw]

* Dependencies
	The following tables: 
	[dbo].[Maintenance_CheckDB]
	[dbo].[Maintenance_CheckDB_Master]
	[dbo].[Maintenance_DatabaseIDs]
	[dbo].[Maintenance_CommandIDs]
	[dbo].[Maintenance_RunIDs]

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20140222	Adam Bean		Added DateStart, DateEnd, Duration, BatchDateStart, BatchDateEnd, BatchDuration, RunID2
******************************************************************************/

CREATE VIEW [dbo].[CheckDB_vw]

AS

SELECT
	d.[DBName]
	,c.[Command]
	,b.[IsOverride]
	,b.[DateStart]
	,b.[DateEnd]
	,DATEDIFF(SECOND, b.[DateStart], b.[DateEnd])	AS [Duration]
	,r.[DateStart]									AS [BatchDateStart]
	,r.[DateEnd]									AS [BatchDateEnd]
	,DATEDIFF(SECOND, r.[DateStart], r.[DateEnd])	AS [BatchDuration]
	,m.*
	,r.[RunID]										AS [RunID2]
FROM [dbo].[Maintenance_CheckDB] b
JOIN [dbo].[Maintenance_DatabaseIDs] d
	ON d.[DatabaseID] = b.[DatabaseID]
JOIN [dbo].[Maintenance_CommandIDs] c
	ON c.[CommandID] = b.[CommandID]
JOIN [dbo].[Maintenance_RunIDs] r
	ON r.[RunID] = b.[RunID]
LEFT JOIN [dbo].[Maintenance_CheckDB_Master] m
	ON m.[NewDbId] = b.[DatabaseID]
	AND m.[RunID] = b.[RunID]