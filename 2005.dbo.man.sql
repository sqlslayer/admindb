IF OBJECT_ID('dbo.man','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[man]
GO

/******************************************************************************
* Name
	[dbo].[man]

* Author
	Matt Stanford
	
* Date
	2010.11.29
	
* Synopsis
	Displays information about SQL Slayer objects and concepts.
	
* Description
	The man procedure displays information about SQL Slayer concepts and commands, including procedures, functions
	and views. To get a list of all admin objects, type "man '%'".

	If you type "man" followed by the exact name of an object, man displays the topic contents. If you enter a word or word pattern 
	that appears in several object names, man displays a list of the matching objects.

* Examples
	EXEC [dbo].[man]

* Dependencies
	dbo.GetManSection_fn

* Parameters
	@Object				- The object(s) that you want to display help for.  Can use wildcards.
	@Verbosity			- How verbose you want this to be.  1-3 are valid.  1 is default and quietest, 3 is most verbose.
	
* Notes
	NONE

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20110705	Adam Bean		Fixed @Verbosity for case sensitive collations
******************************************************************************/
CREATE PROCEDURE [dbo].[man]
(
	@Object				NVARCHAR(256)
	,@Verbosity			TINYINT = 1
)

AS
SET NOCOUNT ON

-- Confirmed good variables
DECLARE
	@object_name		NVARCHAR(128)
	,@schema_name		NVARCHAR(128)
	,@tt				CHAR(2)
	,@nl				CHAR(2)
	,@header			NVARCHAR(MAX)
	,@I1				INT		-- junk scratch varialbe (INT)
	,@I2				INT		-- junk scratch variable (INT)


DECLARE 
	@linedef			char(5)
	,@K1				NVARCHAR(128)
	,@K2				NVARCHAR(128)
	,@str				NVARCHAR(4000)
	,@paramprint		NVARCHAR(4000)
	,@remove			VARCHAR(50)
	,@v_verbosity		TINYINT
	,@syntaxprint		NVARCHAR(4000)
	
-- Define some static values
SET @tt = CHAR(9) + CHAR(9)		-- Two Tabs
SET @nl = CHAR(13) + CHAR(10)	-- Newline (CR&LF)	

-- Lookup the schema and object name
SELECT 
	@object_name	= o.[name]
	,@schema_name	= s.[name]
	,@header		= sm.[definition]
FROM [sys].[objects] o
INNER JOIN [sys].[schemas] s
	ON s.[schema_id] = o.[schema_id]
INNER JOIN [sys].[sql_modules] sm
	ON o.[object_id] = sm.[object_id]
WHERE (s.[name] + '.' + o.[name] LIKE @Object)
OR	('[' + s.[name] + '].[' + o.[name] + ']' LIKE @Object)
OR	('[' + o.[name] + ']' LIKE @Object)
OR	(o.[name] LIKE @Object)

-- Pull the header out of the definition
SET @I1 = CHARINDEX('/*',@header) - 1
SET @I2 = CHARINDEX('*/',@header,@I1) + 1

SET @header = RIGHT(LEFT(@header,@I2),@I2 - @I1)

-- Create tables to hold some data
IF OBJECT_ID('tempdb.dbo.#sections','U') IS NOT NULL
	DROP TABLE #sections
IF OBJECT_ID('tempdb.dbo.#params','U') IS NOT NULL
	DROP TABLE #params
	
CREATE TABLE #sections
(
	[output_header]			VARCHAR(500)
	,[key_start]			VARCHAR(50)
	,[key_end]				VARCHAR(50)
	,[section]				NVARCHAR(4000)
	,[remove]				VARCHAR(50)
	,[verbosity]			TINYINT
	,[sequence]				TINYINT
)

-- Table to hold parameter definitions, with comments if defined
CREATE TABLE #params
(
	[name]					NVARCHAR(128)
	,[parameter_id]			INT
	,[type]					NVARCHAR(128)
	,[is_output]			BIT
	,[has_default_value]	BIT
	,[default_value]		NVARCHAR(4000)
	,[comment]				NVARCHAR(4000)
)
	
-- Load parameter definitions
INSERT INTO #params ([name],[parameter_id],[type],[is_output],[has_default_value],[default_value])
SELECT 
	p.[name]
	,p.[parameter_id]
	,t.[name]
	,p.[is_output]
	,p.[has_default_value]
	,CAST(p.[default_value] AS NVARCHAR)
FROM [sys].[parameters] p
INNER JOIN [sys].[types] t
	ON p.[system_type_id] = t.[system_type_id]
	AND p.[user_type_id] = t.[user_type_id]
WHERE p.[object_id] = OBJECT_ID('[' + @schema_name + '].[' + @object_name + ']')	

-- Build syntax print string
SET @syntaxprint = 'EXEC [' + @schema_name + '].[' + @object_name + ']'
DECLARE #c CURSOR LOCAL STATIC FOR
SELECT 
	[name]
	,[type]
	,[is_output]
	,[has_default_value]
FROM #params
ORDER BY [parameter_id] ASC

OPEN #c
FETCH NEXT FROM #c INTO @K1, @K2, @I1, @I2

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @str = @nl + @tt + @K1 + ' = <' + @K2 + CASE WHEN @I1 = 1 THEN ', OUTPUT' ELSE '' END + '>,'
	
	IF @I2 = 1 
		SET @str = '[' + @str + ']'

	SET @syntaxprint = @syntaxprint + ' ' + @str

	FETCH NEXT FROM #c INTO @K1, @K2, @I1, @I2
END
CLOSE #c
DEALLOCATE #c

SET @syntaxprint = LEFT(@syntaxprint,LEN(@syntaxprint) - 1)

-- Shred the header and get the man sections	
/*
	Verbosity: 1 = always display, 2 = detailed display, 3 = full display
	Sequence: (used for display) 0 is first, 255 is last.  Figure it out.
*/

INSERT INTO #sections ([key_start],[key_end],[remove],[verbosity],[output_header],[sequence])
SELECT @nl + '* Description'	,@nl + '* '	,CHAR(9)	,2,'DESCRIPTION'		,35 UNION ALL
SELECT @nl + '* Author'			,@nl + '* '	,CHAR(9)	,3,'AUTHOR'				,10 UNION ALL
SELECT @nl + '* Examples'		,@nl + '* '	,CHAR(9)	,1,'EXAMPLES'			,32 UNION ALL
SELECT '--U-N-D-E-F-I-N-E-D--'	,''			,''			,1,'SYNTAX'				,31 UNION ALL
SELECT @nl + '* Dependencies'	,@nl + '* '	,CHAR(9)	,2,'DEPENDENCIES'		,40 UNION ALL
SELECT @nl + '* Parameters'		,@nl + '* '	,CHAR(9)	,2,'PARAMETERS'			,50 UNION ALL
SELECT @nl + '* Date'			,@nl + '* '	,CHAR(9)	,2,'DATE'				,20 UNION ALL
SELECT @nl + '* Synopsis'		,@nl + '* '	,CHAR(9)	,1,'SYNOPSIS'			,30 UNION ALL
SELECT @nl + '* License'		,@nl + '* '	,''			,3,'LICENSE'			,90 UNION ALL
SELECT @nl + '* Change History'	,'*/'		,''			,3,'CHANGE HISTORY'		,100 UNION ALL
SELECT @nl + '* Notes'			,@nl + '**'	,''			,3,'NOTES'				,110

-- Get the man sections from the header
UPDATE k
	SET [section] = dbo.GetManSection_fn(@header,k.[key_start],k.[key_end],k.[remove])
FROM #sections k


UPDATE #sections 
	SET [section] = @syntaxprint
WHERE [output_header] = 'SYNTAX'

-- Build parameter listing
IF EXISTS(SELECT * FROM #sections WHERE [output_header] = 'PARAMETERS' AND [section] IS NOT NULL)
BEGIN
	-- We've got parameter definitions
	SELECT @str = [section] 
	FROM #sections 
	WHERE [output_header] = 'PARAMETERS'
	
	DECLARE #c CURSOR LOCAL STATIC FOR
	SELECT 
		[name]
	FROM #params
	OPEN #c
	FETCH NEXT FROM #c INTO @K1

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- @K1 is the parameter name
		SET @I1 = CHARINDEX(@K1 + '- ', @str) + LEN(@K1)
		SET @I2 = CHARINDEX(CHAR(10), @str, @I1)
		
		IF (@I1 > LEN(@K1) + 2)
		BEGIN
			-- @K2 will be the parameter comments
			SET @K2 = RIGHT(LEFT(@str,@I2),@I2 - @I1)
		
			UPDATE p
				SET [comment] = @K2
			FROM #params p
			WHERE p.[name] = @K1
			
		END
		
		FETCH NEXT FROM #c INTO @K1
	END
	CLOSE #c
	DEALLOCATE #c
	
END

--- Output section
-- May be messy.  Hard to format in SQL Server
---------------------------------------------------

PRINT('NAME')
PRINT(CHAR(9) + '[' + @schema_name + '].[' + @object_name + ']')
PRINT('')

DECLARE #c CURSOR LOCAL STATIC FOR
SELECT 
	[output_header]
	,[verbosity]
	,[section]
FROM #sections
WHERE ISNULL([section],'') <> ''
ORDER BY [sequence] ASC

OPEN #c
FETCH NEXT FROM #c INTO @K1, @v_verbosity, @str

WHILE @@FETCH_STATUS = 0
BEGIN

	IF @Verbosity >= @v_verbosity
	BEGIN
		PRINT(@K1)
		
		IF (@K1 = 'PARAMETERS')
		BEGIN
			-- Print parameters
			DECLARE #p CURSOR LOCAL STATIC FOR
			SELECT 
				CHAR(9) + [name] + ' <' + [type] + '>' + CHAR(13) + CHAR(10)
				+ CASE WHEN [is_output] = 1 THEN @tt + '** OUTPUT PARAMETER **' + @nl  ELSE '' END
				+ CASE WHEN [has_default_value] = 1 THEN @tt + 'DEFAULT VALUE: ' + [default_value] + @nl ELSE '' END
				+ @tt + ISNULL([comment],'') + @nl
			FROM #params
			ORDER BY [parameter_id] ASC
			
			OPEN #p
			FETCH NEXT FROM #p INTO @paramprint

			WHILE @@FETCH_STATUS = 0
			BEGIN
				PRINT(@paramprint)
			
				FETCH NEXT FROM #p INTO @paramprint
			END
			CLOSE #p
			DEALLOCATE #p
			
		END
		ELSE
		BEGIN
			PRINT(CHAR(9) + REPLACE(@str,CHAR(13) + CHAR(10),CHAR(13) + CHAR(10) + CHAR(9))) -- The REPLACE just adds a TAB to each newline
		END
		
		PRINT('')
	END

	FETCH NEXT FROM #c INTO @K1, @v_verbosity, @str
END
CLOSE #c
DEALLOCATE #c