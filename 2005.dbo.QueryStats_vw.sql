IF OBJECT_ID('[dbo].[QueryStats_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[QueryStats_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.QueryStats_vw
**	Desc:			View to retrieve query statistics
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2008.10.28
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
**	20091211	Matt Stanford	Bean sucks and can't add Schema_Name so I did it.
********************************************************************************************************/

CREATE VIEW [dbo].[QueryStats_vw]

AS

WITH [Ad_Hoc] AS
(
	SELECT 
		'AD_HOC'											AS [DBName]
		,NULL												AS [Schema_Name]
		,st.[text]											AS [Object_Name]
		,SUM(qs.[execution_count])							AS [Count]
		,SUM(qs.[total_worker_time])						AS [Total_CPU_Time]
		,SUM(qs.[total_worker_time]/qs.[execution_count])	AS [AVG_CPU_Time]
		,SUM(qs.[last_worker_time])							AS [Last_CPU]
		,SUM(qs.[min_worker_time])							AS [Min_CPU]
		,SUM(qs.[max_worker_time])							AS [Max_CPU]
		,SUM(qs.[total_elapsed_time])						AS [Total_Run_Time]
		,SUM(qs.[total_elapsed_time]/qs.[execution_count])	AS [AVG_Run_Time]
		,SUM(qs.[last_elapsed_time])						AS [Last_Run_Time]
		,SUM(qs.[min_elapsed_time])							AS [Min_Run_Time]
		,SUM(qs.[max_elapsed_time])							AS [Max_Run_Time]
		,SUM(qs.[total_logical_writes])						AS [Total_Logical_Writes]
		,SUM(qs.[last_logical_writes])						AS [Last_Logical_Writes]
		,SUM(qs.[min_logical_writes])						AS [Min_Logical_Writes]
		,SUM(qs.[max_logical_writes])						AS [Max_Logical_Writes]
		,SUM(qs.[total_physical_reads])						AS [Total_Physical_Reads]
		,SUM(qs.[last_physical_reads])						AS [Last_Physical_Reads]
		,SUM(qs.[min_physical_reads])						AS [Min_Physical_Reads]
		,SUM(qs.[max_physical_reads])						AS [Max_Physical_Reads]
		,SUM(qs.[total_logical_reads])						AS [Total_Logical_Reads]
		,SUM(qs.[last_logical_reads])						AS [Last_Logical_Reads]
		,SUM(qs.[min_logical_reads])						AS [Min_Logical_Reads]
		,SUM(qs.[max_logical_reads])						AS [Max_Logical_Reads]
	FROM [sys].[dm_exec_query_stats] qs 
	CROSS APPLY [sys].[dm_exec_sql_text](qs.[sql_handle]) st
	WHERE st.[dbid] IS NULL
	AND st.[objectid] IS NULL
	GROUP BY st.[objectid], st.[dbid], st.[text]
) 

SELECT 
	DB_NAME(st.[dbid])									AS [DBName]
	,OBJECT_SCHEMA_NAME(st.[objectid], st.[dbid])		AS [Schema_Name]
	,OBJECT_NAME(st.[objectid], st.[dbid])				AS [Object_Name]
	,SUM(qs.[execution_count])							AS [Count]
	,SUM(qs.[total_worker_time])						AS [Total_CPU_Time]
	,SUM(qs.[total_worker_time]/qs.[execution_count])	AS [AVG_CPU_Time]
	,SUM(qs.[last_worker_time])							AS [Last_CPU]
	,SUM(qs.[min_worker_time])							AS [Min_CPU]
	,SUM(qs.[max_worker_time])							AS [Max_CPU]
	,SUM(qs.[total_elapsed_time])						AS [Total_Run_Time]
	,SUM(qs.[total_elapsed_time]/qs.[execution_count])	AS [AVG_Run_Time]
	,SUM(qs.[last_elapsed_time])						AS [Last_Run_Time]
	,SUM(qs.[min_elapsed_time])							AS [Min_Run_Time]
	,SUM(qs.[max_elapsed_time])							AS [Max_Run_Time]
	,SUM(qs.[total_logical_writes])						AS [Total_Logical_Writes]
	,SUM(qs.[last_logical_writes])						AS [Last_Logical_Writes]
	,SUM(qs.[min_logical_writes])						AS [Min_Logical_Writes]
	,SUM(qs.[max_logical_writes])						AS [Max_Logical_Writes]
	,SUM(qs.[total_physical_reads])						AS [Total_Physical_Reads]
	,SUM(qs.[last_physical_reads])						AS [Last_Physical_Reads]
	,SUM(qs.[min_physical_reads])						AS [Min_Physical_Reads]
	,SUM(qs.[max_physical_reads])						AS [Max_Physical_Reads]
	,SUM(qs.[total_logical_reads])						AS [Total_Logical_Reads]
	,SUM(qs.[last_logical_reads])						AS [Last_Logical_Reads]
	,SUM(qs.[min_logical_reads])						AS [Min_Logical_Reads]
	,SUM(qs.[max_logical_reads])						AS [Max_Logical_Reads]
FROM [sys].[dm_exec_query_stats] qs 
CROSS APPLY [sys].[dm_exec_sql_text](qs.[sql_handle]) st
WHERE st.[objectid] IS NOT NULL
AND DB_NAME(st.[dbid]) IS NOT NULL
GROUP BY st.[objectid], st.[dbid], st.[text]

UNION ALL

SELECT * FROM [Ad_Hoc]
WHERE [Count] > 50