IF OBJECT_ID('[dbo].[DeleteByBatch]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[DeleteByBatch]
GO

/*******************************************************************************************************
**  Name:			dbo.DeleteByBatch
**  Desc:			Runs any delete statement (any statement really) and loops by the BatchCount
**  Auth:			Matt Stanford (SQLSlayer.com)
**  Usage:			See comments below
**  Date:			2009.04.16
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE PROCEDURE [dbo].[DeleteByBatch] 
(
	@DeleteStatement	NVARCHAR(4000)
	,@BatchSize			INT				= 1000
	,@Debug				BIT				= 0
)

AS

SET NOCOUNT ON

/*
-- Parameters
DECLARE
	@DeleteStatement	NVARCHAR(4000)
	,@BatchSize			INT
	,@Debug				BIT

-- Test shit
SET NOCOUNT ON
IF OBJECT_ID('tempdb.dbo.#testdata') IS NOT NULL
	DROP TABLE #testdata
	
CREATE TABLE #testdata (
	id INT IDENTITY PRIMARY KEY CLUSTERED
	,bla VARCHAR(50)
)

DECLARE @i INT
SET @i = 0

WHILE @i < 10000
BEGIN
	INSERT INTO #testdata (bla)
	VALUES (CAST(@i AS VARCHAR(50)))
	
	SET @i = @i + 1
END

SET @DeleteStatement = 'DELETE FROM #testdata'
SET @BatchSize = 99
SET @Debug = 0

-- /Test shit
*/

DECLARE 
	@SQL		NVARCHAR(4000)
	
SET @SQL = 'SET ROWCOUNT ' + CAST(@BatchSize AS VARCHAR(50)) + CHAR(13)
SET @SQL = @SQL + 'DECLARE @ct INT SET @ct = 1' + CHAR(13)
SET @SQL = @SQL + 'WHILE @ct > 0' + CHAR(13) + 'BEGIN' + CHAR(13) + CHAR(9) + @DeleteStatement + CHAR(13) + CHAR(9)
SET @SQL = @SQL + 'SET @ct = @@ROWCOUNT' + CHAR(13)
SET @SQL = @SQL + 'END' + CHAR(13) + 'SET ROWCOUNT 0'


IF @Debug = 0
	EXEC(@SQL)
ELSE
	PRINT(@SQL)
	
SET NOCOUNT OFF