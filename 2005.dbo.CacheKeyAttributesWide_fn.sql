IF OBJECT_ID('[dbo].[CacheKeyAttributesWide_fn]','IF') IS NOT NULL 
	DROP FUNCTION [dbo].[CacheKeyAttributesWide_fn]
GO

/*******************************************************************************************************
**	Name:			[dbo].[CacheKeyAttributesWide_fn]()
**	Desc:			Returns the key attributes for plan cache in a wide format
**	Auth:			Matt Stanford (SQLSlayer.com)
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.	
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20101117	Matt Stanford	Added the "exec_context" columns
********************************************************************************************************/
CREATE FUNCTION [dbo].[CacheKeyAttributesWide_fn](
	@plan_handle VARBINARY(64)			-- The plan handle to return keys for
)
RETURNS TABLE
AS

RETURN
SELECT 
	CAST(pvt.[set_options] AS INT)															AS [set_options]
	,CAST(pvt.[objectid] AS INT)															AS [objectid]
	,CAST(pvt.[dbid] AS INT)																AS [dbid]
	,CAST(pvt.[dbid_execute] AS INT)														AS [dbid_execute]
	,CAST(pvt.[user_id] AS INT)																AS [user_id]
	,CAST(pvt.[language_id] AS INT)															AS [language_id]
	,CAST(pvt.[date_format] AS INT)															AS [date_format]
	,CAST(pvt.[date_first] AS INT)															AS [date_first]
	,CAST(pvt.[status] AS INT)																AS [status]
	,CAST(pvt.[required_cursor_options] AS INT)												AS [required_cursor_options]
	,CAST(pvt.[acceptable_cursor_options] AS INT)											AS [acceptable_cursor_options]
	,CAST(pvt.[hits_exec_context] AS INT)													AS [hits_exec_context]
	,CAST(pvt.[misses_exec_context] AS INT)													AS [misses_exec_context]
	,CAST(pvt.[removed_exec_context] AS INT)												AS [removed_exec_context]
	,CAST(pvt.[inuse_exec_context] AS INT)													AS [inuse_exec_context]
	,CAST(pvt.[free_exec_context] AS INT)													AS [free_exec_context]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 1 = 1 THEN 'ON' ELSE '--' END				AS [ANSI_PADDING]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 2 = 2 THEN 'ON' ELSE '--' END				AS [Parallel Plan]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 4 = 4 THEN 'ON' ELSE '--' END				AS [FORCEPLAN]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 8 = 8 THEN 'ON' ELSE '--' END				AS [CONCAT_NULL_YIELDS_NULL]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 16 = 16 THEN 'ON' ELSE '--' END				AS [ANSI_WARNINGS]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 32 = 32 THEN 'ON' ELSE '--' END				AS [ANSI_NULLS]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 64 = 64 THEN 'ON' ELSE '--' END				AS [QUOTED_IDENTIFIER]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 128 = 128 THEN 'ON' ELSE '--' END			AS [ANSI_NULL_DFLT_ON]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 256 = 256 THEN 'ON' ELSE '--' END			AS [ANSI_NULL_DFLT_OFF]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 512 = 512 THEN 'ON' ELSE '--' END			AS [NoBrowseTable]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 1024 = 1024 THEN 'ON' ELSE '--' END			AS [TriggerOneRow]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 2048 = 2048 THEN 'ON' ELSE '--' END			AS [ResyncQuery]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 4096 = 4096 THEN 'ON' ELSE '--' END			AS [ARITH_ABORT]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 8192 = 8192 THEN 'ON' ELSE '--' END			AS [NUMERIC_ROUNDABORT]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 16384 = 16384 THEN 'ON' ELSE '--' END		AS [DATEFIRST]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 32768 = 32768 THEN 'ON' ELSE '--' END		AS [DATEFORMAT]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 65536 = 65536 THEN 'ON' ELSE '--' END		AS [LanguageID]
	,CASE WHEN CAST(pvt.[set_options] AS INT) & 131072 = 131072 THEN 'ON' ELSE '--' END		AS [UPON]
FROM (SELECT 
		epa.[attribute]
		,epa.[value]
	FROM sys.dm_exec_plan_attributes(@plan_handle) AS epa
) AS ecpa 
PIVOT (
	MAX(ecpa.value) 
	FOR ecpa.attribute IN (
	"set_options", "dbid", "objectid", "dbid_execute", "user_id", "language_id", "date_format"
	, "date_first", "status", "required_cursor_options", "acceptable_cursor_options", "hits_exec_context"
	, "misses_exec_context", "removed_exec_context", "inuse_exec_context", "free_exec_context"
)
) AS pvt;