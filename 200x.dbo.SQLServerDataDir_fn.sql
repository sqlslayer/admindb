IF OBJECT_ID('[dbo].[SQLServerDataDir_fn]','FN') IS NOT NULL 
	DROP FUNCTION [dbo].[SQLServerDataDir_fn]
GO

/*******************************************************************************************************
**	Name:			dbo.SQLServerDataDir_fn()
**	Desc:			Return the SQL Server Data Directory
**	Auth:			SQLSlayer.com
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.	
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE FUNCTION [dbo].[SQLServerDataDir_fn]()
RETURNS NVARCHAR(4000)

AS

BEGIN

	DECLARE @dir NVARCHAR(4000)
		
	EXEC master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultData', @dir output, 'no_output'

	IF (@dir IS NULL)
	BEGIN
		EXEC master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\Setup',N'SQLDataRoot', @dir output, 'no_output'
		SELECT @dir = @dir + N'\Data'
	END

	RETURN @dir

END