IF OBJECT_ID('[dbo].[DeployDatabaseMirroringNotification]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[DeployDatabaseMirroringNotification]
GO

/******************************************************************************
* Name
	[dbo].[DeployDatabaseMirroringNotification]

* Author
	Adam Bean
	
* Date
	2011.11.02
	
* Synopsis
	Wrapper procedure to deploy the DatabaseMirroringNotification procedure
	
* Description
	Wrapper procedure is required as the procedure can not accept email recipients as a parameter because
	a queue can not call a procedure with a parameter. 

* Examples
	EXEC [dbo].[DeployDatabaseMirroringNotification] @Recipients = 'you@domain.com'

* Dependencies
	n/a

* Parameters
	@Recipients			- Email recipients of the notification
	@Debug				- Show the procedure to be deployed but do not execute
	
* Notes
	This procedure will also setup the queue, service and event notification as well as enable TRUSTWORTHY

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20111103	Adam Bean		Added check for TRUSTWORTHY
******************************************************************************/

CREATE PROCEDURE [dbo].[DeployDatabaseMirroringNotification]
(
	@Recipients		VARCHAR(1024)
	,@Debug			BIT	= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@SQL		VARCHAR(MAX)
	,@DBName	NVARCHAR(128)

-- Replace commas with semi-colons to work properly for database mail
SET @Recipients = REPLACE(@Recipients, ',', ';')

-- Find our current database
SET @DBName = (SELECT DB_NAME())

IF @Debug = 0
	BEGIN
	-- Setup service broker
	IF NOT EXISTS
	(
		SELECT 
			[name] 
		FROM [dbo].[sysdatabases_vw] 
		WHERE [is_broker_enabled] = 1
		AND [name] = @DBName
	)
	BEGIN
		EXEC('ALTER DATABASE [' + @DBName + '] SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE')
	END

	-- Setup queue
	IF NOT EXISTS (SELECT [name] FROM [sys].[service_queues] WHERE [name] = 'MirroringNotificationQueue')
	CREATE QUEUE [dbo].[MirroringNotificationQueue] 
		WITH STATUS = ON
		,RETENTION = OFF
		,ACTIVATION (STATUS = ON, PROCEDURE_NAME = [dbo].[DatabaseMirroringNotification]
		,MAX_QUEUE_READERS = 5
		,EXECUTE AS OWNER)
	ON [PRIMARY] 

	-- Setup service
	IF NOT EXISTS (SELECT [name] FROM [sys].[services] WHERE [name] = 'MirroringNotificationService')
	CREATE SERVICE [MirroringNotificationService] AUTHORIZATION [dbo] 
	ON QUEUE [dbo].[MirroringNotificationQueue] ([http://schemas.microsoft.com/SQL/Notifications/PostEventNotification])

	-- Setup event notification
	IF NOT EXISTS (SELECT [name] FROM [sys].[server_event_notifications] WHERE [name] = 'MirroringNotification')
	CREATE EVENT NOTIFICATION MirroringNotification
		ON SERVER
		FOR DATABASE_MIRRORING_STATE_CHANGE
		TO SERVICE 'MirroringNotificationService', 'current database'
		
	-- Enable TRUSTHWORTHY
	IF (SELECT [is_trustworthy_on] FROM [dbo].[sysdatabases_vw] WHERE [name] = @DBName) != 1
	EXEC('ALTER DATABASE [' + @DBName + '] SET TRUSTWORTHY ON')
END

-- Create the procedure
SET @SQL = 
('
IF OBJECT_ID(''[dbo].[DatabaseMirroringNotification]'',''P'') IS NOT NULL
	DROP PROCEDURE [dbo].[DatabaseMirroringNotification]
	
EXEC
(''

/******************************************************************************
* Name
	[dbo].[DatabaseMirroringNotification]

* Author
	Kathy Toth
	
* Date
	2011.08.31
	
* Synopsis
	Procedure to notify recipients of database mirroring state changes.
	
* Description
	Procedure which will send an HTML formatted email showing the last three states of database mirroring.
	The procedure is fired via service broker.

* Examples
	EXEC [dbo].[DatabaseMirroringNotification]

* Dependencies
	[dbo].[DeployDatabaseMirroringNotification] (see notes)

* Parameters
	n/a
	
* Notes
	To only be created from the wrapper procedure: [dbo].[DeployDatabaseMirroringNotification]

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20110912	John DelNostro	Rewritten to utilize service broker
	20111101	Adam Bean		Cleaned up. Fixed formatting of status, added bullet points
******************************************************************************/

CREATE PROCEDURE [dbo].[DatabaseMirroringNotification]

AS 

SET NOCOUNT ON

DECLARE 
	@message_body		XML
	,@email_message		NVARCHAR(MAX)
	,@Subject			VARCHAR(150)
	,@EmailHeader		VARCHAR(2048)
	,@EmailBody			VARCHAR(MAX)
	,@Status			VARCHAR(2048)
	,@EmailCaption		VARCHAR(128)
	,@EmailFooter		VARCHAR(2048)
	,@Recipients		VARCHAR(MAX)
	,@Count				INT
	,@html				NVARCHAR(MAX)


SET @Recipients	= ''''' + @Recipients + '''''
SET @Subject	= ''''Database Mirroring Notification - Status Change''''

-- Parse XML Into Temp Table
DECLARE @EventTable TABLE
(   
	[DatabaseID]		VARCHAR(3)
	,[EventPostTime]	VARCHAR(25)
	,[Status]			NVARCHAR(MAX)
)

-- Take Current Database Mirroring Snapshot Status
WHILE (1 = 1)
BEGIN
	BEGIN TRANSACTION
	-- Receive the next available message FROM the queue
	WAITFOR 
	(
		RECEIVE TOP (1) CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/DatabaseID)[1]'''',''''varchar(128)'''')	AS [DatabaseID]
		               ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/PostTime)[1]'''',''''varchar(20)'''')		AS [EventPostTime]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/TextData)[1]'''',''''varchar(4000)'''')	AS [StatusMsg]
		FROM [dbo].[MirroringNotificationQueue]		
		INTO @EventTable
	), TIMEOUT 5000
	-- If we didn''''t get anything, bail out
	IF (@@ROWCOUNT = 0)
	BEGIN
		ROLLBACK TRANSACTION
		BREAK
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
	END
END	

-- Build the email
SET @EmailCaption = ''''SQL Database Mirroring Notification For '''' + @@SERVERNAME 

-- Setup table to build body
DECLARE @EmailBodyContents TABLE 
(
	[html]			VARCHAR(MAX)
)

-- Setup table to build email
DECLARE @Email TABLE 
(
	[ID]			INT IDENTITY
   ,[html]			VARCHAR(MAX)
)

-- Build the email header
SELECT @EmailHeader = 
''''
<HTML>
<DIV ALIGN="CENTER">
<TABLE BORDER="1" CELLSPACING="5" CELLPADDING="1" ALIGN="CENTER">
<CAPTION>
	<H2>'''' + @EmailCaption + ''''</H2>
	<EM><i>The following event(s) occured for database mirroring on :'''' + @@SERVERNAME + ''''</i></EM>
</CAPTION>
<TR BGCOLOR="#C0C0C0">
	<TH>EventPostTime</TH>
	<TH>DatabaseName</TH>
	<TH>CurrentMirroringState</TH>
	<TH>MirroringRole</TH>
	<TH>MirroringPartnerName</TH>
	<TH>MirroringPartnerInstance</TH>
	<TH>StatusMessage</TH>
</TR>
''''

-- Build the email footer
SELECT @EmailFooter =
''''
</TABLE>
</DIV>
</HTML>
''''

-- Build the email body
INSERT INTO @EmailBodyContents
SELECT
	''''
	<TR>
		<TD>'''' + e.[EventPostTime]				+ ''''</TD>
		<TD>'''' + m.[DatabaseName]					+ ''''</TD>
		<TD>'''' + m.[mirroring_state_desc]			+ ''''</TD>
		<TD>'''' + m.[mirroring_role_desc]			+ ''''</TD>
		<TD>'''' + m.[mirroring_partner_name]		+ ''''</TD>
		<TD>'''' + m.[mirroring_partner_instance]	+ ''''</TD>
		<TD>'''' + s.[Status]						+ ''''</TD>
	</TR>
	'''' 
FROM ( 
		SELECT      
			[DatabaseID]
			,REPLACE(REPLACE([EventPostTime],''''T'''','''' ''''),''''.'''','''''''')						AS [EventPostTime]
			,ROW_NUMBER() OVER (PARTITION BY [DatabaseID] ORDER BY [EventPostTime] DESC)	AS Row 
		FROM @EventTable
      ) e
LEFT JOIN  
	(
		SELECT 
			[name]								AS [DatabaseName]
			,CONVERT(CHAR(2),d.[database_id])	AS [database_id]
			,dm.[mirroring_state_desc] 
			,dm.[mirroring_role_desc]
			,dm.[mirroring_safety_level_desc]
			,dm.[mirroring_partner_name]
			,dm.[mirroring_partner_instance]
		FROM [sys].[database_mirroring] dm 
		JOIN [dbo].[sysdatabases_vw] d 
			ON dm.[database_id] = d.[database_id] 
		WHERE dm.[mirroring_state_desc] IS NOT NULL
		
	) m 
	ON m.[database_id] = e.[DatabaseID]
CROSS APPLY
(
	SELECT 
		''''<OL>'''' +
		REPLACE(REPLACE(REPLACE(STUFF((SELECT '''','''' + [Status]
	FROM @EventTable
	FOR XML PATH('''''''')),1, 1, ''''''''),''''DBM:'''',''''<LI>''''),''''-&gt;'''',''''''''),'''','''','''''''') + ''''</LI>'''' + ''''</OL>'''' AS [Status]
) s
WHERE e.[Row] = 1
ORDER BY [EventPostTime] DESC

-- Build the email
INSERT INTO @Email
SELECT @EmailHeader
UNION ALL
SELECT *
FROM @EmailBodyContents
UNION ALL
SELECT @EmailFooter

-- Put it all together
SET @Count = 1
SET @EmailBody = ''''''''
WHILE @Count <= (SELECT COUNT(*) FROM @Email)
BEGIN
	SET @EmailBody = @EmailBody + (SELECT [html] FROM @Email WHERE [ID] = @Count)
	SET @Count = @Count + 1
END

-- Only send email if there is data
IF @Count > 3
BEGIN
	-- Send mail
	EXEC [msdb].[dbo].[sp_send_dbmail]
		@Recipients		= @Recipients
		,@Subject		= @Subject
		,@Body			= @EmailBody
		,@Body_format	= ''''HTML''''
END
'')
')

IF @Debug = 1
BEGIN
	PRINT '/*** DEBUG ENABLED ****/'
	PRINT (@SQL)
END
ELSE
BEGIN
	EXEC(@SQL)
	IF EXISTS (SELECT [name] FROM [sys].[objects] WHERE [name] = 'DatabaseMirroringNotification')
	BEGIN
		PRINT 'Successfully deployed [dbo].[DatabaseMirroringNotification] with ''' + @Recipients + ''' as the recipient(s).'
	END
	ELSE
	BEGIN
		PRINT 'Failed to deploy [dbo].[DatabaseMirroringNotification]'
	END
END

SET NOCOUNT OFF