IF OBJECT_ID('[dbo].[BackUpDB_LiteSpeed]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[BackUpDB_LiteSpeed]
GO

/*******************************************************************************************************
**	Name:			dbo.BackUpDB_LiteSpeed
**	Desc:			Runs full SQL Server database backups with LiteSpeed support
**	Auth:			Adam Bean (SQLSlayer.com)
**  Parameters:		Inherited from BackupDB
**	Notes:			Should not be run as a stand alone procedure. Use BackupDB.
**	Date:			2008.10.13
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090820	Adam Bean		Fixed for case sensitive collation
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE PROCEDURE [dbo].[BackUpDB_LiteSpeed] 
(
	@DBName					sysname
	,@BackupType			CHAR(1)
	,@BackupLocation		VARCHAR(512)
	,@BackupName			VARCHAR(256)
	,@File					VARCHAR(4000)	
	,@FileGroup				VARCHAR(4000)
	,@Init					BIT		
	,@RetainDays			TINYINT
	,@Stats					TINYINT
	,@Description			VARCHAR(512)		
	,@Verify				BIT				
	,@Compression			TINYINT				
	,@Threads				TINYINT				
	,@EncryptionLevel		TINYINT			
	,@EncryptionKey			VARCHAR(4000)	
	,@Throttle				TINYINT			
	,@Affinity				TINYINT			
	,@Priority				TINYINT			
	,@Debug					BIT				
)

AS

SET NOCOUNT ON

DECLARE
	@Comma					VARCHAR(2)
	,@Tab					VARCHAR(8)
	,@NewLine				VARCHAR(8)
	,@SQL					VARCHAR(4000)

-- Populate some variables
SET @Comma = CHAR(44)
SET @Tab = CHAR(9)	
SET @NewLine = CHAR(10)

-- Build our backup command
SET @SQL = @NewLine
IF @BackupType = 'L'
	SET @SQL = @SQL + 'EXEC [master].[dbo].[xp_backup_log]' + @NewLine
ELSE
	SET @SQL = @SQL + 'EXEC [master].[dbo].[xp_backup_database]' + @NewLine
SET @SQL = @SQL + @Tab + '@Database = [' + @DBName + ']' + @NewLine
SET @SQL = @SQL + @Tab + @Comma + '@Filename = ''' + @BackupName + '''' + @NewLine
IF @BackupType = 'F'
	SET @SQL = @SQL + @Tab + @Comma + '@File = ''' + @File + ''''
ELSE IF @BackupType = 'G'
	SET @SQL = @SQL + @Tab + @Comma + '@Filegroup = ''' + @FileGroup + '''  '
IF @BackupType = 'I'
	SET @SQL = @SQL + @Tab + @Comma + '@With = ''DIFFERENTIAL''' + @NewLine
IF @Init IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@Init = ' + CAST(@Init AS VARCHAR) + @NewLine
IF @Stats IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@With = ''STATS = ' + CAST(@Stats AS VARCHAR) + '''' + @NewLine
IF @Threads IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@Threads = ' + CAST(@Threads AS VARCHAR) + '' + @NewLine
IF @Compression IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@CompressionLevel = ' + CAST(@Compression AS VARCHAR) + '' + @NewLine
IF @EncryptionLevel IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@Cryptlevel = ' + CAST(@EncryptionLevel AS VARCHAR) + '' + @NewLine
IF @EncryptionKey IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@Encryptionkey = ''' + CAST(@EncryptionKey AS VARCHAR) + '''' + @NewLine
IF @Throttle IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@Throttle = ' + CAST(@Throttle AS VARCHAR) + '' + @NewLine
IF @RetainDays IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@RetainDays = ' + CAST(@RetainDays AS VARCHAR) + '' + @NewLine
IF @Affinity IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@Affinity = ' + CAST(@Affinity AS VARCHAR) + '' + @NewLine
IF @Description IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + '@Desc = ''' + @Description + ''''
IF @Verify = 1
BEGIN
	IF @EncryptionKey IS NULL
		SET @SQL = @SQL + @NewLine + 'EXEC [master].[dbo].[xp_restore_verifyonly] @Filename = ''' + @BackupName + ''''		
	ELSE
		SET @SQL = @SQL + @NewLine + 'EXEC [master].[dbo].[xp_restore_verifyonly] @Filename = ''' + @BackupName + '''' + @Comma + ' @Encryptionkey = ''' + @EncryptionKey + ''''
END

IF @Debug = 1
	PRINT (@SQL)
ELSE
	EXEC (@SQL)
	
SET NOCOUNT OFF


