IF OBJECT_ID('dbo.sysdatabases_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[sysdatabases_vw]
GO

/******************************************************************************
* Name
	[dbo].[sysdatabases_vw]

* Author
	Matt Stanford (SQLSlayer.com)
	
* Date
	2009.05.19
	
* Synopsis
	View to give you a common view of sysdatabases amongst all versions
	
* Description
	n/a

* Examples
	n/a

* Dependencies
	n/a

* Parameters
	n/a
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20090729	Adam Bean		Fixed for case sensitive collation
	20090929	Adam Bean		Global header formatting & general clean up
	20140511	Adam Bean		Updated for all new columns in 08 and 12
******************************************************************************/

CREATE VIEW [dbo].[sysdatabases_vw]

AS

SELECT
	CAST([name] AS sysname)														AS [name]
	,CAST([dbid] AS INT)														AS [database_id]
	,CAST(NULL AS INT)															AS [source_database_id]
	,CAST([sid]	AS VARBINARY(85))												AS [owner_sid]
	,CAST([crdate] AS DATETIME)													AS [create_date]
	,CAST([cmptlevel] AS TINYINT)												AS [compatibility_level]
	,CAST(DATABASEPROPERTYEX([name], 'Collation') AS sysname)					AS [collation_name]
	,CAST(CASE DATABASEPROPERTYEX([name], 'UserAccess')
		WHEN 'SINGLE_USER' THEN	1
		WHEN 'MULTI_USER' THEN 0
		WHEN 'RESTRICTED_USER' THEN 2
		ELSE 255
	END AS TINYINT)																AS [user_access]
	,CAST(DATABASEPROPERTYEX([name], 'UserAccess') AS NVARCHAR(60))				AS [user_access_desc]
	,CAST(CASE DATABASEPROPERTYEX([name], 'Updateability')
		WHEN 'READ_WRITE' THEN 0
		ELSE 1
	END	AS BIT)																	AS [is_read_only]
	,CAST(DATABASEPROPERTYEX([name], 'IsAutoClose') AS BIT)						AS [is_auto_close_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsAutoShrink') AS BIT) 					AS [is_auto_shrink_on]
	,CAST(CASE DATABASEPROPERTYEX([name], 'Status')
		WHEN 'ONLINE' THEN 0
		WHEN 'RESTORING' THEN 1
		WHEN 'RECOVERING' THEN 2
		WHEN 'RECOVERY_PENDING' THEN 3
		WHEN 'SUSPECT' THEN 4
		WHEN 'EMERGENCY' THEN 5
		WHEN 'OFFLINE' THEN 6
		ELSE 255
	END AS TINYINT)																AS [state]
	,CAST(DATABASEPROPERTYEX([name], 'Status') AS NVARCHAR(60))					AS [state_desc]
	,CAST(DATABASEPROPERTYEX([name], 'IsInStandby')	AS BIT)						AS [is_in_standby]
	,CAST(1 AS BIT)																AS [is_cleanly_shutdown]
	,CAST(0 AS BIT)																AS [is_supplemental_logging_enabled]
	,CAST(0 AS TINYINT)															AS [snapshot_isolation_state]
	,CAST('OFF' AS NVARCHAR(60))												AS [snapshot_isolation_state_desc]
	,CAST(0 AS BIT)																AS [is_read_committed_snapshot_on]
	,CAST(CASE DATABASEPROPERTYEX([name], 'Recovery')
		WHEN 'FULL' THEN 1
		WHEN 'BULK_LOGGED' THEN 2
		WHEN 'SIMPLE' THEN 3
		ELSE 255
	END AS TINYINT)																AS [recovery_model]
	,CAST(DATABASEPROPERTYEX([name], 'Recovery') AS NVARCHAR(60))				AS [recovery_model_desc]
	,CAST(DATABASEPROPERTYEX([name], 'IsTornPageDetectionEnabled') AS TINYINT)	AS [page_verify_option]
	,CAST(CASE DATABASEPROPERTYEX([name], 'IsTornPageDetectionEnabled')
		WHEN 0 THEN 'NONE'
		WHEN 1 THEN 'TORN_PAGE_DETECTION'
		WHEN 2 THEN 'CHECKSUM'
		ELSE 'UNKNOWN'
	END AS NVARCHAR(60))														AS [page_verify_option_desc]
	,CAST(DATABASEPROPERTYEX([name], 'IsAutoCreateStatistics') AS BIT)			AS [is_auto_create_stats_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsAutoUpdateStatistics') AS BIT)			AS [is_auto_update_stats_on]
	,CAST(0 AS BIT)																AS [is_auto_update_stats_async_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsAnsiNullDefault') AS BIT)				AS [is_ansi_null_default_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsAnsiNullsEnabled') AS BIT)				AS [is_ansi_nulls_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsAnsiPaddingEnabled') AS BIT)			AS [is_ansi_padding_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsAnsiWarningsEnabled') AS BIT)			AS [is_ansi_warnings_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsArithmeticAbortEnabled') AS BIT)		AS [is_arithabort_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsNullConcat') AS BIT)					AS [is_concat_null_yields_null_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsNumericRoundAbortEnabled') AS BIT)		AS [is_numeric_roundabort_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsQuotedIdentifiersEnabled') AS BIT)		AS [is_quoted_identifier_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsRecursiveTriggersEnabled') AS BIT)		AS [is_recursive_triggers_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsCloseCursorsOnCommitEnabled') AS BIT)	AS [is_cursor_close_on_commit_on]
	,CAST(DATABASEPROPERTYEX([name], 'IsLocalCursorsDefault') AS BIT)			AS [is_local_cursor_default]
	,CAST(DATABASEPROPERTYEX([name], 'IsFulltextEnabled') AS BIT)				AS [is_fulltext_enabled]
	,CAST(0 AS BIT)																AS [is_trustworthy_on]
	,CAST(CASE 
		WHEN [status2] & 1024 = 1024 THEN 1
		ELSE 0
	END AS BIT)																	AS [is_db_chaining_on]
	,CAST(0 AS BIT)																AS [is_parameterization_forced]
	,CAST(0 AS BIT)																AS [is_master_key_encrypted_by_server]
	,CAST(DATABASEPROPERTYEX([name], 'IsPublished') AS BIT)						AS [is_published]
	,CAST(DATABASEPROPERTYEX([name], 'IsSubscribed') AS BIT)					AS [is_subscribed]
	,CAST(DATABASEPROPERTYEX([name], 'IsMergePublished') AS BIT)				AS [is_merge_published]
	,CAST([category] & 0x16 AS BIT)												AS [is_distributor]
	,CAST(DATABASEPROPERTYEX([name], 'IsSyncWithBackup') AS BIT)				AS [is_sync_with_backup]
	,CAST('00000000-0000-0000-0000-000000000000' AS UNIQUEIDENTIFIER)			AS [service_broker_guid]
	,CAST(0 AS BIT)																AS [is_broker_enabled]
	,CAST(0 AS TINYINT)															AS [log_reuse_wait]
	,CAST('NOTHING' AS NVARCHAR(60))											AS [log_reuse_wait_desc]
	,CAST(0 AS BIT)																AS [is_date_correlation_on]
	,CAST(NULL AS BIT)															AS [is_cdc_enabled]
	,CAST(NULL AS BIT)															AS [is_encrypted]
	,CAST(NULL AS BIT)															AS [is_honor_broker_priority_on]
	,CAST(NULL AS UNIQUEIDENTIFIER)												AS [replica_id]
	,CAST(NULL AS UNIQUEIDENTIFIER)												AS [group_database_id]
	,CAST(NULL AS SMALLINT)														AS [default_language_lcid]
	,CAST(NULL AS NVARCHAR(128))												AS [default_language_name]
	,CAST(NULL AS INT)															AS [default_fulltext_language_lcid]
	,CAST(NULL AS NVARCHAR(128))												AS [default_fulltext_language_name]
	,CAST(NULL AS BIT)															AS [is_nested_triggers_on]
	,CAST(NULL AS BIT)															AS [is_transform_noise_words_on]
	,CAST(NULL AS SMALLINT)														AS [two_digit_year_cutoff]
	,CAST(NULL AS TINYINT)														AS [containment]
	,CAST(NULL AS NVARCHAR(60))													AS [containment_desc]
	,CAST(NULL AS INT)															AS [target_recovery_time_in_seconds]
FROM [master].[dbo].[sysdatabases]
