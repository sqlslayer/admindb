IF OBJECT_ID ('[dbo].[ShowChangedExtents]') IS NOT NULL
   DROP PROCEDURE [dbo].[ShowChangedExtents]
GO

/******************************************************************************
* Name
	[dbo].[ShowChangedExtents]
	
* Author
	Paul S. Randal, SQLskills.com
	
* Date
	2008.04.01
	
* Synopsis
	This procedure works out what percentage of a database has changed since the previous full database backup.
	
* Description
	This SP cracks all differential bitmap pages for all online
	data files in a database. It creates a sum of changed extents
	and reports it as follows (example small msdb):

	EXEC [dbo].[ShowChangedExtents] 'msdb';
	
	Database Name:        msdb	Total Extents:        102	Changed Extents:      56	Percentage Changed:   54.9	Differential Size MB: 3

* Examples
	EXEC [dbo].[ShowChangedExtents] 'msdb'

* Dependencies
	[dbo].[sysmasterfiles_vw]
	[dbo].[ConvertToExtents_fn]

* Parameters
	@DBName					- A single database to analyze
	@TotalExtents			- (OUTPUT) The total # of extents in the database
	@ChangedExtents			- (OUTPUT) The # of extents changed since the last full backup
	@PercentChanged			- (OUTPUT) The % of changed extents 
	@DiffBackupMB			- (OUTPUT) The estimated resulting size of a DIFF backup in MB
	
* Notes
	Note that after a full backup you will always see some extents
	marked as changed. The number will be 4 + (number of data files - 1).
	These extents contain the file headers of each file plus the
	roots of some of the critical system tables in file 1.
	The number for msdb may be around 20.

*******************************************************************************
* License
*******************************************************************************
  Copyright (C) 2008 Paul S. Randal, SQLskills.com
  All rights reserved.

  For more scripts and sample code, check out 
    http://www.SQLskills.com

  You may alter this code for your own *non-commercial* purposes. You may
  republish altered code as long as you give due credit.
  
  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
  PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20090608	Matt Stanford	Changed to use sysmasterfiles_vw, so that it can work on 2000 too
	20090728	Adam Bean		Fixed for case sensitive collation
	20110923	Matt Stanford	Added new header and added @DiffBackupMB output parameter
******************************************************************************/

CREATE PROCEDURE [dbo].[ShowChangedExtents]
(
	@DBName				NVARCHAR(128)
	,@TotalExtents		INT			= NULL OUTPUT
	,@ChangedExtents	INT			= NULL OUTPUT
	,@PercentChanged	DECIMAL(5,2)= NULL OUTPUT
	,@DiffBackupMB		INT			= NULL OUTPUT
)
AS

BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
		@fileID				INT
		,@fileSizePages		INT
		,@extentID			INT
		,@pageID			INT
		,@DIFFtotal			INT
		,@sizeTotal			INT
		,@total				INT
		,@dbccPageString	VARCHAR (200)
		,@Msg				NVARCHAR(4000)
	
	
	IF DB_ID(@DBName) IS NULL
	BEGIN
		SET @Msg = 'Database "' + @DBName + '" does not exist.  Check yo'' self.';
		RAISERROR(@Msg,16,2)
		RETURN 2
	END

	-- Create the temp table
	IF OBJECT_ID('tempdb.dbo.#DBCCPage') IS NOT NULL
		DROP TABLE #DBCCPage;

	CREATE TABLE #DBCCPage (
		[ParentObject]		VARCHAR (100)
		,[Object]			VARCHAR (100)
		,[Field]			VARCHAR (100)
		,[VALUE]			VARCHAR (100)
	);	

	SELECT @DIFFtotal = 0;
	SELECT @sizeTotal = 0;

	-- Setup a cursor for all online data files in the database
	DECLARE #files CURSOR LOCAL STATIC FOR
	SELECT 
		[file_id]
		,[size] 
	FROM [dbo].[sysmasterfiles_vw]
	WHERE [type_desc] = 'ROWS'
	AND [state_desc] = 'ONLINE'
	AND [database_id] = DB_ID (@DBName);

	OPEN #files;
	FETCH NEXT FROM #files INTO @fileID, @fileSizePages;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @extentID = 0;

		-- The size returned from master.sys.master_files is in
		-- pages - we need to convert to extents
		SELECT @sizeTotal = @sizeTotal + @fileSizePages / 8;

		WHILE (@extentID < @fileSizePages)
		BEGIN

			-- There may be an issue with the DIFF map page position
			-- on the four extents where PFS pages and GAM pages live
			-- (at page IDs 516855552, 1033711104, 1550566656, 2067422208)
			-- but I think we'll be ok.
			-- PFS pages are every 8088 pages (page 1, 8088, 16176, etc)
			-- GAM extents are every 511232 pages
			SELECT @pageID = @extentID + 6;

			-- Build the dynamic SQL
			SELECT @dbccPageString = 'DBCC PAGE ('
				+ @DBName + ', '
				+ CAST (@fileID AS VARCHAR) + ', '
				+ CAST (@pageID AS VARCHAR) + ', 3) WITH TABLERESULTS, NO_INFOMSGS';

			-- Empty out the temp table and insert into it again
			TRUNCATE TABLE #DBCCPage;
			INSERT INTO #DBCCPage EXEC (@dbccPageString);

			-- Aggregate all the changed extents using the function
			SELECT 
				@total = SUM ([dbo].[ConvertToExtents_fn] ([Field]))
			FROM #DBCCPage
			WHERE [VALUE] = '    CHANGED'
			AND [ParentObject] LIKE 'DIFF_MAP%';

			SET @DIFFtotal = @DIFFtotal + @total;

			-- Move to the next GAM extent
			SET @extentID = @extentID + 511232;
		END

		FETCH NEXT FROM #files INTO @fileID, @fileSizePages;
	END;

	-- Clean up
	DROP TABLE #DBCCPage;
	CLOSE #files;
	DEALLOCATE #files;

	-- Output the results
	SELECT
		@TotalExtents		= @sizeTotal
		,@ChangedExtents	= @DIFFtotal
		,@PercentChanged	= ROUND ((CONVERT (FLOAT, @DIFFtotal) /CONVERT (FLOAT, @sizeTotal)) * 100, 2)
		,@DiffBackupMB		= (@DIFFtotal * 64) / 1024;
		
	SET @Msg =        'Database Name:        ' + @DBName + CHAR(13)
	SET @Msg = @Msg + 'Total Extents:        ' + CAST(@TotalExtents AS VARCHAR) + CHAR(13)
	SET @Msg = @Msg + 'Changed Extents:      ' + CAST(@ChangedExtents AS VARCHAR) + CHAR(13)
	SET @Msg = @Msg + 'Percentage Changed:   ' + CAST(@PercentChanged AS VARCHAR) + CHAR(13)
	SET @Msg = @Msg + 'Differential Size MB: ' + CAST(@DiffBackupMB AS VARCHAR) + CHAR(13)
			
	PRINT(@Msg)

END;