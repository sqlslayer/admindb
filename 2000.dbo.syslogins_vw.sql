IF OBJECT_ID('dbo.syslogins_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[syslogins_vw]
GO

/******************************************************************************
* Name
	[dbo].[syslogins_vw]

* Author
	Adam Bean
	
* Date
	2014.02.03
	
* Synopsis
	Bridge view between versions of SQL for syslogins
	
* Description
	Recreates the syslogins data joined to sysxlogins to bring in password_hash

* Examples
	n/a

* Dependencies
	n/a

* Parameters
	n/a
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE VIEW [dbo].[syslogins_vw]

AS

SELECT
	sl.[sid]
	,sl.[status]
	,sl.[createdate]
	,sl.[updatedate]
	,sl.[accdate]
	,sl.[totcpu]
	,sl.[totio]
	,sl.[spacelimit]
	,sl.[timelimit]
	,sl.[resultlimit]
	,sl.[name]
	,sl.[dbname]
	,sl.[password]
	,slx.[password] AS [password_hash]
	,sl.[language]
	,sl.[denylogin]
	,sl.[hasaccess]
	,sl.[isntname]
	,sl.[isntgroup]
	,sl.[isntuser]
	,sl.[sysadmin]
	,sl.[securityadmin]
	,sl.[serveradmin]
	,sl.[setupadmin]
	,sl.[processadmin]
	,sl.[diskadmin]
	,sl.[dbcreator]
	,sl.[bulkadmin]
	,sl.[loginname]
FROM [master].[dbo].[syslogins] sl
LEFT JOIN [master].[dbo].[sysxlogins] slx
	ON slx.[sid] = sl.[sid]