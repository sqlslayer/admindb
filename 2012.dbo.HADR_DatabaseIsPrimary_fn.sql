﻿IF OBJECT_ID('dbo.HADR_DatabaseIsPrimary_fn','FN') IS NOT NULL
        DROP FUNCTION [dbo].[HADR_DatabaseIsPrimary_fn]
GO


/******************************************************************************
* Name
	[dbo].[HADR_DatabaseIsPrimary_fn]

* Author
	Jeff Gogel
	Original Source: http://sqlmag.com/blog/alwayson-availability-groups-and-sql-server-jobs-part-7-detecting-primary-replica-ownership
	
* Date
	2015.05.12
	
* Synopsis
	Function to check if a database belongs to a Primary Availability Group
	
* Description
	Checks to see if the database name passed in belongs to a Primary Availability Group

* Examples
	SELECT [dbo].[HADR_DatabaseIsPrimary_fn] ('admin')

* Dependencies
	[dbo].[sysdatabases_vw]

* Parameters
	@DBName		- Name of database to check
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------

******************************************************************************/
CREATE FUNCTION [dbo].[HADR_DatabaseIsPrimary_fn] (@DBName sysname)
RETURNS bit
AS
BEGIN
	DECLARE @description sysname;

	-- Return 1 if HADR is not enabled
	IF ((SELECT SERVERPROPERTY ('IsHadrEnabled')) = 0)
	BEGIN
		RETURN 1;
	END

	SELECT @description = hars.[role_desc]
	FROM [dbo].[sysdatabases_vw] d
	JOIN [sys].[dm_hadr_availability_replica_states] hars 
		ON d.[replica_id] = hars.[replica_id]
	WHERE [database_id] = DB_ID(@DBName);

	IF @description = 'PRIMARY'
			RETURN 1; -- primary

	RETURN 0; -- not primary
END