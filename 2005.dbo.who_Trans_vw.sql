IF OBJECT_ID('dbo.who_Trans_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[who_Trans_vw]
GO

/******************************************************************************
* Name
	[dbo].[who_Trans_vw]

* Author
	Adam Bean
	
* Date
	2013.10.18
	
* Synopsis
	View to see current database transactions
	
* Description
	n/a

* Examples
	SELECT * FROM dbo.[who_Trans_vw]

	SELECT * FROM dbo.[who_Trans_vw] t 
	JOIN dbo.[who_vw] w 
		ON t.session_id = w.SPID

* Dependencies
	n/a

* Parameters
	n/a
	
* Notes
	If required

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE VIEW [dbo].[who_Trans_vw]

AS

SELECT
	st.[session_id]
	,DB_NAME(dt.[database_id]) AS [DBName]
	,CASE dt.[database_transaction_type]
		WHEN 1 THEN 'Read/write transaction'
		WHEN 2 THEN 'Read-only transaction'
		WHEN 3 THEN 'System transaction'
		ELSE CAST(dt.[database_transaction_type] AS VARCHAR)
	END AS [TranType]
	,CASE dt.[database_transaction_state]
		WHEN 1 THEN 'The transaction has not been initialized.'
		WHEN 3 THEN 'The transaction has been initialized but has not generated any log records.'
		WHEN 4 THEN 'The transaction has generated log records.'
		WHEN 5 THEN 'The transaction has been prepared.'
		WHEN 10 THEN 'The transaction has been committed.'
		WHEN 11 THEN 'The transaction has been rolled back.'
		WHEN 12 THEN 'The transaction is being committed. In this state the log record is being generated, but it has not been materialized or persisted.'
		ELSE CAST(dt.[database_transaction_state] AS VARCHAR)
	END AS [TranState]
	,dt.*
FROM [sys].[dm_tran_session_transactions] st
JOIN [sys].[dm_tran_database_transactions] dt
	ON st.[transaction_id] = dt.[transaction_id]