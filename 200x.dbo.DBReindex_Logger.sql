IF OBJECT_ID('[dbo].[DBReindex_Logger]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[DBReindex_Logger]
GO

/*******************************************************************************************************
**	Name:			dbo.DBReindex_Logger
**	Desc:			Manages the logging for the dbo.DBReindex procedures
**	Auth:			Matt Stanford (SQLSlayer.com)		
**  Debug:			EXEC dbo.DBReindex_Logger 'DB','Table','Index',0,10,10,50,'REBUILD'
**	Date:			2007.08.27
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20080303	Matt Stanford	Small change to the ISNULL() function to set the default fill factor to 255 instead of 0
**  20090615	Adam Bean		Fixed fillfactor statements on indexes as they broke in 2000
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE PROCEDURE [dbo].[DBReindex_Logger] 
(
	@DBName								[NVARCHAR](128)
	,@TableName							[NVARCHAR](128)
	,@IndexName							[NVARCHAR](128)			
	,@FillFactor						[TINYINT]
	,@avg_page_space_used_in_percent	[FLOAT]
	,@avg_record_size_in_bytes			[FLOAT]
	,@Fragmentation						[FLOAT]
	,@CommandIssued						[NVARCHAR](128)
)

AS

SET NOCOUNT ON

DECLARE 
	@IndexID							[INT]
	,@FF								[TINYINT]

--	PRINT('/* Debugging Logging */')
--	PRINT('/* Logging - Database: ' + @DatabaseName)
--	PRINT('Logging - Table: ' + @TableName)
--	PRINT('Logging - Index: ' + @IndexName)
--	PRINT('Logging - FillFactor: ' + CAST(@FillFactor AS VARCHAR))
--	PRINT('Logging - SpaceUsed: ' + CAST(@avg_page_space_used_in_percent AS VARCHAR))
--	PRINT('Logging - RecordSize: ' + CAST(@avg_record_size_in_bytes AS VARCHAR))
--	PRINT('Logging - Fragmentation: ' + CAST(@Fragmentation AS VARCHAR))
--	PRINT('Logging - CommandIssued: ' + @CommandIssued + ' */')
-- RETURN
--DROP TABLE [dbo].[IndexStats_IndexFragmentation]
--DROP TABLE [dbo].[IndexStats_IndexMaster]
--DROP VIEW dbo.IndexStats_vw

/* Setup logging table */
IF OBJECT_ID('DatabaseMaintenance_IndexStats_IndexMaster','U') IS NULL
BEGIN
CREATE TABLE [dbo].[DatabaseMaintenance_IndexStats_IndexMaster]
(
	[IndexID]		[INT] IDENTITY
	,[DatabaseName]	[NVARCHAR](128)	NOT NULL
	,[TableName]	[NVARCHAR](128)	NOT NULL
	,[IndexName]	[NVARCHAR](128)	NOT NULL
	,[FillFactor]	[TINYINT] NULL
	,[DateCreated]	[DATETIME] NOT NULL CONSTRAINT [DF_DatabaseMaintenance_IndexStats_IndexMaster_DateCreated]  DEFAULT (GETDATE())
	,[LastUpdated]	[DATETIME] CONSTRAINT [DF_DatabaseMaintenance_IndexStats_IndexMaster_LastUpdated]  DEFAULT (GETDATE())
) ON [PRIMARY];

ALTER TABLE [dbo].[DatabaseMaintenance_IndexStats_IndexMaster] ADD CONSTRAINT
	PK_DatabaseMaintenance_IndexStats_IndexMaster PRIMARY KEY NONCLUSTERED 
	(
	IndexID,
	DateCreated
	);

ALTER TABLE [dbo].[DatabaseMaintenance_IndexStats_IndexMaster] ADD CONSTRAINT
	IX_DatabaseMaintenance_IndexStats_IndexMaster_Unique UNIQUE NONCLUSTERED 
	(
	DatabaseName,
	TableName,
	IndexName
	);

CREATE UNIQUE NONCLUSTERED INDEX [IX_DatabaseMaintenance_IndexStats_IndexMaster_IndexID]
	ON [dbo].[DatabaseMaintenance_IndexStats_IndexMaster] (IndexID ASC)
	WITH FILLFACTOR = 100;

CREATE CLUSTERED INDEX CI_DatabaseMaintenance_IndexStats_IndexMaster_IndexID_DateCreated 
	ON [dbo].[DatabaseMaintenance_IndexStats_IndexMaster] (IndexID ASC, DateCreated ASC)
	WITH FILLFACTOR = 100;

END

IF OBJECT_ID('DatabaseMaintenance_IndexStats_IndexFragmentation','U') IS NULL
BEGIN
CREATE TABLE [dbo].[DatabaseMaintenance_IndexStats_IndexFragmentation]
(
	[IndexID]							[INT] CONSTRAINT [FK_DatabaseMaintenance_IndexStats_IndexMaster_IndexID] REFERENCES [dbo].[DatabaseMaintenance_IndexStats_IndexMaster](IndexID)
	,[avg_page_space_used_in_percent]	[FLOAT] NOT NULL
	,[avg_record_size_in_bytes]			[FLOAT]	NOT NULL
	,[Fragmentation]					[FLOAT]	NOT NULL
	,[CommandIssued]					[VARCHAR](50) NOT NULL
	,[DateCreated]						[DATETIME] CONSTRAINT [DF_DatabaseMaintenance_IndexStats_IndexFragmentation_DateCreated]  DEFAULT (getdate())
);

CREATE CLUSTERED INDEX [CI_DatabaseMaintenance_IndexStats_IndexFragmentation_DateCreated] 
	ON [dbo].[DatabaseMaintenance_IndexStats_IndexFragmentation]([DateCreated] ASC)
	WITH FILLFACTOR = 85;

CREATE NONCLUSTERED INDEX [IX_DatabaseMaintenance_IndexStats_IndexFragmentation_IndexID ]
	ON [dbo].DatabaseMaintenance_IndexStats_IndexFragmentation ([IndexID] ASC)

END

IF OBJECT_ID('DatabaseMaintenance_IndexStats_vw','V') IS NULL
BEGIN

	EXEC('
	CREATE VIEW [dbo].[DatabaseMaintenance_IndexStats_vw]
	AS

	SELECT 
		m.[DatabaseName]
		,m.[TableName]
		,m.[IndexName]
		,m.[FillFactor]
		,m.[LastUpdated] AS [FillFactorLastUpdated]
		,f.[Fragmentation]
		,f.[CommandIssued]
		,f.[avg_page_space_used_in_percent]
		,f.[avg_record_size_in_bytes]
		,f.[DateCreated]
	FROM [dbo].[DatabaseMaintenance_IndexStats_IndexMaster] m
	INNER JOIN [dbo].[DatabaseMaintenance_IndexStats_IndexFragmentation] f
		ON m.[IndexID] = f.[IndexID]
	')

END

-- Look up the IndexID and the FillFactor of the existing record
SELECT 
	@IndexID = MAX(IndexID) 
	,@FF = MAX(ISNULL([FillFactor],255))
FROM [dbo].[DatabaseMaintenance_IndexStats_IndexMaster] 
WHERE [DatabaseName] = @DBName
AND [TableName] = @TableName
AND [IndexName] = @IndexName

-- Insert the new master record
IF @IndexID IS NULL
BEGIN
	INSERT INTO [dbo].[DatabaseMaintenance_IndexStats_IndexMaster] (
		[DatabaseName]
		,[TableName]
		,[IndexName]
		,[FillFactor]
		,[DateCreated]
		,[LastUpdated]
	)
	VALUES (
		@DBName
		,@TableName
		,@IndexName
		,@FillFactor
		,getdate()
		,getdate()
	)
	SET @IndexID = SCOPE_IDENTITY()
END
ELSE
BEGIN
	-- Update the FillFactor
	IF @FF <> @FillFactor
	BEGIN
		UPDATE [dbo].[DatabaseMaintenance_IndexStats_IndexMaster] 
		SET [FillFactor] = @FillFactor, [LastUpdated] = GETDATE()
		WHERE [IndexID] = @IndexID
	END
END

-- Now Insert a Fragmentation record
INSERT INTO [dbo].[DatabaseMaintenance_IndexStats_IndexFragmentation] 
(
	[IndexID]
	,[avg_page_space_used_in_percent]
	,[avg_record_size_in_bytes]
	,[Fragmentation]
	,[CommandIssued]
	,[DateCreated]
)
VALUES 
(
	@IndexID
	,@avg_page_space_used_in_percent
	,@avg_record_size_in_bytes
	,@Fragmentation
	,@CommandIssued
	,getdate()
)

SET NOCOUNT OFF

-- Return status
IF @@ERROR <> 0
	RETURN 2
ELSE
	RETURN 0
