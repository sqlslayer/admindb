IF OBJECT_ID('[dbo].[ExecutionPlanAttributes_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[ExecutionPlanAttributes_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.ExecutionPlanAttributes_vw
**	Desc:			View to retrieve the query plan cache
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Dependancies:	[dbo].[CacheKeyAttributesWide_fn]
**	Date:			2010.10.21
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20101117	Matt Stanford	Stole most of the base logic from the old ExecutionPlans_vw object.  (Credit: Adam Bean)
********************************************************************************************************/

CREATE VIEW [dbo].[ExecutionPlanAttributes_vw]

AS

SELECT
	CASE
		WHEN ka.[dbid] = 32767 THEN 'ResourceDB'
		ELSE DB_NAME(ka.[dbid])		
	END												AS [DBName]
	,DB_NAME(ka.[dbid_execute]) 					AS [DBNameExecuted]
	,OBJECT_SCHEMA_NAME(
		ka.[objectid]
		,COALESCE(est.[dbid],ka.[dbid])
	)												AS [SchemaName]
	,OBJECT_NAME(
		ka.[objectid]
		,COALESCE(est.[dbid],ka.[dbid])
	)												AS [ObjectName]
	,p.[cacheobjtype]								AS [CacheObjType]
	,p.[objtype]									AS [ObjectType]
	,p.[usecounts]									AS [UseCounts]
	,p.[refcounts]									AS [RefCounts]
	,p.[size_in_bytes]								AS [SizeInBytes]
	,CASE ISNULL(est.[encrypted],0) 
		WHEN 0 THEN 'NO' 
		ELSE 'YES' 
	END												AS [IsEncrypted]
	,est.[text]										AS [Text]
	,p.[plan_handle]								AS [PlanHandle]
	,ka.*
FROM sys.dm_exec_cached_plans p
OUTER APPLY sys.dm_exec_sql_text (p.plan_handle) est
OUTER APPLY [dbo].[CacheKeyAttributesWide_fn](p.plan_handle) ka

