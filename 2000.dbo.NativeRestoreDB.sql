IF OBJECT_ID('[dbo].[NativeRestoreDB]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[NativeRestoreDB]
GO

/*******************************************************************************************************
**	Name:			dbo.NativeRestoreDB
**	Desc:			Restore database based on file name (SQL2000)
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Usage:			EXEC dbo.NativeRestoreDB 'E:\Trace90.bak', 3, null, null, null, 1
**	Date:			2007.06.04
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20071125	Matt Stanford	Changed the 90 version to use the DBO schema
**  20090625	Adam Bean		Added @ChangeDBOwner & @NewDBOwner
**	20090626	Matt Stanford	Added @DropDB parameter to KillProcess call
**	20090629	Matt Stanford	Added @ShowProcesses parameter to KillProcess call
**  20090728	Adam Bean		Fixed for case sensitive collation
**	20090929	Adam Bean		Global header formatting & general clean up
**	20091014	Adam Bean		Resolved issue with changing owner if database was backed up in single user/read only
**	20110503	Adam Bean		Set @DeleteHistory = 0 when calling KillProcess to preserve backup/restore history
**	20100316	John DelNostro	Added User Cleanup options
**  20110616	John DelNostro	Added option for adding standard nt group permissions (useful for new db restores)
**	20120201	Adam Bean		Added @DeleteHistory for KillProcesses call
********************************************************************************************************/

CREATE PROCEDURE dbo.NativeRestoreDB 
(
	@filename					VARCHAR(512)
	,@RestoreFreshnessHours		TINYINT			= 0
	,@NewDBName					VARCHAR(512)	= NULL
	,@DestDataPath				VARCHAR(512)	= NULL
	,@DestLogPath				VARCHAR(512)	= NULL
	,@ChangeDBOwner				BIT				= 1
	,@NewDBOwner				VARCHAR(48)		= NULL
	,@CleanupUsers				BIT				= 0
	,@RestartRestore			BIT				= 0
	,@AddStandardPermissions	BIT				= 0
	,@Domain					NVARCHAR(25)    = NULL
	,@DeleteHistory				BIT				= 0
	,@Debug						BIT				= 0
	
)

AS

SET NOCOUNT ON

--
--declare 
--	@DestLogPath varchar(512),
--	@DestDataPath varchar(512),
--	@filename varchar(512),
--	@RestoreFreshnessHours tinyint,
--	@Debug tinyint


declare
	@DBName varchar(512),
	@SQL nvarchar(4000),
	@tempSQL varchar(1000),
	@killSQL varchar(1000),
	@sort int,
	@didDelete tinyint,
	@didRestore tinyint,
	@scriptKickoff datetime,
	@LastRestoreDate datetime,
	@intentionallySkipped tinyint,
	@CompatLevel tinyint

--set @filename = 'E:\Trace.bak'
--
----set @DestLogPath = 'K:\SQLLogs\Default\'
----set @DestDataPath = 'M:\SQLData\Default\'
--SET @RestoreFreshnessHours = 0
--set @Debug = 1

set @RestoreFreshnessHours = isnull(@RestoreFreshnessHours,0)

set @scriptKickoff = getdate()
set @didDelete = 0
set @didRestore = 0
set @intentionallySkipped = 0

IF cast(left(cast(SERVERPROPERTY('productversion') as varchar(10)),1) + '0' as tinyint) <> 80
	RAISERROR('dbo.Native80RestoreDB is only compatible with version 8.0 of SQL Server',20,1) with LOG

declare @cmds table (
	sort int,
	SQL varchar(4000)
)

-- Remove old crusty temp tables
--if EXISTS (SELECT name FROM tempdb.dbo.sysobjects where name like '#files%')
--	DROP TABLE #files
--if EXISTS (SELECT name FROM tempdb.dbo.sysobjects where name like '#header%')
--	DROP TABLE #header

create table #files (
	LogicalName varchar(256),
	PhysicalName varchar(512),
	Type varchar(10),
	FileGroupName varchar(100),
	Size bigint,
	MaxSize bigint)

create table #header (
	bacnam nvarchar(100),
	bacdes nvarchar(100),
	bactyp nvarchar(100),
	expdat datetime,
	compre nvarchar(100),
	positi nvarchar(100),
	devtyp nvarchar(100),
	usenam nvarchar(100), 
	sernam nvarchar(100),
	datnam nvarchar(100), -- 10
	datver nvarchar(100),
	datcre datetime,
	bacsiz nvarchar(100), -- 13
	firlsn nvarchar(100),
	laslsn nvarchar(100),
	chelsn nvarchar(100),
	difbas nvarchar(100),
	bacsta datetime,		
	bacfin datetime,
	sorord nvarchar(100), -- 20
	codpag nvarchar(100),
	uniloc nvarchar(100),
	unicom nvarchar(100),
	comlev nvarchar(100),
	sofven nvarchar(100), --
	sofvma nvarchar(100),
	sofvmi nvarchar(100),
	sofvbu nvarchar(100),
	macnam nvarchar(100),
	flags  nvarchar(100), -- 30
	bindin nvarchar(100),
	recfor nvarchar(100),
	collat nvarchar(100))
	
--restore headeronly from disk = 'E:\Trace.bak'
-- Get the Backup file header info
SET @SQL = 'RESTORE HEADERONLY FROM DISK = ''' + @filename + ''''

--PRINT (@SQL)
insert into #header EXEC (@SQL)

select 
	@DBName = datnam,
	@CompatLevel = cast(comlev as tinyint) 
from 
	#header 
where 
	bactyp = 1 
and
	bacfin = (SELECT MAX(bacfin) FROM #header)

-- Test to see if the backup will process OK
if (@CompatLevel > cast(left(cast(SERVERPROPERTY('productversion') as varchar(10)),1) + '0' as tinyint)) or (@CompatLevel IS NULL)
	RAISERROR('Backup type or compatibility level mismatch',20,1) with LOG

-- Get the list of files in the backup
SET @SQL = 'RESTORE FILELISTONLY FROM DISK = ''' + @filename + ''''

insert into #files exec (@SQL)

-- Base restore command
insert into @cmds (sort,SQL) values (1,'RESTORE DATABASE [' + @DBName + ']')

insert into @cmds (sort,SQL) values (2, 'FROM DISK = ''' + @filename + '''')

insert into @cmds (sort,SQL) values (9, 'WITH REPLACE')

insert into @cmds (sort,SQL) values (10, ',RECOVERY')

--- DETERMINE THE DATA/LOG DIRECTORIES
if (@DestLogPath is null) or (@DestDataPath is null)
begin
	
	-- Find existing database, use existing locations
	if exists (select name from master.dbo.sysdatabases where name = @DBName)
	BEGIN

		-- Get the log file directory
		SET @SQL = 'SELECT @in_dir = SUBSTRING(filename,0,LEN(filename)-CHARINDEX(''\'',REVERSE(replace(filename,'' '','''')))+2) FROM [' + @DBName + '].dbo.sysfiles WHERE fileid = 2'
		EXEC sp_executesql @SQL, N'@in_dir VARCHAR(512) OUTPUT', @in_dir = @DestLogPath OUTPUT

		-- Get the data file directory
		SET @SQL = 'SELECT @in_dir = SUBSTRING(filename,0,LEN(filename)-CHARINDEX(''\'',REVERSE(replace(filename,'' '','''')))+2) FROM [' + @DBName + '].dbo.sysfiles WHERE fileid = 1'
		EXEC sp_executesql @SQL, N'@in_dir VARCHAR(512) OUTPUT', @in_dir = @DestDataPath OUTPUT

	END
	ELSE
	BEGIN
		-- Get the Default data/log directories

		select @DestLogPath = admin.dbo.SQLServerLogDir_fn()

		select @DestDataPath = admin.dbo.SQLServerDataDir_fn()
	END
end


-- Make sure the Data/Log paths contain a trailing "\"
if right(@DestLogPath,1) <> '\'
	set @DestLogPath = @DestLogPath + '\'

if right(@DestDataPath,1) <> '\'
	set @DestDataPath = @DestDataPath + '\'

-- ADD the MOVE commands to the command list
insert into @cmds (sort,SQL)
select 
	10
	,case [Type]
	When 'L' then ',MOVE ''' + LogicalName + ''' to ''' + 
		@DestLogPath + SUBSTRING(PhysicalName,LEN(PhysicalName) - CHARINDEX('\',REVERSE(PhysicalName)) + 2,100) + ''''
	ELSE ',MOVE ''' + LogicalName + ''' to ''' + 
		@DestDataPath + SUBSTRING(PhysicalName,LEN(PhysicalName) - CHARINDEX('\',REVERSE(PhysicalName)) + 2,100) + ''''
	end as MoveCommand
from #files

drop table #files
drop table #header

PRINT ('Successfully read backup information for ' + @DBName + char(10) + '     Kicking off restore logic now')

-- Check to make sure that we didn't just restore this guy
set @LastRestoreDate = isnull((select max(restore_date) from msdb.dbo.restorehistory where destination_database_name = @DBName),dateadd(year,-1,getdate()))

if (dateadd(hour,@RestoreFreshnessHours,@LastRestoreDate) < @scriptKickoff) or @RestoreFreshnessHours = 0
BEGIN
	print('Last restore was more than ' + cast(@RestoreFreshnessHours as varchar(3)) + ' hours ago.  We''re doing a restore')

	-------------------------------------------------------
	-- OK, the commands have been built, now throw them all together
	-------------------------------------------------------
	declare curs1 cursor for 
		select sort,SQL from @cmds order by sort asc

	open curs1

	fetch next from curs1 into @sort,@tempSQL

	SET @SQL = ''
	WHILE @@FETCH_STATUS = 0
	BEGIN

		set @SQL = @SQL + @tempSQL

		set @SQL = @SQL + char(10) + char(9)

		fetch next from curs1 into @sort,@tempSQL
	END

	close curs1
	deallocate curs1

	SET @killSQL = 'print(''Killing the database'')
	EXEC [dbo].[KillProcess] @DBName = ''' + @DBName + ''', @DropDB = 1, @ShowProcesses = 0, @DeleteHistory = ' + CAST(@DeleteHistory AS VARCHAR) + '
	print(''    Killing done'')
	print(''Restoring ' + @DBName + ''' + char(10))' + char(10) + char(10) 

	-- Check to see if the database exists
	if exists(select * from master.dbo.sysdatabases where name = @DBName)
	BEGIN
		if @Debug = 1
			print (@killSQL)
		else
			exec (@killSQL)

		if (select count(*) from master.dbo.sysdatabases where name = @DBName) = 0
			set @didDelete = 1
	END

	if @Debug = 1
		print (@SQL)
	else
		exec (@SQL)

	set @LastRestoreDate = isnull((select max(restore_date) from msdb.dbo.restorehistory where destination_database_name = @DBName),dateadd(year,-1,getdate()))

	-- Test to see if the restore worked
	if @LastRestoreDate >= @scriptKickoff
	BEGIN
		set @didRestore = 1
	END
END
ELSE
BEGIN
	print('Did not restore anything... the last restore was pretty recent')
	SET @intentionallySkipped = 1
END

IF @ChangeDBOwner = 1
BEGIN
	-- If new owner was specified, use that, if not, determine primary sa account
	IF @NewDBOwner IS NULL
		SET @NewDBOwner = (SELECT dbo.WhatIsGodsName_fn())
		
	-- Make sure the database isn't in single user or read only
	IF (SELECT [user_access_desc] FROM [sysdatabases_vw] WHERE [name] = @DBName) = 'SINGLE_USER'
		EXEC('ALTER DATABASE [' + @DBName + '] SET MULTI_USER')
	IF (SELECT [is_read_only] FROM [sysdatabases_vw] WHERE [name] = @DBName) = 1
		EXEC('ALTER DATABASE [' + @DBName + '] SET READ_WRITE')
			
	-- Change owner
	SET @SQL = 'USE [' + @DBName + '] EXEC [sp_changedbowner] @loginame = ''' + @NewDBOwner + ''''
	IF @Debug = 1
		PRINT (@SQL)
	ELSE
		EXEC (@SQL)
END

-- Fix orphaned users
IF @CleanupUsers = 1
	EXEC [dbo].[UserCleanup] @DBName = @DBName

-- Add back standard nt permissions
IF @AddStandardPermissions = 1 AND @Domain IS NOT NULL
EXEC [dbo].[StandardGroupPermissions] 
	@DBName = @DBName
    ,@Domain = @Domain


select 
	@didDelete as DidDelete, 
	@didRestore as DidRestore, 
	@intentionallySkipped as IntentionallySkipped,
	@LastRestoreDate as LastRestoreDateTime

set nocount off