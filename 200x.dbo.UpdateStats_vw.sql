IF OBJECT_ID('[dbo].[UpdateStats_vw]','V') IS NOT NULL 
	DROP VIEW [dbo].[UpdateStats_vw]
GO

/******************************************************************************
* Name
	[dbo].[UpdateStats_vw]

* Author
	Adam Bean
	
* Date
	2011.09.13
	
* Synopsis
	View to bring together the maintenance data for DBCC CHECKDB.
	
* Description
	View which joins data from all normalized tables which are logged to as part of the CheckDB procedure

* Examples
	SELECT * FROM [dbo].[UpdateStats_vw]

* Dependencies
	The following tables: 
	[dbo].[Maintenance_UpdateStats_Log]
	[dbo].[Maintenance_DatabaseIDs]
	[dbo].[Maintenance_SchemaIDs]
	[dbo].[Maintenance_TableIDs]
	[dbo].[Maintenance_CommandIDs]
	[dbo].[Maintenance_RunIDs]

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20140217	Adam Bean		Added StatID. Being that this data wasn't originally gathered, have to LEFT JOIN it.
								Added RowsChanged.
	20140222	Adam Bean		Added DateStart, DateEnd, Duration, BatchDateStart, BatchDateEnd, BatchDuration, RunID
	20140301	Adam Bean		Added LastStatsUpdate
******************************************************************************/

CREATE VIEW [dbo].[UpdateStats_vw]

AS

SELECT
	d.[DBName]
	,s.[SchemaName]
	,t.[TableName]
	,i.[StatName]
	,u.[RowsChanged]
	,u.[RowCount]
	,u.[PctChange]
	,u.[LastStatsUpdate]
	,c.[Command]
	,u.[IsOverride]
	,u.[DateStart]
	,u.[DateEnd]
	,DATEDIFF(SECOND, u.[DateStart], u.[DateEnd])	AS [Duration]
	,r.[DateStart]									AS [BatchDateStart]
	,r.[DateEnd]									AS [BatchDateEnd]
	,DATEDIFF(SECOND, r.[DateStart], r.[DateEnd])	AS [BatchDuration]
	,r.[RunID]
FROM [dbo].[Maintenance_UpdateStats] u
JOIN [dbo].[Maintenance_DatabaseIDs] d
	ON d.[DatabaseID] = u.[DatabaseID]
JOIN [dbo].[Maintenance_SchemaIDs] s
	ON s.[SchemaID] = u.[SchemaID]
JOIN [dbo].[Maintenance_TableIDs] t
	ON t.[TableID] = u.[TableID]
LEFT JOIN [dbo].[Maintenance_StatIDs] i
	ON i.[StatID] = u.[StatID]
JOIN [dbo].[Maintenance_CommandIDs] c
	ON c.[CommandID ] = u.[CommandID]
JOIN [dbo].[Maintenance_RunIDs] r
	ON r.[RunID] = u.[RunID]