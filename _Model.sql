IF OBJECT_ID('<SchemaName,NVARCHAR(128),dbo>.<ObjectName,NVARCHAR(128),ObjectName>','<ShortObjectType,VARCHAR(2),P>') IS NOT NULL 
	DROP <FullObjectType,VARCHAR(16),PROCEDURE> [<SchemaName,NVARCHAR(128),dbo>].[<ObjectName,NVARCHAR(128),ObjectName>]
GO

/******************************************************************************
* Name
	[<SchemaName,VARCHAR(8),dbo>].[<ObjectName,VARCHAR(24),ObjectName>]

* Author
	<Author,VARCHAR(128),Matt Stanford>
	
* Date
	<Date,YYYY.MM.DD,1955.11.05>
	
* Synopsis
	Short, one sentence description.
	
* Description
	Long, detailed description

* Examples
	EXEC [<SchemaName,NVARCHAR(128),dbo>].[<ObjectName,NVARCHAR(128),ObjectName>]

* Dependencies
	If available.  List one per line.

* Parameters
	@Parameter1			- Description of @Parameter1
	@Parameter2			- Description of @Parameter2
	
* Notes
	If required

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE <FullObjectType,VARCHAR(16),PROCEDURE> [<SchemaName,NVARCHAR(128),dbo>].[<ObjectName,NVARCHAR(128),ObjectName>]
(
)

AS