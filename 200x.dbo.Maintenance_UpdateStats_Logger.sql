IF OBJECT_ID('[dbo].[Maintenance_UpdateStats_Logger]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[Maintenance_UpdateStats_Logger]
GO

/******************************************************************************
* Name
	[dbo].[Maintenance_UpdateStats_Logger]

* Author
	Adam Bean
	
* Date
	2011.09.01
	
* Synopsis
	Logging procedure which supports UpdateStats
	
* Description
	Procedure to normalize and log all appropriate data surrounding the UpdateStats maintenance procedure.

* Examples
	Do not call this procedure directly, it will be utilized within the maintenance routines.

* Dependencies
	[dbo].[Maintenance_GetDatabaseID] 
	[dbo].[Maintenance_GetSchemaID]
	[dbo].[Maintenance_GetTableID]]
	[dbo].[Maintenance_GetCommandID]
	[dbo].[Maintenance_GetRunID] 
	[dbo].[Maintenance_UpdateStats]

* Parameters
	@DBName = Database that had UpdateStats run on it
	@SchemaName = Schema that had UpdateStats run on it
	@TableName = Table that had UpdateStats run on it
	@PctChange = The percent of change since the last stats update
	@RowCount = The amount of rows per table
	@Command = The CHECK and WITH options run with CHECKDB
	@Override = Specifies that an override via extended properties was utilized
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20140217	Adam Bean		Added @StatName & StatID to support changes to UpdateStats
	20140222	Adam Bean		Added @DateStart and @DateEnd
								Removed @RunID logic from this procedure as it's now done in the calling Reindex procedure
	20140301	Adam Bean		Added LastStatsUpdate
******************************************************************************/

CREATE PROCEDURE [dbo].[Maintenance_UpdateStats_Logger]
(
	@DBName				NVARCHAR(128)
	,@SchemaName		NVARCHAR(128)
	,@TableName			NVARCHAR(128)
	,@StatName			NVARCHAR(128)
	,@PctChange			DECIMAL
	,@RowCount			BIGINT
	,@RowsChanged		BIGINT
	,@LastStatsUpdate	DATETIME
	,@Command			VARCHAR(256)
	,@Override			BIT
	,@DateStart			DATETIME
	,@DateEnd			DATETIME
	,@RunID				INT
)

AS

SET NOCOUNT ON

DECLARE 	
	@DatabaseID		INT
	,@SchemaID		INT
	,@TableID		INT
	,@StatID		INT
	,@CommandID		INT

-- Get the database ID
EXEC [dbo].[Maintenance_GetDatabaseID] 
	@DBName = @DBName
	,@DatabaseID = @DatabaseID OUTPUT

-- Get the schema ID
EXEC [dbo].[Maintenance_GetSchemaID] 
	@SchemaName = @SchemaName
	,@SchemaID = @SchemaID OUTPUT
	
-- Get the table ID
EXEC [dbo].[Maintenance_GetTableID] 
	@TableName = @TableName
	,@TableID = @TableID OUTPUT

-- Get the stat ID
EXEC [dbo].[Maintenance_GetStatID] 
	@StatName = @StatName
	,@StatID = @StatID OUTPUT

-- Get the command ID
EXEC [dbo].[Maintenance_GetCommandID] 
	@Command = @Command
	,@CommandID = @CommandID OUTPUT
	
-- Insert the values
INSERT INTO [dbo].[Maintenance_UpdateStats]
([DatabaseID], [SchemaID], [TableID], [StatID], [PctChange], [RowCount], [RowsChanged], [LastStatsUpdate], [CommandID], [IsOverride], [RunID], [DateStart], [DateEnd])
SELECT
	@DatabaseID
	,@SchemaID
	,@TableID
	,@StatID
	,@PctChange
	,@RowCount
	,@RowsChanged
	,@LastStatsUpdate
	,@CommandID
	,@Override
	,@RunID
	,@DateStart
	,@DateEnd

SET NOCOUNT OFF