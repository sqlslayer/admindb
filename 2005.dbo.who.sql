IF OBJECT_ID('[dbo].[who]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[who]
GO


/*******************************************************************************
* Name
	dbo.who

* Author
	Adam Bean (SQLSlayer.com)
	
* Date
	2007.12.10

* Synopsis
	Procedure to retrieve information about sql connections (uses DBCC INPUTBUFFER)

* Description
	The dbo.who procedure is modeled after the sp_who and sp_who2 procedures.  This is, of course, an
	enhanced version of those base procedures.
	
	The goal of this procedure is to provide insight into what is happening on the SQL server, who is connected,
	what are they doing, how did they connect and from where did they connect.

* Examples
	EXEC dbo.who 
	EXEC dbo.who @DBName = 'master'

* Dependencies
	dbo.Split_fn

* Parameters
	@OrderBy1			- Columns to order output by
	@OrderBy2			- Columns to order output by
	@OrderBy3			- Columns to order output by
	@SPID				- Spid(s) to find connections for, CSV supported
	@SPIDExclude		- Spid(s) to exclude connections for, CSV supported
	@DBName				- Database(s) to find connections for, CSV supported
	@DBNameExclude		- Database(s) to exclude connections for for, CSV supported
	@Login				- Login(s) to find connections for, CSV supported
	@LoginExclude		- Login(s) to exclude connections for, CSV supported
	@HostName			- Host(s) to find connections for, CSV supported
	@HostNameExclude	- Host(s) to exclude connections for, CSV supported
	@FilterSpid			- Starting spid in which to filter on, defaulted to 50 (<50 = system)

* Notes
	The logic to gather the current query plan was inspired from beta_lockinfo - Erland Sommarskog: http://www.sommarskog.se/sqlutil/beta_lockinfo.html

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20071218	Matt Stanford	Wrapped ISNULL around Tran and Lock Count
	20080213	Adam Bean		Using DBCC INPUTBUFFER to query information instead of DMV's
	20080214	Adam Bean		Procedure will no longer return session running procedure
	20080214	Adam Bean		Suppressed informational messages on DBCC output
	20080222	Adam Bean		Added additional fields from who_vw
	20080408	Adam Bean		Added sys.sysprocesses cmd
	20080918	Adam Bean		Updated procedure to match changes to who_vw
	20090330	Adam Bean		Added logging
	20090623	Adam Bean		Changed @OrderBy parameters to INT to remove SQL Injection
	20090702	Matt Stanford	Expanded out the Program_Name column so that we can actually read it
								Pivoted out the lock info into separate columns
	20090903	Adam Bean		Changed estimated completion time to minutes
	20090922	Adam Bean		Added @DBName, @LoginName and @HostName and Excludes
	20090929	Adam Bean		Global header formatting & general clean up
	20100117	Adam Bean		Added LTRIM/RTRIM to several columns
	20100205	Adam Bean		Added net_transport and auth_scheme
	20100308	Matt Stanford	Added IsolationLevel
	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
	20110705	Adam Bean		Fixed SPID for case sensitive collations
	20120619	Adam Bean		Resolved column name misspell on [IsolationLevel] for logging
	20121119	Matt Stanford	Added the blocking chain columns, changed the sort order to show blockers first
								Wrapped the DBCC INPUTBUFFER in a TRY CATCH in case the SPIDs go away
	20131018	Adam Bean		Added CurrentPlan
	20130406	Adam Bean		Fixed CurrentPlan by using logic from beta_lockinfo (see notes)
								Removed @Logging and general cleanup
	20140512	Adam Bean		Fixed collation issues with @CurSpid. Added @SPID and @SPIDExclude 
	20161228	Paul Popovich	Remove and change all references to sysprocess to dm_exec_requests thanks to 2016 upgrade advisor, removed kpid entirely
*******************************************************************************/

CREATE	 PROCEDURE [dbo].[who]
    (
      @OrderBy1 INT = 1 -- SPID
      ,
      @OrderBy2 INT = 6 -- Host
      ,
      @OrderBy3 INT = 3 -- DatabaseName
      ,
      @SPID VARCHAR(32) = NULL ,
      @SPIDExclude VARCHAR(32) = NULL ,
      @DBName VARCHAR(512) = NULL ,
      @DBNameExclude VARCHAR(512) = NULL ,
      @Login VARCHAR(512) = NULL ,
      @LoginExclude VARCHAR(512) = NULL ,
      @HostName VARCHAR(512) = NULL ,
      @HostNameExclude VARCHAR(512) = NULL ,
      @FilterSpid INT = 50
    )
AS
    SET NOCOUNT ON;

    DECLARE @CurSpid INT ,
        @request_id INT ,
        @handle VARBINARY(64) ,
        @stmt_start INT ,
        @stmt_end INT;

    IF OBJECT_ID('tempdb.dbo.#who') IS NOT NULL
        DROP TABLE #who;

-- Setup table to hold spid info
    DECLARE @QueryCmd TABLE
        (
          [ID] INT IDENTITY ,
          [SPID] INT ,
          [EventType] VARCHAR(100) ,
          [Parameters] INT ,
          [Command] VARCHAR(MAX)
        );

-- Setup table to temporary data
    CREATE TABLE #who
        (
          [SPID] SMALLINT ,
          [KPID] SMALLINT ,
          [DBName] VARCHAR(128) ,
          [Query] VARCHAR(MAX) ,
          [CurrentPlan] XML ,
          [Login] VARCHAR(128) ,
          [HostName] VARCHAR(128) ,
          [Status] VARCHAR(24) ,
          [Command] VARCHAR(32) ,
          [BlkBy] SMALLINT ,
          [BlockingChainID] INT ,
          [BlockingChainDepth] INT ,
          [TranCount] INT ,
          [ReadLockCount] INT ,
          [WriteLockCount] INT ,
          [SchemaLockCount] INT ,
          [WaitType] VARCHAR(64) ,
          [PercentComplete] REAL ,
          [EstCompTime] BIGINT ,
          [CPU] INT ,
          [IO] BIGINT ,
          [Reads] INT ,
          [Writes] INT ,
          [LastRead] SMALLDATETIME ,
          [LastWrite] SMALLDATETIME ,
          [StartTime] SMALLDATETIME ,
          [LastBatch] SMALLDATETIME ,
          [NetTransport] VARCHAR(16) ,
          [AuthScheme] VARCHAR(16) ,
          [IsolationLevel] VARCHAR(20) ,
          [ProgramName] VARCHAR(256) ,
          [plan_handle] VARBINARY(64) ,
          [stmt_start] INT ,
          [stmt_end] INT ,
          [request_id] INT
        );

-- Insert data
    INSERT  INTO #who
            ( [SPID] ,
            --  [KPID] ,
              [DBName] ,
              [Login] ,
              [HostName] ,
              [Status] ,
              [Command] ,
              [BlkBy] ,
              [TranCount] ,
              [ReadLockCount] ,
              [WriteLockCount] ,
              [SchemaLockCount] ,
              [WaitType] ,
              [PercentComplete] ,
              [EstCompTime] ,
              [CPU] ,
              [IO] ,
              [Reads] ,
              [Writes] ,
              [LastRead] ,
              [LastWrite] ,
              [StartTime] ,
              [LastBatch] ,
              [NetTransport] ,
              [AuthScheme] ,
              [IsolationLevel] ,
              [ProgramName] ,
              [plan_handle] ,
              [stmt_start] ,
              [stmt_end] ,
              [request_id]
            )
            SELECT  c.[session_id] AS [SPID] ,
                    --p.[kpid] AS [KPID] ,
                    --COALESCE(DB_NAME(r.[database_id]), DB_NAME(p.[dbid]),DB_NAME(h.[dbid])) AS [DBName] ,
                    DB_NAME(r.[database_id]) AS [DBName] ,
                    LTRIM(RTRIM(s.[login_name])) AS [Login] ,
                    LTRIM(RTRIM(s.[host_name])) AS [HostName] ,
                    --LTRIM(RTRIM(COALESCE(p.[status], s.[status]))) AS [Status] ,
                    s.[status] AS [Status] ,
                    --COALESCE(r.[command], p.[cmd]) AS [Command] ,
                    r.[command] AS [Command] ,
                    --COALESCE(p.[blocked], r.[blocking_session_id]) AS [BlkBy] ,
                    r.[blocking_session_id] AS [BlkBy] ,
                    ISNULL(t.[trancount], 0) AS [TranCount] ,
                    ISNULL(i.[ReadLockCount], 0) AS [ReadLockCount] ,
                    ISNULL(i.[WriteLockCount], 0) AS [WriteLockCount] ,
                    ISNULL(i.[SchemaLockCount], 0) AS [SchemaLockCount] ,
                    --LTRIM(RTRIM(COALESCE(p.[lastwaittype], r.[wait_type]))) AS [WaitType] ,
                    r.[wait_type] AS [WaitType] ,
                    r.[percent_complete] AS [PercentComplete] ,
                    ( ( r.[estimated_completion_time] / 1000 ) / 60 ) AS [EstCompTimeMins] ,
                    --COALESCE(p.[cpu], s.[cpu_time]) AS [CPU] ,
                    s.[cpu_time] AS [CPU] ,
                    --p.[physical_io] AS [IO] ,
                    r.reads + r.writes AS [IO] ,
                    c.[num_reads] AS [Reads] ,
                    c.[num_writes] AS [Writes] ,
                    c.[last_read] AS [LastRead] ,
                    c.[last_write] AS [LastWrite] ,
                    c.[connect_time] AS [LoginTime] ,
                    s.[last_request_start_time] AS [LastBatch] ,
                    c.[net_transport] AS [NetTransport] ,
                    c.[auth_scheme] AS [AuthScheme] ,
                    CASE s.[transaction_isolation_level]
                      WHEN 0 THEN 'Unspecified'
                      WHEN 1 THEN 'ReadUncomitted'
                      WHEN 2 THEN 'ReadCommitted'
                      WHEN 3 THEN 'Repeatable'
                      WHEN 4 THEN 'Serializable'
                      WHEN 5 THEN 'Snapshot'
                      ELSE 'Unknown'
                    END AS [IsolationLevel] ,
                    CASE WHEN s.[program_name] LIKE 'SQLAgent - TSQL JobStep%'
                         THEN 'SQLAgent Job: ' + SUBSTRING(j.[name], 1, 256)
                         ELSE LTRIM(RTRIM(SUBSTRING(s.[program_name], 1, 256)))
                    END AS [ProgramName] ,
                    r.[plan_handle] ,
                    r.[statement_start_offset] ,
                    r.[statement_end_offset] ,
                    r.[request_id]
            FROM    [sys].[dm_exec_connections] c --LEFT JOIN [sys].[sysprocesses] p ON c.[session_id] = p.[spid]
                    LEFT JOIN [sys].[dm_exec_sessions] s ON c.[session_id] = s.[session_id]
                    LEFT JOIN [sys].[dm_exec_requests] r ON c.[session_id] = r.[session_id]
                    LEFT JOIN [msdb].[dbo].[sysjobs] j ON SUBSTRING(ISNULL(s.[program_name],
                                                              ''),
                                                              CHARINDEX('0x',
                                                              ISNULL(s.[program_name],
                                                              '')) + 18, 16) = SUBSTRING(REPLACE(ISNULL(j.[job_id],
                                                              ''), '-', ''),
                                                              17, 16)
                    LEFT JOIN ( SELECT  [request_session_id] ,
                                        COUNT(CASE WHEN [request_mode] IN (
                                                        'S', 'IS', 'RangeS_S',
                                                        'RangeI_S' ) THEN 1
                                                   ELSE NULL
                                              END) AS [ReadLockCount] ,
                                        COUNT(CASE WHEN [request_mode] IN (
                                                        'U', 'X', 'IU', 'IX',
                                                        'SIU', 'SIX', 'UIX',
                                                        'BU', 'RangeS_U',
                                                        'RangeI_N', 'RangeI_U',
                                                        'RangeI_X', 'RangeX_S',
                                                        'RangeX_U', 'RangeX_X' )
                                                   THEN 1
                                                   ELSE NULL
                                              END) AS [WriteLockCount] ,
                                        COUNT(CASE WHEN [request_mode] IN (
                                                        'Sch-S', 'Sch-M' )
                                                   THEN 1
                                                   ELSE NULL
                                              END) AS [SchemaLockCount]
                                FROM    [sys].[dm_tran_locks] AS DTL
                                GROUP BY [request_session_id]
                              ) i ON i.[request_session_id] = c.[session_id]
                    LEFT JOIN ( SELECT  [session_id] ,
                                        [database_id] = MAX([database_id]) ,
                                        [trancount] = COUNT(*)
                                FROM    [sys].[dm_tran_session_transactions] t
                                        INNER JOIN [sys].[dm_tran_database_transactions] dt ON t.[transaction_id] = dt.[transaction_id]
                                GROUP BY [session_id]
                              ) t ON c.[session_id] = t.[session_id]
                    OUTER APPLY [sys].[dm_exec_sql_text](c.[most_recent_sql_handle]) h
                    LEFT JOIN [dbo].[Split_fn](@SPID, ',') sp ON c.[session_id] = sp.[item] COLLATE DATABASE_DEFAULT
                    LEFT JOIN [dbo].[Split_fn](@SPIDExclude, ',') spe ON c.[session_id] = spe.[item] COLLATE DATABASE_DEFAULT
                    LEFT JOIN [dbo].[Split_fn](@DBName, ',') d ON COALESCE(DB_NAME(r.[database_id]),
                                                              --DB_NAME(p.[dbid]),
                                                              DB_NAME(r.[session_id]),
                                                              DB_NAME(h.[dbid])) = d.[item]
                    LEFT JOIN [dbo].[Split_fn](@DBNameExclude, ',') de ON COALESCE(DB_NAME(r.[database_id]),
                                                              --DB_NAME(p.[dbid]),
                                                              DB_NAME(r.[session_id]),
                                                              DB_NAME(h.[dbid])) = de.[item]
                    LEFT JOIN [dbo].[Split_fn](@Login, ',') l ON s.[login_name] = l.[item] COLLATE DATABASE_DEFAULT
                    LEFT JOIN [dbo].[Split_fn](@LoginExclude, ',') le ON s.[login_name] = le.[item] COLLATE DATABASE_DEFAULT
                    LEFT JOIN [dbo].[Split_fn](@HostName, ',') hn ON s.[host_name] = hn.[item] COLLATE DATABASE_DEFAULT
                    LEFT JOIN [dbo].[Split_fn](@HostNameExclude, ',') he ON s.[host_name] = he.[item] COLLATE DATABASE_DEFAULT
            WHERE   c.[session_id] <> @@SPID -- Exclude your process
                    AND c.[session_id] >= ISNULL(@FilterSpid, 1) -- Exclude filtered spid range
                    AND ( ( sp.[item] IS NOT NULL
                            AND @SPID IS NOT NULL
                          )
                          OR @SPID IS NULL
                        ) -- Specified spids, or all
                    AND ( ( d.[item] IS NOT NULL
                            AND @DBName IS NOT NULL
                          )
                          OR @DBName IS NULL
                        ) -- Specified databases, or all
                    AND ( ( l.[item] IS NOT NULL
                            AND @Login IS NOT NULL
                          )
                          OR @Login IS NULL
                        ) -- Specified logins, or all
                    AND ( ( hn.[item] IS NOT NULL
                            AND @HostName IS NOT NULL
                          )
                          OR @HostName IS NULL
                        ) -- Specified hosts, or all
                    AND spe.[item] IS NULL -- All but excluded spids
                    AND le.[item] IS NULL -- All but excluded logins
                    AND de.[item] IS NULL -- All but excluded databases
                    AND he.[item] IS NULL -- All but excluded hosts
ORDER BY            --p.[blocked] ,
                    r.[blocking_session_id] ,
                    --DB_NAME(p.[dbid]) ,
                    --p.[cpu];
                    DB_NAME(r.[database_id]) ,
                    r.[cpu_time];

-- Get the current plan
    IF @@NESTLEVEL = 1
        BEGIN

            SET LOCK_TIMEOUT 5;

	--  If the plan is too complex, we'll fail with "XML datatype instance has too many levels of nested nodes. Maximum allowed depth is 128 levels."; thus the try/catch
            BEGIN TRY
		UPDATE #who
		SET [CurrentPlan] = CONVERT(XML, etqp.[query_plan])
		FROM #who p
		OUTER APPLY [sys].[dm_exec_text_query_plan](p.[plan_handle], p.[stmt_start], p.[stmt_end]) etqp
		WHERE p.[plan_handle] IS NOT NULL
	END TRY

         

            BEGIN CATCH
                DECLARE #currentplan CURSOR STATIC LOCAL
                FOR
                    SELECT DISTINCT
                            [SPID] ,
                            [request_id] ,
                            [plan_handle] ,
                            [stmt_start] ,
                            [stmt_end]
                    FROM    #who
                    WHERE   [plan_handle] IS NOT NULL;
      
                OPEN #currentplan;
                FETCH NEXT FROM #currentplan INTO @CurSpid, @request_id,
                    @handle, @stmt_start, @stmt_end;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        BEGIN TRY
                            UPDATE  #who
                            SET     [CurrentPlan] = ( SELECT  CONVERT(XML, etqp.[query_plan])
                                                      FROM    [sys].[dm_exec_text_query_plan](@handle,
                                                              @stmt_start,
                                                              @stmt_end) etqp
                                                    )
                            FROM    #who p
                            --WHERE   p.[SPID] = @CurSpid
                            WHERE   r.[session_id] = @CurSpid
                                    --AND p.[request_id] = @request_id;
                                    AND r.[request_id] = @request_id;
                        END TRY
                        BEGIN CATCH
                            UPDATE  #who
                            SET     [CurrentPlan] = ( SELECT  'Could not get query plan' AS [@alert] ,
                                                              ERROR_NUMBER() AS [@errno] ,
                                                              ERROR_SEVERITY() AS [@level] ,
                                                              ERROR_MESSAGE() AS [@errmsg]
                                                    FOR
                                                      XML PATH('ERROR')
                                                    )
                            WHERE   [SPID] = @CurSpid
                                    AND [request_id] = @request_id;
                        END CATCH;

                        FETCH NEXT FROM #currentplan INTO @CurSpid,
                            @request_id, @handle, @stmt_start, @stmt_end;
                    END;
                CLOSE #currentplan;
                DEALLOCATE #currentplan;
            END CATCH;

            SET LOCK_TIMEOUT 0

   -- There is a bug in dm_exec_text_query_plan which causes the attribute StatementText to include the full text of the batch up to current statement. This causes bloat in SSMS. Whence we fix the attribute.
	;
            WITH XMLNAMESPACES('http://schemas.microsoft.com/sqlserver/2004/07/showplan' AS SP)
	UPDATE #who
	SET [CurrentPlan].modify('
            replace value of (
                  /SP:ShowPlanXML/SP:BatchSequence/SP:Batch/
                   SP:Statements/SP:StmtSimple/@StatementText)[1]
            with
               substring((/SP:ShowPlanXML/SP:BatchSequence/SP:Batch/
                         SP:Statements/SP:StmtSimple/@StatementText)[1],
                        (sql:column("stmt_start") + 2) div 2)
          ')
	WHERE [CurrentPlan] IS NOT NULL
	AND [stmt_start] IS NOT NULL;
        END;

-- Drop the columns only used for this section (so we don't have to specify the columns in the final select)
    ALTER TABLE #who
    DROP COLUMN [plan_handle],[stmt_start],[stmt_end],[request_id]

-- Build the blocking chain info
;
    WITH    [Blocking] ( [SPID], [BlkBy], [BlockingChain], [Depth] )
              AS (
	-- Lead blockers
                   SELECT   s.[SPID] ,
                            CAST(NULL AS SMALLINT) ,
                            ROW_NUMBER() OVER ( ORDER BY s.[SPID] ) ,
                            0 AS [LevelRow]
                   FROM     #who s
                            INNER JOIN #who s1 ON s.[SPID] = s1.[BlkBy]
                   WHERE    s.[BlkBy] = 0
                   UNION ALL

	-- Victims
                   SELECT   s.[SPID] ,
                            s.[BlkBy] ,
                            d.[BlockingChain] ,
                            d.[Depth] + 1
                   FROM     #who s
                            INNER JOIN Blocking d ON s.[BlkBy] = d.[SPID]
                   WHERE    s.[BlkBy] <> s.[SPID] -- Avoid infinite loops of self-blocking
                 )
        UPDATE  a
        SET     [BlockingChainID] = b.[BlockingChain] ,
                [BlockingChainDepth] = b.[Depth]
        FROM    #who a
                INNER JOIN Blocking b ON a.[SPID] = b.[SPID];

-- Get DBCC INPUTBUFFER per spid
    SET @CurSpid = NULL;
    DECLARE #spids CURSOR STATIC LOCAL
    FOR
        SELECT  [SPID]
        FROM    #who;

    OPEN #spids;
    FETCH NEXT FROM #spids INTO @CurSpid;
    WHILE @@FETCH_STATUS = 0
        BEGIN
	
            BEGIN TRY
                INSERT  INTO @QueryCmd
                        ( [EventType] ,
                          [Parameters] ,
                          [Command]
                        )
                        EXEC
                            ( 'DBCC INPUTBUFFER( ' + @CurSpid
                              + ') WITH NO_INFOMSGS'
                            );
            END TRY
            BEGIN CATCH
            END CATCH;

	-- Update the table with the spid (to be used later to join it back together)
            UPDATE  @QueryCmd
            SET     [SPID] = @CurSpid
            WHERE   [ID] = SCOPE_IDENTITY();	

            FETCH NEXT FROM #spids INTO @CurSpid;
        END;
    CLOSE #spids;
    DEALLOCATE #spids;

-- Update the working table with the command
    UPDATE  a
    SET     a.[Query] = q.[Command]
    FROM    #who a
            INNER JOIN @QueryCmd q ON a.[SPID] = q.[SPID]
    WHERE   q.[Command] IS NOT NULL;

    EXEC('
    SELECT * FROM #who
    ORDER BY 
    CASE WHEN [BlockingChainID] IS NULL THEN 32768 ELSE [BlockingChainID] END
    ,CASE WHEN [BlockingChainDepth] IS NULL THEN 32768 ELSE [BlockingChainDepth] END
    ,' + @OrderBy1 + ', ' + @OrderBy2 + ', ' + @OrderBy3 + '
    ');

    SET NOCOUNT OFF;
GO 