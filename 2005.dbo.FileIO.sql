IF OBJECT_ID('[dbo].[FileIO]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[FileIO]
GO

/*******************************************************************************************************
**	Name:			dbo.FileIO
**	Desc:			Procedure to retrieve resource totals based on duration
**	Auth:			Matt Stanford (SQLSlayer.com)
**  Parameters:		@SecondsToSample = Delay in seconds of time to sample
**	Date:			2007.11.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright ? SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20080617	Adam Bean		Formatted code, put together procedure
**  20090602	Adam Bean		Cleaned up
**	20090929	Adam Bean		Global header formatting & general clean up
**	20101021	Matt Stanford	Added Disk Summary data
**	20101025	Matt Stanford	Added percentage columns
**  20170111	Srujana Gorge	Removed ORDER BY ordinal column number
********************************************************************************************************/

CREATE PROCEDURE [dbo].[FileIO] 
(
	@SecondsToSample	INT = 30
	,@DBName			sysname = NULL
	,@ShowZeros			BIT = 0
	,@ShowDiskData		BIT = 0
	,@ShowFolderData	BIT = 0
)

AS

SET NOCOUNT ON

DECLARE 
	@EndTime DATETIME

SET @EndTime = DATEADD(s,@SecondsToSample,GETDATE())

IF OBJECT_ID('tempdb.dbo.#io') IS NOT NULL
DROP TABLE #io

CREATE TABLE #io (
	[database_name]					NVARCHAR(128)
	,[file_id]						SMALLINT
	,[file_type]					NVARCHAR(128)
	,[file_name]					NVARCHAR(128)
	,[sample_ms]					INT 
	,[sample_ms_post]				INT 
	,[num_of_reads]					BIGINT 
	,[num_of_reads_post]			BIGINT 
	,[num_of_bytes_read]			BIGINT 
	,[num_of_bytes_read_post]		BIGINT 
	,[io_stall_read_ms]				BIGINT 
	,[io_stall_read_ms_post]		BIGINT 
	,[num_of_writes]				BIGINT 
	,[num_of_writes_post]			BIGINT 
	,[num_of_bytes_written]			BIGINT 
	,[num_of_bytes_written_post]	BIGINT 
	,[io_stall_write_ms]			BIGINT 
	,[io_stall_write_ms_post]		BIGINT 
	,[io_stall]						BIGINT 
	,[io_stall_post]				BIGINT 
)

-- Retrieve our file stats as of right now
INSERT INTO #io (
	[database_name]
	,[file_id]
	,[file_type]
	,[file_name]
	,[sample_ms]
	,[num_of_reads]
	,[num_of_bytes_read]
	,[io_stall_read_ms]
	,[num_of_writes]
	,[num_of_bytes_written]
	,[io_stall_write_ms]
	,[io_stall]
)
SELECT 
	DB_NAME(s.[database_id])	AS [database_name]
	,s.[file_id]
	,CASE
		WHEN f.[type] = 0 THEN 'Data'
		ELSE 'Log'
	END							AS [file_type]
	,f.[name]					AS [file_name]
	,s.[sample_ms]
	,s.[num_of_reads]
	,s.[num_of_bytes_read]
	,s.[io_stall_read_ms]
	,s.[num_of_writes]
	,s.[num_of_bytes_written]
	,s.[io_stall_write_ms]
	,s.[io_stall]
FROM [sys].[dm_io_virtual_file_stats](NULL, NULL) s
INNER JOIN [master].[sys].[master_files] f
	ON f.[database_id] = s.[database_id]
	AND f.[file_id] = s.[file_id]
WHERE (DB_NAME(s.[database_id]) = @DBName OR @DBName IS NULL)

-- Wait for specified delay
WAITFOR TIME @EndTime

-- Get file stats after delay
UPDATE #io
SET [sample_ms_post]				= t.[sample_ms]
	,[num_of_reads_post]			= t.[num_of_reads]
	,[num_of_bytes_read_post]		= t.[num_of_bytes_read]
	,[io_stall_read_ms_post]		= t.[io_stall_read_ms]
	,[num_of_writes_post]			= t.[num_of_writes]
	,[num_of_bytes_written_post]	= t.[num_of_bytes_written]
	,[io_stall_write_ms_post]		= t.[io_stall_write_ms]
	,[io_stall_post]				= t.[io_stall]
FROM
(
	SELECT 
		DB_NAME([database_id]) AS [database_name]
		,[file_id]
		,[sample_ms]
		,[num_of_reads]
		,[num_of_bytes_read]
		,[io_stall_read_ms]
		,[num_of_writes]
		,[num_of_bytes_written]
		,[io_stall_write_ms]
		,[io_stall]
	FROM [sys].[dm_io_virtual_file_stats](NULL, NULL)
) t
WHERE #io.[database_name] = t.[database_name]
and #io.[file_id] = t.[file_id]

-- Save the compiled data
SELECT 
	[database_name]											AS [Database]
	,[file_id]												AS [FileID]
	,[file_type]											AS [FileType]
	,[file_name]											AS [FileName]
	,[sample_ms_post] - [sample_ms]							AS [MilliSecondsElapsed]
	,[num_of_reads_post] - [num_of_reads]					AS [NumberOfReads]
	,[num_of_bytes_read_post] - [num_of_bytes_read]			AS [BytesRead]
	,[io_stall_read_ms_post] - [io_stall_read_ms]			AS [IOStallRead(MS)]
	,[num_of_writes_post] - [num_of_writes]					AS [NumberOfWrites]
	,[num_of_bytes_written_post] - [num_of_bytes_written]	AS [BytesWritten]
	,[io_stall_write_ms_post] - [io_stall_write_ms]			AS [IOStallWrite(MS)]
	,[io_stall_post] - [io_stall]							AS [IOStall(MS)]
INTO #io2
FROM #io

-- Show comparison of file stats after delay
SELECT 
	t.[Database]
	,t.[FileID]
	,t.[FileType]
	,t.[FileName]
	,t.[MilliSecondsElapsed] / 1000							AS [SecondsElapsed]
	,t.[NumberOfReads]
	,t.[BytesRead]
	,t.[IOStallRead(MS)]
	,CAST(ROUND(((t.[IOStallRead(MS)] * 100.0)/(t.[MilliSecondsElapsed])),1) AS DECIMAL(10,1)) AS [IOStallRead(%)]
	,t.[NumberOfWrites]
	,t.[BytesWritten]
	,t.[IOStallWrite(MS)]
	,CAST(ROUND(((t.[IOStallWrite(MS)] * 100.0)/(t.[MilliSecondsElapsed])),1) AS DECIMAL(10,1)) AS [IOStallWrite(%)]
	,t.[IOStall(MS)]
	,CAST(ROUND(((t.[IOStall(MS)] * 100.0)/(t.[MilliSecondsElapsed])),1) AS DECIMAL(10,1)) AS [IOStall(%)]
	,LEFT(f.[physical_name],LEN(f.[physical_name]) - CHARINDEX('\',REVERSE(f.[physical_name]))) AS [Folder]
	,(f.[size] * 8)/1024									AS [FileSize(MB)]
FROM #io2 t
LEFT OUTER JOIN [master].[sys].[master_files] f
	ON t.[database] = DB_NAME(f.[database_id])
	AND t.[fileid] = f.[file_id]
WHERE (
	@ShowZeros = 0 AND 
		(t.[NumberOfReads] > 0
		OR t.[NumberOfWrites] > 0)
) OR (@ShowZeros = 1)
ORDER BY t.[Database], t.[FileID]

-- Show the disk data summary
IF @ShowDiskData = 1
SELECT 
	UPPER(LEFT(f.[physical_name],1))						AS [DriveLetter]
	,SUM(t.[NumberOfReads])									AS [NumberOfReads]
	,SUM(t.[BytesRead])										AS [BytesRead]
	,SUM(t.[IOStallRead(MS)])								AS [IOStallRead(MS)]
	,SUM(t.[NumberOfWrites])								AS [NumberOfWrites]
	,SUM(t.[BytesWritten])									AS [BytesWritten]
	,SUM(t.[IOStallWrite(MS)])								AS [IOStallWrite(MS)]
	,SUM(t.[IOStall(MS)])									AS [IOStall(MS)]
FROM #io2 t
LEFT OUTER JOIN [master].[sys].[master_files] f
	ON t.[database] = DB_NAME(f.[database_id])
	AND t.[fileid] = f.[file_id]
GROUP BY LEFT(f.physical_name,1)
ORDER BY [DriveLetter]

-- Show the folder data summary
IF @ShowFolderData = 1
SELECT 
	LEFT(f.[physical_name],LEN(f.[physical_name]) - CHARINDEX('\',REVERSE(f.[physical_name]))) AS [Folder]
	,SUM(t.[NumberOfReads])									AS [NumberOfReads]
	,SUM(t.[BytesRead])										AS [BytesRead]
	,SUM(t.[IOStallRead(MS)])								AS [IOStallRead(MS)]
	,SUM(t.[NumberOfWrites])								AS [NumberOfWrites]
	,SUM(t.[BytesWritten])									AS [BytesWritten]
	,SUM(t.[IOStallWrite(MS)])								AS [IOStallWrite(MS)]
	,SUM(t.[IOStall(MS)])									AS [IOStall(MS)]
FROM #io2 t
LEFT OUTER JOIN [master].[sys].[master_files] f
	ON t.[database] = DB_NAME(f.[database_id])
	AND t.[fileid] = f.[file_id]
GROUP BY LEFT(f.[physical_name],LEN(f.[physical_name]) - CHARINDEX('\',REVERSE(f.[physical_name])))
ORDER BY [Folder]

DROP TABLE #io
DROP TABLE #io2

SET NOCOUNT OFF


