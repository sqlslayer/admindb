IF OBJECT_ID('dbo.JobHistoryDateTime_fn','FN') IS NOT NULL 
	DROP FUNCTION [dbo].[JobHistoryDateTime_fn]
GO

/******************************************************************************
* Name
	[dbo].[JobHistoryDateTime_fn]

* Author
	Matt Stanford
	
* Date
	2011.04.25
	
* Synopsis
	Function to convert the aweful run_date and run_time fields in sysjobhistory to a datetime
	
* Description
	Function to convert the aweful run_date and run_time fields in sysjobhistory to a datetime

* Examples
	SELECT *,[dbo].[JobHistoryDateTime_fn]([run_date],[run_time]) AS [Run_DateTime]
	FROM msdb.dbo.sysjobhistory

* Parameters
	@run_date			- The run_date column
	@run_time			- The run_time column

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE FUNCTION [dbo].[JobHistoryDateTime_fn]
(
	@run_date		INT
	,@run_time		INT
)
RETURNS DATETIME
AS
BEGIN

	DECLARE 
		@run_date_s		CHAR(8)
		,@run_time_s	CHAR(6)
	
	SET @run_date_s = RIGHT('00000000' + CAST(@run_date AS VARCHAR(8)),8)
	SET @run_time_s = RIGHT('000000' + CAST(@run_time AS VARCHAR(6)),6)
	
	RETURN CAST(LEFT(@run_date_s,4) + '/' + SUBSTRING(@run_date_s,5,2) + '/' + RIGHT(@run_date_s,2) + ' ' + LEFT(@run_time_s,2) + ':' + SUBSTRING(@run_time_s,3,2) + ':' + RIGHT(@run_time_s,2) AS DATETIME)

END