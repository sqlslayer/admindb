IF OBJECT_ID('[dbo].[Reindex]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[Reindex]
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[dbo].[Reindex]

* Author
	Adam Bean
	
* Date
	2007.08.27
	
* Synopsis
	Rebuilds/reorganizes indexes
	
* Description
	Maintenance routine that will dynamically determine how to maintain indexing. It will determine based on several thresholds whether or not to rebuild or reorganize.
	The procedure also supports being run in seperate sessions to avoid duration constratints. One to gather and one to execute.

* Examples
	EXEC [dbo].[Reindex]
	EXEC [dbo].[Reindex] @DBName = 'db1, db2, db3'
	EXEC [dbo].[Reindex] @MaintenanceType = 'G'
	EXEC [dbo].[Reindex] @MaintenanceType = 'E'

* Dependencies
	dbo.DBSearch_fn
	dbo.Split_fn
	dbo.sysdatabases_vw
	dbo.Maintenance_Reindex_Logger
	dbo.Maintenance_Reindex_Stage

* Parameters
	@DBName						= Database(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@DBNameExclude				= Database(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@TableName					= Table(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@TableNameExclude			= Table(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@IndexName					= Indexes(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@IndexNameExclude			= Index(es) to excluded (CSV supported, NULL for all, accepts % for searching)
	@ReindexType				= Three options: FULL (ignore @MinFragLevel), REBUILDALL (REBUILD all indexes) and ONLINEONLY (only process those that can be rebuilt online or reorganized)
	@MaintenanceType			= Three options: A or ALL (full normal schedule), G or GATHER (only gather the work to be done), E or EXECUTE (only execution the work gathered from a previous run)
	@WaitForGatherMinutes		= How long to wait on a previous gather session 
	@MinFragLevel				= The minimum fragmentation level of the indexing to ignore
	@RebuildFragLevel			= The fragmentation level in which a rebuild will occur vs. a reorganize
	@MinPageCount				= The minimum page count of the indexing to ignore
	@MinRecordCount				= The minimum record count of the indexing to ignore
	@IndexScanType				= LIMITED, SAMPLED, or DETAILED - see bol: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@PAD_INDEX					= see bol: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@FILLFACTOR					= see bol: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@SORT_IN_TEMPDB				= reference BOL: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@IGNORE_DUP_KEY				= reference BOL: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@STATISTICS_NORECOMPUTE		= see bol: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@ALLOW_ROW_LOCKS			= reference BOL: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@ALLOW_PAGE_LOCKS			= reference BOL: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@MAXDOP						= reference BOL: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@LOB_COMPACTION				= reference BOL: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@DROP_EXISTING				= reference BOL: http://msdn.microsoft.com/en-us/library/ms188388.aspx
	@Logging					= Log the state of the statistics prior to executing
	@RAISERRORSuppressFlag		= Default = 0, RaiseError. If = 1, suppress non-critical errors.
	@Debug						= Display but don't run queries 
	
* Notes
	The @MaintenanceType parameter is leveraged to split up the work to be performed. In large environments, gathering all of your data and then executing thereafter can be
	a quite expensive process in regards to duration. By creating two seperate runs of this procedure, one with @MaintenanceType = 'G' and one with @MaintenanceType = 'E', 
	you can effectively gather all of the work to be done in one time frame and then process the work at a later time. 

	With the @MaintenanceType, there may be times where your gather and execute sessions collide. To combat this, a temporary record is inserted into the staging table at the time of run.
	If @MaintenanceType of E/Execute is passed, it will wait until that temporary record has been removed before attempting to perform the execute. Next release, this will be better
	and allow for parallel executions, timed, partial runs, etc.

	Add the extended property "REINDEX_TYPE" on an index to set the reindex type you want to override the logic of this procedure on (REBUILD or REORGANIZE)

	Add the extended property "REINDEX_WITH" on an index to set any custom WITH option that you want to override the logic of this procedure on. 
	The REINDEX_WITH is primarily for REBUILDS, only LOB_COMPACTION exists for REORGANIZE. Refer to books online for more information.
	Example how to add: 
	EXEC sys.sp_addextendedproperty 
		@name=N'REINDEX_WITH'
		,@value=N'SORT_IN_TEMPDB,MAXDOP = 2'
		,@level0type=N'SCHEMA'
		,@level0name=N'SCHEMA_NAME_HERE'
		,@level1type=N'TABLE'
		,@level1name=N'TABLE_NAME_HERE'
		,@level2type=N'INDEX'
		,@level2name=N'INDEX_NAME_HERE'
*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
    20071127	Matt Stanford	Added logic to skip over disabled indexes
    20071127	Matt Stanford	De-dynmaicized it (a.k.a. made it readable)
    20071128	Adam Bean		Added date and database name on print output
    20071203	Matt Stanford	(slightly) normalized the tables and moved the logging logic to different proc.  Also added data to logging.
    20071205	Matt Stanford	Started over.
    20080218	Matt Stanford	Added ReindexType logic
    20080302	Matt Stanford	Set Deadlock priority and added FillFactor saving logic
    20080314	Adam Bean		Renaming, formatting and variable renaming for maintenance wrapper
    20080409	Adam Bean		Added TableName parameter
    20090511	Adam Bean		Added ARITHABORT setting
    20090611	Matt Stanford	Merged in my 20080302 changes.  Also changed parameter order for compatibility reasons.
    20090929	Adam Bean		Global header formatting & general clean up
    20091006	Adam Bean		Removed brackets from data types
								Fixed collation issues on SYSNAME
	20110919	Adam Bean		Started over
	20111212	Adam Bean		Added exclusion for read only databases
	20120326	Adam Bean		Properly use @IndexScanType
								Fixed issue with rebuilding partitions
	20130321	Adam Bean		Added logic to disable online rebuilds on tables with column store indexing
	20130613	Adam Bean		ColumnStore indexing changes broke SQL2005 pre SP2 (two part object_name), resolved
	20131212	Adam Bean		Added @MaintenanceType to allow for more two part maintenance (see notes)
	20131219	Adam Bean		Added Processed and Error columns to staging table to allow for processing without failing as they occur, as well as rerun ability
	20140106	Adam Bean		Updated the default thresholds that determine if an index needs to be maintained. 
								Added @AvgPageSpaceUsed as a parameter
	20140108	Adam Bean		Fixed partition logic (if a rebuild is issued, have to specify PARTITION = ALL)
	20140113	Adam Bean		Added logic to not cause an Execute session to fail if the Gather session did not return any data
	20140115	Adam Bean		Added new logic to fail an execution run if waiting longer than @WaitForGatherMinutes from a previous gather session
	20140121	Adam Bean		Fixed two sets of duplicate issues: Added aggregates to the LEFT JOIN for sys.columns. Limited indexing to allocation types of IN_ROW_DATA ONLY
    20140214	Adam Bean		WITH the recent duplicate fix put in place, the ONLINE logic was damaged. Resolved this by changing the CROSS APPLY not TO search for the column criteria that already took place in the subquery JOIN
    20140222	Adam Bean		Added @DateStart, @DateEnd to record start and end times of the index maintenance per object
								Brought in RunID from logger to here so that this is recorded for the whole run vs. by object
	20140329	Adam Bean		Added @FILLFACTOR and @MAXDOP
	20140504	Adam Bean		Added #sys_indexcolumns to fix incorrect OFFLINE REBUILD logic
	20140511	Adam Bean		Excluded AlwaysOn replicas
	20150423	Peng Chan		Removed replica_id
	20150519	Peng Chan		Moved the Search/Match section outside of the Gather/All maintenance type. These setting changes are now for all maintenace types.
								Added predicates to allow Execute to 'Include" or 'Exclude' certain DBs, tables or indexes
	20150912	Peng Chan		Changed the 'Include' and 'Exclude' tables/indexes in the Execute module to allow multiple inclusion and exclusion.  
 								Parameters patterns: '%Table1%,%Table2%' or '%Index1%,'%Index2%'. 
 	20151118	Peng Chan		Added new predicates to include table/index inclusion/exclusion to return the correct script outcome. Script originally reported if 
 								there were more one line with processed = 0. 
 	20161121	Peng Chan		Comment out RAISERROR messages if they are not truly an error, per Andy's request. 
 	20161229	Peng Chan		ORDER BY Ordinal number is deprecated. Replaced it with column names. 
 	20170106	Peng Chan		Added @RAISERRORSuppressFlag. Default to 0.  If 1, "some" informational, non-critical RAISERROR messages will be suppressed. 


******************************************************************************/

CREATE PROCEDURE [dbo].[Reindex]
(
	@DBName						NVARCHAR(2048)	= NULL		
	,@DBNameExclude				NVARCHAR(2048)	= NULL		
	,@TableName					NVARCHAR(2048)	= NULL		
	,@TableNameExclude			NVARCHAR(2048)	= NULL
	,@IndexName					NVARCHAR(2048)	= NULL		
	,@IndexNameExclude			NVARCHAR(2048)	= NULL	
	,@ReindexType				VARCHAR(16)		= NULL
	,@MaintenanceType			VARCHAR(8)		= 'ALL'
	,@WaitForGatherMinutes		TINYINT			= 120
	,@MinFragLevel				TINYINT			= 10
	,@RebuildFragLevel			TINYINT			= 30
	,@MinPageCount				INT				= 1000
	,@MinRecordCount			INT				= 5
	,@AvgPageSpaceUsed			INT				= 75
	,@IndexScanType				VARCHAR(8)		= 'SAMPLED'
	,@PAD_INDEX					BIT				= 0
	,@FILLFACTOR				TINYINT			= NULL
	,@SORT_IN_TEMPDB			BIT				= 0
	,@IGNORE_DUP_KEY			BIT				= 0
	,@STATISTICS_NORECOMPUTE	BIT				= 0
	,@ALLOW_ROW_LOCKS			BIT				= 0
	,@ALLOW_PAGE_LOCKS			BIT				= 0
	,@MAXDOP					TINYINT			= NULL
	,@LOB_COMPACTION			BIT				= 0
	,@DROP_EXISTING				BIT				= 0
	,@Logging					BIT				= 1
	,@RAISERRORSuppressFlag		BIT				= 0
	,@Debug						BIT				= 0
)

AS

SET NOCOUNT ON
SET DEADLOCK_PRIORITY HIGH
SET ARITHABORT ON 

DECLARE
	@TableNameSearch			NVARCHAR(512)
	,@TableNameSearchExclude	NVARCHAR(512)
	,@IndexNameSearch			NVARCHAR(512)
	,@IndexNameSearchExclude	NVARCHAR(512)
	,@DBID						INT
	,@Edition					VARCHAR(32)
	,@Comma						VARCHAR(1)
	,@ThisDB					NVARCHAR(128)
	,@RunID						INT
	,@DateStart					DATETIME
	,@DateEnd					DATETIME
	,@IsDataGatherRunning		BIT
	,@IsDataGatherRunningTime	DATETIME
	,@ReindexFailureError		NVARCHAR(256)
	,@WaitingForDataGather		NVARCHAR(128)  
	,@WaitingForDataGatherError NVARCHAR(256)
	,@SQL						NVARCHAR(2048)
	,@ReindexPrefix				VARCHAR(2048)
	,@ReindexSuffix				VARCHAR(1024)
	,@ReindexCommand			NVARCHAR(4000)
	,@Command					VARCHAR(3072)
	,@SchemaName				NVARCHAR(128)
	,@AllowPageLocks			BIT
	,@PartitionNumber			INT
	,@PartitionCount			INT
	,@IndexFillFactor			INT
	,@AvgFrag					FLOAT
	,@PageSpaceUsed				FLOAT
	,@RecordSize				FLOAT
	,@Type						VARCHAR(16)
	,@TypeOptionOverride		VARCHAR(128)
	,@WithOptionsOverride		VARCHAR(128)
	,@ONLINE					BIT
	,@Override					BIT
	,@OverrideValue				VARCHAR(32)
	,@ObjectExists				BIT
	,@StatTableName				VARCHAR(128)
	,@StatIndexName				VARCHAR(128)
	

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'
	
-- Initialize variables
SET @ThisDB = (SELECT DB_NAME())
SET @Comma = CHAR(44)
SET @Edition = (SELECT CAST(SERVERPROPERTY('EDITION') AS VARCHAR))
SET @IsDataGatherRunning = (SELECT COUNT(*) FROM dbo.[Maintenance_Reindex_Stage] WHERE [database_id] = -1 AND [DBName] = 'Previous data gathering session running')
SET @WaitingForDataGather = CONVERT(VARCHAR, GETDATE(), 120) + ' - Waiting for previous data gathering session to complete...'  
SET @WaitingForDataGatherError = CONVERT(VARCHAR, GETDATE(), 120) + ' - Reindex has been waiting for ' + CAST(@WaitForGatherMinutes AS VARCHAR(2)) + ' minutes for the gather session to complete, aborting execution run. Investigate previous run appropriately.'
SET @ReindexFailureError = CONVERT(VARCHAR, GETDATE(), 120) + ' - Reindexing failed. Investigate the failures by running SELECT [Error], * FROM [' + @ThisDB + '].[dbo].[Maintenance_Reindex_Stage] WHERE [Processed] = 0'

-- Get the run ID
IF @Debug = 0
	EXEC [dbo].[Maintenance_GetRunID] @RunID = @RunID OUTPUT

-- If running an execute, ensure a previous data gathering session is not running. If it is, wait for 30 seconds and check again.
IF @MaintenanceType IN ('E','EXECUTE')
BEGIN
	SET @IsDataGatherRunningTime = GETDATE()
	WHILE @IsDataGatherRunning = 1
	IF @Debug = 1
	BEGIN
		PRINT @WaitingForDataGather
		RETURN      
	END
	ELSE  
	BEGIN
		-- If the execute has been waiting for longer than @WaitForGatherMinutes for the gather session to complete, bail and error
		IF DATEDIFF(MINUTE,@IsDataGatherRunningTime,GETDATE()) >= @WaitForGatherMinutes
		BEGIN
  			RAISERROR(@WaitingForDataGatherError,11,1) WITH NOWAIT
			RETURN      
		END      

		-- Return an informative message stating that we're still waiting for the gather to complete  
		IF @RAISERRORSuppressFlag = 0 
 			RAISERROR(@WaitingForDataGather,0,1) WITH NOWAIT 
		WAITFOR DELAY '00:00:30'

		-- Check status again, if gather is done, set @IsDataGatherRunning to 1 to exit the loop
		IF (SELECT COUNT(*) FROM dbo.[Maintenance_Reindex_Stage] WHERE [database_id] = -1 AND [DBName] = 'Previous data gathering session running') = 0
			SET @IsDataGatherRunning = 0  
	END
END
-- 5/19/2015: Peng Chan
-- Move this section out of the IF Gather to allow both Gather and Excute options to set these values
	-- Determine if we're doing search or a match for database, table or index
	IF CHARINDEX('%',@DBName) > 0
		SET @DBName = (SELECT [dbo].[DBSearch_fn](@DBName))
	IF CHARINDEX('%',@DBNameExclude) > 0
		SET @DBNameExclude = (SELECT [dbo].[DBSearch_fn](@DBNameExclude))
	IF CHARINDEX('%',@TableName) > 0
	BEGIN
		SET @TableNameSearch = @TableName
		SET @TableName = NULL
	END
	IF CHARINDEX('%',@TableNameExclude) > 0
	BEGIN
		SET @TableNameSearchExclude = @TableNameExclude
		SET @TableNameExclude = NULL
	END
	IF CHARINDEX('%',@IndexName) > 0
	BEGIN
		SET @IndexNameSearch = @IndexName
		SET @IndexName = NULL
	END
	IF CHARINDEX('%',@IndexNameExclude) > 0
	BEGIN
		SET @IndexNameSearchExclude = @IndexNameExclude
		SET @IndexNameExclude = NULL
	END
    
-- Only gather the data if not in execute
IF @MaintenanceType IN ('A','ALL','G','GATHER') 
BEGIN

	-- If the staging table exists from a previous run (ie. running in Debug without executing, remove any existing data)
	IF OBJECT_ID('dbo.Maintenance_Reindex_Stage') IS NOT NULL
	   TRUNCATE TABLE dbo.[Maintenance_Reindex_Stage]
	-- Create a dummy record to avoid contention of an execute running over a gather session
	INSERT INTO dbo.[Maintenance_Reindex_Stage]
	([database_id],[DBName])
	VALUES
	(-1,'Previous data gathering session running')

	-- Get all of the catalog views
	IF OBJECT_ID('tempdb.dbo.#sysindexes') IS NOT NULL
	   DROP TABLE #sysindexes
	IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
	   DROP TABLE #sysobjects
	IF OBJECT_ID('tempdb.dbo.#syspartitions') IS NOT NULL
	   DROP TABLE #syspartitions
	IF OBJECT_ID('tempdb.dbo.#sysschemas') IS NOT NULL
	   DROP TABLE #sysschemas
	IF OBJECT_ID('tempdb.dbo.#syscolumns') IS NOT NULL
	   DROP TABLE #syscolumns
	IF OBJECT_ID('tempdb.dbo.#sysindex_columns') IS NOT NULL
	   DROP TABLE #sysindex_columns
	IF OBJECT_ID('tempdb.dbo.#PhysStats') IS NOT NULL
	   DROP TABLE #PhysStats

	-- Create the tables
	SELECT TOP 0 *, [database_id] = CAST(NULL AS INT), [TypeOption] = CAST(NULL AS VARCHAR(128)), [WithOptions] = CAST(NULL AS VARCHAR(128)) INTO #sysindexes FROM [sys].[indexes]
	SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysobjects FROM [sys].[objects]
	SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #syspartitions FROM [sys].[partitions]
	SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysschemas FROM [sys].[schemas]
	SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #syscolumns FROM [sys].[columns]
	SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysindex_columns FROM [sys].[index_columns]
	SELECT TOP 0 *, [alloc_count] = CAST(NULL AS INT) INTO #PhysStats FROM [sys].[dm_db_index_physical_stats] (NULL, NULL, NULL , NULL, @IndexScanType)

	-- Retrieve data per database
	DECLARE #dbs CURSOR LOCAL STATIC FOR
	SELECT 
		[name]
		,[database_id]
	FROM [dbo].[sysdatabases_vw] s
	LEFT JOIN [dbo].[Split_fn](@DBName,',') d
		ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
		ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
	WHERE s.[state_desc] = 'ONLINE'
	AND s.[user_access_desc] = 'MULTI_USER'
	AND s.[is_read_only] = 0
	AND s.[source_database_id] IS NULL
	AND s.[name] NOT IN ('tempdb')
	AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
	AND de.[item] IS NULL -- All but excluded databases
	ORDER BY [name]

	OPEN #dbs
	FETCH NEXT FROM #dbs INTO @DBName, @DBID
	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXEC
		('
			USE [' + @DBName + '] 
			
			-- sys.indexes
			INSERT INTO #sysindexes
			SELECT *, ' + @DBID + ', NULL, NULL FROM [sys].[indexes]
			WHERE [is_disabled] = 0
			AND [index_id] != 0 -- Heaps

			-- sys.objects
			INSERT INTO #sysobjects
			SELECT *, ' + @DBID + ' FROM [sys].[objects]

			-- sys.partitions 
			INSERT INTO #syspartitions
			SELECT *, ' + @DBID + ' FROM [sys].[partitions]

			-- sys.schemas 
			INSERT INTO #sysschemas
			SELECT *, ' + @DBID + ' FROM [sys].[schemas]

			-- sys.columns 
			INSERT INTO #syscolumns
			SELECT *, ' + @DBID + ' FROM [sys].[columns]

			-- sys.index_columns 
			INSERT INTO #sysindex_columns
			SELECT *, ' + @DBID + ' FROM [sys].[index_columns]
				
			-- Retrieve any Override values
			-- Find TYPE options
			UPDATE #sysindexes
			SET [TypeOption] = se.[TypeOption]
			FROM #sysindexes si
			LEFT JOIN
			(
				SELECT 
					[major_id]
					,[minor_id]
					,CAST([value] AS NVARCHAR(128)) AS [TypeOption]
				FROM [' + @DBName + '].[sys].[extended_properties]
				WHERE [class_desc] = ''INDEX''
				AND [name] = ''REINDEX_TYPE''
			) se
				ON se.[major_id] = si.[object_id] 
				AND se.[minor_id] = si.[index_id]
			WHERE si.[database_id] = DB_ID(''' + @DBName + ''')

			-- Find WITH options
			UPDATE #sysindexes
			SET [WithOptions] = se.[WithOptions]
			FROM #sysindexes si
			LEFT JOIN
			(
				SELECT 
					[major_id]
					,[minor_id]
					,CAST([value] AS NVARCHAR(128)) AS [WithOptions]
				FROM [' + @DBName + '].[sys].[extended_properties]
				WHERE [class_desc] = ''INDEX''
				AND [name] = ''REINDEX_WITH''
			) se
				ON se.[major_id] = si.[object_id] 
				AND se.[minor_id] = si.[index_id]
			WHERE si.[database_id] = DB_ID(''' + @DBName + ''')
		
			-- Now get the index stats
			INSERT INTO #PhysStats
			SELECT *, 0 FROM [sys].[dm_db_index_physical_stats] (' + @DBID + ', NULL, NULL, NULL, ''' + @IndexScanType + ''')		
			WHERE [index_id] != 0 -- Heaps
		')

	FETCH NEXT FROM #dbs INTO @DBName, @DBID
	END
	CLOSE #dbs
	DEALLOCATE #dbs

	-- Bring all the data together into a staging table
	INSERT INTO dbo.[Maintenance_Reindex_Stage]
	SELECT
		ps.[database_id]								AS [database_id]
		,DB_NAME(ps.[database_id])						AS [DBName]
		,o.[name]										AS [TableName]
		,s.[name]										AS [SchemaName]
		,i.[object_id]									AS [idx_object_id]
		,i.[name]										AS [IndexName]
		,i.[type_desc]									AS [IndexType]
		,i.[allow_page_locks]							AS [AllowPageLocks]
		,ps.[alloc_unit_type_desc]						AS [AllocationType]
		,(	
			SELECT 
				COUNT(*) 
			FROM #syspartitions p 
			WHERE o.[database_id] = p.[database_id] 
			AND o.[object_id] = p.[object_id]
			AND i.[index_id] = p.[index_id]
		) 												AS [PartitionCount]
		,ps.[partition_number]							AS [PartitionNumber]
		,i.[fill_factor]								AS [IndexFillFactor]
		,ROUND(ps.[avg_fragmentation_in_percent],0)		AS [AvgFrag]
		,ROUND(ps.[avg_page_space_used_in_percent],0)	AS [PageSpaceUsed]
		,ps.[page_count]
		,ps.[record_count]
		,ROUND(ps.[avg_record_size_in_bytes],0)			AS [RecordSize]
		-- Determine how to reindex (reorganize or rebuild)
		,CASE
			-- If REBUILDALL is specified, REBUILD everything
			WHEN @ReindexType = 'REBUILDALL' THEN 'REBUILD'
			-- If ONLINEONLY is specified, REORGANIZE if it can't be done online
			WHEN @ReindexType = 'ONLINEONLY' AND ca.[ONLINE] = 0 AND i.[allow_page_locks] = 0 THEN 'REORGANIZE'
			-- If the fragmentation is greater than @RebuildFragLevel, or space used is less than @AvgPageSpaceUsed, REBUILD
			WHEN ROUND(ps.[avg_fragmentation_in_percent],0) >= @RebuildFragLevel OR ROUND(ps.[avg_page_space_used_in_percent],0) <= @AvgPageSpaceUsed THEN 'REBUILD'
			-- If the index doesn't allow page locks then we can't do a REORGANIZE
			WHEN i.[allow_page_locks] = 0 THEN 'REBUILD'
			-- Everything else, REORGANIZE
			ELSE 'REORGANIZE'
		END												AS [Type]
		,ca.[ONLINE]
		,i.[TypeOption]									AS [TypeOptionOverride]
		,i.[WithOptions]								AS [WithOptionsOverride]
		,CAST(NULL AS VARCHAR(3072))					AS [Command]
		,GETDATE()										AS [DateCreated]
		,0												AS [Processed]
		,NULL											AS [Error]
	FROM #PhysStats ps
	JOIN #sysindexes i
		ON i.[database_id] = ps.[database_id]
		AND i.[object_id] = ps.[object_id]
		AND i.[index_id] = ps.[index_id] 
	JOIN #sysobjects o
		ON o.[database_id] = ps.[database_id]
		AND o.[object_id] = ps.[object_id]
	JOIN #sysschemas s
		ON s.[database_id] = ps.[database_id]
		AND s.[schema_id] = o.[schema_id]
	LEFT JOIN
	( -- Find the indexes that don't supprt online rebuilds
		SELECT
			sc.[database_id]
			,sc.[object_id]
			,sic.[index_id]
			,sc.[system_type_id]
			,sc.[max_length]
		FROM #syscolumns sc
		JOIN [sys].[types] st
			ON st.[system_type_id] = sc.[system_type_id]
		LEFT JOIN #sysindex_columns sic
			ON sic.[database_id] = sc.[database_id]
			AND sic.[object_id] = sc.[object_id]
			AND sic.[column_id] = sc.[column_id]
		WHERE (st.[name] IN ('image','text','ntext','xml') OR sc.[max_length] = -1)
	) c
		ON c.[database_id] = ps.[database_id]
		AND c.[object_id] = ps.[object_id]
		AND c.[index_id] = ps.[index_id]
	LEFT JOIN
	( -- Find clustered indexes that have columns in the table that don't support online rebuilds
		SELECT DISTINCT
			si.[database_id]
			,si.[object_id]
			,si.[index_id]
		FROM #sysindexes si
		JOIN #syscolumns sc
			ON sc.[database_id] = si.[database_id]
			AND sc.[object_id] = si.[object_id]		
		JOIN [sys].[types] st
			ON st.[system_type_id] = sc.[system_type_id]
		WHERE (st.[name] IN ('image','text','ntext','xml') OR sc.[max_length] = -1)
	) ci
		ON ci.[database_id] = ps.[database_id]
		AND ci.[object_id] = ps.[object_id]
		AND ci.[index_id] = ps.[index_id]
	LEFT JOIN [dbo].[Split_fn](@TableName,',') tbl
		ON o.[name] = tbl.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@TableNameExclude,',') tble
		ON o.[name] = tble.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@IndexName,',') idx
		ON i.[name] = idx.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@IndexNameExclude,',') idxe
		ON i.[name] = idxe.[item] COLLATE DATABASE_DEFAULT
	CROSS APPLY
	(
		SELECT
		-- Determine if we can perform the operations online
			CASE
				-- Image, text, ntext, xml nor -1 length (max) column's can be rebuilt online
				WHEN c.[system_type_id] IS NOT NULL THEN 0
				-- Clustered indexes with the aforementioned data types on the table can't be rebuilt online
				WHEN ci.[index_id] = 1 THEN 0
				-- If an Override to reorganize was passed, we can not rebuild online
				WHEN i.[TypeOption] = 'REORGANIZE' THEN 0
				-- Only ENTERPRISE & DEVELOPER editions allow for online rebuilds
				WHEN @Edition NOT LIKE 'Enterprise%' AND @Edition NOT LIKE 'Developer%' THEN 0
			ELSE 1
			END									AS [ONLINE]
	) ca([ONLINE])
	WHERE ps.[alloc_unit_type_desc] = 'IN_ROW_DATA'
	AND
	(
		(o.[name] LIKE @TableNameSearch) -- Target specified table names based on a search
		OR (i.[name] LIKE @IndexNameSearch) -- Target specified index names based on a search
		OR (o.[name] NOT LIKE @TableNameSearchExclude) -- Exclude specified table names based on a search
		OR (i.[name] NOT LIKE @IndexNameSearchExclude) -- Exclude specified index names based on a search
		OR (@TableNameSearch IS NULL AND @IndexNameSearch IS NULL AND @TableNameSearchExclude IS NULL AND @IndexNameSearchExclude IS NULL)
	)	
	AND
	(
		((tbl.[item] IS NOT NULL AND @TableName IS NOT NULL) OR @TableName IS NULL) -- Specified table(s), or all
		AND ((idx.[item] IS NOT NULL AND @IndexName IS NOT NULL) OR @IndexName IS NULL) -- Specified index(s), or all
		AND tble.[item] IS NULL -- All but excluded tables
		AND idxe.[item] IS NULL -- All but excluded indexes
	)
	AND 
	(
		(
			ROUND(ps.[avg_fragmentation_in_percent],0) >= @MinFragLevel -- Only retrieve indexes with greater than @MinFragLevel
			AND ps.[page_count] > @MinPageCount -- Only retrieve indexes with greater than @MinPageCount
			AND ps.[record_count] > @MinRecordCount -- Only retrieve indexes with greater than @MinRecordCount
		)
		OR @ReindexType IN ('FULL', 'REBUILDALL') -- Reindex everything (ignoring fragmentation levels) if the type is "FULL" or "REBUILDALL"
	)

	-- If there is a partition, then we can't rebuild online
	UPDATE dbo.[Maintenance_Reindex_Stage]
	SET [ONLINE] = 0
	WHERE [PartitionCount] > 1

	-- If the table has a column store index, then we can't rebuild online (this has to be done here because the dmv won't register the index)
	UPDATE i
	SET i.[ONLINE] = 0
	FROM dbo.[Maintenance_Reindex_Stage] i
	JOIN #sysindexes si
		ON i.[database_id] = si.[database_id]
		AND i.[idx_object_id] = si.[object_id]
	JOIN #sysobjects o
		ON o.[database_id] = si.[database_id]
		AND o.[object_id] = si.[object_id]
	JOIN #sysschemas s
		ON s.[database_id] = si.[database_id]
		AND s.[schema_id] = o.[schema_id]
	WHERE si.[type] = 6

	-- If ONLINEONLY is specified and the index doesn't allow page locks then we can't do anything
	-- This is the only one scenario that we'll remove this index from the work to do as nothing can be done for these poor guys
	IF @ReindexType = 'ONLINEONLY'
	BEGIN
		DELETE FROM dbo.[Maintenance_Reindex_Stage]
		WHERE [AllowPageLocks] = 0
	END

	-- Delete our temporary record for execution runs
	DELETE FROM dbo.[Maintenance_Reindex_Stage]
	WHERE [database_id] = -1
	AND [DBName] = 'Previous data gathering session running'

	-- If we're performing two part maintenance, bail here as we have all the data staged and ready for later execution
	IF @MaintenanceType IN ('G','GATHER')
	BEGIN
		-- If in Debug, display and kill off the data
		IF @Debug = 1
		BEGIN
			SELECT * FROM dbo.[Maintenance_Reindex_Stage] ORDER BY [DBName], [TableName], [IndexName]
			TRUNCATE TABLE dbo.[Maintenance_Reindex_Stage]
		END
		ELSE
			-- If there's no work to do and a gather was issued, the execute will fail. This will prevent that from occuring.
			IF (SELECT COUNT(*) FROM [Maintenance_Reindex_Stage]) = 0
			BEGIN
				PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - No indexes require maintenance at this time.'                  
				INSERT INTO dbo.[Maintenance_Reindex_Stage]
				([database_id],[DBName])
				VALUES
				(-2,'Previous data gathering session returned no data.')	  			
			END
			ELSE
			BEGIN          
				PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - Data has been gathered in dbo.[Maintenance_Reindex_Stage] and can be executed later by running EXEC dbo.[Reindex] @MaintenanceType = ''E'''                  
			END	
		-- All done!
		RETURN
	END     

END -- Done with gathering

-- Now that we're ready to execute, if @MaintenanceType of E was passed, ensure a gathering run was completed.
IF @MaintenanceType IN ('E','EXECUTE') AND (SELECT COUNT(*) FROM dbo.[Maintenance_Reindex_Stage] WHERE [database_id] != -1) = 0
BEGIN
	IF @RAISERRORSuppressFlag = 0 
   		RAISERROR('There is no re-indexing data to execute. Rerun dbo.[Reindex] @MaintenanceType = ''G'' to gather data.',11,1) WITH NOWAIT 
  	 	PRINT 'There is no re-indexing data to execute. Rerun dbo.[Reindex] @MaintenanceType = ''G'' to gather data.' 
	RETURN
END

-- If the previous gather session did not have any data, nothing left to do here
IF (SELECT COUNT(*) FROM dbo.[Maintenance_Reindex_Stage] WHERE [database_id] = -2) = 1
BEGIN
	PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - No indexes require maintenance at this time.'
	DELETE FROM dbo.[Maintenance_Reindex_Stage]	WHERE [database_id] = -2
	RETURN
END

-- Now loop through our list and rebuild/reorganize as appropriate
DECLARE #Reindex CURSOR LOCAL STATIC FOR 
SELECT DISTINCT
	[DBName]
	,[TableName]
	,[SchemaName]
	,[IndexName]
	,[PartitionCount]
	,[PartitionNumber]
	,[IndexFillFactor]
	,[AvgFrag]
	,[PageSpaceUsed]
	,[RecordSize]
	,[Type]
	,[ONLINE]
	,[TypeOptionOverride]
	,[WithOptionsOverride]
FROM dbo.[Maintenance_Reindex_Stage]
	LEFT JOIN [dbo].[Split_fn](@TableNameSearch,',') TableInclude
		ON [TableName] LIKE TableInclude.[item] 
	LEFT JOIN [dbo].[Split_fn](@TableNameSearchExclude,',') TableExclude
		ON [TableName] LIKE TableExclude.[item] 
	LEFT JOIN [dbo].[Split_fn](@IndexNameSearch,',') IndexInclude
		ON [IndexName] LIKE IndexInclude.[item] 
	LEFT JOIN [dbo].[Split_fn](@IndexNameSearchExclude,',') IndexExclude
		ON [IndexName] LIKE IndexExclude.[item] 
WHERE [Processed] = 0
	-- 9/12/2015 Peng Chan:  Add the following predicates to allow multiple table/index inclusion and exclusion.
	AND (@TableNameSearch IS NULL 
	OR TableInclude.item IS NOT NULL)
	AND (@TableNameSearchExclude IS NULL 
	OR TableExclude.item IS NULL)
	AND (@IndexNameSearch IS NULL
	OR IndexInclude.item IS NOT NULL)
	AND (@IndexNameSearchExclude IS NULL
	OR IndexExclude.item IS NULL)


OPEN #Reindex
FETCH NEXT FROM #Reindex INTO @DBName, @TableName, @SchemaName, @IndexName, @PartitionCount, @PartitionNumber, @IndexFillFactor, @AvgFrag, @PageSpaceUsed, @RecordSize, @Type, @ONLINE, @TypeOptionOverride, @WithOptionsOverride
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Reset @Override (for logging)
	SET @Override = 0

	-- Build the reindex prefix
	IF @TypeOptionOverride IS NULL
	BEGIN
		IF @Type = 'REBUILD'
			SET @ReindexPrefix = 'ALTER INDEX [' + @IndexName + '] ON [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '] REBUILD'
		ELSE
			SET @ReindexPrefix = 'ALTER INDEX [' + @IndexName + '] ON [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '] REORGANIZE'
		-- Modify for partitions
		IF @PartitionCount > 1
		BEGIN
			IF @Type = 'REBUILD'
				SET @ReindexPrefix = @ReindexPrefix + ' PARTITION = ALL'
			ELSE      
				SET @ReindexPrefix = @ReindexPrefix + ' PARTITION = ' + CONVERT(CHAR, @PartitionNumber)
		END
	END
	ELSE
	BEGIN
		-- If an Override was detected, use that instead
		SET @ReindexPrefix	= 'ALTER INDEX [' + @IndexName + '] ON [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '] ' + @TypeOptionOverride + ''
	END

	-- Build the reindex suffix
	SET @ReindexSuffix = ' WITH ('

	-- Add any WITH options
	IF @WithOptionsOverride IS NULL
	BEGIN
		IF @Type = 'REBUILD'
		BEGIN
			IF @PAD_INDEX = 1
				SET @ReindexSuffix = @ReindexSuffix + 'PAD_INDEX = ON' + @Comma
			IF @FILLFACTOR IS NOT NULL
				SET @ReindexSuffix = @ReindexSuffix + 'FILLFACTOR = ' + CAST(@FILLFACTOR AS VARCHAR) + @Comma
			IF @SORT_IN_TEMPDB = 1
				SET @ReindexSuffix = @ReindexSuffix + 'SORT_IN_TEMPDB = ON' + @Comma
			IF @IGNORE_DUP_KEY = 1
				SET @ReindexSuffix = @ReindexSuffix + 'IGNORE_DUP_KEY = ON' + @Comma
			IF @ONLINE = 1
				SET @ReindexSuffix = @ReindexSuffix + 'ONLINE = ON' + @Comma
			IF @STATISTICS_NORECOMPUTE = 1
				SET @ReindexSuffix = @ReindexSuffix + 'STATISTICS_NORECOMPUTE = ON' + @Comma
			IF @ALLOW_ROW_LOCKS = 1
				SET @ReindexSuffix = @ReindexSuffix + 'ALLOW_ROW_LOCKS = ON' + @Comma
			IF @ALLOW_PAGE_LOCKS = 1
				SET @ReindexSuffix = @ReindexSuffix + 'ALLOW_PAGE_LOCKS = ON' + @Comma
			IF @MAXDOP IS NOT NULL
				SET @ReindexSuffix = @ReindexSuffix + 'MAXDOP = ' + CAST(@MAXDOP AS VARCHAR) + @Comma
			IF @DROP_EXISTING = 1
				SET @ReindexSuffix = @ReindexSuffix + 'DROP_EXISTING = ON' + @Comma
		END
		ELSE IF @Type = 'REORGANIZE'
		BEGIN
			IF @LOB_COMPACTION = 1
				SET @ReindexSuffix = @ReindexSuffix + 'LOB_COMPACTION = ON' + @Comma
		END
		-- Remove trailing comma
		IF @ReindexSuffix IS NOT NULL
			IF RIGHT(@ReindexSuffix,1) = ','
				SET @ReindexSuffix = LEFT(@ReindexSuffix,LEN(@ReindexSuffix) -1)
	END
	ELSE
	BEGIN
		-- If an Override was detected, use that instead
		SET @ReindexSuffix = @ReindexSuffix + @WithOptionsOverride
	END

	-- Add a closing paren
	SET @ReindexSuffix = @ReindexSuffix + ')'
		
	-- If no WITH options are specified, NULL it out
	IF @ReindexSuffix = ' WITH ()'
		SET @ReindexSuffix = NULL

	-- If we performed an override, it will be logged
	IF (@TypeOptionOverride IS NOT NULL OR @WithOptionsOverride IS NOT NULL)
		SET @Override = 1
	ELSE
		SET @Override = 0
	
	-- Build our logging command and update the temp table for debugging purposes
	SET @Command = ISNULL(@TypeOptionOverride,@Type) + ISNULL(@ReindexSuffix,'')

	-- Update the staging table with the command for debugging purposes
	UPDATE dbo.[Maintenance_Reindex_Stage]
		SET [Command] = @Command
	WHERE [DBName] = @DBName
	AND [TableName] = @TableName
	AND [SchemaName] = @SchemaName
	AND [IndexName] = @IndexName

	-- Build the final command by combining the prefix and suffix
	SET @ReindexCommand = @ReindexPrefix + ISNULL(@ReindexSuffix,'')

	-- Let's make sure the object still exists (avoid temporary real tables)
	SET @ObjectExists = 0
	SET @SQL = N'IF EXISTS (SELECT * FROM [' + @DBName + '].[sys].[indexes] WHERE [object_id] = OBJECT_ID(''[' + @DBName + '].[' + @SchemaName + '].[' + REPLACE(@TableName,'''','''''') + ']'') AND [name] = ''' + REPLACE(@IndexName,'''','''''') + ''') SET @ObjectExists = 1'
	EXEC sp_executesql @SQL, N'@ObjectExists BIT OUTPUT', @ObjectExists = @ObjectExists OUTPUT
	
	-- Check for object existance and remove those with nothing to do and run or execute
	IF @ObjectExists = 1 AND @ReindexCommand IS NOT NULL
	BEGIN	
		-- Print or execute
		IF @Debug = 1
			PRINT(@ReindexCommand)
		ELSE
		BEGIN
			PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - REINDEX started on [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '].[' + @IndexName + ']'
			BEGIN TRY
				-- Execute
				SET @DateStart = (SELECT GETDATE())
				EXEC(@ReindexCommand)
				SET @DateEnd = (SELECT GETDATE())

				-- Log the data if desired
				IF @Logging = 1
					EXEC [dbo].[Maintenance_Reindex_Logger]
						@DBName = @DBName
						,@SchemaName = @SchemaName
						,@TableName = @TableName
						,@IndexName = @IndexName
						,@IndexFillFactor = @IndexFillFactor
						,@PageSpaceUsed = @PageSpaceUsed
						,@RecordSize = @RecordSize
						,@AvgFrag = @AvgFrag
						,@Command = @Command
						,@Override = @Override
						,@DateStart = @DateStart
						,@DateEnd = @DateEnd
						,@RunID = @RunID
				PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - REINDEX finished on [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '].[' + @IndexName + ']'
				-- Set index to processed 
				UPDATE dbo.[Maintenance_Reindex_Stage]
				SET [Processed] = 1
				WHERE [DBName] = @DBName
				AND [SchemaName] = @SchemaName
				AND [TableName] = @TableName
				AND [IndexName] = @IndexName
			END TRY
			BEGIN CATCH
				-- Dump the error to the output
				PRINT 
					'Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) +
					',Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) +
					',State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + 
					',Line ' + CONVERT(VARCHAR(5), ERROR_LINE())
				PRINT ERROR_MESSAGE()
				-- Capture the error in the staging table
				UPDATE dbo.[Maintenance_Reindex_Stage]
				SET [Error] = ERROR_MESSAGE()
				WHERE [DBName] = @DBName
				AND [SchemaName] = @SchemaName
				AND [TableName] = @TableName
				AND [IndexName] = @IndexName
			END CATCH          
		END
	END
	ELSE
	BEGIN  
		-- If the index no longer exists, remove it from the staging table to avoid failure at the end of the run
		DELETE FROM dbo.[Maintenance_Reindex_Stage]
		WHERE [DBName] = @DBName
		AND [SchemaName] = @SchemaName
		AND [TableName] = @TableName
		AND [IndexName] = @IndexName  
	END
    
FETCH NEXT FROM #Reindex INTO @DBName, @TableName, @SchemaName, @IndexName, @PartitionCount, @PartitionNumber, @IndexFillFactor, @AvgFrag, @PageSpaceUsed, @RecordSize, @Type, @ONLINE, @TypeOptionOverride, @WithOptionsOverride
END
CLOSE #Reindex
DEALLOCATE #Reindex

-- Wrap up time. Return data if debugging or check for and report on failures
IF @Debug = 1
BEGIN
	-- Return the staged data
	SELECT 
		* 
	FROM dbo.[Maintenance_Reindex_Stage] 
	ORDER BY [DBName], [TableName], [IndexName]
END
ELSE
BEGIN
	-- 11/18/2015 Peng Chan:  Add the following predicates to report correct outcome based on table/index inclusion/exclusion.	
	-- Report failure if any processing failed

--	IF (SELECT COUNT(*) FROM dbo.[Maintenance_Reindex_Stage] WHERE [Processed] = 0) > 0
	
	IF (SELECT COUNT(*)
		FROM dbo.[Maintenance_Reindex_Stage]
			LEFT JOIN [dbo].[Split_fn](@TableNameSearch,',') TableInclude
				ON [TableName] LIKE TableInclude.[item] 
			LEFT JOIN [dbo].[Split_fn](@TableNameSearchExclude,',') TableExclude
				ON [TableName] LIKE TableExclude.[item] 
			LEFT JOIN [dbo].[Split_fn](@IndexNameSearch,',') IndexInclude
				ON [IndexName] LIKE IndexInclude.[item] 
			LEFT JOIN [dbo].[Split_fn](@IndexNameSearchExclude,',') IndexExclude
				ON [IndexName] LIKE IndexExclude.[item] 
		WHERE [Processed] = 0
			AND 
			(@TableNameSearch IS NULL 
			OR TableInclude.item IS NOT NULL)
			AND (@TableNameSearchExclude IS NULL 
			OR TableExclude.item IS NULL)
			AND (@IndexNameSearch IS NULL
			OR IndexInclude.item IS NOT NULL)
			AND (@IndexNameSearchExclude IS NULL
			OR IndexExclude.item IS NULL)
			) > 0

	BEGIN
		PRINT (@ReindexFailureError)
		IF @RAISERRORSuppressFlag = 0
			RAISERROR (@ReindexFailureError, 16, 2) WITH LOG

		RETURN
	END
END

SET ARITHABORT OFF
SET QUOTED_IDENTIFIER OFF
SET NOCOUNT OFF