IF OBJECT_ID('[dbo].[Reindex_vw]','V') IS NOT NULL 
	DROP VIEW [dbo].[Reindex_vw]
GO

/******************************************************************************
* Name
	[dbo].[Reindex_vw]

* Author
	Adam Bean
	
* Date
	2011.09.20
	
* Synopsis
	View to bring together the maintenance data for reindexing
	
* Description
	View which joins data from all normalized tables which are logged to as part of the Reindex procedure

* Examples
	SELECT * FROM [dbo].[Reindex_vw]

* Dependencies
	The following tables: 
	[dbo].[Maintenance_Reindex]
	[dbo].[Maintenance_DatabaseIDs]
	[dbo].[Maintenance_SchemaIDs]
	[dbo].[Maintenance_TableIDs]
	[dbo].[Maintenance_IndexIDs]
	[dbo].[Maintenance_CommandIDs]
	[dbo].[Maintenance_RunIDs]

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20140222	Adam Bean		Added DateStart, DateEnd, Duration, BatchDateStart, BatchDateEnd, BatchDuration, RunID
******************************************************************************/

CREATE VIEW [dbo].[Reindex_vw]

AS

SELECT
	d.[DBName]
	,s.[SchemaName]
	,t.[TableName]
	,i.[IndexName]
	,r.[IndexFillFactor]
	,r.[PageSpaceUsed]
	,r.[RecordSize]
	,r.[AvgFrag]
	,c.[Command]
	,r.[IsOverride]
	,r.[DateStart]
	,r.[DateEnd]
	,DATEDIFF(SECOND, r.[DateStart], r.[DateEnd])	AS [Duration]
	,ri.[DateStart]									AS [BatchDateStart]
	,ri.[DateEnd]									AS [BatchDateEnd]
	,DATEDIFF(SECOND, ri.[DateStart], ri.[DateEnd])	AS [BatchDuration]
	,ri.[RunID]
FROM [dbo].[Maintenance_Reindex] r
JOIN [dbo].[Maintenance_DatabaseIDs] d
	ON d.[DatabaseID] = r.[DatabaseID]
JOIN [dbo].[Maintenance_SchemaIDs] s
	ON s.[SchemaID] = r.[SchemaID]
JOIN [dbo].[Maintenance_TableIDs] t
	ON t.[TableID] = r.[TableID]
JOIN [dbo].[Maintenance_IndexIDs] i
	ON i.[IndexID] = r.[IndexID]
JOIN [dbo].[Maintenance_CommandIDs] c
	ON c.[CommandID ] = r.[CommandID]
JOIN [dbo].[Maintenance_RunIDs] ri
	ON r.[RunID] = ri.[RunID]	