IF OBJECT_ID('[dbo].[LiteSpeedRestoreDB]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[LiteSpeedRestoreDB]
GO

/*******************************************************************************************************
**	Name:			dbo.LiteSpeedRestoreDB
**	Desc:			Restores LiteSpeed databases faster than crap!
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Usage:			EXEC dbo.LiteSpeedRestoreDB '\\sisbss1\SavvisDnlds\DMSLink.lsbak', 3, null, null, null, 1
**					EXEC dbo.LiteSpeedRestoreDB '\\sisbss1\SavvisDnlds\DMSLink.lsbak', 3, 'DMSLink_New', null, null, 1
**	Date:			2007.05.14
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20071019	Matt Stanford	Added functionality to use NORECOVERY or STANDBY for further TLog or Diff restores
**	20071114	Matt Stanford	Added line to test for existence of DB.  If not exists then unconditionally do restore.
**	20071129	Matt Stanford	Fixed version from 20071114 to be compatible with SQL 2000
**	20071203	Matt Stanford	Modified db select query to only return online dbs
**	20080320	Adam Bean		Changed kill to use new kill procedure
**  20081017	Adam Bean		Added @EncryptionKey
**  20090625	Adam Bean		Added @ChangeDBOwner & @NewDBOwner
**	20090626	Matt Stanford	Added @DropDB parameter to KillProcess call
**	20090629	Matt Stanford	Added @ShowProcesses parameter to KillProcess call
**  20090728	Adam Bean		Fixed for case sensitive collation
**	20090929	Adam Bean		Global header formatting & general clean up
**	20091014	Adam Bean		Resolved issue with changing owner if database was backed up in single user/read only
**	20120201	Adam Bean		Added @DeleteHistory for KillProcesses call
**	20130403	Adam Bean		Updates for newer version of LiteSpeed (#files and #header expansions)
								Changed @Move commands to use single quotes instead of double
								Commented out check on "Test to see if the backup will process OK"
**	20161227	Jeff Gogel	Removed references to old master.sys.sysdatabases
********************************************************************************************************/

CREATE PROCEDURE [dbo].[LiteSpeedRestoreDB]
(
	@filename				VARCHAR(1000)
	,@RestoreFreshnessHours TINYINT			= 0
	,@NewDBName				VARCHAR(512)	= NULL
	,@DestDataPath			VARCHAR(512)	= NULL
	,@DestLogPath			VARCHAR(512)	= NULL
	,@Recover				TINYINT			= 1
	,@Standby				TINYINT			= 0
	,@EncryptionKey			VARCHAR(512)	= NULL
	,@ChangeDBOwner			BIT				= 1
	,@NewDBOwner			VARCHAR(48)		= NULL
	,@DeleteHistory			BIT				= 0
	,@Debug					BIT				= 0
)

AS

SET NOCOUNT ON

--------------------------------------------------------------------------------------------------
-- DEBUGGING GARBAGE
--DECLARE 
--	@DestLogPath				VARCHAR(512)
--	,@DestDataPath				VARCHAR(512)
--	,@filename					VARCHAR(1000)
--	,@RestoreFreshnessHours		TINYINT
--	,@Debug						TINYINT
--	,@NewDBName					VARCHAR(512)
--	,@Recover					TINYINT
--	,@Standby					TINYINT
--
--set @filename = 'E:\APM_FG_full.lsbak'
--set @NewDBName = 'APM_FG'
----set @DestLogPath = 'K:\SQLLogs\Default\'
----set @DestDataPath = 'M:\SQLData\Default\'
--SET @RestoreFreshnessHours = 0
--SET @Recover = 0
--SET @Standby = 1
--set @Debug = 0

---------------------------------------------------------------------------------------------------

declare
	@DBName						VARCHAR(512)
	,@OrigDBName				VARCHAR(512)
	,@SQL						NVARCHAR(4000)
	,@tempSQL					VARCHAR(1000)
	,@killSQL					VARCHAR(1000)
	,@sort						int
	,@didDelete					TINYINT
	,@didRestore				TINYINT
	,@scriptKickoff				DATETIME
	,@LastRestoreDate			DATETIME
	,@intentionallySkipped		TINYINT
	,@IsRenamingDB				TINYINT


set @RestoreFreshnessHours		= isnull(@RestoreFreshnessHours,0)

set @scriptKickoff				= getdate()
set @didDelete					= 0
set @didRestore					= 0
set @intentionallySkipped		= 0

declare @cmds table (
	sort				INT
	,SQL				VARCHAR(4000)
)

DECLARE @PhysFiles TABLE (
	FileID				INT IDENTITY
	,OrigFileName		VARCHAR	(512)
	,NewFileName		VARCHAR	(512)	
	,LogicalName		VARCHAR	(512)
	,ContainsBUName		TINYINT DEFAULT 0
	,IsLogFile			TINYINT
)

CREATE TABLE #files (
	LogicalName			VARCHAR	(256)
	,PhysicalName		VARCHAR	(512)
	,Type				VARCHAR	(10)
	,FileGroupName		VARCHAR	(100)
	,Size				BIGINT
	,MaxSize			BIGINT
	,FileId				INT
	,BackupSizeInBytes	BIGINT
	,FileGroupId		INT  
)

create table #header (
	filnum				NVARCHAR(100),
	bakfor				NVARCHAR(100),
	guid				NVARCHAR(100),
	bacnam				NVARCHAR(100),
	bacdes				NVARCHAR(100),
	bactyp				NVARCHAR(100),
	expdat				DATETIME,
	compre				NVARCHAR(100),
	positi				NVARCHAR(100),
	devtyp				NVARCHAR(100),
	usenam				NVARCHAR(100), -- 11
	sernam				NVARCHAR(100),
	datnam				NVARCHAR(100),
	datver				NVARCHAR(100),
	datcre				DATETIME,
	bacsiz				NVARCHAR(100),
	firlsn				NVARCHAR(100),
	laslsn				NVARCHAR(100),
	chelsn				NVARCHAR(100),
	difbas				NVARCHAR(100),
	bacsta				DATETIME,		--21
	bacfin				DATETIME,
	sorord				NVARCHAR(100),
	codpag				NVARCHAR(100),
	comlev				NVARCHAR(100),
	sofven				NVARCHAR(100),
	sofvma				NVARCHAR(100),
	sofvmi				NVARCHAR(100),
	sofvbu				NVARCHAR(100),
	macnam				NVARCHAR(100),
	bindin				NVARCHAR(100),
	recfor				NVARCHAR(100),
	encryp				NVARCHAR(100),
	IsCopyOnly			BIT
)
	
-- Get the Backup file header info
insert into #header
exec master.dbo.xp_restore_headeronly @filename = @filename

SELECT @OrigDBName = datnam FROM #header WHERE filnum = 1

-- Restore DB AS name
IF (@NewDBName IS NOT NULL)
BEGIN
	SET @DBName = @NewDBName
	SET @IsRenamingDB = 1
END
ELSE
BEGIN
	SET @DBName = @OrigDBName
	SET @IsRenamingDB = 0
END

-- Test to see if the backup will process OK
--if not exists(select * from #header where bactyp = 1 and cast(comlev as tinyint) <= cast(left(cast(SERVERPROPERTY('productversion') as varchar(10)),1) + '0' as tinyint))
--	RAISERROR('Backup type or compatibility level mismatch',20,1) with LOG

insert into #files
exec master.dbo.xp_restore_filelistonly @filename = @filename

-- Rename the files as necessary (if the restore changes names)
INSERT INTO @PhysFiles (OrigFileName,IsLogFile,LogicalName)
SELECT 
	PhysicalName
	,CASE [Type]
		WHEN 'L' THEN 1
		ELSE 0
	END AS IsLogFile
	,LogicalName
FROM #files
	
-- Find where the file names contain the backup name
UPDATE 
	@PhysFiles 
SET	
	ContainsBUName = 1,
	NewFileName = REPLACE(OrigFileName,@OrigDBName,@DBName)
WHERE CHARINDEX(@OrigDBName,OrigFileName) > 0

-- Change the file names of any files that don't have the DB name in them
UPDATE
	@PhysFiles
SET
	ContainsBUName = 0,
	NewFileName = '\' + @OrigDBName + '_' + @NewDBName + '_' + LTRIM(STR(FileID)) + SUBSTRING(OrigFileName,LEN(OrigFileName) - CHARINDEX('.',REVERSE(OrigFileName)) + 1,100)
WHERE CHARINDEX(@OrigDBName,OrigFileName) <= 0

-- Revert the changes if we're not renaming this thing
IF @IsRenamingDB = 0
BEGIN
	UPDATE
		@PhysFiles
	SET 
		NewFileName = OrigFileName
END

-- Base restore command
INSERT INTO @cmds (sort,SQL) values (1,'EXEC master.dbo.xp_restore_database')

INSERT INTO @cmds (sort,SQL)  
	VALUES(2,'@database = ''' + @DBName + '''')

INSERT INTO @cmds (sort,SQL) values (2, '@filename = ''' + @filename + '''')

--insert into @cmds (sort,SQL) values (9, '@priority = 2')

-- Add encryptionkey
IF @EncryptionKey IS NOT NULL
	INSERT INTO @cmds (sort,SQL) VALUES (3,'@EncryptionKey = ''' + @EncryptionKey + '''')
	
INSERT INTO @cmds (sort,SQL) values (9, '@with = ''REPLACE''')

--- DETERMINE THE DATA/LOG DIRECTORIES
if (@DestLogPath is null) or (@DestDataPath is null)
begin
	
	-- Find existing database, use existing locations
	if exists (select name from [dbo].[sysdatabases_vw] where name = @DBName)
	BEGIN

		-- Get the log file directory
		SET @SQL = 'SELECT @in_dir = SUBSTRING(filename,0,LEN(filename)-CHARINDEX(''\'',REVERSE(replace(filename,'' '','''')))+2) FROM [' + @DBName + '].dbo.sysfiles WHERE fileid = 2'
		EXEC sp_executesql @SQL, N'@in_dir VARCHAR(512) OUTPUT', @in_dir = @DestLogPath OUTPUT

		-- Get the data file directory
		SET @SQL = 'SELECT @in_dir = SUBSTRING(filename,0,LEN(filename)-CHARINDEX(''\'',REVERSE(replace(filename,'' '','''')))+2) FROM [' + @DBName + '].dbo.sysfiles WHERE fileid = 1'
		EXEC sp_executesql @SQL, N'@in_dir VARCHAR(512) OUTPUT', @in_dir = @DestDataPath OUTPUT

	END
	ELSE
	BEGIN
		-- Get the Default data/log directories

		select @DestLogPath = admin.dbo.SQLServerLogDir_fn()

		select @DestDataPath = admin.dbo.SQLServerDataDir_fn()
	END
end


-- Make sure the Data/Log paths contain a trailing "\"
if right(@DestLogPath,1) <> '\'
	set @DestLogPath = @DestLogPath + '\'

if right(@DestDataPath,1) <> '\'
	set @DestDataPath = @DestDataPath + '\'

-- Generate the MOVE commands
INSERT INTO @cmds (sort,SQL)
SELECT 
	5
	,CASE IsLogFile
		WHEN 1 then '@with = ''MOVE ''''' + LogicalName + ''''' to ''''' + 
			@DestLogPath + SUBSTRING(NewFileName,LEN(NewFileName) - CHARINDEX('\',REVERSE(NewFileName)) + 2,100) + ''''''''
		ELSE '@with = ''MOVE ''''' + LogicalName + ''''' to ''''' + 
			@DestDataPath + SUBSTRING(NewFileName,LEN(NewFileName) - CHARINDEX('\',REVERSE(NewFileName)) + 2,100) + ''''''''
	END as MoveCommand
FROM @PhysFiles

-- Generate the standby command
IF @Standby = 1 and @Recover = 0
BEGIN
	INSERT INTO @cmds (sort,SQL)
	VALUES (10,'@with = ''STANDBY = "' + @DestLogPath + @DBName + '_STANDBY.stb' + '"''')
END
ELSE
BEGIN
	IF @Recover = 0
		INSERT INTO @cmds (sort,SQL) VALUES (10, '@with = ''NORECOVERY''')
	ELSE
		INSERT INTO @cmds (sort,SQL) VALUES (10, '@with = ''RECOVERY''')
END

drop table #files
drop table #header

PRINT ('Successfully read backup information for ' + @DBName + char(10) + '     Kicking off restore logic now')

-- Check to make sure that we didn't just restore this guy
set @LastRestoreDate = isnull((select max(restore_date) from msdb.dbo.restorehistory where destination_database_name = @DBName),dateadd(year,-1,getdate()))

if 
	(dateadd(hour,@RestoreFreshnessHours,@LastRestoreDate) < @scriptKickoff) 
	or @RestoreFreshnessHours = 0
	or NOT EXISTS(SELECT * FROM [dbo].[sysdatabases_vw] WHERE name = @DBName)
BEGIN
	print('Last restore was more than ' + cast(@RestoreFreshnessHours as varchar(3)) + ' hours ago.  We''re doing a restore')

	-------------------------------------------------------
	-- OK, the commands have been built, now throw them all together
	-------------------------------------------------------
	declare curs1 cursor for 
		select sort,SQL from @cmds order by sort asc

	open curs1

	fetch next from curs1 into @sort,@tempSQL

	set @SQL = ''
	WHILE @@FETCH_STATUS = 0
	BEGIN

		set @SQL = @SQL + @tempSQL

		if (@sort not in (1,10))
			set @SQL = @SQL + ',' + char(10)
		else
			set @SQL = @SQL + char(10)

		fetch next from curs1 into @sort,@tempSQL
	END

	close curs1
	deallocate curs1

	SET @killSQL = 'print(''Killing the database'')
	EXEC [dbo].[KillProcess] @DBName = ''' + @DBName + ''', @DropDB = 1, @ShowProcesses = 0, @DeleteHistory = ' + CAST(@DeleteHistory AS VARCHAR) + '
	print(''    Killing done'')
	print(''Restoring ' + @DBName + ''' + char(10))' + char(10) + char(10) 

	-- Check to see if the database exists
	if exists(select * from [dbo].[sysdatabases_vw] where name = @DBName)
	BEGIN
		if @Debug = 1
			print (@killSQL)
		else
			exec (@killSQL)

		if (select count(*) from [dbo].[sysdatabases_vw] where name = @DBName) = 0
			set @didDelete = 1
	END

	if @Debug = 1
		print (@SQL)
	else
		exec (@SQL)

	set @LastRestoreDate = isnull((select max(restore_date) from msdb.dbo.restorehistory where destination_database_name = @DBName),dateadd(year,-1,getdate()))

	-- Test to see if the restore worked
	if @LastRestoreDate >= @scriptKickoff
	BEGIN
		set @didRestore = 1
	END
END
ELSE
BEGIN
	print('Did not restore anything... the last restore was pretty recent')
	SET @intentionallySkipped = 1
END

IF @ChangeDBOwner = 1
BEGIN
	-- If new owner was specified, use that, if not, determine primary sa account
	IF @NewDBOwner IS NULL
		SET @NewDBOwner = (SELECT dbo.WhatIsGodsName_fn())
		
	-- Make sure the database isn't in single user or read only
	IF (SELECT [user_access_desc] FROM [dbo].[sysdatabases_vw] WHERE [name] = @DBName) = 'SINGLE_USER'
		EXEC('ALTER DATABASE [' + @DBName + '] SET MULTI_USER')
	IF (SELECT [is_read_only] FROM [sysdatabases_vw] WHERE [name] = @DBName) = 1
		EXEC('ALTER DATABASE [' + @DBName + '] SET READ_WRITE')
			
	-- Change owner
	SET @SQL = 'USE [' + @DBName + '] EXEC [sp_changedbowner] @loginame = ''' + @NewDBOwner + ''''
	IF @Debug = 1
		PRINT (@SQL)
	ELSE
		EXEC (@SQL)
END

select 
	@didDelete as DidDelete, 
	@didRestore as DidRestore, 
	@intentionallySkipped as IntentionallySkipped,
	@LastRestoreDate as LastRestoreDateTime

set nocount off
