IF OBJECT_ID('dbo.JobLogging','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[JobLogging]
GO

/******************************************************************************
* Name
	[dbo].[JobLogging]

* Author
	Adam Bean
	
* Date
	2009.11.01
	
* Synopsis
	Adds job step logging to specified job(s)
	
* Description
	Gives the ability to quickly and easily add output logging per job, per step. Can specify a central location or find the default install location.

* Examples
	EXEC [dbo].[JobLogging] @ExcludeExisting = 1
	EXEC [dbo].[JobLogging] @JobName = 'DBA - REINDEX, DBA - CHECKDB, DBA - UPDATESTATS'', @JobLogPath = '\\SERVERNAME\SQLJobLogs
	EXEC [dbo].[JobLogging] @JobLogPath = '\\SERVERNAME\SQLJobLogs, @CentralLogging = 1
	EXEC [dbo].[JobLogging] @Debug = 1

* Dependencies
	dbo.Split_fn
	dbo.SQLServerJobLogDir_fn

* Parameters
	@JobName			= Job name(s) to add logging for (CSV supported) [leave NULL for all]
	@JobNameExclude		= Job name(s) to exclude logging for (CSV supported) [leave NULL for all]
	@JobLogPath			= Path for file output [leave NULL to use ErrorLog location]
	@JobExtension		= File extension for outfile file
	@flags				= Control the logging behavior (defaulted to 0) http://technet.microsoft.com/en-us/library/ms189827.aspx
	@CentralLogging		= Used to build output path based on server and instance name when all output is wanted to be stored centrally
	@ExcludeExisting	= Do not update any step outputs that already have an output
	@DisplayResults		= Show or omit results (defaulted to show)
	@Debug				= Show work to be done but don't execute procedure
	
* Notes
	The file output name is based on the job name + step id + step name
*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20091112	Adam Bean		Added @ExcludeExisting
	20091207	Adam Bean		Added single quotes to the replace on the output file
	20100117	Adam Bean		Added exclusion for QueueReader job steps
	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
	20120205	Adam Bean		Added parameter (@DisplayResults) to show or omit results
	20131016	Adam Bean		Updated step exclusions to be properly cased as it broke on certain collations
	20140205	Adam Bean		Added a join to sysdatabases to avoid failure when a job step database no longer exists
	20140330	Adam Bean		Fixed the previous update which broke steps that don't have a database
								Added @flags to control the logging behavior
								General cleanup
	20140513	Adam Bean		Updated again to resolve job steps that don't have an active database (this time it works!)
******************************************************************************/

CREATE PROCEDURE [dbo].[JobLogging]
(
	@JobName				NVARCHAR(256)	= NULL
	,@JobNameExclude		NVARCHAR(256)	= NULL
	,@JobLogPath			VARCHAR(256)	= NULL
	,@JobExtension			VARCHAR(8)		= '.txt'
	,@flags					TINYINT			= 0
	,@CentralLogging		BIT				= 0
	,@ExcludeExisting		BIT				= 0
	,@DisplayResults		BIT				= 1
	,@Debug					BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@ServerName				VARCHAR(128)
	,@InstanceName			VARCHAR(128)
	,@StepId				TINYINT
	,@StepName				NVARCHAR(256)
	,@CurrentLogLocation	NVARCHAR(256)
	,@OutputFileName		VARCHAR(512)
	,@SQL					VARCHAR(512)
	,@DBName				NVARCHAR(128)
	,@StepType				VARCHAR(64)

-- Populate some working variables
SET @ServerName = (SELECT @@SERVERNAME)
SET @InstanceName = (SELECT CAST(SERVERPROPERTY('InstanceName') AS VARCHAR(128)))

-- Build logging location based on server/instance and @JobLogPath (if passed)
IF @CentralLogging = 1
BEGIN
	-- If central logging is desired, a path must be specified
	IF @JobLogPath IS NULL 
	BEGIN
		PRINT '@JobLogPath must be passed if you''re using @CentralLogging'
		RETURN 2
	END
	ELSE
	BEGIN
		-- Add trailing backslash to @JobLogPath
		IF RIGHT(@JobLogPath,1) != '\'
			SET @JobLogPath = @JobLogPath + '\'

		-- Build logging path based on servername and instancename for central logging
		IF @InstanceName IS NOT NULL 
			BEGIN
				SET @ServerName = LEFT(@ServerName,CHARINDEX('\',@ServerName) - 1)
				SET @JobLogPath = @JobLogPath + @ServerName + '\' + @InstanceName + '\'
			END
		ELSE
			BEGIN
				SET @ServerName = @ServerName
				SET @JobLogPath = @JobLogPath + @ServerName + '\DEFAULT\'
			END
	END
END
ELSE
BEGIN
	-- Change the logging location to the location of the error log if not specified
	IF @JobLogPath IS NULL
		SET @JobLogPath = [dbo].[SQLServerJobLogDir_fn]()

	-- Add trailing backslash to @JobLogPath
	IF RIGHT(@JobLogPath,1) != '\'
		SET @JobLogPath = @JobLogPath + '\'
END

-- Stage up all the supporting data
SELECT
	REPLACE(j.[name], '''','''''')	AS [JobName]
	,js.[step_id]					AS [StepId]
	,js.[step_name]					AS [StepName]
	,js.[subsystem]					AS [StepType]
	,js.[database_name]				AS [DBName]
	,js.[output_file_name]			AS [CurrentLocation]
	,CAST(NULL AS VARCHAR(1024))	AS [NewLocation]
	,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(j.[name]
		+ '_(' + CAST(js.[step_id] AS VARCHAR) + ')_' + js.[step_name],
		+ '''', ''),'\', ' '), '/', ' '), ':', ' '), '*', ' '), '?', ' '), '<', ' '), '>', ' '), '|', ' '), '"', ' ')	AS [OutputFileName]
	,CAST(0 AS BIT)					AS [Changed]
	,CAST(NULL AS BIT)				AS [Supported]
	,CAST(NULL AS VARCHAR(256))		AS [SupportReason]
INTO #JobLogging
FROM [msdb].[dbo].[sysjobs] j
JOIN [msdb].[dbo].[sysjobsteps] js
	ON j.[job_id] = js.[job_id]
LEFT JOIN [dbo].[Split_fn](@JobName,',') ji
	ON j.[name] = ji.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@JobNameExclude,',') je
	ON j.[name] = je.[item] COLLATE DATABASE_DEFAULT
WHERE ((ji.[item] IS NOT NULL AND @JobName IS NOT NULL) OR @JobName IS NULL) -- Specified job(s), or all
AND je.[item] IS NULL -- All but excluded job(s)

-- Set the exclusions
-- Certain step types (subsystems) do not allow for step logging
UPDATE #JobLogging
SET 
	[Supported] = 0
	,[SupportReason] = 'Job step type (subsystem) is set ' + [StepType] + '.'
WHERE [StepType] IN ('ActiveScripting','Distribution','Snapshot','Logreader','QueueReader')

-- If the database no longer exists for a TSQL step, no logging can exist
UPDATE jl
SET 
	jl.[Supported] = 0
	,jl.[SupportReason] = 'Job step type (subsystem) is set TSQL and no database exists.'
FROM #JobLogging jl
LEFT JOIN [dbo].[sysdatabases_vw] s
	ON jl.[DBName] = s.[name]
WHERE s.[name] IS NULL AND jl.[StepType] = 'TSQL' 

-- If @ExcludeExisting was passed, skip those that already have a path
IF @ExcludeExisting = 1
UPDATE #JobLogging
SET 
	[Supported] = 0
	,[SupportReason] = '@ExcludeExisting was passed, location will not change.'
WHERE [CurrentLocation] IS NOT NULL

-- Set the rest to supported
UPDATE #JobLogging
SET [Supported] = 1 
WHERE [Supported] IS NULL

-- Update the new location for support steps
UPDATE #JobLogging
SET [NewLocation] = @JobLogPath + [OutputFileName] + @JobExtension
WHERE [Supported]= 1

-- Add the logging to each job step
DECLARE #JobList CURSOR LOCAL STATIC FOR
SELECT
	[JobName]
	,[StepId]
	,[StepName]
	,[StepType]
	,[DBName]
	,[CurrentLocation]
	,[NewLocation]
FROM #JobLogging
WHERE [Supported] = 1

OPEN #JobList
FETCH NEXT FROM #JobList INTO @JobName, @StepId, @StepName, @StepType, @DBName, @CurrentLogLocation, @OutputFileName
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Build the update statement
	SET @SQL = 
	'
	EXEC [msdb].[dbo].[sp_update_jobstep] 
		@job_name = ''' + @JobName + '''
		,@step_id = ' + CAST(@StepId AS VARCHAR) + '
		,@output_file_name = ''' + @OutputFileName + '''
		,@flags = ' + CAST(@flags AS VARCHAR) + '
	'

	-- Print or execute
	IF @Debug = 1
		PRINT (@SQL)
	ELSE
	BEGIN
		EXEC (@SQL)
		-- Update working table to show that log file location was changed
		UPDATE #JobLogging
		SET [Changed] = 1
		WHERE [JobName] = @JobName
		AND [StepName] = @StepName
	END

FETCH NEXT FROM #JobList INTO @JobName, @StepId, @StepName, @CurrentLogLocation, @StepType, @DBName, @OutputFileName
END
CLOSE #JobList
DEALLOCATE #JobList

-- Return work done
IF @DisplayResults = 1
	SELECT * FROM #JobLogging
	ORDER BY [JobName], [StepId]

SET NOCOUNT OFF