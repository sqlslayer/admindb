﻿IF OBJECT_ID('dbo.HADR_AGIsPrimary_fn','FN') IS NOT NULL
        DROP FUNCTION [dbo].[HADR_AGIsPrimary_fn]
GO

/******************************************************************************
* Name
	[dbo].[HADR_AGIsPrimary_fn]

* Author
	Jeff Gogel
	Original Source: http://sqlmag.com/blog/alwayson-availability-groups-and-sql-server-jobs-part-7-detecting-primary-replica-ownership
	
* Date
	2015.05.12
	
* Synopsis
	Function to check if an Availability Group is currently in the Primary role
	
* Description
	Checks to see if the Availability Group name passed in is currently serving as the Primary

* Examples
	SELECT [dbo].[HADR_AGIsPrimary_fn] ('AGNameHere')

* Dependencies
	n/a

* Parameters
	@AGName		- Name of Availability Group to check
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------

******************************************************************************/
CREATE FUNCTION [dbo].[HADR_AGIsPrimary_fn] (@AGName sysname)
RETURNS bit 
AS
BEGIN 
	DECLARE @PrimaryReplica sysname; 

	-- Return 1 if HADR is not Enabled
	IF ((SELECT SERVERPROPERTY ('IsHadrEnabled')) = 0)
	BEGIN
		RETURN 1
	END

	SELECT @PrimaryReplica = hags.[primary_replica]
	FROM [sys].[dm_hadr_availability_group_states] hags
	JOIN [sys].[availability_groups] ag 
		ON ag.[group_id] = hags.[group_id]
	WHERE ag.[name] = @AGName;

	IF UPPER(@PrimaryReplica) =  UPPER(@@SERVERNAME)
		RETURN 1; -- primary

	RETURN 0; -- not primary
END