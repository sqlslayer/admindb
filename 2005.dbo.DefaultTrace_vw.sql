IF OBJECT_ID('dbo.DefaultTrace_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[DefaultTrace_vw]
GO

/******************************************************************************
**	Name:			[dbo].[DefaultTrace_vw]
**	Desc:			View to query all available default traces
**	Auth:			Adam Bean
**	Date:			2010.04.16
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20100707	Adam Bean		Added is_default = 1 to only query the default traces
**  20120201	Kathy Toth		Modified the view to include SQL 2008 R2 support for the file path
********************************************************************************************************/

CREATE VIEW [dbo].[DefaultTrace_vw]

AS

SELECT 
	tg.[LoginName]
	,tg.[SessionLoginName]
	,ss.[name]				AS [PrincipalName]
	,tg.[NTUserName]
	,tg.[StartTime]
	,tg.[SPID]
	,tg.[HostName]
	,tg.[ApplicationName]
	,tg.[ServerName]
	,tg.[DatabaseName]
	,tg.[ObjectName]
	,tg.[EventClass]
	,tg.[EventSubClass]
	,te.[name]				AS [EventName]
	,tc.[name]				AS [CategoryName]
	,tg.[TextData]
FROM sys.traces st
CROSS APPLY fn_trace_gettable
	(CASE 
		WHEN CHARINDEX('_',st.[path]) <> 0 
			THEN SUBSTRING(st.[path], 1, LEN(st.[path])-CHARINDEX('_',REVERSE(st.[path]))) + '.trc' 
		ELSE st.[path] 
        END
        ,st.[max_files]
	) tg
JOIN sys.trace_events te
	ON tg.[eventclass] = te.[trace_event_id]
JOIN sys.trace_categories tc
	ON te.[category_id] = tc.[category_id]
LEFT JOIN sys.server_principals ss
	ON CONVERT(VARBINARY(MAX), tg.[loginsid]) = ss.[sid]
WHERE st.[is_default] = 1