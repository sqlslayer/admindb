IF OBJECT_ID('[dbo].[SQLAgentStart]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[SQLAgentStart]
GO

/*******************************************************************************************************
**	Name:			dbo.SQLAgentStart
**	Desc:			Run with a SQLAgent startup job to send notification that instance/agent was restarted
					Will notify of current node if instanced
**	Auth:			Adam Bean (SQLSlayer.com)
**	Dependancies:	Database mail configured
**  Parameters:		@Recipients = Email recipients
**	Date:			2009.05.15
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090708	Adam Bean		Changed @@SERVERNAME to SERVERPROPERTY('ServerName')
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE PROCEDURE [dbo].[SQLAgentStart]
(
	@Recipients		VARCHAR(1024)
)

AS

SET NOCOUNT ON

DECLARE 
	@ServerName		VARCHAR(64)
	,@IsInstanced	BIT
	,@StartTime		DATETIME
	,@Subject		VARCHAR(64)
	,@Body			VARCHAR(128)
	,@ComputerName	VARCHAR(64)

SET @ServerName		= (SELECT CAST(SERVERPROPERTY('ServerName') AS VARCHAR(64)))
SET @IsInstanced	= (SELECT CAST(SERVERPROPERTY('IsClustered') AS BIT))
SET @StartTime		= (SELECT [login_time] FROM [master].[sys].[dm_exec_sessions] WHERE [session_id] = 1)
SET @Subject		= 'SQL Agent started on ' + @ServerName + ' @ ' + CONVERT(VARCHAR(20), GETDATE(), 100) + ''
SET @Body			= 'SQL has been running since ' + CAST(@StartTime AS VARCHAR) + '.'

IF @IsInstanced = 1
BEGIN
	SET @ComputerName	= (SELECT CAST(SERVERPROPERTY('ComputerNamePhysicalNetBIOS') AS VARCHAR(64)))
	SET @Body			= @Body + CHAR(10) + CHAR(10) + 'SQL is currently running on ' + @ComputerName + '.'
END

-- Send mail using sp_send_dbmail
EXEC [msdb].[dbo].[sp_send_dbmail]
	@Recipients		= @Recipients
	,@Subject		= @Subject
	,@Body			= @Body
	,@Body_format	= 'HTML'