IF OBJECT_ID('[dbo].[JobActivityMonitor_JobHistory]','P') IS NOT NULL 
DROP PROCEDURE [dbo].[JobActivityMonitor_JobHistory]
GO

/*******************************************************************************************************
**	Name:			dbo.JobActivityMonitor_JobHistory 
**	Desc:			Pulls back job history for specified job, used with Job Activity Monitor procedure
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2008.09.08
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090708	Adam Bean		Changed @@SERVERNAME to SERVERPROPERTY('ServerName')
**  20090929	Adam Bean		Resolved collation issues
**	20090929	Adam Bean		Global header formatting & general clean up
**  20151208	Jeff Gogel		Updated duration to return seconds, added additional columns, removed spaces from column names
********************************************************************************************************/

CREATE PROCEDURE [dbo].[JobActivityMonitor_JobHistory]
(
	@DateStart	DATETIME = NULL
	,@DateEnd	DATETIME = NULL
	,@JobName	sysname
)

AS

SET NOCOUNT ON

SELECT
	a.*
FROM
(
	SELECT 
		SERVERPROPERTY('ServerName')	AS [ServerName]
		,sj.[name]						AS [JobName]
		,jh.[step_id]					AS [StepID]
		,jh.[step_name]					AS [StepName]
		,[dbo].[agent_datetime](run_date, run_time) AS [RunDate]
		,(jh.[run_duration]/10000*3600 + (jh.[run_duration]/100)%100*60 + jh.[run_duration]%100 + 31 / 60) AS [SecDuration]
		,CASE jh.[run_status] 
			WHEN 0 THEN 'Failed'
			WHEN 1 THEN 'Succeeded'
			WHEN 2 THEN 'Retry'
			WHEN 3 THEN 'Canceled'
			WHEN 4 THEN 'In progress'
		END								AS [Status]
		,jh.[message]					AS [Message]
		,SUM(CASE WHEN jh.[run_status] = 1 THEN 1 ELSE 0 END) AS [Successful]
		,SUM(CASE WHEN jh.[run_status] = 0 THEN 1 ELSE 0 END) AS [Failed]
		,SUM(CASE WHEN jh.[run_status] = 3 THEN 1 ELSE 0 END) AS [Cancelled]
		,jh.[retries_attempted]			AS [RetriesAttempted]
		,jh.[sql_message_id]			AS [SQLMessageID]
		,jh.[sql_severity]				AS [SQLSeverity]
		,jh.[run_status]				AS [RunStatus]
		,jh.[instance_id]				AS [InstanceID]
	FROM [msdb].[dbo].[sysjobhistory] jh
	INNER JOIN [msdb].[dbo].[sysjobs] sj
		ON sj.[job_id] = jh.[job_id]
	WHERE sj.[name] = @JobName
	AND jh.[step_id] != 0
	GROUP BY sj.[name], jh.[step_id], jh.[step_name], jh.[run_date], jh.[run_time], jh.[run_duration], jh.[run_status], jh.[message], jh.[retries_attempted], jh.[sql_message_id], jh.[sql_severity], jh.[run_status], jh.[instance_id]
) a
WHERE ((a.[RunDate] BETWEEN @DateStart AND @DateEnd) 
OR (@DateStart IS NULL AND @DateEnd IS NULL))
ORDER BY a.[RunDate] DESC

SET NOCOUNT OFF