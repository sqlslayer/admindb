IF OBJECT_ID('dbo.ChangeSchema','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[ChangeSchema]
GO

/******************************************************************************
* Name
	[dbo].[ChangeSchema]

* Author
	Adam Bean
	
* Date
	2012.09.16
	
* Synopsis
	Change schema ownership of specified object(s)
	
* Description
	Robust procedure allowing for a multitude of CSV and wildcard parameters (database, object name/type, schema) to create a new schema 
		and transfer ownership of specified object(s)/type(s) to a new schema.

* Examples
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @debug = 1
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @ObjectName = 'MoveDatabase,SpaceUsed,TableSizes', @debug = 1
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @ObjectName = 'Maintenance%', @debug = 1
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @ObjectNameExclude = 'Maintenance%', @debug = 1
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @ObjectType = 'V,U', @debug = 1
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @ObjectTypeExclude = 'U', @debug = 1
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @SourceSchema = 'test', @debug = 0
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @SourceSchema = 'test', @ObjectTypeExclude = 'U', @debug = 1
	EXEC [ChangeSchema] @DBName = 'admin', @NewSchema = 'new', @SourceSchema = 'test', @ObjectName = 'Get%', @debug = 1

* Dependencies
	dbo.DBSearch_fn
	dbo.Split_fn
	dbo.sysdatabases_vw

* Parameters
	@DBName = Database(s) to be queried (CSV supported, NULL for all, accepts % for searching)
	@DBNameExclude = Database(s) to exclude (CSV supported, NULL for all, accepts % for searching)
	@NewSchema = New schema name to transfer ownership to (will create it if it doesn't exist)
	@Authorization = Owner of the schema (defaults to dbo)
	@SourceSchema = Source schema(s) to be modified (CSV supported, NULL for all)
	@SourceSchemaExclude = Source schema(s) to exclude (CSV supported, NULL for all)
	@ObjectType = Object type(s) to switch schema on (CSV supported, NULL for all, accepts % for searching)
	@ObjectTypeExclude = Object type(s) to exclude (CSV supported, NULL for all, accepts % for searching)
	@ObjectName = Object(s) to switch schema on (CSV supported, NULL for all, accepts % for searching)
	@ObjectNameExclude = Object(s) to exclude (CSV supported, NULL for all, accepts % for searching)
	@Debug	= Display but don't run queries 

* Notes
	Always run with @Debug = 1 to ensure you're not potentially altering too many objects
	You can not change transfer an object that is owned by a parent object (PK's, Contraints, Triggers) [they are omitted by default]

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credio.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	2017-02-10	Mike Wolfe		Changed orderby clauses to reference columns not positions
******************************************************************************/

CREATE PROCEDURE [dbo].[ChangeSchema]
(
	@DBName						NVARCHAR(2048)	= NULL
	,@DBNameExclude				NVARCHAR(2048)	= NULL  
	,@NewSchema					NVARCHAR(128)
	,@Authorization				NVARCHAR(128)	= 'dbo'
	,@SourceSchema				NVARCHAR(2048)	= NULL
	,@SourceSchemaExclude		NVARCHAR(2048)	= NULL  
	,@ObjectType				NVARCHAR(2048)	= NULL
	,@ObjectTypeExclude			NVARCHAR(2048)	= NULL  
	,@ObjectName				NVARCHAR(2048)	= NULL
	,@ObjectNameExclude			NVARCHAR(2048)	= NULL  
	,@Debug						BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE
	@ObjectNameSearch			NVARCHAR(512)
	,@ObjectNameSearchExclude	NVARCHAR(512)
	,@ObjectTypeDesc			NVARCHAR(60)
	,@SQL						NVARCHAR(1024)

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'

-- Determine if we're doing search or a match for database or object
IF CHARINDEX('%',@DBName) > 0
	SET @DBName = (SELECT [dbo].[DBSearch_fn](@DBName))
IF CHARINDEX('%',@DBNameExclude) > 0
	SET @DBNameExclude = (SELECT [dbo].[DBSearch_fn](@DBNameExclude))
IF CHARINDEX('%',@ObjectName) > 0
BEGIN
	SET @ObjectNameSearch = @ObjectName
	SET @ObjectName = NULL
END
IF CHARINDEX('%',@ObjectNameExclude) > 0
BEGIN
	SET @ObjectNameSearchExclude = @ObjectNameExclude
	SET @ObjectNameExclude = NULL
END

-- Get all of the catalog views
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#ChangeSchema') IS NOT NULL
   DROP TABLE #ChangeSchema

-- Create the tables
SELECT TOP 0 *, [SchemaName] = CAST(NULL AS NVARCHAR(128)), [DBName] = CAST(NULL AS NVARCHAR(128)) INTO #sysobjects FROM [sys].[objects]

-- Retrieve data per database
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [dbo].[sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[is_read_only] = 0
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 
			
		-- sys.objects
		INSERT INTO #sysobjects
		SELECT *, SCHEMA_NAME([schema_id]), ''' + @DBName + ''' FROM [sys].[objects]
		WHERE [is_ms_shipped] = 0
		AND [type] NOT IN (''C'',''D'',''F'',''UQ'',''PK'',''TR'')
	')

FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs

-- Bring the data together using the parameters, into a smaller working set
SELECT 
	[DBName]
	,[SchemaName]			AS [CurrentSchema]
	,@NewSchema				AS [NewSchema]
	,[name]					AS [ObjectName]
	,[type]					AS [ObjectType]
	,[type_desc]			AS [ObjectTypeDesc]
	,0						AS [SchemaTransfered]
INTO #ChangeSchema
FROM #sysobjects o
LEFT JOIN [dbo].[Split_fn](@SourceSchema,',') ss
	ON o.[SchemaName] = ss.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@SourceSchemaExclude,',') sse
	ON o.[SchemaName] = sse.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@ObjectType,',') ot
	ON o.[type] = ot.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@ObjectTypeExclude,',') ote
	ON o.[type] = ote.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@ObjectName,',') obj
	ON o.[name] = obj.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@ObjectNameExclude,',') obje
	ON o.[name] = obje.[item] COLLATE DATABASE_DEFAULT
WHERE 
(
	((ss.[item] IS NOT NULL AND @SourceSchema IS NOT NULL) OR @SourceSchema IS NULL) -- Specified schema(s), or all
	AND sse.[item] IS NULL -- All but excluded schema(s)
)
AND
(
	((ot.[item] IS NOT NULL AND @ObjectType IS NOT NULL) OR @ObjectType IS NULL) -- Specified object type(s), or all
	AND ote.[item] IS NULL -- All but object type(s)
)
AND
(
	(o.[name] LIKE '%' + @ObjectNameSearch + '%') -- Specified object name(s) based on a search
	OR (o.[name] NOT LIKE '%' + @ObjectNameSearchExclude + '%') -- Exclude specified object name(s) based on a search
	OR (@ObjectNameSearch IS NULL AND @ObjectNameSearchExclude IS NULL)
)	
AND
(
	((obj.[item] IS NOT NULL AND @ObjectName IS NOT NULL) OR @ObjectName IS NULL) -- Specified object(s), or all
	AND obje.[item] IS NULL -- All but object(s)
)
ORDER BY [DBname], [SchemaName], @NewSchema, [type], [name]

-- Do the work
DECLARE #ChangeSchema CURSOR STATIC LOCAL FOR
SELECT
	[DBName]
	,[CurrentSchema]
	,[ObjectName]
	,[ObjectType]
	,[ObjectTypeDesc]
FROM #ChangeSchema

OPEN #ChangeSchema
FETCH NEXT FROM #ChangeSchema INTO @DBName, @SourceSchema, @ObjectName, @ObjectType, @ObjectTypeDesc
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Check to see if the new schema exists, if not, create it
	-- The create schema must be run in its own batch; thus the additional EXEC
	-- Transfer the object(s) to a new schema
	SET @SQL = '
	USE [' + @DBName + ']
	IF NOT EXISTS (SELECT [name] FROM sys.schemas WHERE [name] = ''' + @NewSchema + ''')
		EXEC(''CREATE SCHEMA [' + @NewSchema + '] AUTHORIZATION [' + @Authorization + ']'')

	ALTER SCHEMA [' + @NewSchema + '] TRANSFER [' + @SourceSchema + '].[' + @ObjectName + ']'

	IF @Debug = 1
		PRINT (@SQL)
	ELSE
	BEGIN
		EXEC (@SQL)
		-- Update working table to show that schema was transfered
		UPDATE #ChangeSchema
		SET [SchemaTransfered] = 1
		WHERE [DBName] = @DBName
		AND [ObjectName] = @ObjectName
	END

FETCH NEXT FROM #ChangeSchema INTO @DBName, @SourceSchema, @ObjectName, @ObjectType, @ObjectTypeDesc
END
CLOSE #ChangeSchema
DEALLOCATE #ChangeSchema

-- Return results
SELECT * FROM #ChangeSchema

SET NOCOUNT OFF