IF OBJECT_ID('[dbo].[RemoveComments_fn]','FN') IS NOT NULL
	DROP FUNCTION [dbo].[RemoveComments_fn]
	
GO
/******************************************************************************
* Name
	[dbo].[RemoveComments_fn]

* Author
	Matt Stanford
	
* Date
	2010.11.29
	
* Synopsis
	Inline function to strip T-SQL comments from a block of string.
	
* Description
	Looping inline function to remove all T-SQL comments (both C-Style and inline) from a block of text.
	The text block can and should contain all newline characters, etc in order to operate correctly.
	
	This function was built for searching purposes.  I was finding that when I searched for a string from sys.sql_modules
	I was matching on the comments more often than not.

* Examples
	SELECT [dbo].[RemoveComments_fn]('Some T-SQL')

* Dependencies
	If available.  List one per line.

* Parameters
	@SQL				- Block of T-SQL to strip comments from
	
* Notes
	Current version does not work correctly when a c-style comment is embedded in a bit of dynamic SQL (in a string)
	For example 
		PRINT('/*')
		PRINT('You'll never see this')
		SELECT 'You'll never see this either',* FROM sys.databases
		PRINT('*/')
	The above section will collapse down to "PRINT('')".  The function doesn't detect strings correctly for c-style comments.

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/
CREATE FUNCTION [dbo].[RemoveComments_fn](
	@SQL		NVARCHAR(4000)
)
RETURNS NVARCHAR(4000)
AS
BEGIN
DECLARE 
	@C1				NVARCHAR(4000)
	,@I1			INT -- comment start
	,@I2			INT -- comment end
	,@I3			INT	-- quote end
	,@SingleTics	INT -- number of single tics
	
SET @C1 = @SQL

SET @C1 = REPLACE(@C1,CHAR(13) + CHAR(10),CHAR(10))		-- Replace all CRLFs as LFs
SET @C1 = REPLACE(@C1,CHAR(10),CHAR(13))				-- Replace all LFs as CRs
	
-- Remove C-Style comments
WHILE CHARINDEX('/*',@C1) >= 1 AND CHARINDEX('*/',@C1) >= 1 AND (CHARINDEX('*/',@C1) > CHARINDEX('/*',@C1))
BEGIN
	SET @I1 = CHARINDEX('/*',@C1)
	SET @I2 = CHARINDEX('*/',@C1,@I1)

	SET @C1 = ISNULL(LEFT(@C1,@I1 - 1),'') + ISNULL(RIGHT(@C1,LEN(@C1) - @I2 -1),'')

END

-- Remove inline comments
WHILE CHARINDEX('--',@C1) >= 1
BEGIN
	SET @I1 = CHARINDEX('--',@C1)
	SET @I2 = CHARINDEX(CHAR(13),@C1,@I1)
	
	IF (@I2 <= 0) -- No terminator.  Must be at the end of the block.
	BEGIN
		SET @C1 = LEFT(@C1,@I1 - 1)
	END
	ELSE
	BEGIN 
		-- There are newlines after this, so figure out where to stop removing.
		SET @I3 = CHARINDEX(CHAR(39),@C1,@I1) -- Find the location of the next single quote.
		
		-- Count the number of single tics (single-quotes) that appear
		-- before the inline comment.  If odd, then we're inside of a string.
		SET @SingleTics = @I1 - LEN(REPLACE(LEFT(@C1,@I1),CHAR(39),''))
		
		IF (@SingleTics % 2 = 0)	-- Not inside of a string
			OR (@I2 < @I3)			-- Inside of a string, but no terminating quote on this line
			SET @C1 = LEFT(@C1,@I1 - 1) + RIGHT(@C1,LEN(@C1) - @I2 + 1)
		ELSE						-- Inside of a string with a terminating quote on this line
			SET @C1 = LEFT(@C1,@I1 - 1) + RIGHT(@C1,LEN(@C1) - @I3 + 1)
	END
END

RETURN @C1
END