IF OBJECT_ID('[dbo].[DBSearch_fn]','FN') IS NOT NULL 
	DROP FUNCTION [dbo].[DBSearch_fn]
GO

/******************************************************************************
* Name
	[dbo].[DBSearch_fn]

* Author
	Adam Bean
	
* Date
	2011.09.27
	
* Synopsis
	Returns a CSV for database names.
	
* Description
	Returns a CSV for database names either based on single names, or wildcard searching with %

* Examples
	SELECT dbo.DBSearch_fn('admin,dbacentral,citrix%')

* Parameters
	@name			- CSV list of names that can include a % anywhere within one of the names

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE FUNCTION [dbo].[DBSearch_fn]
(
	@name			NVARCHAR(4000)
)

RETURNS NVARCHAR(4000)

AS

BEGIN

	DECLARE 
		@DBName		NVARCHAR(128)
		,@DBNames	NVARCHAR(4000)

	SET @DBNames = ''
	
	DECLARE @databases TABLE
	(
		[name]		NVARCHAR(128)
	)

	-- Insert the dbnames into a temp table
	INSERT INTO @databases
	SELECT * FROM [dbo].[Split_fn](@name,',')

	-- Build our new strings
	DECLARE #dbs CURSOR LOCAL STATIC FOR 
	SELECT 
		[name]
	FROM @databases
	ORDER BY 1

	OPEN #dbs
	FETCH NEXT FROM #dbs INTO @DBName
	WHILE @@FETCH_STATUS = 0
	BEGIN

		 -- Determine which names have a wild card and populate the names appropriately
		IF CHARINDEX('%',@DBName) > 0
			SELECT @DBNames = @DBNames + COALESCE([name] + ',', '') FROM [dbo].[sysdatabases_vw] WHERE [name] LIKE @DBName
		ELSE
			SELECT @DBNames = @DBNames + COALESCE(@DBName + ',', '')

	FETCH NEXT FROM #dbs INTO @DBName
	END
	CLOSE #dbs
	DEALLOCATE #dbs

	-- Remove trailing comma
	SET @DBNames = LEFT(@DBNames,LEN(@DBNames) -1)

	-- Return the results
	RETURN @DBNames
END