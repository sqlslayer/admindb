IF OBJECT_ID('dbo.ExecutionPlans','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[ExecutionPlans]
GO

/******************************************************************************
**	Name:			[dbo].[ExecutionPlans]
**	Desc:			Retrieve all relevant information surrounding execution plans
**	Auth:			Adam Bean (SQLSlayer.com)
**	Notes:			Procedure utilized to support single argument OBJECT_NAME
**					NULL fields populated to mimic 2005 and later versions
**	Date:			2010.04.18
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  YYYYDDMM	Full Name	
********************************************************************************************************/

CREATE PROCEDURE [dbo].[ExecutionPlans]
(
	@OrderBy1			INT				= 1 -- DBName
	,@OrderBy2			INT				= 3 -- ObjectName
	,@OrderBy3			INT				= 4 -- ObjType
	,@DBName			VARCHAR(512)	= NULL
	,@DBNameExclude		VARCHAR(512)	= NULL
)

AS

DECLARE 
	@ObjectName			VARCHAR(128)

-- Cleanup
IF OBJECT_ID('tempdb.dbo.#syscacheobjects') IS NOT NULL
   DROP TABLE #syscacheobjects

-- Populate temp table with data from syscacheobjects
SELECT 
	DB_NAME([dbid])								AS [DBName]
	,DB_NAME([dbidexec])						AS [DBNameExecuted]
	,CAST([uid] AS NVARCHAR(128))				AS [SchemaName]
	,CAST([objid] AS NVARCHAR(128))				AS [ObjectName]
	,[objtype]
	,[cacheobjtype]
	,CAST(NULL AS VARCHAR(1))					AS [memory_type]
	,CAST(NULL AS VARCHAR(1))					AS [cache_name]
	,CAST(NULL AS VARCHAR(1))					AS [cache_type]
	,[usecounts]
	,[refcounts]
	,CAST(NULL AS VARCHAR(1))					AS [hits_exec_context]
	,CAST(NULL AS VARCHAR(1))					AS [misses_exec_context]
	,CAST(NULL AS VARCHAR(1))					AS [removed_exec_context]
	,CAST(NULL AS VARCHAR(1))					AS [inuse_execution_contexts]
	,CAST(NULL AS VARCHAR(1))					AS [free_execution_contexts]
	,[sqlbytes]									AS [size_in_bytes]
	,CAST(NULL AS VARCHAR(1))					AS [pages_allocated_count]
	,CAST(NULL AS VARCHAR(1))					AS [users_in_cache_count]
	,CAST(NULL AS VARCHAR(1))					AS [is_dirty]
	,CAST(NULL AS VARCHAR(1))					AS [disk_ios_count]
	,CAST(NULL AS VARCHAR(1))					AS [context_switches_count]
	,CAST(NULL AS VARCHAR(1))					AS [original_cost]
	,CAST(NULL AS VARCHAR(1))					AS [current_cost]
	,CAST(NULL AS VARCHAR(1))					AS [encrypted]
	,[sql]										AS [text]
	,CAST(NULL AS VARCHAR(1))					AS [query_plan]
	,CASE WHEN CAST([setopts] AS INT) & 1 = 1
		THEN 1
		ELSE 0
	END AS 'ANSI_PADDING'
	,CASE WHEN CAST([setopts] AS INT) & 2 = 2
		THEN 1
		ELSE 0
	END AS 'Parallel Plan'
	,CASE WHEN CAST([setopts] AS INT) & 4 = 4
		THEN 1
		ELSE 0
	END AS 'FORCEPLAN'
	,CASE WHEN CAST([setopts] AS INT) & 8 = 8
		THEN 1
		ELSE 0
	END AS 'CONCAT_NULL_YIELDS_NULL'
	,CASE WHEN CAST([setopts] AS INT) & 16 = 16
		THEN 1
		ELSE 0
	END AS 'ANSI_WARNINGS'
	,CASE WHEN CAST([setopts] AS INT) & 32 = 32
		THEN 1
		ELSE 0
	END AS 'ANSI_NULLS'
	,CASE WHEN CAST([setopts] AS INT) & 64 = 64
		THEN 1
		ELSE 0
	END AS 'QUOTED_IDENTIFIER'
	,CASE WHEN CAST([setopts] AS INT) & 128 = 128
		THEN 1
		ELSE 0
	END AS 'ANSI_NULL_DFLT_ON'
	,CASE WHEN CAST([setopts] AS INT) & 256 = 256
		THEN 1
		ELSE 0
	END AS 'ANSI_NULL_DFLT_OFF'
	,CASE WHEN CAST([setopts] AS INT) & 512 = 512
		THEN 1
		ELSE 0
	END AS 'NoBrowseTable'
	,CASE WHEN CAST([setopts] AS INT) & 1024 = 1024
		THEN 1
		ELSE 0
	END AS 'TriggerOneRow'
	,CASE WHEN CAST([setopts] AS INT) & 2048 = 2048
		THEN 1
		ELSE 0
	END AS 'ResyncQuery'
	,CASE WHEN CAST([setopts] AS INT) & 4096 = 4096
		THEN 1
		ELSE 0
	END AS 'ARITH_ABORT'
	,CASE WHEN CAST([setopts] AS INT) & 8192 = 8192
		THEN 1
		ELSE 0
	END AS 'NUMERIC_ROUNDABORT'
	,CASE WHEN CAST([setopts] AS INT) & 16384 = 16384
		THEN 1
		ELSE 0
	END AS 'DATEFIRST'
	,CASE WHEN CAST([setopts] AS INT) & 32768 = 32768
		THEN 1
		ELSE 0
	END AS 'DATEFORMAT'
	,CASE WHEN CAST([setopts] AS INT) & 65536 = 65536
		THEN 1
		ELSE 0
	END AS 'LanguageID'
	,CASE WHEN CAST([setopts] AS INT) & 131072 = 131072
		THEN 1
		ELSE 0
	END AS 'UPON'
INTO #syscacheobjects
FROM [master].[dbo].[syscacheobjects] co
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON DB_NAME(co.[dbid]) = d.[item]
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON DB_NAME(co.[dbid]) = de.[item]
WHERE ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases

-- Retrieve object and schema names
DECLARE #cache CURSOR LOCAL STATIC FOR 
SELECT
	[DBName]
	,[ObjectName]
FROM #syscacheobjects
OPEN #cache
FETCH NEXT FROM #cache INTO @DBName, @ObjectName

WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + ']
		UPDATE #syscacheobjects
		SET [ObjectName] = OBJECT_NAME('+ @ObjectName + ')
		,[SchemaName] = USER_NAME([uid])
		FROM [dbo].[sysobjects]
		WHERE [DBName] = ''' + @DBName + '''
		AND [ObjectName] = ''' + @ObjectName + '''
	')

FETCH NEXT FROM #cache INTO @DBName, @ObjectName
END
CLOSE #cache
DEALLOCATE #cache

-- Return results with parameter based ordering
EXEC
('
	SELECT * FROM #syscacheobjects co
	ORDER BY ' + @OrderBy1 + ', ' + @OrderBy2 + ', ' + @OrderBy3 + '
')
