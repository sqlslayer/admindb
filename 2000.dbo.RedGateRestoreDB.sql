IF OBJECT_ID('[dbo].[RedGateRestoreDB]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[RedGateRestoreDB]
GO

/*******************************************************************************************************
**	Name:			dbo.RedGateRestoreDB 
**	Desc:			Restores the Red Gate SQLBackup files without knowing a huge amount about the system
**	Auth:			Matt Stanford (SQLSlayer.com)		
**  Usage:			EXEC dbo.RedGateRestoreDB '\\sisbss1\SavvisDnlds\DMSLink.sqb', 3, null, null, null, 1
**					EXEC dbo.RedGateRestoreDB '\\sisbss1\SavvisDnlds\DMSLink.sqb', 3, 'DMSLink_New', null, null, 1
**	Notes:			Branched from RedGate90RestoreDB
**	Date:			2007.11.29
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20071203	Matt Stanford	Modified db select query to only return online dbs
**	20071217	Matt Stanford	Just changed some temp-table logic, nothing major
**	20090610	Matt Stanford	Converted to use the standard function/kill procs
**  20090625	Adam Bean		Added @ChangeDBOwner & @NewDBOwner
**	20090625	Matt Stanford	Fixed missing parameter for KillProcess
**	20090629	Matt Stanford	Added @ShowProcesses parameter to KillProcess call
**  20090728	Adam Bean		Fixed for case sensitive collation
**	20090929	Adam Bean		Global header formatting & general clean up
**	20091014	Adam Bean		Resolved issue with changing owner if database was backed up in single user/read only
**	20120201	Adam Bean		Added @DeleteHistory for KillProcesses call
********************************************************************************************************/

CREATE PROCEDURE [dbo].[RedGateRestoreDB]
(
	@filename				VARCHAR(1000)
	,@RestoreFreshnessHours TINYINT			= 0
	,@NewDBName				VARCHAR(512)	= NULL
	,@DestDataPath			VARCHAR(512)	= NULL
	,@DestLogPath			VARCHAR(512)	= NULL
	,@Recover				TINYINT			= 1
	,@Standby				TINYINT			= 0
	,@Password				NVARCHAR(4000)	= NULL
	,@ChangeDBOwner			BIT				= 1
	,@NewDBOwner			VARCHAR(48)		= NULL
	,@DeleteHistory			BIT				= 0
	,@Debug					BIT				= 0
)

AS

SET NOCOUNT ON

--------------------------------------------------------------------------------------------------
---- DEBUGGING GARBAGE
--DECLARE 
--	@DestLogPath				VARCHAR(512)
--	,@DestDataPath				VARCHAR(512)
--	,@filename					VARCHAR(1000)
--	,@RestoreFreshnessHours		TINYINT
--	,@Debug						TINYINT
--	,@NewDBName					VARCHAR(512)
--	,@Recover					TINYINT
--	,@Standby					TINYINT
--	,@Password					NVARCHAR(4000)
--
--set @filename = '\\Oecfp3\sqlbu$\OECNBS\OECNAV1\NAVISION-DAILY\pubs.sqb'
--set @NewDBName = 'pubs_new'
----set @Password = 'kkljlkj'
----set @DestLogPath = 'K:\SQLLogs\Default\'
----set @DestDataPath = 'M:\SQLData\Default\'
--SET @RestoreFreshnessHours = 0
--SET @Recover = 1
--SET @Standby = 0
--set @Debug = 0

---------------------------------------------------------------------------------------------------

DECLARE
	@DBName						VARCHAR(512)
	,@OrigDBName				VARCHAR(512)
	,@SQL						NVARCHAR(4000)
	,@tempSQL					VARCHAR(1000)
	,@killSQL					VARCHAR(1000)
	,@sort						BIGINT
	,@didDelete					TINYINT
	,@didRestore				TINYINT
	,@scriptKickoff				DATETIME
	,@LastRestoreDate			DATETIME
	,@intentionallySkipped		TINYINT
	,@IsRenamingDB				TINYINT
	,@RGExitCode				BIGINT
	,@SQLErrorCode				BIGINT


SET @RestoreFreshnessHours	= isnull(@RestoreFreshnessHours,0)

SET @scriptKickoff			= getdate()
SET @didDelete				= 0
SET @didRestore				= 0
SET @intentionallySkipped	= 0

declare @cmds table (
	sort					BIGINT
	,SQL					VARCHAR(4000)
)

DECLARE @PhysFiles TABLE (
	FileID					INT IDENTITY
	,OrigFileName			VARCHAR	(512)
	,NewFileName			VARCHAR	(512)	
	,LogicalName			VARCHAR	(512)
	,ContainsBUName			TINYINT DEFAULT 0
	,IsLogFile				TINYINT
)

IF OBJECT_ID('tempdb.dbo.#files') IS NOT NULL
	DROP TABLE #files

CREATE TABLE #files (
	LogicalName				NVARCHAR(128)
	,PhysicalName			NVARCHAR(260)
	,Type					CHAR	(1)
	,FileGroupName			NVARCHAR(128)
	,Size					NUMERIC	(20,0)
	,MaxSize				NUMERIC	(20,0)
)

IF OBJECT_ID('tempdb.dbo.#header') IS NOT NULL
	DROP TABLE #header

CREATE TABLE #header
(
	Col1					VARCHAR(4000)
	,FirstHalf				AS REPLACE(REPLACE(LEFT(Col1,21),' ',''),':','')
	,SecondHalf				AS SUBSTRING(Col1,23,4000)
)
	
-- Get the Backup file header info
IF @Password IS NULL
	SET @SQL = N'-SQL "RESTORE SQBHEADERONLY FROM DISK = [' + @filename + '] WITH SINGLERESULTSET" '
ELSE
	SET @SQL = N'-SQL "RESTORE SQBHEADERONLY FROM DISK = [' + @filename + '] WITH SINGLERESULTSET, PASSWORD = [' + @Password + ']" '

insert into #header (Col1)
EXECUTE MASTER.dbo.sqlbackup @SQL, @RGExitCode OUTPUT, @SQLErrorCode OUTPUT

-- Make sure we processed the header correctly
IF (@RGExitCode <> 0)
BEGIN
	IF OBJECT_ID('tempdb.dbo.#files') IS NOT NULL
		DROP TABLE #files
	IF OBJECT_ID('tempdb.dbo.#header') IS NOT NULL
		DROP TABLE #header

	SET @SQL = 'RedGate SQLBackup returned an exit code of ' + CAST(@RGExitCode AS VARCHAR) + ' during the header operation'
	RAISERROR(@SQL,20,1) WITH LOG
END

IF (@SQLErrorCode <> 0)
BEGIN
	DROP TABLE #files
	DROP TABLE #header

	SET @SQL = 'SQL Server returned an error code of ' + CAST(@RGExitCode AS VARCHAR) + ' during the header operation' 
	RAISERROR(@SQL,20,1) WITH LOG
END

SELECT @OrigDBName = SecondHalf FROM #header WHERE FirstHalf = 'Databasename'

-- Restore DB AS name
IF (@NewDBName IS NOT NULL)
BEGIN
	SET @DBName = @NewDBName
	SET @IsRenamingDB = 1
END
ELSE
BEGIN
	SET @DBName = @OrigDBName
	SET @IsRenamingDB = 0
END

-- Get the list of files in the backup
IF @Password IS NULL
	SET @SQL = N'-SQL "RESTORE FILELISTONLY FROM DISK = [' + @filename + ']" '
ELSE
	SET @SQL = N'-SQL "RESTORE FILELISTONLY FROM DISK = [' + @filename + '] WITH PASSWORD = [' + @Password + ']" '

insert into #files
EXECUTE MASTER.dbo.sqlbackup @SQL

-- Rename the files as necessary (if the restore changes names)
INSERT INTO @PhysFiles (OrigFileName,IsLogFile,LogicalName)
SELECT 
	PhysicalName
	,CASE [Type]
		WHEN 'L' THEN 1
		ELSE 0
	END AS IsLogFile
	,LogicalName
FROM #files
	
-- Find where the file names contain the backup name
UPDATE 
	@PhysFiles 
SET	
	ContainsBUName = 1,
	NewFileName = REPLACE(OrigFileName,@OrigDBName,@DBName)
WHERE CHARINDEX(@OrigDBName,OrigFileName) > 0

-- Change the file names of any files that don't have the DB name in them
UPDATE
	@PhysFiles
SET
	ContainsBUName = 0,
	NewFileName = '\' + @OrigDBName + '_' + @NewDBName + '_' + LTRIM(STR(FileID)) + SUBSTRING(OrigFileName,LEN(OrigFileName) - CHARINDEX('.',REVERSE(OrigFileName)) + 1,100)
WHERE CHARINDEX(@OrigDBName,OrigFileName) <= 0

-- Revert the changes if we're not renaming this thing
IF @IsRenamingDB = 0
BEGIN
	UPDATE
		@PhysFiles
	SET 
		NewFileName = OrigFileName
END

INSERT INTO @cmds (sort,SQL)
	VALUES (1,'CREATE TABLE #crap (col1 VARCHAR(1000))')
INSERT INTO @cmds (sort,SQL)
	VALUES (11,'DROP TABLE #crap')

-- Base restore command
INSERT INTO @cmds (sort,SQL) 
	values (2,'INSERT INTO #crap (col1) EXECUTE master..sqlbackup N''-SQL "RESTORE DATABASE [' + @DBName + '] FROM DISK = [' + @filename + ']')

INSERT INTO @cmds (sort,SQL) 
	values (3, 'WITH SINGLERESULTSET')

INSERT INTO @cmds (sort,SQL) 
	values (4, 'REPLACE')

IF @Password IS NOT NULL
	INSERT INTO @cmds (sort,SQL) 
		values (4, 'PASSWORD = [' + @Password + ']')

--- DETERMINE THE DATA/LOG DIRECTORIES
if (@DestLogPath is null) or (@DestDataPath is null)
begin
	
	-- Find existing database, use existing locations
	if exists (select name from master.dbo.sysdatabases where name = @DBName AND (status & 32) <> 32)
	BEGIN

		-- Get the log file directory
		SET @SQL = 'SELECT @in_dir = SUBSTRING(filename,0,LEN(filename)-CHARINDEX(''\'',REVERSE(replace(filename,'' '','''')))+2) FROM [' + @DBName + '].dbo.sysfiles WHERE fileid = 2'
		EXEC sp_executesql @SQL, N'@in_dir VARCHAR(512) OUTPUT', @in_dir = @DestLogPath OUTPUT

		-- Get the data file directory
		SET @SQL = 'SELECT @in_dir = SUBSTRING(filename,0,LEN(filename)-CHARINDEX(''\'',REVERSE(replace(filename,'' '','''')))+2) FROM [' + @DBName + '].dbo.sysfiles WHERE fileid = 1'
		EXEC sp_executesql @SQL, N'@in_dir VARCHAR(512) OUTPUT', @in_dir = @DestDataPath OUTPUT

	END
	ELSE
	BEGIN
		-- Get the Default data/log directories
		SELECT @DestLogPath		= [dbo].[SQLServerLogDir_fn]()
		SELECT @DestDataPath	= [dbo].[SQLServerDataDir_fn]()
	END
end


-- Make sure the Data/Log paths contain a trailing "\"
if right(@DestLogPath,1) <> '\'
	set @DestLogPath = @DestLogPath + '\'

if right(@DestDataPath,1) <> '\'
	set @DestDataPath = @DestDataPath + '\'

-- Generate the MOVE commands
INSERT INTO @cmds (sort,SQL)
SELECT 
	5
	,CASE IsLogFile
		WHEN 1 then 'MOVE [' + LogicalName + '] to [' + 
			@DestLogPath + SUBSTRING(NewFileName,LEN(NewFileName) - CHARINDEX('\',REVERSE(NewFileName)) + 2,100) + ']'
		ELSE 'MOVE [' + LogicalName + '] to [' + 
			@DestDataPath + SUBSTRING(NewFileName,LEN(NewFileName) - CHARINDEX('\',REVERSE(NewFileName)) + 2,100) + ']'
	END as MoveCommand
FROM @PhysFiles

-- Generate the standby command
IF @Standby = 1 and @Recover = 0
BEGIN
	INSERT INTO @cmds (sort,SQL)
	VALUES (10,'STANDBY = [' + @DestLogPath + @DBName + '_STANDBY.stb' + ']"'',@exitcode OUTPUT, @sqlerrorcode OUTPUT')
END
ELSE
BEGIN
	IF @Recover = 0
		INSERT INTO @cmds (sort,SQL) VALUES (10, 'NORECOVERY"'',@exitcode OUTPUT, @sqlerrorcode OUTPUT')
	ELSE
		INSERT INTO @cmds (sort,SQL) VALUES (10, 'RECOVERY"'',@exitcode OUTPUT, @sqlerrorcode OUTPUT')
END

DROP TABLE #files
DROP TABLE #header

PRINT ('/* Successfully read backup information for ' + @DBName + char(10) + '     Kicking off restore logic now')

-- Check to make sure that we didn't just restore this guy
set @LastRestoreDate = isnull((select max(restore_date) from msdb.dbo.restorehistory where destination_database_name = @DBName),dateadd(year,-1,getdate()))

IF
	(dateadd(hour,@RestoreFreshnessHours,@LastRestoreDate) < @scriptKickoff) 
	OR @RestoreFreshnessHours = 0
	OR NOT EXISTS(SELECT * FROM master.dbo.sysdatabases WHERE name = @DBName)
BEGIN
	print('Last restore was more than ' + cast(@RestoreFreshnessHours as varchar(3)) + ' hours ago.  We''re doing a restore */')

	-------------------------------------------------------
	-- OK, the commands have been built, now throw them all together
	-------------------------------------------------------
	declare curs1 cursor for 
		select sort,SQL from @cmds order by sort asc

	open curs1

	fetch next from curs1 into @sort,@tempSQL

	set @SQL = ''
	WHILE @@FETCH_STATUS = 0
	BEGIN

		set @SQL = @SQL + @tempSQL

		if (@sort between 3 and 9)
			set @SQL = @SQL + ',' + char(10)
		else
			set @SQL = @SQL + char(10)

		fetch next from curs1 into @sort,@tempSQL
	END

	close curs1
	deallocate curs1

	SET @killSQL = 'print(''/* Killing the database */'')
EXEC [dbo].[KillProcess] @DBName = ''' + @DBName + ''', @DropDB = 1, @ShowProcesses = 0, @DeleteHistory = ' + CAST(@DeleteHistory AS VARCHAR) + '
print(''/*    Killing done */'')
print(''/* Restoring ' + @DBName + ' */'')' + char(10) + char(10) 

	-- Check to see if the database exists
	if exists(select * from master.dbo.sysdatabases where name = @DBName)
	BEGIN
		if @Debug = 1
			print (@killSQL)
		else
			exec (@killSQL)

		if (select count(*) from master.dbo.sysdatabases where name = @DBName) = 0
			set @didDelete = 1
	END

	if @Debug = 1
		print (@SQL)
	else
		EXEC sp_executesql @SQL, N'@exitcode INT OUTPUT, @sqlerrorcode INT OUTPUT', @exitcode = @RGExitCode OUTPUT, @sqlerrorcode = @SQLErrorCode OUTPUT

	-- Test for success of the restore
	IF (@RGExitCode <> 0)
	BEGIN
		DROP TABLE #files
		DROP TABLE #header

		SET @SQL = 'RedGate SQLBackup returned an exit code of ' + CAST(@RGExitCode AS VARCHAR) + ' during the RESTORE operation'
		RAISERROR(@SQL,20,1) WITH LOG
	END

	IF (@SQLErrorCode <> 0)
	BEGIN
		DROP TABLE #files
		DROP TABLE #header

		SET @SQL = 'SQL Server returned an error code of ' + CAST(@RGExitCode AS VARCHAR) + ' during the RESTORE operation' 
		RAISERROR(@SQL,20,1) WITH LOG
	END

	SET @LastRestoreDate = isnull((select max(restore_date) from msdb.dbo.restorehistory where destination_database_name = @DBName),dateadd(year,-1,getdate()))

	-- Test to see if the restore worked
	if @LastRestoreDate >= @scriptKickoff
	BEGIN
		set @didRestore = 1
	END
END
ELSE
BEGIN
	print('/* Did not restore anything... the last restore was pretty recent */')
	SET @intentionallySkipped = 1
END

IF @ChangeDBOwner = 1
BEGIN
	-- If new owner was specified, use that, if not, determine primary sa account
	IF @NewDBOwner IS NULL
		SET @NewDBOwner = (SELECT dbo.WhatIsGodsName_fn())
		
	-- Make sure the database isn't in single user or read only
	IF (SELECT [user_access_desc] FROM [sysdatabases_vw] WHERE [name] = @DBName) = 'SINGLE_USER'
		EXEC('ALTER DATABASE [' + @DBName + '] SET MULTI_USER')
	IF (SELECT [is_read_only] FROM [sysdatabases_vw] WHERE [name] = @DBName) = 1
		EXEC('ALTER DATABASE [' + @DBName + '] SET READ_WRITE')
			
	-- Change owner
	SET @SQL = 'USE [' + @DBName + '] EXEC [sp_changedbowner] @loginame = ''' + @NewDBOwner + ''''
	IF @Debug = 1
		PRINT (@SQL)
	ELSE
		EXEC (@SQL)
END

select 
	@didDelete as DidDelete, 
	@didRestore as DidRestore, 
	@intentionallySkipped as IntentionallySkipped,
	@LastRestoreDate as LastRestoreDateTime

set nocount off
