IF OBJECT_ID('dbo.ExecutionPlans_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[ExecutionPlans_vw]
GO

/******************************************************************************
**	Name:			[dbo].[ExecutionPlans_vw]
**	Desc:			Retrieve all relevant information surrounding execution plans
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2010.04.18
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20101117	Matt Stanford	Moved all of the logic to ExecutionPlanAttributes_vw.  This is an extension of that.
********************************************************************************************************/

CREATE VIEW [dbo].[ExecutionPlans_vw]

AS

SELECT 
	pa.*
	,qp.[query_plan]				AS [QueryPlan]
FROM [dbo].[ExecutionPlanAttributes_vw] pa
OUTER APPLY sys.dm_exec_query_plan(pa.[PlanHandle]) qp
