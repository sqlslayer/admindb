IF OBJECT_ID('dbo.DeployAlerts','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[DeployAlerts]
GO

/******************************************************************************
* Name
	[dbo].[DeployAlerts]

* Author
	Adam Bean
	
* Date
	2013.10.15
	
* Synopsis
	Procedure to push out alerts
	
* Description
	Alerts will be enabled with email notification for the following alerts:
		- Data, log file full
		- Severity 16 through 25 (severe SQL errors)
		- 823, 824 and 825 (hardware errors)

* Examples
	EXEC [dbo].[DeployAlerts] @Operator = 'SQLDBA', @Recreate = 1

* Dependencie
	If available.  List one per line.

* Parameters
	@Operator			= Operator to recieve alerts
	,@Recreate			= Drop and recreate the alerts
	,@DelayResponse		= Interval (in minutes) in which to wait to re-send the alert
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [dbo].[DeployAlerts]
(
	@Operator			VARCHAR(1024)
	,@Recreate			BIT = 0
	,@DelayResponse		INT = 60
)

AS

SET NOCOUNT ON

DECLARE
	@message_id			SMALLINT
	,@severity			SMALLINT
	,@alert_name		NVARCHAR(128)
	,@alert_name_new	NVARCHAR(128)

-- Setup our staging table 
DECLARE @Alerts TABLE
(
	[message_id]		SMALLINT
	,[severity]			SMALLINT
	,[name]				NVARCHAR(128)
	,[name_new]			NVARCHAR(128)
)

-- Hardware errors
INSERT INTO @Alerts
([message_id], [name_new])
SELECT 823, 'Error Number 823'

-- Data/Log file full
INSERT INTO @Alerts
([message_id], [name_new])
SELECT 1105,'Data file is full'
UNION
SELECT 9002,'Log file is full'

-- Severe errors
INSERT INTO @Alerts
([severity], [name_new])
SELECT 16, 'Severity 016'
UNION 
SELECT 17, 'Severity 017'
UNION
SELECT 18, 'Severity 018'
UNION
SELECT 19, 'Severity 019'
UNION
SELECT 20, 'Severity 020'
UNION
SELECT 21, 'Severity 021'
UNION
SELECT 22, 'Severity 022'
UNION
SELECT 23, 'Severity 023'
UNION
SELECT 24, 'Severity 024'
UNION
SELECT 25, 'Severity 025'

-- Find existing alerts and their names
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 823 AND [database_name] IS NULL) WHERE [message_id] = 823
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 1105 AND [database_name] IS NULL) WHERE [message_id] = 1105
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 9002 AND [database_name] IS NULL) WHERE [message_id] = 9002
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 16) WHERE [severity] = 16
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 17) WHERE [severity] = 17
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 18) WHERE [severity] = 18
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 19) WHERE [severity] = 19
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 20) WHERE [severity] = 20
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 21) WHERE [severity] = 21
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 22) WHERE [severity] = 22
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 23) WHERE [severity] = 23
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 24) WHERE [severity] = 24
UPDATE @Alerts SET [name] = (SELECT [name] FROM [msdb].[dbo].[sysalerts] WHERE [message_id] = 0 AND [severity] = 25) WHERE [severity] = 25

DECLARE #alerts CURSOR LOCAL STATIC FOR
SELECT 
	[message_id]
	,[severity]
	,[name]
	,[name_new]
FROM @Alerts

OPEN #alerts
FETCH NEXT FROM #alerts INTO @message_id, @severity, @alert_name, @alert_name_new
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Drop existing alerts if desired
	IF @Recreate = 1
	BEGIN
		IF @alert_name IS NOT NULL  
			EXEC [msdb].[dbo].[sp_delete_alert] @name = @alert_name
	END
    
	IF @message_id IS NOT NULL 
	BEGIN
		IF @alert_name IS NULL OR @Recreate = 1
		BEGIN
			EXEC [msdb].[dbo].[sp_add_alert]
				@name = @alert_name_new
				,@severity = 0
				,@message_id = @message_id
				,@enabled = 1
				,@delay_between_responses = @DelayResponse
				,@include_event_description_in = 1

			EXEC [msdb].[dbo].[sp_add_notification] 
				@alert_name = @alert_name_new
				,@operator_name = @Operator
				,@notification_method = 1      
		END      
	END
    
	IF @severity IS NOT NULL 
	BEGIN
		IF @alert_name IS NULL OR @Recreate = 1
		BEGIN
  			EXEC [msdb].[dbo].[sp_add_alert]
				@name = @alert_name_new
				,@severity = @severity
				,@enabled = 1
				,@delay_between_responses = @DelayResponse
				,@include_event_description_in = 1

			EXEC [msdb].[dbo].[sp_add_notification] 
				@alert_name = @alert_name_new
				,@operator_name = @Operator
				,@notification_method = 1
		END
	END  

FETCH NEXT FROM #alerts INTO @message_id, @severity, @alert_name, @alert_name_new
END
CLOSE #alerts
DEALLOCATE #alerts

SET NOCOUNT OFF