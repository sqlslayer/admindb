IF OBJECT_ID('[dbo].[KillProcess]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[KillProcess]
GO

/*******************************************************************************************************
**	Name:			dbo.KillProcess
**	Desc:			Kills a connection based on login (with exclusions), database name, or both. (SQL 2000)
					Optionally puts database into single user, offline, drops database, logs locally or sends email
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	Split_fn - To pass in multiple database names and/or login exclusions
**  Parameters:		@Login = Login Name to kill, CSV supported
					@DBName = Database Name to kill connections against, CSV supported
					@HostName = Host Name to kill connections against, CSV supported
					@LoginExclude = Login(s) to exclude from the kill list, CSV supported
					@DBNameExclude = Login(s) to exclude from the kill list, CSV supported
					@DBNameExclude = Host Name(s) to exclude from the kill list, CSV supported
					@SetSingleUser = Put database into single user mode
					@SetOffline = Put database into offline mode
					@DropDB = Drop database 
					@TableLogging = If you want to log the results of the kill locally to this server
					@SendEmail = Send an email with the results of what was killed
					@FromEmail = Email sender if using xp_smtp_sendmail
					@EmailServer = Email server if using xp_smtp_sendmail
					@Recipients = Email recipients if @SendEmail is enabled
					@Debug = Shows but does not kill connections
**  Usage:			EXEC killprocess @dbname='admin',@SetSingleUser=0,@sendemail=1,@Recipients='you@me.com', @debug=1
					EXEC killprocess @dbname='admin',@LoginExclude='SQLAccount,domain\ServiceAccount',@debug=1
					EXEC killprocess @Login='user',@DropDB=1,@dbname='admin',@sendemail=1,@Recipients='you@me.com', @debug=1
**  Notes:			- You can use any combination of the 4 main parameters ... if left null, all will be populated
					- No connections will be killed to system databases
					- If you're sending email, make sure you have sqlmail or smtp_sentmail setup
					- If xp_smtp_sendmail (http://sqldev.net/xp/xpsmtp.htm) is not present, than the default xp_sendmail will be used. 
					-- This results in poor formatting on the email.
**	Date:			2008.02.14
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20080215	Adam Bean		Fixed email debug, Added nolocks, Omitted system databases, 
								Added ability to drop database, Test for work to do,
								Verify database and login
**  20080327	Adam Bean		Added ability to pass in exclusions for logins
								Added functionality for xp_smtp_sendmail
								Added table logging
**	20080824	Adam Bean		Added offline
**  20090610	Adam Bean		Removed hardcoded FromEmail & EmailServer
**	20090629	Matt Stanford	Added @ShowProcesses parameter
**  20090630	Adam Bean		Added @DBNameExclude, @KillEverything, Removed verification checks, added enhanced CSV support
**	20090706	Matt Stanford	Made the table creation dynamic SQL, cleaned up a bit and changed the KillProcesses table to
								not be dropped and recreated each time
**	20090702	Adam Bean		Removed requirement of physical table to use xp_sendmail by using a global temp table
								Added support to set single/offline/drop for multiple databases (excluding system)
								Added support to kill processes in system databases (spid > 50)
								Moved KillProcessesLog to external script								
								Changed @ConnectionsKilled from TINYINT to INT
**  20090708	Adam Bean		Changed @@SERVERNAME to SERVERPROPERTY('ServerName')
**  20090709	Adam Bean		Switched from temp table insert into to temp variable to support empty table if no connections
								Populate staging table with values from @DBName if no active connections
								Trimmed down the query column for xp_sendmail as it only supports 8,000 bytes
								Added USE [master] when setting offline
**  20090716	Adam Bean		Added @HostName and @HostNameExclude
**  20090728	Adam Bean		Fixed for case sensitive collation
**  20090922	Adam Bean		Added @DeleteHistory to remove backup history if the database is dropped
**	20090929	Adam Bean		Global header formatting & general clean up
**	20091025	Matt Stanford	Changed the @SQL variable to be NVARCHAR(MAX), was VARCHAR(128)
**	20100601	Adam Bean		Resolved collation issues
********************************************************************************************************/

CREATE PROCEDURE [dbo].[KillProcess]
(
	@Login				VARCHAR(512)	= NULL
	,@DBName			VARCHAR(512)	= NULL
	,@HostName			VARCHAR(512)	= NULL
	,@LoginExclude		VARCHAR(512)	= NULL
	,@DBNameExclude		VARCHAR(512)	= NULL
	,@HostNameExclude	VARCHAR(512)	= NULL
	,@KillEverything	BIT				= 0
	,@SetSingleUser		BIT				= 0
	,@SetOffline		BIT				= 0
	,@DropDB			BIT				= 0
	,@DeleteHistory		BIT				= 1
	,@TableLogging		BIT				= 0
	,@SendEmail			BIT				= 0	
	,@FromEmail			VARCHAR(32)		= NULL -- Only set these if you're using xp_smtp_sendmail
	,@EmailServer		VARCHAR(64)		= NULL -- Only set these if you're using xp_smtp_sendmail
	,@Recipients		VARCHAR(1024)	= NULL
	,@ShowProcesses		BIT				= 1
	,@Debug				BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE
	@Body				VARCHAR(4000)
	,@EmailHeader		VARCHAR(2048)
	,@EmailFooter		VARCHAR(2048)
	,@EmailBody			VARCHAR(2048)
	,@Subject			VARCHAR(256)
	,@spid				INT
	,@SQLID				INT
	,@SQL				NVARCHAR(4000)
	,@Count				INT
	,@Query				VARCHAR(64)
	,@ConnectionsKilled	INT
	,@ServerName		VARCHAR(64)

SET NOCOUNT ON

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'

SET @ServerName = (SELECT CAST(SERVERPROPERTY('ServerName') AS VARCHAR(64)))

-- Sanity check ... If all 4 main parameters are NULL, everything minus system db spids will be killed 
-- More than likely that's not what was intended, but just in case, the user has to pass in @KillEverything = 1
IF 
	(
		@Login IS NULL 
		AND @DBName IS NULL 
		AND @LoginExclude IS NULL 
		AND @DBNameExclude IS NULL
		AND @Debug = 0
		AND @KillEverything = 0
	)
BEGIN
	PRINT 'Pass in at least one value for any of the main parameters: @Login, @DBName, @LoginExclude, @DBNameExclude'
	PRINT 'Use @Debug = 1 to view connections'
	PRINT 'If you REALLY want to kill all non system processes, than rerun with @KillEverything = 1'
	RETURN
END

-- Setup staging table
DECLARE @KillProcesses TABLE
(
	[Action]		VARCHAR(32)
	,[spid]			INT
	,[DBName]		VARCHAR(128)
	,[Login]		VARCHAR(128)
	,[HostName]		VARCHAR(128)
	,[ProgramName]	VARCHAR(128)
	,[command]		VARCHAR(32)
	,[Status]		VARCHAR(32)
	,[LoginTime]	DATETIME
	,[LastBatch]	DATETIME
	,[Query]		VARCHAR(7000) 
)

-- Populate staging table
INSERT INTO @KillProcesses
SELECT DISTINCT
	'Connection To Be Killed ->'	AS [Action]
	,s.[spid]						AS [spid]
	,DB_NAME(s.[dbid])				AS [DBName]
	,s.[loginame]					AS [Login]
	,s.[hostname]					AS [HostName]
	,s.[program_name]				AS [ProgramName]
	,s.[cmd]						AS [command]
	,s.[status]						AS [Status]
	,s.[login_time]					AS [LoginTime]
	,s.[last_batch]					AS [LastBatch]
	,CONVERT(VARCHAR(4000),NULL)	AS [Query]
FROM [master].[dbo].[sysprocesses] s WITH (NOLOCK)
LEFT JOIN [master].[dbo].[syslockinfo] sl WITH (NOLOCK)
  	ON s.[dbid] = sl.[rsc_dbid]
	AND sl.[req_spid] = s.[spid]
LEFT JOIN [dbo].[Split_fn](@Login,',') l
	ON s.[loginame] = l.[item]
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON DB_NAME(s.[dbid]) = d.[item]
LEFT JOIN [dbo].[Split_fn](@HostName,',') h
	ON s.[hostname] = h.[item]
LEFT JOIN [dbo].[Split_fn](@LoginExclude,',') le
	ON s.[loginame] = le.[item]
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON DB_NAME(s.[dbid]) = de.[item]
LEFT JOIN [dbo].[Split_fn](@HostNameExclude,',') he
	ON s.[hostname] = he.[item]
WHERE s.[spid] > 50 -- Exclude system processes
AND s.[spid] != @@spid -- Exclude your process
AND ((l.[item] IS NOT NULL AND @Login IS NOT NULL) OR @Login IS NULL) -- Specified logins, or all
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND ((h.[item] IS NOT NULL AND @HostName IS NOT NULL) OR @HostName IS NULL) -- Specified hosts, or all
AND le.[item] IS NULL -- All but excluded logins
AND de.[item] IS NULL -- All but excluded databases
AND he.[item] IS NULL -- All but excluded hosts
ORDER BY DB_NAME(s.[dbid]), s.[loginame], s.[hostname], s.[status]

-- Setup temp table to hold spid info
CREATE TABLE #QueryCmd
(
	[SQLID]			INT IDENTITY
	,[spid]			INT
	,[EventType]	VARCHAR(128)
	,[Parameters]	INT
	,[command]		VARCHAR(4000)
)

-- Get query command per spid
DECLARE #spids CURSOR LOCAL STATIC FOR
SELECT [spid] FROM @KillProcesses 

OPEN #spids
FETCH NEXT FROM #spids INTO @spid
WHILE @@FETCH_STATUS = 0
BEGIN
	
	INSERT INTO #QueryCmd 
	([EventType], [Parameters], [command])
	EXEC('DBCC INPUTBUFFER(' + @spid + ') WITH NO_INFOMSGS')

	SELECT @SQLID = MAX(SQLID) FROM #QueryCmd
	UPDATE #QueryCmd 
	SET [spid] = @spid 
	WHERE [SQLID] = @SQLID 	

FETCH NEXT FROM #spids INTO @spid
END
CLOSE #spids
DEALLOCATE #spids

-- Update main table with new spid info
UPDATE a
	SET a.[Query] = q.[command]
FROM @KillProcesses a
JOIN #QueryCmd q
	ON a.[spid] = q.[spid]

-- Get a total of the connections to be killed
SELECT @ConnectionsKilled = COUNT(*) FROM @KillProcesses

-- If no active connections, populate staging table with database names (required for other operations in procedure)
IF @ConnectionsKilled = 0
BEGIN
	INSERT INTO @KillProcesses
	([DBName])
	SELECT [item] AS [DBName] FROM [dbo].[Split_fn](@DBName,',')
END

-- Log connections to be killed
IF @Debug = 0 AND @TableLogging = 1
BEGIN
	-- Log the data
	INSERT INTO [dbo].[KillProcessesLog]
	([spid],[DBName],[Login],[HostName],[ProgramName],[command],[Status],[LoginTime],[LastBatch],[Query])
	SELECT 
		[spid]
		,[DBName]
		,[Login]
		,[HostName]
		,[ProgramName]
		,[command]
		,[Status]
		,[LoginTime]
		,[LastBatch]
		,[Query] 
	FROM @KillProcesses
END
ELSE IF @Debug = 1 AND @TableLogging = 1
	PRINT 'Debug enabled, nothing will be logged'

-- Kill the connection(s) 
IF @ConnectionsKilled > 0
BEGIN
	DECLARE #killspids CURSOR FOR
	SELECT [spid] FROM @KillProcesses

	OPEN #killspids
	FETCH NEXT FROM #killspids INTO @spid
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'KILL ' + CAST(@spid AS VARCHAR(5)) + ''
		IF @Debug = 0
		BEGIN
			EXEC(@SQL)
			PRINT 'KILLED spid ' + CAST(@spid AS VARCHAR(5)) + ''
			UPDATE @KillProcesses
			SET [Action] = 'Killed spid ->'
		END	
		ELSE
		BEGIN	
			PRINT (@SQL)
		END

	FETCH NEXT FROM #killspids INTO @spid
	END
	CLOSE #killspids
	DEALLOCATE #killspids
END

-- Return results after kill
IF @ShowProcesses = 1
BEGIN
	IF @ConnectionsKilled > 0
		SELECT * FROM @KillProcesses
	ELSE
		SELECT 'No active connections to kill' AS [Action]
END

-- Set database to single user 
IF @SetSingleUser = 1
BEGIN
	DECLARE #SetSingleUser CURSOR FOR
	SELECT DISTINCT [DBName] FROM @KillProcesses
	WHERE DB_ID([DBName]) > 4 
	AND [DBName] != 'admin'
	
	OPEN #SetSingleUser
	FETCH NEXT FROM #SetSingleUser INTO @DBName
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		SET @SQL = 'ALTER DATABASE [' + @DBName + '] SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
		IF @Debug = 0
		BEGIN
			EXEC(@SQL)
			PRINT @DBName + ' has been put into single user mode.'
			PRINT 'To put ' + @DBName + ' back into multi user mode, issue: ALTER DATABASE [' + @DBName + '] SET MULTI_USER WITH NO_WAIT'
		END
		ELSE
			PRINT(@SQL)
	
	FETCH NEXT FROM #SetSingleUser INTO @DBName
	END
	CLOSE #SetSingleUser
	DEALLOCATE #SetSingleUser
END

-- Set database to offline
IF @SetOffline = 1
BEGIN
	DECLARE #SetOffline CURSOR FOR
	SELECT DISTINCT [DBName] FROM @KillProcesses
	WHERE DB_ID([DBName]) > 4 
	AND [DBName] != 'admin'
	
	OPEN #SetOffline
	FETCH NEXT FROM #SetOffline INTO @DBName
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		SET @SQL = 'USE [master] ALTER DATABASE [' + @DBName + '] SET OFFLINE WITH ROLLBACK IMMEDIATE'
		IF @Debug = 0
		BEGIN
			EXEC(@SQL)
			PRINT @DBName + ' has been put into offline mode.'
			PRINT 'To put ' + @DBName + ' back into multi user mode, issue: USE [master] ALTER DATABASE [' + @DBName + '] SET ONLINE WITH NO_WAIT'
		END
		ELSE
			PRINT(@SQL)
		
	FETCH NEXT FROM #SetOffline INTO @DBName
	END
	CLOSE #SetOffline
	DEALLOCATE #SetOffline
END

-- Drop database 
IF @DropDB = 1
BEGIN
	DECLARE #DropDB CURSOR FOR
	SELECT DISTINCT [DBName] FROM @KillProcesses
	WHERE DB_ID([DBName]) > 4 
	AND [DBName] != 'admin'
	
	OPEN #DropDB
	FETCH NEXT FROM #DropDB INTO @DBName
	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF @DeleteHistory = 1
			SET @SQL = 'EXEC [msdb].[dbo].[sp_delete_database_backuphistory] @db_nm = ''' + @DBName + '''' + CHAR(10)
					
		SET @SQL = @SQL + 'DROP DATABASE [' + @DBName + ']'
		
		IF @Debug = 0
		BEGIN
			EXEC(@SQL)
			PRINT @DBName + ' has been dropped.'
		END
		ELSE
			PRINT(@SQL)
		
	FETCH NEXT FROM #DropDB INTO @DBName
	END
	CLOSE #DropDB
	DEALLOCATE #DropDB
END

-- Build email
IF @SendEmail = 1
BEGIN
	IF @Debug = 0
	BEGIN
		IF @Recipients IS NULL
		BEGIN
			RAISERROR ('@Recipients must be passed in.',16,1) WITH NOWAIT
			RETURN
		END
		ELSE
		BEGIN
			-- Only send email if connections were killed
			IF @ConnectionsKilled > 0 
			BEGIN
				-- Setup table to build body
				DECLARE @BodyContents TABLE 
				(
					[html]			VARCHAR(4000)
				)

				-- Setup table to build email
				DECLARE @Email TABLE 
				(
					[ID]			INT IDENTITY
					,[html]			VARCHAR(4000)
				)
				
				-- Set subject
				SET @Subject = 'Active connections terminated on ' + @ServerName  + ''

				-- Build the email header
				SELECT @EmailHeader = 
				'
				<HTML>
				<DIV ALIGN="CENTER">
				<TABLE BORDER="5" CELLSPACING="5" CELLPADDING="5" ALIGN="CENTER">
				<CAPTION>
					<H2>Terminated connections on ' + @ServerName  + '</H2>
					<EM>Message sent from ' + @ServerName  + ' @ ' + CAST(GETDATE() AS VARCHAR(24)) + '.</EM>
				</CAPTION>
				<TR>
					<TD COLSPAN="9" ALIGN="CENTER">Total connections killed: ' + CAST(@ConnectionsKilled AS VARCHAR(8)) + '
					</TD>
				</TR>
				<TR BGCOLOR="#C0C0C0">
					<TH>Database</TH>
					<TH>Login</TH>
					<TH>HostName</TH>
					<TH>ProgramName</TH>
					<TH>Query</TH>
					<TH>command</TH>
					<TH>Status</TH>
					<TH>LoginTime</TH>
					<TH>LastBatch</TH>
				</TR>
				'

				-- Build the email footer
				SELECT @EmailFooter =
				'
				</TABLE>
				</DIV>
				</HTML>
				'

				-- Build the email body
				INSERT INTO @BodyContents
				SELECT '
						<TR>
							<TD>' + [DBName] + '</TD>
							<TD>' + [Login] + '</TD>
							<TD>' + [HostName] + '</TD>
							<TD>' + [ProgramName] + '</TD>
							<TD>' + LEFT([Query],200) + '</TD>
							<TD>' + [command] + '</TD>
							<TD>' + [Status] + '</TD>
							<TD>' + CONVERT(VARCHAR(24), [LoginTime], 100)+ '</TD>
							<TD>' + CONVERT(VARCHAR(24), [LastBatch], 100) + '</TD>
						</TR>' AS [html]
				FROM @KillProcesses

				-- Build the email
				INSERT INTO @Email
				SELECT @EmailHeader
				UNION ALL
				SELECT * FROM @BodyContents
				UNION ALL
				SELECT @EmailFooter

				-- Put it all together
				SET @Count = 1
				SET @Body = ''
				WHILE @Count <= (SELECT COUNT(*) FROM @Email)
				BEGIN
					SET @Body = @Body + (SELECT [html] FROM @Email WHERE [ID] = @Count)
					SET @Count = @Count + 1
				END

				-- Determine which mail format to use
				IF EXISTS (SELECT * FROM [master].[dbo].[sysobjects] WHERE [name] = 'xp_smtp_sendmail' AND [type] = 'X')
				BEGIN
				-- Send mail using xp_smtp_sendmail
					EXEC [master].[dbo].[xp_smtp_sendmail]
						@From       = @FromEmail
						,@To        = @Recipients
						,@Subject   = @Subject
						,@Message   = @Body
						,@Type      = 'text/html'
						,@Server	= @EmailServer
				END
				ELSE
				BEGIN
				-- Send mail using xp_sendmail
				SELECT [spid], [DBName], [Login], [HostName], [ProgramName], [command], [Status], [LoginTime], [LastBatch], [Query] 
				INTO ##KillProcesses-- Only way to have xp_sendmail query a table, can't use temp table/variable
				FROM @KillProcesses
					EXEC [master].[dbo].[xp_sendmail]
						@Recipients	= @Recipients
						,@Subject	= @Subject
						,@Query		= 'SELECT [spid], [DBName], [Login], [HostName], [ProgramName], [command], [Status], [LoginTime], [LastBatch], SUBSTRING([Query],1,128) FROM ##KillProcesses'
				END
			END
		END
	END
	ELSE
		PRINT 'Debug enabled, no email will be sent'
END

-- Cleanup
IF OBJECT_ID('tempdb.dbo.##KillProcesses') IS NOT NULL
	DROP TABLE ##KillProcesses
	
SET NOCOUNT OFF

-- Return status
IF @@ERROR <> 0
	RETURN 2
ELSE
	RETURN 0