IF OBJECT_ID('dbo.DeploySQLServerStartupInstructions','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[DeploySQLServerStartupInstructions]
GO

/******************************************************************************
* Name
	[dbo].[DeploySQLServerStartupInstructions]

* Author
	Adam Bean
	
* Date
	2011.04.25
	
* Synopsis
	Email notification based on database extended properties for post SQL restarts.
	
* Description
	This procedure is a wrapper procedure to SQLServerStartupNotification to deploy the object in the master
	database and mark it as a startup procedure, with the parameter of @Recipients.
	
	The startup procedure will scan all databases extended properties.
	If the extended property "PostSQLRestart" is found the value of that property will be emailed to @Recipients.

* Examples
	EXEC [dbo].[DeploySQLServerStartupInstructions] @Recipients = 'dba@company.com'

* Dependencies
	Database mail configured

* Parameters
	@Recipients		- Email recipients (comma or semi-colon seperated - ie. dba@company.com;joe@company.com)

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [dbo].[DeploySQLServerStartupInstructions]
(
	@Recipients		VARCHAR(1024)
	,@Debug			BIT	= 0
)

AS 

SET NOCOUNT ON

DECLARE 
	@SQL			VARCHAR(MAX)

-- Replace commas with semi-colons to work properly for database mail
SET @Recipients = REPLACE(@Recipients, ',', ';')

-- Deploy SQLServerStartupInstructions to master database and mark as system started procedure
SET @SQL = 
('
USE [master]
IF OBJECT_ID(''[dbo].[SQLServerStartupInstructions]'',''P'') IS NOT NULL
	DROP PROCEDURE [dbo].[SQLServerStartupInstructions]
	
EXEC
(''

/******************************************************************************
* Name
	[dbo].[SQLServerStartupInstructions]

* Author
	Adam Bean (SQLSlayer.com)
	
* Date
	2011.04.25
	
* Synopsis
	Email notification based on database extended properties for post SQL restarts
	
* Description
	This procedure is created from a wrapper procedure DeploySQLServerStartupInstructions

* Examples
	Should only be called as a system startup procedure which the deployment procedure will handle

* Dependencies
	Database mail configured

* Parameters
	@Recipients		- Is deployed as part of the deployment wrapper procedure (DeploySQLServerStartupInstructions)

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [dbo].[SQLServerStartupInstructions]

AS 

SET NOCOUNT ON

DECLARE 
	@ServerName		VARCHAR(64)
	,@DBName		VARCHAR(128)
	,@Recipients	VARCHAR(1024)
	,@EmailSubject	VARCHAR(64)
	,@EmailHeader	VARCHAR(2048)
	,@EmailBody		VARCHAR(MAX)
	,@EmailCaption	VARCHAR(128)
	,@EmailFooter	VARCHAR(2048)
	,@Count			INT

SET @Recipients		= ''''' + @Recipients + '''''
SET @ServerName		= (SELECT CAST(SERVERPROPERTY(''''ServerName'''') AS VARCHAR(64)))
SET @EmailSubject	= ''''Post restart SQL Server instructions for '''' + @ServerName + ''''''''
SET @EmailCaption	= ''''Based on database extended properties, these are the Post SQL restart instructions for this environment.''''

-- Setup table to build body
DECLARE @EmailBodyContents TABLE 
(
	[html]			VARCHAR(MAX)
	
)

-- Setup table to build email
DECLARE @Email TABLE 
(
	[ID]			INT IDENTITY
	,[html]			VARCHAR(MAX)
)

-- Create temp table to store extended properties
IF OBJECT_ID(''''tempdb.dbo.#DBExtProps'''') IS NOT NULL
	DROP TABLE #DBExtProps
CREATE TABLE #DBExtProps
(
	[DBName]			NVARCHAR(128)	
	,[name]				NVARCHAR(128)	NULL
	,[value]			SQL_VARIANT		NULL
)
CREATE INDEX IX__DBExtProps__DBName ON #DBExtProps
(
	[DBName]
)

-- Find extended properties
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [sys].[databases]
WHERE [state_desc] = ''''ONLINE''''
AND [source_database_id] IS NULL
ORDER BY 1

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

INSERT INTO #DBExtProps
EXEC
(
	''''
	SELECT 
		'''''''''''' + @DBName + ''''''''''''
		,[name]
		,[value]
	FROM ['''' + @DBName + ''''].[sys].[fn_listextendedproperty](DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
	WHERE [name] = ''''''''PostSQLRestart''''''''
	''''
)
	
FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs

-- Build the email header
SELECT @EmailHeader = 
''''
<HTML>
<DIV ALIGN="CENTER">
<TABLE BORDER="5" CELLSPACING="5" CELLPADDING="5" ALIGN="CENTER">
<CAPTION>
	<H2>'''' + @EmailCaption + ''''</H2>
</CAPTION>
<TR BGCOLOR="#C0C0C0">
	<TH>DBName</TH>
	<TH>Instructions</TH>
</TR>
''''

-- Build the email footer
SELECT @EmailFooter =
''''
</TABLE>
</DIV>
</HTML>
''''

-- Build the email body
INSERT INTO @EmailBodyContents
SELECT ''''
		<TR>
			<TD>'''' + [DBName] + ''''</TD>
			<TD>'''' + CAST([Value] AS VARCHAR(1024)) + ''''</TD>
		</TR>'''' AS [html]
FROM #DBExtProps

-- Build the email
INSERT INTO @Email
SELECT @EmailHeader
UNION ALL
SELECT * FROM @EmailBodyContents
UNION ALL
SELECT @EmailFooter

-- Put it all together
SET @Count = 1
SET @EmailBody = ''''''''
WHILE @Count <= (SELECT COUNT(*) FROM @Email)
BEGIN
	SET @EmailBody = @EmailBody + (SELECT [html] FROM @Email WHERE [ID] = @Count)
	SET @Count = @Count + 1
END

-- Only send email if there is data
IF @Count > 3
BEGIN
	-- Send mail
	EXEC [msdb].[dbo].[sp_send_dbmail]
		@Recipients		= @Recipients
		,@Subject		= @EmailSubject
		,@Body			= @EmailBody
		,@Body_format	= ''''HTML''''
END
'')
')
 
IF @Debug = 1
BEGIN
	PRINT '/*** DEBUG ENABLED ****/'
	PRINT (@SQL)
END
ELSE
BEGIN
	EXEC(@SQL)
	-- Set object as a startup
	EXEC [master].[sys].[sp_procoption] 'dbo.SQLServerStartupInstructions', 'startup', 'true' 
	-- Query to ensure it's a startup object
	DECLARE @ObjectExists TABLE
	(
		[ObjectName]	sysname
	)
	
	INSERT INTO @ObjectExists
	EXEC
	('
		USE [master]
		SELECT 
			[name]
		FROM [sys].[objects]
		WHERE OBJECTPROPERTY(OBJECT_ID, ''ExecIsStartup'') = 1
	')
	
	IF EXISTS (SELECT [ObjectName] FROM @ObjectExists WHERE [ObjectName] = 'SQLServerStartupInstructions')
	BEGIN
		PRINT 'Successfully deployed dbo.SQLServerStartupInstructions to master database with ''' + @Recipients + ''' as the recipient(s).'
		PRINT 'Successfully marked dbo.SQLServerStartupInstructions as a system startup procedure'
	END
	ELSE
	BEGIN
		PRINT 'Failed to deploy dbo.SQLServerStartupInstructions to master database'
		PRINT 'Failed to mark dbo.SQLServerStartupInstructions as a system startup procedure'
	END
END
	
SET NOCOUNT OFF