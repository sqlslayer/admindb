IF OBJECT_ID('dbo.BackupHistory_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[BackupHistory_vw]
GO

/******************************************************************************
* Name
	[dbo].[BackupHistory_vw]

* Author
	Adam Bean
	
* Date
	2012.06.25
	
* Synopsis
	Overview of backups
	
* Description
	Overview of backups

* Examples
	SELECT * FROM [dbo].[BackupHistory_vw]

* Dependencies
	n/a

* Parameters
	n/a
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20161609	Jeff Gogel		Added backup set id	
******************************************************************************/

CREATE VIEW [dbo].[BackupHistory_vw]

AS

SELECT 
	bs.[server_name]															AS [ServerName]
	,bs.[machine_name]															AS [MachineName]
	,bs.[database_name]															AS [DBName]
	,bs.[backup_start_date]														AS [StartDate]
	,bs.[backup_finish_date]													AS [EndDate]
	,DATEDIFF(SS,bs.[backup_start_date],bs.[backup_finish_date])				AS [BUTimeSeconds]
	,CAST(bs.[backup_size]/1024/1024 AS INT)									AS [SizeMB]
	,CAST(bs.[compressed_backup_size]/1024/1024 AS INT)							AS [SizeMBCompressed]
	,CAST((bs.[backup_size] - bs.[compressed_backup_size])/1024/1024 AS INT)	AS [CompressedSavingsMB]
	,CASE
		WHEN CAST((bs.[backup_size] / bs.[compressed_backup_size]) AS DECIMAL(10,2)) = 1 THEN 0
		ELSE CAST((bs.[backup_size] / bs.[compressed_backup_size]) AS DECIMAL(10,2))
	END AS [CompressedSavingsPercent]
	,bs.[type]																	AS [BUType]
	,bs.[is_copy_only]															AS [IsCopyOnly]
	,bs.[user_name]																AS [UserName]
	,bmf.[logical_device_name]													AS [LogicalDevice]
	,bmf.[physical_device_name]													AS [PhysicalDevice]
	,bmf.[device_type]															AS [DeviceType]
	,bs.[backup_set_id]															AS [BackupSetID]
FROM [msdb].[dbo].[backupset] bs
JOIN [msdb].[dbo].[backupmediafamily] bmf
	ON bs.[media_set_id] = bmf.[media_set_id]
WHERE bmf.[family_sequence_number] = 1
AND bs.[server_name] = SERVERPROPERTY('SERVERNAME')