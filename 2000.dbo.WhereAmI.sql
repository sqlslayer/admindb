IF OBJECT_ID('[dbo].[WhereAmI]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[WhereAmI]
GO
/******************************************************************************
* Name
	dbo.WhereAmI 

* Author
	Adam Bean (SQLSlayer.com)
	
* Date
	2008.06.05
	
* Synopsis
	Procedure to crawl specified database(s) and search objects for passed in string, or data types
	
* Description
	None yet

* Examples
	EXEC dbo.WhereAmI @SearchString = 'error', @DBName ='admin,master'
	EXEC dbo.WhereAmI @DataType = 'varchar'
	EXEC dbo.WhereAmI @DateSearch = 1, @FindMax = 1

* Dependencies
	dbo.sysobjecttype_fn
	dbo.split_fn
	dbo.sysdatabases_vw
	dbo.RemoveComments_fn

* Parameters
	@SearchString		- String to search for
	@DBName				- Database to be queried, CSV supported, NULL for all
	@DBNameExclude		- Databases to be excluded (useful when wanting all but one or two databases)
	@TableName			- Tables to be queried (usually useful when using @DateSearch), CSV supported, NULL for all
	@TableNameExclude	- Tables to be excluded (usually useful when using @DateSearch and when wanting all but one or two tables)
	@ObjectType			- Search for a specific object type
	@ObjectTypeExclude	- Object types to be excluded (useful when wanting all but one or two data types)
	@DataType			- Search for a specific data type
	@DataTypeExclude	- Data types to be excluded (useful when wanting all but one or two data types)
	@DateSearch			- Search for date columns, a helper version of @DataType so user doesn't have to specify all datetime types
	@IncludeSystem		- 1 to include any Microsoft system objects
	@FindMax			- Find max value of the retrieved columns	
	@IgnoreComments		- 1 to remove comments from the search
	@SearchJobs			- 1 to search job definitions
	
* Notes
	@DataType and @DataTypeExclude values are representative of those in systypes (xusertype column)
	@ObjectType and @ObjectTypeExclude values are representative of those in sysobjects (type column)
	When running with any of the aggregates turned on, do not expect quick results on large databases, especially if you don't specify data types
	In @FindAvg, INT is casted to BIGINT because if the SUM() over the column is bigger than than max INT (2147483647)
		then AVG() will throw "Arithmetic overflow error converting expression to data type int." So it is casted to BIGINT
		regardless as it is faster to do this than to check the SUM().


*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20090611	Adam Bean		Removed case statement for object type in favor of dbo.sysobjecttype_fn
								Removed most dynamic SQL to better match 2005 version
								General code cleanup
	20090612	Adam Bean		Removed substring on definition search as results were being limited
								Added objectid
								Added CSV support for @DBName as well as ability to leave null for all DB's
	20090617	Adam Bean		Added support for all object types
	20090618	Adam Bean		Added ability to exclude system objects 
	20090803	Adam Bean		Added all new changes from 2005 version
	20090929	Adam Bean		Global header formatting & general clean up
	20101129	Matt Stanford	Changed to new header and added @IgnoreComments
	20110330	Matt Stanford	Added @SearchJobs
******************************************************************************/
CREATE PROCEDURE [dbo].[WhereAmI]
(
	@SearchString		VARCHAR(8000)	= NULL	-- String to search for
	,@DBName			NVARCHAR(512)	= NULL	-- Database names (CSV supported), NULL for all
	,@DBNameExclude		NVARCHAR(512)	= NULL	-- Database names to exclude (CSV supported)
	,@TableName			NVARCHAR(512)	= NULL	-- Table name, one or many, NULL for all
	,@TableNameExclude	NVARCHAR(512)	= NULL	-- Table names to exclude (CSV supported)
	,@ObjectType		NVARCHAR(512)	= NULL	-- Search for specific object types (CSV supported), NULL for all
	,@ObjectTypeExclude NVARCHAR(512)	= NULL	-- Object types to exclude
	,@DataType			NVARCHAR(512)	= NULL	-- Search for specific data types (CSV supported), NULL for all
	,@DataTypeExclude	NVARCHAR(512)	= NULL	-- Data types to exclude
	,@IncludeSystem		BIT				= 0		-- Include or exclude system objects
	,@DateSearch		BIT				= 0		-- 1 to auto populate @DataType with datetime data types
	,@FindMax			BIT				= 0		-- Show max value of column, 0 no, 1 yes
	,@FindMin			BIT				= 0		-- Show min value of column, 0 no, 1 yes
	,@FindAvg			BIT				= 0		-- Show avg value of column, 0 no, 1 yes
	,@IgnoreComments	BIT				= 0		-- Ignore searching through comments.  1 yes, 0 no.
	,@SearchJobs		BIT				= 1		-- Search job definitions
)

AS

SET NOCOUNT ON

DECLARE 
	@SchemaName			NVARCHAR(128)
	,@ObjectName		NVARCHAR(128)
	,@ColumnName		NVARCHAR(128)
	,@AggregateExcludes	VARCHAR(512)
	,@AvgExcludes		VARCHAR(512)
	,@ID				INT

-- Set all the datatime data types if desired
IF @DateSearch = 1
	SET @DataType = 'date,datetime,datetime2,datetimeoffset,smalldatetime,time,timestamp'

-- Populate variables to exclude on for use when finding aggregates
SET @AggregateExcludes = 'binary,bit,image,geography,geometry,ntext,sql_variant,text,uniqueidentifier,varbinary,xml'
SET @AvgExcludes = 'char,date,datetime,datetime2,smalldatetime,datetimeoffset,hierarchyid,nvarchar,nchar,nvarchar,time,timestamp,smalldatetime,NVARCHAR(128),varchar'

-- Get all of the catalog views
-- Drop the tables if they exist
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#syscolumns') IS NOT NULL
   DROP TABLE #syscolumns
IF OBJECT_ID('tempdb.dbo.#systypes') IS NOT NULL
   DROP TABLE #systypes
IF OBJECT_ID('tempdb.dbo.#search') IS NOT NULL
   DROP TABLE #search

-- Create the tables
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT), [is_encrypted] = CAST(NULL AS BIT), [schemaname] = CAST(NULL AS NVARCHAR(128)), [is_ms_shipped] = CAST(NULL AS BIT) INTO #sysobjects FROM [dbo].[sysobjects]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #syscolumns FROM [dbo].[syscolumns]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #systypes FROM [dbo].[systypes]
SELECT TOP 0 [id], [definition] = CAST(NULL AS VARCHAR(8000)), [database_id] = CAST(NULL AS INT) INTO #syscomments FROM [dbo].[syscomments]

-- Retrieve objects from database(s)
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item]
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item]
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database(s), or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY 1

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 
			
		-- sysobjects
		INSERT INTO #sysobjects
		SELECT *, DB_ID(''' + @DBName + '''), ISNULL(OBJECTPROPERTY([id],''IsEncrypted''), 0), USER_NAME([uid]), ISNULL(OBJECTPROPERTY([id],''IsMSShipped''), 0) FROM [dbo].[sysobjects]

		-- syscolumns 
		INSERT INTO #syscolumns
		SELECT *, DB_ID(''' + @DBName + ''') FROM [dbo].[syscolumns]
			
		-- systypes
		INSERT INTO #systypes
		SELECT *, DB_ID(''' + @DBName + ''') FROM [dbo].[systypes]

		-- syscomments
		INSERT INTO #syscomments
		SELECT [id], CAST([text] AS VARCHAR(8000)), DB_ID(''' + @DBName + ''') FROM .[dbo].[syscomments]
	')

FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs

-- Filter out comments
IF @IgnoreComments = 1
	UPDATE t
	SET [definition] = [dbo].[RemoveComments_fn](t.[definition])
	FROM #syscomments t

-- Index the temp tables
CREATE NONCLUSTERED INDEX [#IX_sysobjects_1] ON #sysobjects
(
	[database_id]
	,[id]
)
CREATE NONCLUSTERED INDEX [#IX_syscolumns_1] ON #syscolumns
(
	[database_id]
	,[id]
	,[xusertype]
) 
CREATE NONCLUSTERED INDEX [#IX_systypes_1] ON #systypes
(
	[database_id]
	,[xusertype]
)
CREATE NONCLUSTERED INDEX [#IX_syscomments_1] ON #syscomments
(
	[database_id]
	,[id]
)

SELECT
	ID = IDENTITY (INT,1,1)
	,DB_NAME(so.[database_id])									AS [DBName]
	,so.[schemaname]											AS [SchemaName]
	,so.[name]													AS [ObjectName]
	,CAST(dbo.[sysobjecttype_fn](so.[type]) AS NVARCHAR(128))	AS [ObjectType]
	,so.[crdate]												AS [DateCreated]
	,CASE
		WHEN so.[is_encrypted] = 1 THEN '***Encrypted***'
		ELSE sm.[definition]										
	END															AS [Definition]
	,sc.[name]													AS [ColumnName]
	,st.[name]													AS [DataType]
	,CAST(NULL AS VARCHAR(8000))								AS [Min]
	,CAST(NULL AS VARCHAR(8000))								AS [Max]
	,CAST(NULL AS VARCHAR(8000))								AS [Avg]
INTO #search
FROM #sysobjects so
LEFT JOIN #syscomments sm
	ON sm.[id] = so.[id]
	AND sm.[database_id] = so.[database_id]
LEFT JOIN #syscolumns sc
	ON sc.[database_id] = so.[database_id]
	AND sc.[id] = so.[id]
LEFT JOIN #systypes st
	ON st.[database_id] = so.[database_id]
	AND st.[xusertype] = sc.[xusertype]
LEFT JOIN [dbo].[Split_fn](@TableName,',') tb
	ON so.[name] = tb.[item]
LEFT JOIN [dbo].[Split_fn](@TableNameExclude,',') te
	ON so.[name] = te.[item] 
LEFT JOIN [dbo].[Split_fn](@ObjectType,',') ot
	ON ot.[item] = so.[type] COLLATE DATABASE_DEFAULT	-- Resolve "Latin1_General_CI_AS_KS_WS" and "SQL_Latin1_General_CP1_CI_AS" conflict
LEFT JOIN [dbo].[Split_fn](@ObjectTypeExclude,',') ote
	ON ote.[item] = so.[type] COLLATE DATABASE_DEFAULT	-- Resolve "Latin1_General_CI_AS_KS_WS" and "SQL_Latin1_General_CP1_CI_AS" conflict
LEFT JOIN [dbo].[Split_fn](@DataType,',') dt
	ON dt.[item] = st.[name]
LEFT JOIN [dbo].[Split_fn](@DataTypeExclude,',') dte
	ON dte.[item] = st.[name]
WHERE so.[is_ms_shipped] = @IncludeSystem				-- Include or omit system objects
AND ((sm.[definition] LIKE '%' + @SearchString + '%')	-- Find object definitions
		OR (so.[name] LIKE '%' + @SearchString + '%')	-- Find object names
		OR (sc.[name] LIKE '%' + @SearchString + '%')	-- Find table columns		
		OR (@SearchString IS NULL))						-- Searching just for data types
AND ((tb.[item] IS NOT NULL AND @TableName IS NOT NULL) OR @TableName IS NULL)		-- Specified table(s), or all
AND ((ot.[item] IS NOT NULL AND @ObjectType IS NOT NULL) OR @ObjectType IS NULL)	-- Specified object type(s), or all
AND ((dt.[item] IS NOT NULL AND @DataType IS NOT NULL) OR @DataType IS NULL)	-- Specified data type(s), or all
AND te.[item] IS NULL	-- All but excluded tables
AND ote.[item] IS NULL	-- All but excluded object types
AND dte.[item] IS NULL	-- All but excluded data types
ORDER BY 1

-- Do the jobs search
IF @SearchJobs = 1
BEGIN

	INSERT INTO #search ([DBName], [SchemaName], [ObjectName], [ObjectType], [Definition], [DateCreated])
	SELECT
		'SQL JOB'
		,NULL
		,j.[name]
		,LEFT('STEP: ' + CAST(js.[step_id] AS VARCHAR(2)) + ' - ''' + js.[step_name] + '''',128)
		,js.[command]
		,j.[date_created]
	FROM [msdb].[dbo].[sysjobs] j
	INNER JOIN [msdb].[dbo].[sysjobsteps] js
		ON j.[job_id] = js.[job_id]
	WHERE js.[command] LIKE '%' + @SearchString + '%'
	OR js.[database_name] LIKE '%' + @SearchString + '%'
	OR j.[name] LIKE '%' + @SearchString + '%'
	OR js.[step_name] LIKE '%' + @SearchString + '%'

END

-- Find the aggregate values
IF (@FindMax = 1 OR @FindMin = 1 OR @FindAvg = 1)
BEGIN
	DECLARE #aggregates CURSOR LOCAL STATIC FOR
	SELECT
		s.[ID]
		,s.[DBName]
		,s.[SchemaName]
		,s.[ObjectName]
		,s.[ColumnName]
		,s.[DataType]
	FROM #search s
	LEFT JOIN [dbo].[Split_fn](@AggregateExcludes,',') ae
		ON s.[DataType] = ae.[item]
	WHERE [ObjectType] IN ('Table (user-defined)','System base table','View')
	AND ae.[item] IS NULL -- All but excluded aggregate data types 

	OPEN #aggregates
	FETCH NEXT FROM #aggregates INTO @ID, @DBName, @SchemaName, @ObjectName, @ColumnName, @DataType
	WHILE @@FETCH_STATUS = 0
	BEGIN

		-- Find maximum value
		IF @FindMax = 1
		BEGIN
			EXEC
			('
				UPDATE #search
				SET [Max] = 
				(
					SELECT MAX(CAST([' + @ColumnName + '] AS VARCHAR(8000)))
					FROM [' + @DBName + '].[' + @SchemaName + '].[' + @ObjectName + '] WITH (NOLOCK)
				)
				WHERE [ID] = ' + @ID + '
			')
		END

		-- Find minimum value
		IF @FindMin = 1
		BEGIN
			EXEC
			('
				UPDATE #search
				SET [Min] = 
				(
					SELECT MIN(CAST([' + @ColumnName + '] AS VARCHAR(8000)))
					FROM [' + @DBName + '].[' + @SchemaName + '].[' + @ObjectName + '] WITH (NOLOCK)
				)
				WHERE [ID] = ' + @ID + '
			')
		END
		
		-- Find average value
		IF @FindAvg = 1
		BEGIN
			IF @DataType NOT IN (SELECT [item] FROM [dbo].[Split_fn](@AvgExcludes,',')) -- All but excluded aggregate data types
			BEGIN
				EXEC
				('
					UPDATE #search
					SET [Avg] = 
					(
						CASE 
							WHEN [DataType] IN (''int'') THEN -- Resolve overflow error
								(
									SELECT AVG(CAST([' + @ColumnName + '] AS BIGINT)) 
									FROM [' + @DBName + '].[' + @SchemaName + '].[' + @ObjectName + '] WITH (NOLOCK)
								)
							ELSE
								(
									SELECT AVG([' + @ColumnName + ']) 
									FROM [' + @DBName + '].[' + @SchemaName + '].[' + @ObjectName + '] WITH (NOLOCK)
								)
						END
					)
					WHERE [ID] = ' + @ID + '
				')
			END
		END

	FETCH NEXT FROM #aggregates INTO @ID, @DBName, @SchemaName, @ObjectName, @ColumnName, @DataType
	END
	CLOSE #aggregates
	DEALLOCATE #aggregates
END

-- Return results
SELECT 
	[DBName]
	,[SchemaName]
	,[ObjectName]
	,[ObjectType]
	,[DateCreated]
	,[Definition]
	,[ColumnName]
	,[DataType]
	,[Min]
	,[Max]
	,[Avg]
FROM #search
ORDER BY [DBName], [SchemaName], [ObjectName], [ObjectType], [ColumnName]