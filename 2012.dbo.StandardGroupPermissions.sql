IF OBJECT_ID('[dbo].[StandardGroupPermissions]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[StandardGroupPermissions]
GO

/******************************************************************************
* Name
	[dbo].[StandardGroupPermissions]

* Author
	Adam Bean
	
* Date
	2012.04.09
	
* Synopsis
	Creates standardized NT group permissions
	
* Description
	Based on domain, instance, server and optional parameters, this procedure will dynamically create and apply applicable permissions as define

* Examples
	EXEC [admin].[dbo].[StandardGroupPermissions]
		@Domain = 'UH'
		
	EXEC [admin].[dbo].[StandardGroupPermissions]
		@Domain = 'ETC'
		,@Reader = 'Readers'
		,@PwrUser = 'PwrUsers'
		,@Captain = 'Captains'
		,@Admin = 'Admins'
		,@Prefix = 'SQL'
		,@Sufix = 'sg'
		,@debug = 1

* Dependencies
	dbo.Split_fn
	dbo.sysdatabases_vw

* Parameters
	@DBName			= Database name(s) to apply permissions to (CSV supported, NULL for all)
	@DBNameExclude	= Database name(s) to exclude permissions to (CSV supported)
	@Domain			= Domain name where NT groups resides
	@Reader			= Login name for reader group, defaulted to SQLReaders
	@PwrUser		= Login name for reader group, defaulted to SQLPwrUsers		
	@Captain		= Login name for reader group, defaulted to SQLCaptains
	@Admin			= Login name for reader group, defaulted to SQLAdmins
	@ReaderRole		= Role name for reader group, defaulted to db_nt_reader
	@PwrUserRole	= Role name for reader group, defaulted to db_nt_pwruser	
	@IncludeSystem	= Apply group permissions to system databses, off by default
	@Prefix			= Add an additional prefix name before ths standard name
	@Sufix			= Add an additional sufix name before ths standard name
	@NameOverride	= Override login name to use this string instead of Server_InstanceName convention
	@Debug			= Show, but don't execute work to be done
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20130404	Jeff Gogel		Granted SHOWPLAN to readers
	20140306	Jeff Gogel		Granted VIEW ANY DEFINITION to Captains so they can view all logins
	20150507	Jeff Gogel		Added @NameOverride parameter to create custom group names
	20160428	Jeff Gogel		Added check for AG replicas
	20161227	Jeff Gogel		Removed ORDER BY ordinal column number
	20161227	Jeff Gogel		sys.sysmembers is deprecated.  Changed to reference sys.database_role_members
	20170425	Adam Bean		Added @Prefix to allow for a prefixed name to be added to the naming standard & new standard header
	20170426	Adam Bean		Added @Suffix to allow for a suffixed name to be added to the naming standard
	20170512	Adam Bean		Removed execute permissions from Readers, fixed adding role to user based on parameter
	20170515	Adam Bean		Added SQLsafe support Fixed permissions to system databases (used to get read by default, now it's just the specific permissions granted to the roles)
	20170519	Adam Bean		Removed read only databases
******************************************************************************/

CREATE PROCEDURE [dbo].[StandardGroupPermissions]
(
	@DBName				NVARCHAR(512)	= NULL
	,@DBNameExclude		NVARCHAR(512)	= NULL
	,@Domain			VARCHAR(16)
	,@Reader			VARCHAR(32)		= 'SQLReaders'
	,@PwrUser			VARCHAR(32)		= 'SQLPwrUsers'
	,@Captain			VARCHAR(32)		= 'SQLCaptains'
	,@Admin				VARCHAR(32)		= 'SQLAdmins'
	,@ReaderRole		VARCHAR(16)		= 'db_nt_reader'
	,@PwrUserRole		VARCHAR(16)		= 'db_nt_pwruser'
	,@CaptainRole		VARCHAR(16)		= 'db_nt_captain'
	,@IncludeSystem		BIT				= 0
	,@Prefix			VARCHAR(16)		= NULL
	,@Sufix				VARCHAR(16)		= NULL
	,@NameOverride		VARCHAR(256)	= NULL
	,@Debug				BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE
	@ServerName			NVARCHAR(128)
	,@InstanceName		NVARCHAR(128)
	,@DatabaseName		NVARCHAR(128)
	,@SQL				VARCHAR(MAX)
	,@ReaderLogin		VARCHAR(272)
	,@PwrUserLogin		VARCHAR(272)
	,@CaptainLogin		VARCHAR(272)
	,@AdminLogin		VARCHAR(272)
	,@DropRoleUser		VARCHAR(272)
	,@DropRoleName		VARCHAR(272)
	,@LF				CHAR(1)

-- Check for override and create login names appropriately
IF (@NameOverride IS NULL)
BEGIN
	-- Populate some working variables
	SET @ServerName = (SELECT CAST(SERVERPROPERTY('MachineName') AS VARCHAR(128)))
	SET @InstanceName = (SELECT CAST(SERVERPROPERTY('InstanceName') AS VARCHAR(128)))
	SET @LF = CHAR(10)

	-- Make sure @Domain has a backslash
	IF RIGHT(@Domain,1) != '\'
		SET @Domain = @Domain + '\'
	
	-- Build login names based on domain, server, instance names and optionally if @Prefix was passed
	IF @InstanceName IS NOT NULL
		SET @ServerName = UPPER(@Domain) + ISNULL(@Prefix + '_' + UPPER(@ServerName), UPPER(@ServerName)) + '_' + UPPER(@InstanceName) + '_'
	ELSE
		SET @ServerName = UPPER(@Domain) + ISNULL(@Prefix + '_' + UPPER(@ServerName), UPPER(@ServerName)) + '_DEFAULT_' 

	-- Build login name based on servername + instancename + login type and optionally if @Sufix was passed
	SET @ReaderLogin	= @ServerName + ISNULL(@Reader + '_' + @Sufix, @Reader)
	SET @PwrUserLogin	= @ServerName + ISNULL(@PwrUser + '_' + @Sufix, @PwrUser)
	SET @CaptainLogin	= @ServerName + ISNULL(@Captain + '_' + @Sufix, @Captain)
	SET @AdminLogin		= @ServerName + ISNULL(@Admin + '_' + @Sufix, @Admin)
END
ELSE
BEGIN
	-- Build login names based on override passed in
	SET @ReaderLogin	= @NameOverride + '_' + @Reader
	SET @PwrUserLogin	= @NameOverride + '_' + @PwrUser
	SET @CaptainLogin	= @NameOverride + '_' + @Captain
	SET @AdminLogin		= @NameOverride + '_' + @Admin
END

-- Get all of the catalog views
IF OBJECT_ID('tempdb.dbo.#sysdatabase_principals') IS NOT NULL
   DROP TABLE #sysdatabase_principals
SELECT TOP 0 * INTO #sysdatabase_principals FROM [sys].[database_principals]

IF OBJECT_ID('tempdb.dbo.#sysmembers') IS NOT NULL
   DROP TABLE #sysmembers
SELECT TOP 0 CAST([member_principal_id] AS VARCHAR(272)) AS [memberuid], CAST([role_principal_id] AS VARCHAR(272)) AS [groupuid] INTO #sysmembers FROM [sys].[database_role_members]

IF @Debug = 1
	SET @SQL = '-- *** Debug Enabled ***'
ELSE SET @SQL = ''

-- Create logins
SET @SQL = @SQL + @LF + '-- Create logins'
IF EXISTS(SELECT [name] FROM [sys].[server_principals] WHERE [name] = @ReaderLogin)
	SET @SQL = @SQL + @LF + 'DROP LOGIN [' + @ReaderLogin + ']'
SET @SQL = @SQL + @LF + 'CREATE LOGIN [' + @ReaderLogin + '] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
IF EXISTS(SELECT [name] FROM [sys].[server_principals] WHERE [name] = @PwrUserLogin)
	SET @SQL = @SQL + @LF + 'DROP LOGIN [' + @PwrUserLogin + ']'
SET @SQL = @SQL + @LF + 'CREATE LOGIN [' + @PwrUserLogin + '] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
IF EXISTS(SELECT [name] FROM [sys].[server_principals] WHERE [name] = @CaptainLogin)
	SET @SQL = @SQL + @LF + 'DROP LOGIN [' + @CaptainLogin + ']'
SET @SQL = @SQL + @LF + 'CREATE LOGIN [' + @CaptainLogin + '] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
IF EXISTS(SELECT [name] FROM [sys].[server_principals] WHERE [name] = @AdminLogin)
	SET @SQL = @SQL + @LF + 'DROP LOGIN [' + @AdminLogin + ']'
SET @SQL = @SQL + @LF + 'CREATE LOGIN [' + @AdminLogin + '] FROM WINDOWS WITH DEFAULT_DATABASE=[master]'
	
IF @Debug = 1
	PRINT (@SQL)
ELSE
	EXEC (@SQL)

-- Server roles
SET @SQL = @LF + '-- Server roles' +
'
EXEC [sys].[sp_addsrvrolemember] ''' + @PwrUserLogin + ''', ''dbcreator''
EXEC [sys].[sp_addsrvrolemember] ''' + @PwrUserLogin + ''', ''bulkadmin''
EXEC [sys].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''dbcreator''
EXEC [sys].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''bulkadmin''
EXEC [sys].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''diskadmin''
EXEC [sys].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''processadmin''
EXEC [sys].[sp_addsrvrolemember] ''' + @CaptainLogin + ''', ''setupadmin''
EXEC [sys].[sp_addsrvrolemember] ''' + @AdminLogin + ''', ''sysadmin''

USE [master]
GRANT VIEW SERVER STATE TO [' + @ReaderLogin + ']
GRANT VIEW SERVER STATE TO [' + @PwrUserLogin + ']
GRANT VIEW SERVER STATE TO [' + @CaptainLogin + ']
GRANT VIEW ANY DEFINITION TO [' + @CaptainLogin + ']
'

IF @Debug = 1
	PRINT (@SQL)
ELSE
	EXEC (@SQL)

-- Create users, roles & grant permissions
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[is_read_only] = 0
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND s.[name] != 'tempdb'
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database(s), or all
AND de.[item] IS NULL -- All but excluded databases
AND ([dbo].[HADR_DatabaseIsPrimary_fn](s.[name]) = 1 OR s.[replica_id] IS NULL) -- Only primary replicas or databases not in an availability group
ORDER BY [name];

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DatabaseName
WHILE @@FETCH_STATUS = 0
BEGIN

	SET @SQL = @LF + '-- Database permissions for: ' + @DatabaseName + ''

	-- Gather database users
	TRUNCATE TABLE #sysdatabase_principals
	INSERT INTO #sysdatabase_principals
	EXEC('SELECT * FROM [' + @DatabaseName + '].[sys].[database_principals]')

	-- Gather non standard users in standard roles
	TRUNCATE TABLE #sysmembers
	INSERT INTO #sysmembers
	EXEC('USE [' + @DatabaseName + ']
	SELECT USER_NAME([member_principal_id]) AS [memberuid], USER_NAME([role_principal_id]) AS [groupuid]
	FROM [sys].[database_role_members]
	WHERE USER_NAME([role_principal_id]) IN (''' + @ReaderRole + ''', ''' + @PwrUserRole + ''', ''' + @CaptainRole + ''')
	AND USER_NAME([member_principal_id]) NOT IN (''' + @Reader + ''', ''' + @PwrUser + ''', ''' + @Captain + ''')')

	-- Create database users
	SET @SQL = @SQL + @LF + '-- Create users'
	
	-- Reader
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @Reader)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] DROP USER [' + @Reader + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] CREATE USER [' + @Reader + '] FOR LOGIN [' + @ReaderLogin + ']'

	-- PowerUser
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @PwrUser)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] DROP USER [' + @PwrUser + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] CREATE USER [' + @PwrUser + '] FOR LOGIN [' + @PwrUserLogin + ']'

	-- Captain
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @Captain)
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] DROP USER [' + @Captain + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] CREATE USER [' + @Captain + '] FOR LOGIN [' + @CaptainLogin + ']'

	-- Drop non standard users from roles
	IF EXISTS(SELECT [memberuid] FROM #sysmembers)
	BEGIN
		DECLARE #droprolemembers CURSOR LOCAL STATIC FOR
		SELECT 
			[memberuid]	AS [UserName] 
			,[groupuid] AS [RoleName]
		FROM #sysmembers
		
		OPEN #droprolemembers
		FETCH NEXT FROM #droprolemembers INTO @DropRoleUser, @DropRoleName
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @SQL = @SQL + @LF + @LF + '-- Drop non standard users from roles'
			SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] EXEC sp_droprolemember ''' + @DropRoleName + ''', ''' + @DropRoleUser + ''''
		
		FETCH NEXT FROM #droprolemembers INTO @DropRoleUser, @DropRoleName
		END
		CLOSE #droprolemembers
		DEALLOCATE #droprolemembers
	END

	-- Create database roles
	SET @SQL = @SQL + @LF + @LF + '-- Create roles'
	
	-- Reader
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @ReaderRole AND [type] = 'R')
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] DROP ROLE [' + @ReaderRole + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] CREATE ROLE [' + @ReaderRole + '] AUTHORIZATION [dbo]'

	-- PowerUser
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @PwrUserRole AND [type] = 'R')
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] DROP ROLE [' + @PwrUserRole + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] CREATE ROLE [' + @PwrUserRole + '] AUTHORIZATION [dbo]'

	-- Captain
	IF EXISTS(SELECT [name] FROM #sysdatabase_principals WHERE [name] = @CaptainRole AND [type] = 'R')
		SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] DROP ROLE [' + @CaptainRole + ']'
	SET @SQL = @SQL + @LF + 'USE [' + @DatabaseName + '] CREATE ROLE [' + @CaptainRole + '] AUTHORIZATION [dbo]'
	
	-- Only grant permissions to system databses if specified (@IncludeSystem)
	IF DB_ID(@DatabaseName) > 4 OR @IncludeSystem = 1
	BEGIN	
		-- Database and role permissions
		-- Reader
		SET @SQL = @SQL + @LF + @LF + '-- Roles & permissions'
		SET @SQL = @SQL +
		'
		-- Reader
		USE [' + @DatabaseName + '] 
		GRANT VIEW DEFINITION TO [' + @ReaderRole + ']
		GRANT SHOWPLAN TO [' + @ReaderRole + ']
		EXEC [sys].[sp_addrolemember] ''db_datareader'', ''' + @Reader + '''
		EXEC [sys].[sp_addrolemember] ''' + @ReaderRole + ''', ''' + @Reader + '''
		'
			
		-- PwrUser
		SET @SQL = @SQL + 
		'
		-- PwrUser
		USE [' + @DatabaseName + ']
		GRANT EXECUTE TO [' + @PwrUserRole + ']
		GRANT VIEW DEFINITION TO [' + @PwrUserRole + ']
		GRANT SHOWPLAN TO [' + @PwrUserRole + ']
		GRANT ALTER ANY SCHEMA TO [' + @PwrUserRole + ']
		EXEC [sys].[sp_addrolemember] ''db_datareader'', ''' + @PwrUser + '''
		EXEC [sys].[sp_addrolemember] ''db_datawriter'', ''' + @PwrUser + '''
		EXEC [sys].[sp_addrolemember] ''db_ddladmin'', ''' + @PwrUser + '''
		EXEC [sys].[sp_addrolemember] ''db_backupoperator'', ''' + @PwrUser + '''
		EXEC [sys].[sp_addrolemember] ''' + @PwrUserRole + ''', ''' + @PwrUser + '''
		'

		-- Captain
		SET @SQL = @SQL +
		'
		-- Captain
		USE [' + @DatabaseName + '] 
		EXEC [sys].[sp_addrolemember] ''db_owner'', ''' + @Captain + '''
		'
	END
	
	IF @Debug = 1
		PRINT (@SQL)
	ELSE
		EXEC (@SQL)
		
FETCH NEXT FROM #dbs INTO @DatabaseName
END
CLOSE #dbs
DEALLOCATE #dbs


-- System database permissions
-- Only apply if no database was specified as the users have to be in the system databases
IF @DBName IS NULL
BEGIN
	SET @SQL = @LF + @LF + '-- System permissions'
	-- Database roles
	SET @SQL = @SQL + @LF + 
	'
	-- Bind user to role
	USE [master]
	EXEC [sys].[sp_addrolemember] ''' + @ReaderRole + ''', ''' + @Reader + '''
	EXEC [sys].[sp_addrolemember] ''' + @PwrUserRole + ''', ''' + @PwrUser + '''
	EXEC [sys].[sp_addrolemember] ''' + @CaptainRole+ ''', ''' + @Captain + '''
	
	USE [msdb]
	EXEC [sys].[sp_addrolemember] ''' + @ReaderRole + ''', ''' + @Reader + '''
	EXEC [sys].[sp_addrolemember] ''' + @PwrUserRole + ''', ''' + @PwrUser + '''
	EXEC [sys].[sp_addrolemember] ''' + @CaptainRole+ ''', ''' + @Captain + '''
	'
	
	-- SQL Agent, DatabaseMail, SSIS & data collector permissions
	SET @SQL = @SQL + @LF + 
	'	
	-- SQL Agent, DatabaseMail, SSIS & data collector permissions
	USE [msdb]
	EXEC [sys].[sp_addrolemember] ''SQLAgentReaderRole'', ''' + @Reader + '''
	EXEC [sys].[sp_addrolemember] ''SQLAgentOperatorRole'', ''' + @PwrUser + '''
	EXEC [sys].[sp_addrolemember] ''SQLAgentOperatorRole'', ''' + @Captain + '''
	EXEC [sys].[sp_addrolemember] ''DatabaseMailUserRole'', ''' + @PwrUser + '''
	EXEC [sys].[sp_addrolemember] ''DatabaseMailUserRole'', ''' + @Captain + '''
	EXEC [sys].[sp_addrolemember] ''db_ssisadmin'', ''' + @PwrUser + '''
	EXEC [sys].[sp_addrolemember] ''db_ssisadmin'', ''' + @Captain + '''
	EXEC [sys].[sp_addrolemember] ''dc_operator'', ''' + @PwrUser + '''
	EXEC [sys].[sp_addrolemember] ''dc_operator'', ''' + @Captain + '''
	EXEC [sys].[sp_addrolemember] ''dc_proxy'', ''' + @PwrUser + '''
	EXEC [sys].[sp_addrolemember] ''dc_proxy'', ''' + @Captain + '''
	'

	-- Debug, Extended/stored procedures & trace
	SET @SQL = @SQL + @LF + 
	'
	-- Debug, Extended/stored procedures & trace
	USE [master]
	GRANT ALTER TRACE TO [' + @PwrUserLogin + '], [' + @CaptainLogin + ']
	GRANT EXECUTE ON [sp_enable_sql_debug] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [sp_execute] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [sp_executesql] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [xp_readerrorlog] TO [' + @ReaderRole + '], [' + @PwrUserRole + '], [' + @CaptainRole + ']
	'

	-- Misc.
	-- LiteSpeed	
	IF OBJECT_ID('[master].[dbo].[xp_backup_database]','X') IS NOT NULL 
	SET @SQL = @SQL + @LF + 
	'
	-- LiteSpeed	
	USE [master]
	GRANT EXECUTE ON [dbo].[xp_backup_database] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_restore_database] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_backup_log] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_restore_log] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	'

	-- SQLBackup	
	IF OBJECT_ID('[master].[dbo].[sqlbackup]','X') IS NOT NULL 
	SET @SQL = @SQL + @LF + 
	'
	-- SQLBackup	
	USE [master]
	GRANT EXECUTE ON [dbo].[sqlbackup] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbdata] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbdir] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbmemory] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbstatus] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbtest] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbtestcancel] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbteststatus] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[sqbutility] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	'
	-- SQLSafe
	IF OBJECT_ID('[master].[dbo].[xp_ss_backup]','X') IS NOT NULL 
	SET @SQL = @SQL + @LF + 
	'
	-- SQLSafe	
	USE [master]
	GRANT EXECUTE ON [dbo].[xp_ss_backup] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_restore] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_verify] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_list] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_listfiles] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_objectrestore] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_objectlist] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_browse] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_extract] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_restoreheaderonly] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_restorefilelistonly] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_expire] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_delete] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_restorelast] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	GRANT EXECUTE ON [dbo].[xp_ss_instantrestore] TO [' + @PwrUserRole + '], [' + @CaptainRole + ']
	'

	IF @Debug = 1
		PRINT (@SQL)
	ELSE
		EXEC (@SQL)
		
	-- Replication
	IF (SELECT MAX(CAST([is_distributor] AS TINYINT)) FROM [dbo].[sysdatabases_vw]) > 0
	BEGIN
		DECLARE #dbs CURSOR LOCAL STATIC FOR
		SELECT 
			[name]
		FROM [sysdatabases_vw]
		WHERE [is_distributor] = 1
		ORDER BY [name]

		OPEN #dbs
		FETCH NEXT FROM #dbs INTO @DatabaseName
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
		SET @SQL = 
		'
		-- Replication
		USE [' + @DatabaseName + '] 
		EXEC [sys].[sp_addrolemember] ''replmonitor'', ''' + @Reader + '''
		EXEC [sys].[sp_addrolemember] ''replmonitor'', ''' + @PwrUser + '''
		EXEC [sys].[sp_addrolemember] ''replmonitor'', ''' + @Captain + '''
		'	
		
		IF @Debug = 1
			PRINT (@SQL)
		ELSE
			EXEC (@SQL)
				
		FETCH NEXT FROM #dbs INTO @DatabaseName
		END
		CLOSE #dbs
		DEALLOCATE #dbs
	END
	
END
SET NOCOUNT OFF