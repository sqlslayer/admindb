IF OBJECT_ID('dbo.DatabaseAttributes_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[DatabaseAttributes_vw]
GO

/******************************************************************************
* Name
	[dbo].[DatabaseAttributes_vw]

* Author
	Matt Stanford
	
* Date
	2011.04.11
	
* Synopsis
	Attribute style view of all database properties
	
* Description
	An unpivoted view of all database attributes along with some file attributes.
	It is in this format for ease of query as well as ease of storage (for consumption by 
	a DBACentral collector)
	
	The 2000 version of this view sucks.

* Examples
	SELECT * FROM dbo.DatabaseAttributes_vw

* Dependencies
	dbo.sysdatabases_vw
	dbo.sysmasterfiles_vw

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20120607	Adam Bean		Added new column for tlog shipping
******************************************************************************/

CREATE VIEW [dbo].[DatabaseAttributes_vw]
AS

SELECT
	[name]									AS [Database_Name]
	,NULL									AS [file_id]
	,'database_id'							AS [AttributeName]
	,CAST([database_id] AS SQL_VARIANT)		AS [AttributeValue]
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'owner_sid'		
	,CAST([owner_sid] AS SQL_VARIANT)		
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'create_date'		
	,CAST([create_date] AS SQL_VARIANT)		
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'compatibility_level'
	,CAST([compatibility_level] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'collation_name'	
	,CAST([collation_name] AS SQL_VARIANT)	
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'user_access_desc'	
	,CAST([user_access_desc] AS SQL_VARIANT)	
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_read_only'		
	,CAST([is_read_only] AS SQL_VARIANT)		
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_auto_close_on'	
	,CAST([is_auto_close_on] AS SQL_VARIANT)	
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_auto_shrink_on'
	,CAST([is_auto_shrink_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'state_desc'
	,CAST([state_desc] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_in_standby'
	,CAST([is_in_standby] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'recovery_model_desc'
	,CAST([recovery_model_desc] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'page_verify_option_desc'
	,CAST([page_verify_option_desc] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_auto_create_stats_on'
	,CAST([is_auto_create_stats_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_auto_update_stats_on'
	,CAST([is_auto_update_stats_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_ansi_null_default_on'
	,CAST([is_ansi_null_default_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_ansi_nulls_on'
	,CAST([is_ansi_nulls_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_ansi_padding_on'
	,CAST([is_ansi_padding_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_ansi_warnings_on'
	,CAST([is_ansi_warnings_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_arithabort_on'
	,CAST([is_arithabort_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_concat_null_yields_null_on'
	,CAST([is_concat_null_yields_null_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_numeric_roundabort_on'
	,CAST([is_numeric_roundabort_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_quoted_identifier_on'
	,CAST([is_quoted_identifier_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_recursive_triggers_on'
	,CAST([is_recursive_triggers_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_cursor_close_on_commit_on'
	,CAST([is_cursor_close_on_commit_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_local_cursor_default'
	,CAST([is_local_cursor_default] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_fulltext_enabled'
	,CAST([is_fulltext_enabled] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_db_chaining_on'
	,CAST([is_db_chaining_on] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_published'
	,CAST([is_published] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_subscribed'
	,CAST([is_subscribed] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_merge_published'
	,CAST([is_merge_published] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_distributor'
	,CAST([is_distributor] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]
UNION ALL
SELECT
	[name]				
	,NULL				
	,'is_sync_with_backup'
	,CAST([is_sync_with_backup] AS SQL_VARIANT)
FROM [dbo].[sysdatabases_vw]

-- File Info
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'type_desc'
	,CAST([type_desc] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'data_space_id'
	,CAST([data_space_id] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'name'
	,CAST([name] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'physical_name'
	,CAST([physical_name] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'state_desc'
	,CAST([state_desc] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'size'
	,CAST([size] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'max_size'
	,CAST([max_size] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'growth'
	,CAST([growth] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'is_read_only'
	,CAST([is_read_only] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
	[database_name]				
	,[file_id]				
	,'is_percent_growth'
	,CAST([is_percent_growth] AS SQL_VARIANT)
FROM [dbo].[sysmasterfiles_vw]
UNION ALL
SELECT
    d.[name]			AS [database_name]
    ,NULL				AS [file_id]
    ,'is_tlog_shipped'
    ,CASE
        WHEN s.[primary_database_name] IS NOT NULL THEN CAST(1 AS SQL_VARIANT)
        ELSE CAST(0 AS SQL_VARIANT)
	END
FROM [admin].[dbo].[sysdatabases_vw] d
LEFT JOIN [msdb].[dbo].[log_shipping_primaries] s
    ON d.[name] = s.[primary_database_name]
