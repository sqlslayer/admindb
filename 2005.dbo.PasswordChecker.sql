IF OBJECT_ID('dbo.PasswordChecker','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[PasswordChecker]
GO

/******************************************************************************
* Name
	[dbo].[PasswordChecker]

* Author
	Adam Bean
	
* Date
	2014.01.29
	
* Synopsis
	Reports results of bad passwords for SQL logins.
	
* Description
	Using PWDCOMPARE, SQL login passwords are compared to a control table (dbo.PasswordCheckerList) to determine
	if a password is bad (bad password (stored in the table), same as login or blank)

* Examples
	EXEC [dbo].[PasswordChecker]

* Dependencies
	[dbo].[syslogins_vw]
	[dbo].[PasswordCheckerList] - A control table holding most basic bad passwords

* Parameters
	@LoginName			- Login(s) to analyze
	@LoginNameExclude	- Login(s) to exclude
	@Results			- PASS, FAIL or NULL for both
	
* Notes
	Do not expect great performance on this procedure. 
	The PWDCOMPARE function simply doesn't run all that great. Expect around a 1-4 second runtime per login (pending on server performance)

	The UNION's are in place to help increase performance, the old logic was OR's:
	LEFT JOIN dbo.[PasswordCheckerList] pc
		ON (PWDCOMPARE(pc.[Password], sl.[password_hash]) = 1									-- Was the password stored in the list?
			OR PWDCOMPARE(REVERSE(pc.[Password]), sl.[password_hash]) = 1						-- How about known passwords reversed?
			OR PWDCOMPARE(REPLACE(pc.[Password],'_Replace_',sl.name), sl.[password_hash]) = 1)	-- Is the password the same as the user name?	

	The seperate INSERTS are to support two different possibilities of duplicates:
		1) The password meets two critiera in the form of bad password + same as login (ex. "test")
		2) The password meets two critiera for a known bad password + also a reversed (ex. "0001" and "1000")

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20140205	Adam Bean		Switched sql_logins to syslogins_vw
								Resolved collation issue on REPLACE
******************************************************************************/

CREATE PROCEDURE [dbo].[PasswordChecker]
(
	@LoginName				NVARCHAR(2048)	= NULL
	,@LoginNameExclude		NVARCHAR(2048)	= NULL
	,@Results				VARCHAR(8)		= NULL
)

AS

-- Test variables
IF @Results NOT IN ('PASS', 'FAIL') 
BEGIN
	PRINT '@Results must be either ''PASS'', ''FAIL'' or NULL for all.'
	RETURN
END

-- Setup staging table 
IF OBJECT_ID('tempdb.dbo.#PasswordChecker') IS NOT NULL
	DROP TABLE #PasswordChecker

CREATE TABLE #PasswordChecker
(
	[Login]					NVARCHAR(128)
	,[Password]				NVARCHAR(128)
)

-- Is the password stored in the control table?
INSERT INTO #PasswordChecker
SELECT
	sl.[name]																AS [Login]
	,pc.[Password]
FROM [dbo].[syslogins_vw] sl
JOIN [dbo].[PasswordCheckerList] pc
	ON PWDCOMPARE(pc.[Password], sl.[password_hash]) = 1	
LEFT JOIN [dbo].[Split_fn](@LoginName,',') ln
	ON sl.[name] = ln.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@LoginNameExclude,',') lne
	ON sl.[name] = lne.[item] COLLATE DATABASE_DEFAULT
WHERE sl.[isntname] = 0																		-- Only retrieve SQL accounts
AND LEFT(sl.[name],4) != '##MS'																-- Exclude certicate based logins
AND ((ln.[item] IS NOT NULL AND @LoginName IS NOT NULL) OR @LoginName IS NULL)				-- Specified login(s), or all
AND lne.[item] IS NULL																		-- All but excluded login(s)

-- Is the password the same as the user name?
INSERT INTO #PasswordChecker
SELECT
	sl.[name]																AS [Login]
	,REPLACE(pc.[Password] COLLATE DATABASE_DEFAULT,'_Replace_',sl.[name])	AS [Password]
FROM [dbo].[syslogins_vw] sl
JOIN [dbo].[PasswordCheckerList] pc
	ON PWDCOMPARE(REPLACE(pc.[Password] COLLATE DATABASE_DEFAULT,'_Replace_',sl.name), sl.[password_hash]) = 1
LEFT JOIN [dbo].[Split_fn](@LoginName,',') ln
	ON sl.[name] = ln.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@LoginNameExclude,',') lne
	ON sl.[name] = lne.[item] COLLATE DATABASE_DEFAULT
WHERE sl.[name] NOT IN (SELECT DISTINCT [Login] FROM #PasswordChecker)
AND sl.[isntname] = 0																		-- Only retrieve SQL accounts
AND LEFT(sl.[name],4) != '##MS'																-- Exclude certicate based logins
AND ((ln.[item] IS NOT NULL AND @LoginName IS NOT NULL) OR @LoginName IS NULL)				-- Specified login(s), or all
AND lne.[item] IS NULL																		-- All but excluded login(s)

-- Is the password in the list, but reversed?
INSERT INTO #PasswordChecker
SELECT
	sl.[name]																AS [Login]
	,pc.[Password]
FROM [dbo].[syslogins_vw] sl
LEFT JOIN [dbo].[PasswordCheckerList] pc
	ON PWDCOMPARE(REVERSE(pc.[Password]), sl.[password_hash]) = 1
LEFT JOIN [dbo].[Split_fn](@LoginName,',') ln
	ON sl.[name] = ln.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@LoginNameExclude,',') lne
	ON sl.[name] = lne.[item] COLLATE DATABASE_DEFAULT
WHERE sl.[name] NOT IN (SELECT DISTINCT [Login] FROM #PasswordChecker)
AND sl.[isntname] = 0																		-- Only retrieve SQL accounts
AND LEFT(sl.[name],4) != '##MS'																-- Exclude certicate based logins
AND ((ln.[item] IS NOT NULL AND @LoginName IS NOT NULL) OR @LoginName IS NULL)				-- Specified login(s), or all
AND lne.[item] IS NULL																		-- All but excluded login(s)

-- Return results
SELECT
	*
FROM
(
	SELECT
		[Login]
		,CASE 
			WHEN [Password] IS NULL THEN 'PASS'
			ELSE 'FAIL'
		END AS [Result]
		,CASE 
			WHEN [Password] = [Login] THEN 'Same as Login'
			WHEN [Password] = '' THEN 'Blank'
			WHEN [Password] IS NOT NULL THEN 'Bad Password'
			ELSE NULL
		END AS [Reason]
		,[Password]
	FROM #PasswordChecker
) t
WHERE [Result] = @Results
OR @Results IS NULL 
ORDER BY [Result] DESC, [Login]