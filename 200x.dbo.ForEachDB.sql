IF OBJECT_ID('[dbo].[ForEachDB]','P') IS NOT NULL 
       DROP PROCEDURE [dbo].[ForEachDB]
GO

/*******************************************************************************************************
**     Name:                dbo.[ForEachDB]
**     Desc:                Procedure to execute SQL in each DB.  Similar to MS_ForEachDB
**     Auth:                Matt Stanford (SQLSlayer.com)
**	   Dependancies:		dbo.Split_fn, dbo.sysdatabases_vw
**     Parameters:			@DBName = Database name(s) to run code against (CSV supported, NULL for all)
							@DBNameExclude = Database name(s) to exclude from code (CSV supported)
							@Debug = Show, but don't execute work to be done
**	   Usage:               EXEC dbo.ForEachDB 'SELECT * FROM sys.objects', 1
**     Date:                2008.01.07
*******************************************************************************
**  License
*******************************************************************************
**  Copyright ? SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**     Change History
*******************************************************************************
**     Date:         Author:                    Description:
**     --------      --------             ---------------------------------------
**     20080627      Adam Bean            Updated view to point to new sysdatabases_vw
**     20090929      Adam Bean            Global header formatting & general clean up
**     20130605      Jeff Gogel           Added CSV options for include/exclude multiple DB's, header cleanup
********************************************************************************************************/

CREATE PROCEDURE [dbo].[ForEachDB]
(
       @SQL                 NVARCHAR(4000)
       ,@DBName             NVARCHAR(2048)  = NULL
       ,@DBNameExclude		NVARCHAR(2048)	= NULL
       ,@SwitchContext      BIT				= 0
       ,@Debug              BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE 
       @CurrentDBName       VARCHAR(1000)
       ,@Context            NVARCHAR(1000)
       ,@SQLToRun           NVARCHAR(4000)

SET @Context = ''

-- Note that this dbo.sysdatabases_vw is the one in the admin database
SET @CurrentDBName = (SELECT MIN([name]) 
                      FROM [dbo].[sysdatabases_vw] s
						LEFT JOIN [dbo].[Split_fn](@DBName,',') d
							ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
                        LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
                            ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT 
                       WHERE [state_desc] = 'ONLINE'
                            AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
                            AND de.[item] IS NULL) -- All but excluded databases

WHILE @CurrentDBName IS NOT NULL
BEGIN

       -- Add a USE statement if SwitchContext is set
       IF @SwitchContext = 1
              SET @Context = 'USE [' + @CurrentDBName + ']' + char(10)

       SET @SQLToRun = @Context + @SQL

       SET @SQLToRun = REPLACE(@SQLToRun,'?',@CurrentDBName)

       IF @Debug = 0
              EXEC(@SQLToRun)
       ELSE
              PRINT(@SQLToRun)

       SET @CurrentDBName = (SELECT MIN([name]) 
                             FROM dbo.sysdatabases_vw s
								LEFT JOIN [dbo].[Split_fn](@DBName,',') d
									ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
                                LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
									ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT 
                             WHERE [state_desc] = 'ONLINE' 
							    AND [name] > @CurrentDBName                                         
								AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
                                AND de.[item] IS NULL) -- All but excluded databases
END
GO
