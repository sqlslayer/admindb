﻿IF OBJECT_ID('[dbo].[who_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[who_vw]
GO

/******************************************************************************
* Name
	[dbo].[who_vw]

* Author
	Adam Bean (SQLSlayer.com)
	
* Date
	2008.02.22
	
* Synopsis
	View to retrieve information about sql connections 
	
* Description
	The proper replacement to sp_who/sp_who2

* Examples
	SELECT * FROM dbo.who_vw

* Dependencies
	n/a

* Parameters
	n/a
	
* Notes
	Use the procedure dbo.who to get query plans, blocking chains and an alternative look at the current query

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
    20080616	Adam Bean		Added job name for running jobs
								Switched to DMV's from legacy views
								Pull tran count from dm_exec_requests vs. dm_tran_session_transactions + dm_tran_database_transactions
    20080617	Adam Bean		Grouped up locks by read, write, schema
								Using sys.sysprocesses for dbid and command until new solution is found
  	20080703	Adam Bean		Switched tran count back to DMV's as exec_requests only returns rows for sessions that are currently active
    20080916	Adam Bean		Fixed duplicate records on lock join
								Pulling additional fields from sys.sysprocesses as dmv's do not account for them
								Changed several fields to sys.sysprocesses to account for parallelization
    20090708	Adam Bean		Expanded length of ProgramName
  	20090713	Matt Stanford	Pivoted out the Read/Write/Schema locks into their own columns
    20090903	Adam Bean		Changed estimated completion time to minutes
  	20090929	Adam Bean		Global header formatting & general clean up
  	20100117	Adam Bean		Added LTRIM/RTRIM to several columns
  	20100205	Adam Bean		Added net_transport and auth_scheme
  	20100308	Matt Stanford	Added IsolationLevel
  	20100507	Adam Bean		Changed CROSS to OUTER APPLY for dm_exec_sql_text as some connections wouldn't show up without valid text
	20131018	Adam Bean		Added CurrentPlan & updated header
	20131212    John DelNostro	Changed sys.dm_tran_locks select to a left join sub select (WriteLockCount, SchemaLockCount, ReadLockCount)
	20140402	Adam Bean		Updated the SUBSTRING in QUERY to be 2 + 2 (from 2 + 1) to resolve " Invalid length parameter passed to the LEFT or SUBSTRING function."
								Moved the CurrentPlan out of this view and into the procedure because of the limitation on complex queries "XML datatype instance has too many levels of nested nodes. Maximum allowed depth is 128 levels."
	20140413	Adam Bean		Rewrote the case statement for the query command being that previous updates still did not resolve issues (credit to Tim Ford at mssqltips)
********************************************************************************************************/

CREATE VIEW [dbo].[who_vw]

AS

SELECT
	c.[session_id]																AS [SPID]
	,p.[kpid]																	AS [KPID]
	,COALESCE(DB_NAME(r.[database_id]),DB_NAME(p.[dbid]),DB_NAME(h.[dbid]))		AS [DBName]
	,COALESCE
	(
		OBJECT_NAME(h.[objectID],h.[dbid])
		,CASE 
			WHEN h.[encrypted] = 1 THEN 'Encrypted'
			WHEN r.[session_id] IS NULL	THEN h.[text]
			WHEN r.[statement_start_offset] > 0 THEN  
				--The start of the active command is not at the beginning of the full command text 
				CASE r.[statement_end_offset]
				   WHEN -1 THEN SUBSTRING(h.[text], (r.[statement_start_offset] / 2) + 1, 2147483647) --The end of the full command is also the end of the active statement 
				   ELSE SUBSTRING(h.[text], (r.[statement_start_offset] / 2) + 1, (r.[statement_end_offset] - r.[statement_start_offset]) / 2) --The end of the active statement is not at the end of the full command 
				END
				ELSE
				--First part of full command is running
				CASE r.[statement_end_offset]
				   WHEN -1 THEN RTRIM(LTRIM(h.[text])) --The end of the full command is also the end of the active statement
				   ELSE LEFT(h.[text], (r.[statement_end_offset] / 2) + 1) --The end of the active statement is not at the end of the full command 
				END
		END
	)																			AS [Query]
	,LTRIM(RTRIM(s.[login_name]))												AS [Login]
	,LTRIM(RTRIM(s.[host_name]))												AS [HostName]
	,LTRIM(RTRIM(COALESCE(p.[status],s.[status])))								AS [Status]
	,COALESCE(r.[command],p.[cmd])												AS [Command]
	,COALESCE(p.[blocked],r.[blocking_session_id])								AS [BlkBy]
	,ISNULL(t.[trancount],0)													AS [TranCount]
	,ISNULL(i.[ReadLockCount],0)												AS [ReadLockCount]
	,ISNULL(i.[WriteLockCount],0)												AS [WriteLockCount]
	,ISNULL(i.[SchemaLockCount],0)												AS [SchemaLockCount]
	,LTRIM(RTRIM(COALESCE(p.[lastwaittype], r.[wait_type])))					AS [WaitType]
	,r.[percent_complete]														AS [PercentComplete]
	,((r.[estimated_completion_time]/1000)/60)									AS [EstCompTimeMins]
	,COALESCE(p.[cpu], s.[cpu_time])											AS [CPU]
	,p.[physical_io]															AS [IO]
	,c.[num_reads]																AS [Reads]
	,c.[num_writes]																AS [Writes]
	,c.[last_read]																AS [LastRead]
	,c.[last_write]																AS [LastWrite]
	,c.[connect_time]															AS [LoginTime]
	,s.[last_request_start_time]												AS [LastBatch]
	,c.[net_transport]															AS [NetTransport]
	,c.[auth_scheme]															AS [AuthScheme]
	,CASE s.[transaction_isolation_level]
		WHEN 0 THEN 'Unspecified'
		WHEN 1 THEN 'ReadUncomitted'
		WHEN 2 THEN 'ReadCommitted'
		WHEN 3 THEN 'Repeatable'
		WHEN 4 THEN 'Serializable'
		WHEN 5 THEN 'Snapshot'
		ELSE 'Unknown'
	END																			AS [IsolationLevel]
	,CASE
		WHEN s.[program_name] LIKE 'SQLAgent - TSQL JobStep%' THEN 'SQLAgent Job: ' + SUBSTRING(j.[name],1,256)
		ELSE LTRIM(RTRIM(SUBSTRING(s.[program_name],1,256)))
	END																			AS [ProgramName]
FROM [sys].[dm_exec_connections] c
LEFT JOIN [sys].[sysprocesses] p
	ON c.[session_id] = p.[spid]
LEFT JOIN [sys].[dm_exec_sessions] s 
	ON c.[session_id] = s.[session_id]
LEFT JOIN [sys].[dm_exec_requests] r
	ON c.[session_id] = r.[session_id]
LEFT JOIN [msdb].[dbo].[sysjobs] j
	ON SUBSTRING(ISNULL(s.[program_name],''),CHARINDEX('0x', ISNULL(s.[program_name],'')) + 18, 16) = SUBSTRING(REPLACE(ISNULL(j.[job_id],''), '-',''),17,16)  
LEFT JOIN 
(
	SELECT 
		[request_session_id]
		,COUNT(CASE WHEN [request_mode] IN ('S','IS','RangeS_S','RangeI_S') THEN 1 ELSE NULL END)																							AS [ReadLockCount]
		,COUNT(CASE WHEN [request_mode] IN ('U','X','IU','IX','SIU','SIX','UIX','BU','RangeS_U','RangeI_N','RangeI_U','RangeI_X','RangeX_S','RangeX_U','RangeX_X') THEN 1 ELSE NULL END)	AS [WriteLockCount]
		,COUNT(CASE WHEN [request_mode] IN ('Sch-S','Sch-M') THEN 1 ELSE NULL END)																											AS [SchemaLockCount]
	FROM [sys].[dm_tran_locks] AS DTL
	GROUP BY [request_session_id]
) i 
	ON i.[request_session_id] = c.[session_id]
LEFT JOIN
(
	SELECT
		[session_id]
		,[database_id] = MAX([database_id])
		,[trancount] = COUNT(*)
	FROM [sys].[dm_tran_session_transactions] t
	INNER JOIN [sys].[dm_tran_database_transactions] dt
		ON t.[transaction_id] = dt.[transaction_id]
	GROUP BY [session_id]
) t
	ON c.[session_id] = t.[session_id]
OUTER APPLY [sys].[dm_exec_sql_text](c.[most_recent_sql_handle]) h
WHERE c.[session_id] != @@SPID