IF OBJECT_ID('[dbo].[MoveDatabase]','P') IS NOT NULL 
DROP PROCEDURE [dbo].[MoveDatabase]
GO

/******************************************************************************
**	Name:			dbo.MoveDatabase
**	Desc:			Detaches, moves, and attaches databases - called from MoveDatabases
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.sysmasterfiles_vw, dbo.sysdatabases_vw, dbo.ActiveDBLocks_vw, dbo.Split_fn, dbo.KillProcess
**  Parameters:		@DBName = Database name to move, CSV supported
					@DBNameExclude = Database name to exclude when moving all, CSV supported
					@DestDataPath = New path to store data file(s)
					@DestLogPath = New path to store log file(s)
					@KillConnections = Forcefully remove connections, required to detach (won't kill in debug)
					@NewDBOwner = New database owner when attaching, leave NULL to preserve existing
					@UpdateStats = Set to 1 if you want to update database statistics before detach
					@FileTransfer = Method of how to transfer files between source and destination
					@MoveEverything = Sanity check to ensure that all databases (minus system) are to be moved
					@Debug = Show the work to be done, but don't execute
**  Useage:			EXEC [dbo].[MoveDatabases]
						@DBName = 'test,mydb'
						,@DestDataPath = 'c:\test\data'
						,@DestLogPath = 'c:\test\logs'
						,@Debug = 1
**  Notes:			You can not move system, non online/offline status, snapshots, distribution, published, or mirrored databases
					If you want to move one of the above: http://msdn.microsoft.com/en-us/library/ms188031.aspx
**	Date:			2008.06.03
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090707	Adam Bean		Started over
**	20090929	Adam Bean		Global header formatting & general clean up
**  20100215	Adam Bean		Fixed SYSNAME collation incompatibility
**	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
*******************************************************************************/

CREATE PROCEDURE [dbo].[MoveDatabase] 
(
	@DBName				VARCHAR(512)	= NULL		-- One, many or NULL for all
	,@DBNameExclude		VARCHAR(512)	= NULL		-- Leave @DBName NULL and exclude one or many here
	,@DestDataPath		VARCHAR(256)
	,@DestLogPath		VARCHAR(256)
	,@KillConnections	TINYINT			= 1			-- Don't worry, won't kill if in in debug
	,@NewDBOwner		sysname			= NULL
	,@UpdateStats		TINYINT			= 0			
	,@FileTransfer		CHAR(4)			= 'MOVE'	-- Move or copy
	,@MoveEverything	BIT				= 0			-- You really want to move all databases?
	,@Debug				TINYINT			= 0			
)

AS

SET NOCOUNT ON

DECLARE 
	@Access				VARCHAR(128)
	,@Attach			VARCHAR(8000)
	,@AttachFailure		VARCHAR(8000)
	,@AttachCmd			VARCHAR(8000)
	,@AttachCmdFailure	VARCHAR(8000)
	,@ChangeOwner		VARCHAR(128)
	,@Comma				CHAR(3)
	,@Copy				VARCHAR(8000)
	,@CopyData			VARCHAR(8000)
	,@CopyLog			VARCHAR(8000)
	,@CopyDataCmd		VARCHAR(8000)
	,@CopyLogCmd		VARCHAR(8000)
	,@DBOwner			sysname
	,@Detach			VARCHAR(8000)
	,@FileID			TINYINT
	,@FileType			VARCHAR(8)
	,@FileName			VARCHAR(256)
	,@Kill				VARCHAR(256)
	,@PhysicalName		VARCHAR(256)
	,@SkipChecks		VARCHAR(5)
	,@Space				CHAR(2)

-- Populate some formatting variables
SET @Space = CHAR(32)
SET @Comma = ','

-- Sanity check ... If @DBName and @DBNameExclude are left NULL, all databases will be moved 
-- More than likely that's not what was intended, but just in case, the user has to pass in @MoveEverything = 1
IF 
	(
		@DBName IS NULL 
		AND @DBNameExclude IS NULL
		AND @Debug = 0
		AND @MoveEverything = 0
	)
BEGIN
	PRINT 'Pass in at least one value for any of the main parameters: @DBName or @DBNameExclude'
	PRINT 'If you REALLY want to move all user databases, than rerun with @MoveEverything = 1'
	RETURN 2
END

-- Verify new owner is valid
IF @NewDBOwner IS NOT NULL
BEGIN
	IF NOT EXISTS(SELECT [name] FROM [sys].[server_principals] WHERE [name] = @NewDBOwner)
	BEGIN
		PRINT 'Specified @NewDBOwner is not a valid login, procedure is aborting.'
		RETURN 2
	END
END

-- Setup table to hold file locations
DECLARE	@Files TABLE 
(
	[DBName]			NVARCHAR(128)
	,[FileID]			SMALLINT
	,[FileName]			NVARCHAR(128)
	,[Type]				NVARCHAR(60)
	,[PhysicalName]		VARCHAR(260)
) 
	
-- Populate file locations prior to moving
INSERT INTO @Files
SELECT 
	sm.[database_name] 
	,sm.[file_id] 
	,sm.[name] 
	,sm.[type_desc] 
	,sm.[physical_name] 
FROM [dbo].[sysmasterfiles_vw] sm 
JOIN [dbo].[sysdatabases_vw] sd
	ON sm.[database_id] = sd.[database_id] 
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON sm.[database_name] = d.[item] COLLATE DATABASE_DEFAULT 
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON sm.[database_name] = de.[item] COLLATE DATABASE_DEFAULT 
WHERE sm.[database_id] > 4					-- Exclude system databases
AND sm.[database_name] != DB_NAME()			-- Exclude database running procedure
AND sm.[state_desc] IN ('ONLINE','OFFLINE')	-- Exclude databases in non moveable states
AND sd.[source_database_id] IS NULL			-- Exclude snapshots
AND sd.[is_distributor] = 0					-- Exclude distribution databases
AND sd.[is_published] = 0					-- Exclude published databases
AND sd.[is_merge_published] = 0				-- Exclude published databases
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL						-- All but excluded databases

-- Setup table to hold the commands
DECLARE	@Commands TABLE 
(
	[DBName]					sysname
	,[Owner]					sysname
	,[AttachCommand]			VARCHAR(1536)
	,[AttachCommandFailure]		VARCHAR(1536)
	,[CopyDataCommand]			VARCHAR(1536)
	,[CopyLogCommand]			VARCHAR(1536)
) 

-- Populate the commands table with the database names
INSERT INTO @Commands
([DBName],[Owner])
SELECT DISTINCT
	sd.[name]
	,ISNULL(@DBOwner, (SELECT SUSER_SNAME([owner_sid]) FROM [dbo].[sysdatabases_vw] WHERE [name] = [DBName]))
FROM [dbo].[sysdatabases_vw] sd
JOIN @Files f
	ON sd.[name] = f.[DBName]

-- Return current file locations and connections to kill if in debug
IF @Debug = 1
BEGIN
	SELECT 'View the messages tab for detach/copy/attach/commands' AS [MoreDetail]
	SELECT * FROM @Files
	EXEC [dbo].[KillProcess] @DBName = @DBName, @Debug = 1
END
	
-- Append a trailing backlash to destination directories if not present
IF RIGHT(@DestDataPath,1) != '\'
	SET @DestDataPath = @DestDataPath + '\'
IF RIGHT(@DestLogPath,1) != '\'
	SET @DestLogPath = @DestLogPath + '\'

-- Cursor through all database files and build the attach commands
DECLARE #files CURSOR LOCAL STATIC FOR
SELECT 
	[DBName]
	,[FileID]
	,[FileID]
	,[Type]
	,[PhysicalName]
FROM @Files

OPEN #files
FETCH NEXT FROM #files INTO @DBName, @FileID, @FileName, @FileType, @PhysicalName
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Build copy commands
	IF @FileType = 'ROWS'
	BEGIN
		UPDATE @Commands
		SET [CopyDataCommand] = ISNULL([CopyDataCommand],'') + '"' + @PhysicalName + '"' + @Space
		WHERE [DBName] = @DBName
	END
	ELSE
	BEGIN
		UPDATE @Commands
		SET [CopyLogCommand] = ISNULL([CopyLogCommand],'') + '"' + @PhysicalName + '"' + @Space
		WHERE [DBName] = @DBName
	END
	
	-- Build attach commands
	IF @FileType = 'ROWS' -- data files
	BEGIN
		UPDATE @Commands
		SET [AttachCommand] = ISNULL([AttachCommand],'') + '@filename' + CAST(@FileID AS CHAR(2)) + '= ''' + @DestDataPath + SUBSTRING(@PhysicalName,LEN(@PhysicalName)-CHARINDEX('\',REVERSE(@PhysicalName))+2,100) + '''' + @Comma 
		WHERE [DBName] = @DBName

		UPDATE @Commands
		SET [AttachCommandFailure] = ISNULL([AttachCommandFailure],'') + '@filename' + CAST(@FileID AS CHAR(2)) + '= ''' + @PhysicalName + '''' + @Comma 
		WHERE [DBName] = @DBName
	END
	ELSE
	BEGIN -- log files
		UPDATE @Commands
		SET [AttachCommand] = ISNULL([AttachCommand],'') + '@filename' + CAST(@FileID AS CHAR(2)) + '= ''' + @DestLogPath + SUBSTRING(@PhysicalName,LEN(@PhysicalName)-CHARINDEX('\',REVERSE(@PhysicalName))+2,100) + '''' + @Comma 
		WHERE [DBName] = @DBName

		UPDATE @Commands
		SET [AttachCommandFailure] = ISNULL([AttachCommandFailure],'') + '@filename' + CAST(@FileID AS CHAR(2)) + '= ''' + @PhysicalName + '''' + @Comma 
		WHERE [DBName] = @DBName
	END

FETCH NEXT FROM #files INTO @DBName, @FileID, @FileName, @FileType, @PhysicalName
END
CLOSE #files
DEALLOCATE #files

-- Replace trailing comma with a semi-colon
UPDATE @Commands
SET 
	[AttachCommand] = LEFT([AttachCommand], LEN([AttachCommand])-1) + ';'
	,[AttachCommandFailure] = LEFT([AttachCommandFailure], LEN([AttachCommandFailure])-1) + ';'

-- Build the commands and execute
DECLARE #commands CURSOR LOCAL STATIC FOR
SELECT DISTINCT
	[DBName]
	,[Owner]
	,[AttachCommand]
	,[AttachCommandFailure]
	,[CopyDataCommand]
	,[CopyLogCommand]
FROM @Commands

OPEN #commands
FETCH NEXT FROM #commands INTO @DBName, @DBOwner, @AttachCmd, @AttachCmdFailure, @CopyDataCmd, @CopyLogCmd
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Build kill command
	SET @Kill = 'EXEC [dbo].[KillProcess] @DBName = ''' + @DBName + ''', @SetSingleUser = 1, @ShowProcesses = 0'
	-- Build detach command
	IF @UpdateStats = 1
		SET @SkipChecks = 'false'
	ELSE
		SET @SkipChecks = 'true'
	SET @Detach = 'EXEC sp_detach_db @DBName = ''' + @DBName + ''', @skipchecks = ''' + @SkipChecks + ''''
	-- Build copy command
	SET @Copy = 'EXEC [master].[dbo].[xp_cmdshell] '''
	-- Build attach command
	SET @Attach = 'EXEC sp_attach_db ''' + @DBName + ''', '

	-- Put it all together
	SET @CopyData = @Copy + 'FOR %F IN (' + @CopyDataCmd + ') DO ' + @FileTransfer + ' %F "' + @DestDataPath + '"'''
	SET @CopyLog = @Copy + 'FOR %F IN (' + @CopyLogCmd + ') DO ' + @FileTransfer + ' %F "' + @DestLogPath + '"'''
	SET @AttachFailure = @Attach + @AttachCmdFailure
	SET @Attach = @Attach + @AttachCmd
	SET @ChangeOwner = 'USE [' + @DBName + '] EXEC sp_changedbowner ''' + ISNULL(@NewDBOwner, @DBOwner) + ''''
	SET @Access = 'ALTER DATABASE [' + @DBName + ']  SET MULTI_USER WITH NO_WAIT'
	
	-- Print it out
	PRINT '/***Move database commands for database: ' + @DBName + '***/'
	PRINT ''
	PRINT '/*Kill Commands*/'
	PRINT @Kill
	PRINT ''
	PRINT '/*Detach Commands*/'
	PRINT @Detach
	PRINT ''
	PRINT '/*Copy Commands*/'
	PRINT @CopyData
	PRINT @CopyLog
	PRINT ''
	PRINT '/*Attach Commands*/'
	PRINT @Attach
	PRINT ''
	PRINT '/*Attach Failure Commands (if copy failed)*/'
	PRINT @AttachFailure
	PRINT ''
	PRINT '/*Change Owner Commands*/'
	PRINT @ChangeOwner
	PRINT ''
	PRINT '/*Database Access Commands*/'
	PRINT @Access
	PRINT ''

	-- Do the dirty work
	IF @Debug = 0
	BEGIN
		-- Kill connections to specified database(s)
		IF @KillConnections = 1
			EXEC (@Kill)
						
		-- Check for active connections
		IF	(
				SELECT COUNT(*) 
				FROM [master].[dbo].[sysprocesses] s WITH (NOLOCK)
				LEFT OUTER JOIN [dbo].[ActiveDBLocks_vw] l WITH (NOLOCK)
					ON s.[dbid] = l.[dbid]
					AND s.[spid] = l.[spid]
				WHERE DB_NAME(s.[dbid]) = @DBName	
			) > 0
		BEGIN
			PRINT 'There are active connections on the specified database, unable to detach. Rerun with @KillConnections = 1.'
			RETURN 2
		END

		-- Detach, copy, attach, change owner and change access
		EXEC (@Detach)
		EXEC (@CopyData)
		EXEC (@CopyLog)
		EXEC (@Attach)
		EXEC (@ChangeOwner)
		EXEC (@Access)	
			IF @@ERROR != 0 -- If copy fails, attempt to reattach at original location
				EXEC (@AttachFailure)		

		-- Show before and after locations
		SELECT 
			f.[DBName]
			,f.[FileID]
			,f.[FileName]
			,f.[Type]
			,f.[PhysicalName]	AS [OldLocation]
			,sm.[physical_name]	AS [NewLocation]
		FROM @Files f
		JOIN [dbo].[sysmasterfiles_vw] sm
			ON f.[DBName] = sm.[database_name] COLLATE DATABASE_DEFAULT
			AND f.[FileID] = sm.[file_id]
		WHERE [DBName] = @DBName
	END

FETCH NEXT FROM #commands INTO @DBName, @DBOwner, @AttachCmd, @AttachCmdFailure, @CopyDataCmd, @CopyLogCmd
END
CLOSE #commands
DEALLOCATE #commands

SET NOCOUNT OFF

-- Return status
IF @@ERROR <> 0
	RETURN 2
ELSE
	RETURN 0