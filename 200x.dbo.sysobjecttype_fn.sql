IF OBJECT_ID('[dbo].[sysobjecttype_fn]','FN') IS NOT NULL 
	DROP FUNCTION [dbo].[sysobjecttype_fn]
GO

/*******************************************************************************************************
**	Name:			dbo.sysobjecttype_fn
**	Desc:			Function to return type of sysobject
**	Auth:			Adam Bean (SQLSlayer.com)
**  Parameters:		@type = 2 letter type from sysobjects
**  Usage:			SELECT dbo.sysobjecttype_fn([type]), * FROM sys.objects
**	Date:			2008.12.12
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090617	Adam Bean		ELSE is now @type instead of 'other'
								Added support for type K and reorganized types
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE FUNCTION [dbo].[sysobjecttype_fn]
(
	@type	CHAR(2)
)
RETURNS NVARCHAR (64)

AS

BEGIN

	DECLARE 
		@ObjType NVARCHAR(64)

	SELECT 
		@ObjType = CASE
			WHEN @type = 'AF'	THEN 'Aggregate function (CLR)'
			WHEN @type = 'C'	THEN 'CHECK constraint'
			WHEN @type = 'D'	THEN 'DEFAULT (constraint or stand-alone)'
			WHEN @type = 'F'	THEN 'FOREIGN KEY constraint'
			WHEN @type = 'FN'	THEN 'SQL scalar function'
			WHEN @type = 'FS'	THEN 'Assembly (CLR) scalar function'
			WHEN @type = 'FT'	THEN 'Assembly (CLR) table-valued function'
			WHEN @type = 'IF'	THEN 'SQL inline table-valued function'
			WHEN @type = 'IT'	THEN 'Internal table'
			WHEN @type = 'K'	THEN 'PRIMARY KEY or UNIQUE constraint'
			WHEN @type = 'L'	THEN 'Log'
			WHEN @type = 'P'	THEN 'SQL stored procedure'
			WHEN @type = 'PC'	THEN 'Assembly (CLR) stored procedure'
			WHEN @type = 'PK'	THEN 'PRIMARY KEY constraint'
			WHEN @type = 'R'	THEN 'Rule (old-style, stand-alone)'
			WHEN @type = 'RF'	THEN 'Replication-filter-procedure'
			WHEN @type = 'S'	THEN 'System base table'
			WHEN @type = 'SN'	THEN 'Synonym'
			WHEN @type = 'SQ'	THEN 'Service queue'
			WHEN @type = 'TA'	THEN 'Assembly (CLR) DML trigger'
			WHEN @type = 'TF'	THEN 'SQL table-valued-function'
			WHEN @type = 'TR'	THEN 'SQL DML trigger'
			WHEN @type = 'U'	THEN 'Table (user-defined)'
			WHEN @type = 'UQ'	THEN 'UNIQUE constraint'
			WHEN @type = 'V'	THEN 'View'
			WHEN @type = 'X'	THEN 'Extended stored procedure'
			ELSE @type
		END
		
	RETURN @ObjType
END