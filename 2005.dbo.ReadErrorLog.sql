IF OBJECT_ID('dbo.ReadErrorLog','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[ReadErrorLog]
GO

/******************************************************************************
* Name
	[dbo].[ReadErrorLog]

* Author
	Adam Bean
	
* Date
	2010.11.14
	
* Synopsis
	Procedure to query the error log
	
* Description
	Enhanced view of the error log based on parameters to filter on which error logs,
	specific data, or even the amount of records. Also has the ability to find when
	SQL restarted and the duration in which the instance was down.

* Examples
	EXEC [dbo].[ReadErrorLog] @Search = 'I/O requests taking longer than'
	EXEC [dbo].[ReadErrorLog] @FindRestarts = 1

* Dependencies
	If available.  List one per line.

* Parameters
	@Search				- Query string to search for
	@Search2			- Additional query string to search for
	@Order				- 'A' or 'ASC' for ascending, 'D' or 'DESC' for descending
	@RowCount			- Amount of rows to be returned
	@LogCount			- Amount of error logs to query, NULL for all
	@LogNumber			- Specific error log to query
	@DateStart			- Start time to query from
	@DateEnd			- End time to query from
	@FindRestarts		- Find when SQL was restarted and the duration in which it was down
	
* Notes
	The last restart will almost always have a NULL [OutageInSeconds]. This happens because
	the oldest restart doesn't have a previous error log to compare it's time to. The only way
	this wouldn't be NULL is if the previous error log was a reinitialized log.

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
**  20101119	Adam Bean		Changed name of cursor, fixed collation issue
**	20110504	Adam Bean		Added @FindRestarts, updated header
**	20130910	Adam Bean		Changed from xp_readerrorlog to sp_readerrolog to support new change in 2012
									Added zero padding to the archive # for proper ordering
******************************************************************************/

CREATE PROCEDURE [dbo].[ReadErrorLog]
(
	@Type				INT				= 1			-- 1 for engine, 2 for agent
	,@Search			VARCHAR(256)	= NULL		-- Query string to search for
	,@Search2			VARCHAR(256)	= NULL		-- Additional query string to search for	
	,@Order				CHAR(4)			= 'ASC'		-- 'A' or 'ASC' for ascending, 'D' or 'DESC' for descending
	,@RowCount			INT				= NULL		-- Amount of rows to be returned
	,@LogCount			INT				= NULL		-- Amount of error logs to query, NULL for all
	,@LogNumber			INT				= NULL		-- Specific error log to query
	,@DateStart			DATETIME		= NULL		-- Start time to query from
	,@DateEnd			DATETIME		= NULL		-- End time to query from
	,@FindRestarts		BIT				= 0			-- Find all restart occurences and the downtime
)

AS

SET NOCOUNT ON

DECLARE
	@ArchiveCount		INT
	,@ErrorLogRowCount	BIGINT
	,@ArchiveNumber		INT

-- If @DateStart was passed and @DateEnd was not, set it to now
IF (@DateStart IS NOT NULL AND @DateEnd IS NULL)
	SET @DateEnd = GETDATE()

-- The default running log has an archive number of 0
-- If @LogCount is passed, subtract 1 to properly match the numbering of the logs
IF @LogCount IS NOT NULL
	SET @LogCount = @LogCount - 1

-- If searching for restarts, set @Search
IF @FindRestarts = 1
BEGIN
	SET @Type = 1
	SET @Search = 'Starting up database ''master'''
	SET @Search2 = NULL
END

-- Make sure all working tables do not exist
IF OBJECT_ID('tempdb.dbo.#ErrorLogCount') IS NOT NULL
	DROP TABLE #ErrorLogCount
IF OBJECT_ID('tempdb.dbo.#ErrorLog') IS NOT NULL
	DROP TABLE #ErrorLog

-- Setup working tables
-- Setup table to hold error log count
CREATE TABLE #ErrorLogCount
(
	[ArchiveNo]			VARCHAR(2)
	,[Date]				DATETIME
	,[Size]				INT
)

-- Setup table to hold error log contents
CREATE TABLE #ErrorLog
(
	[id]				INT IDENTITY(1,1)
	,[ArchiveNo]		VARCHAR(2)
	,[LogDate]			DATETIME
	,[ProcessInfo]		VARCHAR(24)
	,[Text]				VARCHAR(MAX)
)

-- Retrieve the amount of error logs
INSERT INTO #ErrorLogCount
	EXEC [master].[dbo].[xp_enumerrorlogs] @Type

-- If a specific archive was requested, remove all others
IF @LogNumber IS NOT NULL
	DELETE FROM #ErrorLogCount WHERE [ArchiveNo] != @LogNumber
	
-- Store error log count
SET @ArchiveCount = (SELECT MAX(CAST([ArchiveNo] AS INT)) FROM #ErrorLogCount)

-- Pad the ArchiveNo with a leading zero
UPDATE #ErrorLogCount SET [ArchiveNo] = RIGHT(REPLICATE('0',2) + [ArchiveNo],2)

-- Cursor through error logs and retrieve data
DECLARE #ReadErrorLog CURSOR LOCAL STATIC FOR
SELECT 
	[ArchiveNo]
FROM #ErrorLogCount
WHERE ISNULL(@LogCount,@ArchiveCount) >= [ArchiveNo]

OPEN #ReadErrorLog
FETCH NEXT FROM #ReadErrorLog INTO @ArchiveNumber
WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO #ErrorLog
	([LogDate], [ProcessInfo], [Text])
	EXEC [master].[dbo].[sp_readerrorlog] @ArchiveNumber, @Type, @Search, @Search2
	
	UPDATE #ErrorLog SET [ArchiveNo] = @ArchiveNumber WHERE [ArchiveNo] IS NULL

FETCH NEXT FROM #ReadErrorLog INTO @ArchiveNumber
END
CLOSE #ReadErrorLog
DEALLOCATE #ReadErrorLog

-- Pad the ArchiveNo with a leading zero
UPDATE #ErrorLog SET [ArchiveNo] = RIGHT(REPLICATE('0',2) + [ArchiveNo],2)

-- Retrieve the overall amount of rows 
SET @ErrorLogRowCount = (SELECT COUNT(*) FROM #ErrorLog)

-- Retrieve the data set
IF @FindRestarts = 0
BEGIN
	SELECT 
		[ArchiveNo]
		,[LogDate]
		,[ProcessInfo]
		,[Text]
	FROM #ErrorLog
	WHERE (@DateStart IS NULL AND @DateEnd IS NULL OR [LogDate] BETWEEN @DateStart AND @DateEnd)
	AND ISNULL(@RowCount,@ErrorLogRowCount) >= [id]
	ORDER BY 
		CASE             
			WHEN @Order IN ('A','ASC') THEN [id]
		END ASC
		,CASE 
			WHEN @Order IN ('D', 'DESC') THEN [id]
		END DESC 
END
ELSE
BEGIN -- Check the notes to better understand the logic here
	SELECT 
		el.[ArchiveNo]	AS [ArchiveNo]
		,el.[LogDate]	AS [TimeOfRestart]
		,CASE
			WHEN DATEDIFF(SECOND,elc.[Date],el.[LogDate]) < 0 THEN DATEDIFF(SECOND,elc.[Date],DATEADD(HOUR,1,el.[LogDate]))
			-- Checking for < than 0 is to support DST and a bug with how SQL stores the error logs
			ELSE DATEDIFF(SECOND,elc.[Date],el.[LogDate])
		END				AS [OutageInSeconds]
	FROM #ErrorLog el
	-- The last restart will never have a cooresponding previous error log to determine outage; thus the + 1. 
	-- The last restart will almost always have a NULL [OutageInSeconds]
	LEFT JOIN #ErrorLogCount elc 
		ON el.[ArchiveNo] + 1 = elc.[ArchiveNo]
		AND elc.ArchiveNo != 0
	ORDER BY 1
END
SET NOCOUNT OFF