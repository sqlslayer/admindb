IF OBJECT_ID('[dbo].[TableSizes]','P') IS NOT NULL 
       DROP PROCEDURE [dbo].[TableSizes]
GO

/******************************************************************************
* Name
	[dbo].[TableSizes]

* Author
	Jason Weitzel
	
* Date
	2011.06.02
	
* Synopsis
	Retrieve table sizes for any supplied database
	
* Description
	Provid @DBName (database name) and a @OrderBy to view a databases tables and
	their associated row numbers, reserved size, physical data size, index size,
	and unused table space.

* Examples
	EXEC [dbo].[TableSizes] 'admin', 'DatabaseName'

* Dependencies
	None

* Parameters
	@DBName				- Database name of table sizes you would like
	@OrderBy			- Column to order by.  Can be comma separated.
	
* Notes
	If required

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20110603	Matt Stanford	Now works for all databases by default.  Can also enter comma separated list.
	20110705	Adam Bean		Fixed so procedure works with pre SP4 SQL2000
	20161228	S. Peng Chan	Order by Ordinal number is deprecated.  Changed to column name. Options are: 
								[DatabaseName],[SchemaName],[TableName],[NumberOfRows],[ReservedSizeKB],[DataSizeKB],[IndexSizeKB],[UnusedSizeKB]
******************************************************************************/

CREATE PROCEDURE [dbo].[TableSizes]
(
	 @DBName		NVARCHAR(4000) = NULL
	,@OrderBy		NVARCHAR(1000) = '[DatabaseName],[SchemaName],[TableName]'  --	[DatabaseName],[SchemaName],[TableName],[NumberOfRows],[ReservedSizeKB],[DataSizeKB],[IndexSizeKB],[UnusedSizeKB]
)

AS

SET NOCOUNT ON

/*-------------------------------------------------------------------------
--Collect your tables
-------------------------------------------------------------------------*/
DECLARE 
	@TableName		NVARCHAR(128)
	,@SchemaName	NVARCHAR(128)
	,@LoopSQL		NVARCHAR(1000)
	
IF OBJECT_ID('tempdb.dbo.#RawTableSizes') IS NOT NULL
   DROP TABLE #RawTableSizes	
IF OBJECT_ID('tempdb.dbo.#FullTableSizes') IS NOT NULL
   DROP TABLE #FullTableSizes
IF OBJECT_ID('tempdb.dbo.#systables') IS NOT NULL
   DROP TABLE #systables
   
CREATE TABLE #RawTableSizes
(
	TableName		NVARCHAR(128)
	,NumberofRows	INT
	,ReservedSize	VARCHAR(100)
	,DataSize		VARCHAR(100)
	,IndexSize		VARCHAR(100)
	,UnusedSize		VARCHAR(100)
)

CREATE TABLE #FullTableSizes
(
	DatabaseName	NVARCHAR(128)
	,SchemaName		NVARCHAR(128)
	,TableName		NVARCHAR(128)
	,NumberofRows	INT
	,ReservedSize	VARCHAR(100)
	,DataSize		VARCHAR(100)
	,IndexSize		VARCHAR(100)
	,UnusedSize		VARCHAR(100)
	,ReservedSizeKB AS CAST(REPLACE(ReservedSize,' KB','') AS DECIMAL(15,0))
	,DataSizeKB		AS CAST(REPLACE(DataSize,' KB','') AS DECIMAL(15,0))
	,IndexSizeKB	AS CAST(REPLACE(IndexSize,' KB','') AS DECIMAL(15,0))
	,UnusedSizeKB	AS CAST(REPLACE(UnusedSize,' KB','') AS DECIMAL(15,0))
)
   
CREATE TABLE #systables (
	[DBName]		NVARCHAR(128)
	,[SchemaName]	NVARCHAR(128)
	,[TableName]	NVARCHAR(128)
)
   
-- Populate the tables
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [dbo].[sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 
		
		-- sys.objects
		INSERT INTO #systables
		SELECT ''' + @DBName + ''', s.name, t.name
		FROM [sys].[tables] t
		INNER JOIN [sys].[schemas] s
			ON t.[schema_id] = s.[schema_id]
	')
	
	FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs

/*-------------------------------------------------------------------------
--Loop and gather the table size info
-------------------------------------------------------------------------*/
DECLARE #c CURSOR LOCAL STATIC FOR
SELECT
	[DBName]
	,[SchemaName]
	,[TableName]
FROM #systables

OPEN #c
FETCH NEXT FROM #c INTO @DBName, @SchemaName, @TableName

WHILE @@FETCH_STATUS = 0
BEGIN

	SET @LoopSQL = 'USE [' + @DBName + ']; EXEC sp_spaceused ''' + @SchemaName + '.' + @TableName + ''''

	--PRINT @LoopSQL
	INSERT INTO #RawTableSizes
	EXEC (@LoopSQL)
	
	-- Add it to our final table
	INSERT INTO #FullTableSizes (DatabaseName,SchemaName,TableName,NumberofRows,ReservedSize,DataSize,IndexSize,UnusedSize)
	SELECT @DBName, @SchemaName, TableName,NumberofRows,ReservedSize,DataSize,IndexSize,UnusedSize
	FROM #RawTableSizes
	
	-- Remove this record from the Raw table
	DELETE FROM #RawTableSizes
	
	FETCH NEXT FROM #c INTO @DBName, @SchemaName, @TableName
	
END
CLOSE #c
DEALLOCATE #c

-- Return your data
SET @LoopSQL = '
SELECT 
	[DatabaseName]
	,[SchemaName]
	,[TableName]
	,[NumberOfRows]
	,[ReservedSizeKB]
	,[DataSizeKB]
	,[IndexSizeKB]
	,[UnusedSizeKB]
FROM #FullTableSizes
ORDER BY ' + @OrderBy

EXEC(@LoopSQL)


SET NOCOUNT OFF