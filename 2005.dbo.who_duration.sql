IF OBJECT_ID('[dbo].[who_duration]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[who_duration]
GO

/******************************************************************************
* Name
	[dbo].[who_duration]

* Author
	Adam Bean
	
* Date
	2008.06.17
	
* Synopsis
	Procedure to view resource utilization over a period of time
	
* Description
	Captures data from who_vw at start of procedure, waits for a specified duration and captures again
	CPU/Reads/Writes aggregated during this period of time are displayed

* Examples
	EXEC dbo.[who_duration] @SecondsToSample = 60

* Dependencies
	dbo.[who_vw]

* Parameters
	@SecondsToSample = Delay in seconds of time to sample
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20090929	Adam Bean		Global header formatting & general clean up
	20131018	Adam Bean		Fixed overflow error & new header
******************************************************************************/

CREATE PROCEDURE [dbo].[who_duration]
(
	@SecondsToSample	INT = 30
)

AS

SET NOCOUNT ON

DECLARE
	@EndTime			DATETIME

SET @EndTime = DATEADD(s,@SecondsToSample,GETDATE())

DECLARE @who TABLE
(
	[SPID]			SMALLINT
	,[DBName]		SYSNAME
	,[Login]		VARCHAR(128)
	,[HostName]		VARCHAR(128)
	,[Query]		VARCHAR(MAX)
	,[CPU]			INT
	,[CPU_Post]		INT NULL
	,[Reads]		INT
	,[Reads_Post]	INT NULL
	,[Writes]		INT
	,[Writes_Post]	INT NULL
)

INSERT INTO @who
([SPID],[DBName],[Login],[HostName],[Query],[CPU],[Reads],[Writes])
SELECT
	[SPID]
	,[DBName]
	,[Login]
	,[HostName]
	,[Query]
	,[CPU]
	,[Reads]
	,[Writes]
FROM dbo.[who_vw]

WAITFOR TIME @EndTime

UPDATE @who
SET
	[CPU_Post]		= t.[CPU]
	,[Reads_Post]	= t.[Reads]
	,[Writes_Post]	= t.[Writes]
FROM
(
	SELECT
		[SPID]
		,[DBName]
		,[CPU]
		,[Reads]
		,[Writes]
	FROM dbo.[who_vw]
) t
INNER JOIN @who w
	ON w.[SPID] = t.[SPID]
	AND w.[DBName] = t.[DBName]

SELECT * FROM 
(
	SELECT
		[SPID]
		,[DBName]
		,[Login]
		,[HostName]
		,[Query]
		,[CPU_Post] - [CPU]			AS 'CPUTotal'
		,[Reads_Post] - [Reads]		AS 'ReadsTotal'
		,[Writes_Post] - [Writes]	AS 'WritesTotal'
	FROM @who
) t
WHERE t.[CPUTotal] > 0
OR t.[ReadsTotal] > 0
OR t.[WritesTotal] > 0
ORDER BY [CPUTotal] DESC