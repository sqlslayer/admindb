IF OBJECT_ID('[dbo].[sysserver_principals_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[sysserver_principals_vw]
GO
/*******************************************************************************************************
**	Name:			dbo.sysserver_principals_vw
**	Desc:			View to give you a common 2005 view of sys.server_principals
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2010.03.10
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/

CREATE VIEW [dbo].[sysserver_principals_vw]
AS
SELECT 
	[name]
	,NULL as [principal_id]
	,[sid]
	,CASE 
		WHEN [isntname] = 1 AND [isntuser] = 1 THEN 'U'
		WHEN [isntname] = 1 AND [isntgroup] = 1 THEN 'G'
		WHEN [isntname] = 0 THEN 'S'
		ELSE NULL 
	END AS [type]
	,CASE 
		WHEN [isntname] = 1 AND [isntuser] = 1 THEN 'WINDOWS_LOGIN'
		WHEN [isntname] = 1 AND [isntgroup] = 1 THEN 'WINDOWS_GROUP'
		WHEN [isntname] = 0 THEN 'SQL_LOGIN'
		ELSE NULL 
	END AS [type_desc]
	,0 AS [is_disabled]
	,[createdate] AS [create_date]
	,[updatedate] AS [modify_date]
	,[dbname] AS [default_database_name]
	,[language] AS [default_language_name]
	,NULL AS [credential_id]
FROM [master].[dbo].[syslogins]

UNION ALL
-- Need to manually add all of the server roles
SELECT 'public', NULL, 0x02, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL
UNION ALL
SELECT 'sysadmin', NULL, 0x03, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL
UNION ALL
SELECT 'securityadmin', NULL, 0x04, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL
UNION ALL
SELECT 'serveradmin', NULL, 0x05, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL
UNION ALL
SELECT 'setupadmin', NULL, 0x06, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL
UNION ALL
SELECT 'processadmin', NULL, 0x07, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL
UNION ALL
SELECT 'diskadmin', NULL, 0x08, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL
UNION ALL
SELECT 'dbcreator', NULL, 0x09, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL
UNION ALL
SELECT 'bulkadmin', NULL, 0x0A, 'R', 'SERVER_ROLE', 0, '2000-01-01', '2000-01-01', NULL, NULL, NULL