IF OBJECT_ID('[dbo].[sysconfigurations_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[sysconfigurations_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.sysconfigurations_vw
**	Desc:			Compatibility view for consistency between 2000, 2005 and 2008
**					Used like sys.configurations in '05 and 08
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2009.07.22
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE VIEW [dbo].[sysconfigurations_vw]

AS

SELECT
	CAST(c.[config] AS INT)					AS [configuration_id]
	,CAST(sv.[name] AS NVARCHAR(35))		AS [name]
	,CAST(c.[value] AS SQL_VARIANT)			AS [value]
	,CAST(sv.[low] AS SQL_VARIANT)			AS [minimum]
	,CAST(sv.[high] AS SQL_VARIANT)			AS [maximum]
	,CAST(cc.[value] AS SQL_VARIANT)		AS [value_in_use]
	,CAST(c.[comment] AS NVARCHAR(255))		AS [description]
	,CAST(CASE c.[status]
		WHEN 0 THEN 0
		WHEN 1 THEN 1
		WHEN 2 THEN 0
		WHEN 3 THEN 1
		ELSE 0
	END AS BIT)								AS [is_dynamic]
	,CAST(CASE c.[status]
		WHEN 0 THEN 0
		WHEN 1 THEN 0
		WHEN 2 THEN 1
		WHEN 3 THEN 1
		ELSE 0
	END AS BIT)								AS [is_advanced]
FROM [master].[dbo].[sysconfigures] c
INNER JOIN [master].[dbo].[syscurconfigs] cc
	ON c.[config] = cc.[config]
INNER JOIN [master].[dbo].[spt_values] sv
	ON c.[config] = sv.[number]
WHERE sv.[type] = 'C'