IF OBJECT_ID('[dbo].[sysmasterfiles_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[sysmasterfiles_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.sysmasterfiles_vw
**	Desc:			Standard view (similar to sys.master_files) for all versions of SQL
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2009.06.09
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090730	Adam Bean		Added LTRIM(RTRIM to physical name
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE VIEW [dbo].[sysmasterfiles_vw]

AS

SELECT
	DB_NAME(f.[database_id])						AS [database_name]
	,f.[database_id]								AS [database_id]
	,f.[file_id]									AS [file_id]
	,f.[file_guid]									AS [file_guid]
	,f.[type]										AS [type]
	,f.[type_desc]									AS [type_desc]
	,f.[data_space_id]								AS [data_space_id]
	,f.[name]										AS [name]
	,f.[physical_name]								AS [physical_name]
	,f.[state]										AS [state]
	,f.[state_desc]									AS [state_desc]
	,f.[size]										AS [size]
	,CAST(f.[size] * 8 / 1024. AS DECIMAL(12,2))	AS [size_MB]
	,f.[max_size]									AS [max_size]
	,CAST(CASE 
		WHEN f.[is_percent_growth] = 0 
			THEN f.[growth] * 8 / 1024.
		ELSE f.[size] * 8 / 1024. * f.[growth] / 100
 	END	AS DECIMAL(12,2))							AS [next_growth_MB]
	,f.[growth]										AS [growth]
	,f.[is_media_read_only]							AS [is_media_read_only]
	,f.[is_read_only]								AS [is_read_only]
	,f.[is_sparse]									AS [is_sparse]
	,f.[is_percent_growth]							AS [is_percent_growth]
	,f.[is_name_reserved]							AS [is_name_reserved]
FROM [master].[sys].[master_files] f
	