IF OBJECT_ID('[dbo].[SQLServerVersion_fn]','FN') IS NOT NULL
	DROP FUNCTION [dbo].[SQLServerVersion_fn]
GO

/*******************************************************************************************************
**	Name:			[dbo].[SQLServerVersion_fn]()
**	Desc:			Returns SQL Server Version (8,9,10, etc.)
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2008.03.14
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090317	Matt Stanford	Now returns as a TINYINT, not string
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE FUNCTION [dbo].[SQLServerVersion_fn]()
RETURNS TINYINT

AS

BEGIN
	DECLARE
		@SQLVersion	[TINYINT]
		,@ProductVersion VARCHAR(16)
		
	SET @ProductVersion = CAST(SERVERPROPERTY('ProductVersion') AS VARCHAR(16))
	
	SET @SQLVersion = CAST(LEFT(@ProductVersion, CHARINDEX('.',@ProductVersion) - 1) AS TINYINT)
		
	RETURN @SQLVersion
END