IF OBJECT_ID('dbo.RestoreHistory_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[RestoreHistory_vw]
GO

/******************************************************************************
* Name
	[dbo].[RestoreHistory_vw]

* Author
	Jeff Gogel
	
* Date
	2013.10.16
	
* Synopsis
	Overview of restores and where their backups came from.
	
* Description
	Overview of restores and where their backups came from.

* Examples
	SELECT * FROM [dbo].[RestoreHistory_vw]

* Dependencies
	n/a

* Parameters
	n/a
	
* Notes


*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE VIEW [dbo].[RestoreHistory_vw]

AS

SELECT 
	rh.[destination_database_name] AS [DBName]
	,bs.[server_name] AS [SourceServer]
	,CASE
		WHEN bs.[software_major_version] = 9 THEN '2005'
		WHEN bs.[software_major_version] = 10 AND bs.[software_minor_version] = 0 THEN '2008'
		WHEN bs.[software_major_version] = 10 AND bs.[software_minor_version] = 50 THEN '2008 R2'
		WHEN bs.[software_major_version] = 11 THEN '2012'
		WHEN bs.[software_major_version] > 11 THEN '2014 or later'
		ELSE 'Unknown'
	END AS [SourceServerSQLVersion]
	,bs.[database_name] AS [SourceDB]
	,bs.[compatibility_level] AS [SourceDBCompatibilityLevel]
	,bs.[recovery_model] AS [SourceDBRecoveryModel]
	,bs.[is_copy_only] AS [SourceBackupIsCopyOnly]
	,CASE
		WHEN rh.[restore_type] = 'D' THEN 'Database'
		WHEN rh.[restore_type] = 'F' THEN 'File'
		WHEN rh.[restore_type] = 'G' THEN 'Filegroup'
		WHEN rh.[restore_type] = 'I' THEN 'Differential'
		WHEN rh.[restore_type] = 'L' THEN 'Log'
		WHEN rh.[restore_type] = 'V' THEN 'Verifyonly'
		ELSE NULL
	END AS [RestoreType]
	,rh.[replace] AS [Replace]
	,rh.[recovery] AS [Recovery]
	,rh.[user_name] AS [UserName]
	,rh.[restore_date] AS [RestoreDate]
FROM [msdb].[dbo].[restorehistory] rh
JOIN [msdb].[dbo].[backupset] bs
	ON rh.[backup_set_id] = bs.[backup_set_id]