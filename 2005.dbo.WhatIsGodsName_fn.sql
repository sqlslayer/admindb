IF OBJECT_ID('[dbo].[WhatIsGodsName_fn]','FN') IS NOT NULL 
	DROP FUNCTION [dbo].[WhatIsGodsName_fn]
GO

/*******************************************************************************************************
**	Name:			dbo.WhatIsGodsName_fn
**	Desc:			Function to return sa account
**	Auth:			Adam Bean (SQLSlayer.com)
**  Usage:			SELECT dbo.WhatIsGodsName_fn()
**	Date:			2009.06.25
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:		Description:
**	--------	--------	---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE FUNCTION [dbo].[WhatIsGodsName_fn]()
RETURNS sysname

AS

BEGIN

	DECLARE 
		@Name	sysname

	SET @Name = (SELECT [name] FROM [master].[sys].[server_principals] WHERE [sid] = 0x01)
		
	RETURN @Name
END