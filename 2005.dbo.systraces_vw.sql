IF OBJECT_ID('[dbo].[systraces_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[systraces_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.systraces_vw
**	Desc:			View to give you a common 2005 view of sys.traces
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2009.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE VIEW [dbo].[systraces_vw]

AS

SELECT
	CAST([id] AS INT)					AS [id]
	,CAST([status] AS INT)				AS [status]
	,CAST([path] AS NVARCHAR(260))		AS [path]
	,CAST([max_size] AS BIGINT)			AS [max_size]
	,CAST([stop_time] AS DATETIME)		AS [stop_time]
	,CAST([max_files] AS INT)			AS [max_files]
	,CAST([is_rowset] AS BIT)			AS [is_rowset]
	,CAST([is_rollover] AS BIT)			AS [is_rollover]
	,CAST([is_shutdown] AS BIT)			AS [is_shutdown]
	,CAST([is_default] AS BIT)			AS [is_default]
	,CAST([buffer_count] AS INT)		AS [buffer_count]
	,CAST([buffer_size] AS INT)			AS [buffer_size]
	,CAST([file_position] AS BIGINT)	AS [file_position]
FROM sys.traces