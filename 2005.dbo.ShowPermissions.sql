IF OBJECT_ID('[dbo].[ShowPermissions]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[ShowPermissions]
GO

SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			dbo.ShowPermissions
**	Desc:			Retrieves all relevant permissions
**	Auth:			Adam Bean (SQLSlayer.com)
**  Parameters:		@ShowPublic = 1 to include public permissions
					@ShowServer = 1 to show server permissions
					@ShowUsers = 1 to show user permissions
					@ShowRoles = 1 to show role permissions
					@DBName =  Database to be queried, CSV supported, NULL for all
					@DBNameExclude =  Databases to be excluded (useful when wanting all but one or two databases)
					@LoginName = Specific login names to find permissions for (CSV supported)
					@LoginNameExclude = Specific login names to find permissions for (CSV supported)
					@UserName = Specific user names to find permissions for (CSV supported)
					@UserNameExclude = Specific user names to find permissions for (CSV supported)
					@RoleName = Specific role names to find permissions for (CSV supported)
					@RoleNameExclude = Specific role names to find permissions for (CSV supported)
					@ObjectName = Specific object names to find permissions for (CSV supported)
					@ObjectType = Specofic object types to find permissions for (CSV supported)
					@ObjectTypeExclude = Specific object types to exclude (CSV supported)
**	Date:			2008.09.22
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
**	20100310	Matt Stanford	Minor typo fix
**	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
**	20161227	Paul Popovich		Update order by columns to column names from numbers thanks to 2016 upgrade advisor
********************************************************************************************************/

CREATE PROCEDURE [dbo].[ShowPermissions] 
(
	@ShowPublic				BIT				= 0		-- Include public permissions
	,@ShowServer			BIT				= 1		-- Exclude or show server specific permissions
	,@ShowUsers				BIT				= 1		-- Exclude or show user specific permissions
	,@ShowRoles				BIT				= 1		-- Exclude or show role specific permissions
	,@DBName				NVARCHAR(512)	= NULL	-- Database names (CSV supported), NULL for all
	,@DBNameExclude			NVARCHAR(512)	= NULL	-- Database names to exclude (CSV supported)
	,@LoginName				NVARCHAR(512)	= NULL	-- Login names (CSV supported), NULL for all
	,@LoginNameExclude		NVARCHAR(512)	= NULL	-- Login names to exclude (CSV supported)
	,@UserName				NVARCHAR(512)	= NULL	-- User names (CSV supported), NULL for all
	,@UserNameExclude		NVARCHAR(512)	= NULL	-- User names to exclude (CSV supported)
	,@RoleName				NVARCHAR(512)	= NULL	-- Role names (CSV supported), NULL for all
	,@RoleNameExclude		NVARCHAR(512)	= NULL	-- Role names to exclude (CSV supported)
	,@ObjectName			NVARCHAR(512)	= NULL	-- Object name, one or many, NULL for all
	,@ObjectType			NVARCHAR(512)	= NULL	-- Search for specific object types (CSV supported), NULL for all
	,@ObjectTypeExclude		NVARCHAR(512)	= NULL	-- Object types to exclude
)

AS

SET NOCOUNT ON

-- Get all of the catalog views
-- Drop the tables if they exist
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#syscolumns') IS NOT NULL
   DROP TABLE #syscolumns
IF OBJECT_ID('tempdb.dbo.#sysdatabaseprincipals') IS NOT NULL
   DROP TABLE #sysdatabaseprincipals
IF OBJECT_ID('tempdb.dbo.#sysdatabasepermissions') IS NOT NULL
   DROP TABLE #sysdatabasepermissions
IF OBJECT_ID('tempdb.dbo.#sysdatabaserolemembers') IS NOT NULL
   DROP TABLE #sysdatabaserolemembers
   
-- Create the tables
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysobjects FROM [sys].[objects]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #syscolumns FROM [sys].[columns]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysdatabaseprincipals FROM [sys].[database_principals]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysdatabasepermissions FROM [sys].[database_permissions]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysdatabaserolemembers FROM [sys].[database_role_members]

-- Capture the permissions
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 
			
		-- sys.objects
		INSERT INTO #sysobjects
		EXEC(''SELECT *, DB_ID(''''' + @DBName + ''''') FROM [sys].[all_objects]'')

		-- sys.columns
		INSERT INTO #syscolumns
		EXEC(''SELECT *, DB_ID(''''' + @DBName + ''''') FROM [sys].[columns]'')
		
		-- sys.database_permissions
		INSERT INTO #sysdatabasepermissions
		EXEC(''SELECT *, DB_ID(''''' + @DBName + ''''') FROM [sys].[database_permissions]'')

		-- sys.database_principals
		INSERT INTO #sysdatabaseprincipals
		EXEC(''SELECT *, DB_ID(''''' + @DBName + ''''') FROM [sys].[database_principals]'')
		
		-- #sysdatabaserolemembers
		INSERT INTO #sysdatabaserolemembers
		EXEC(''SELECT *, DB_ID(''''' + @DBName + ''''') FROM [sys].[database_role_members]'')
	')

FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs

--Server permissions
IF @ShowServer = 1
BEGIN
	SELECT 
		srp.[name]													AS [LoginName]
		,spe.[state_desc]											AS [PermissionState]
		,spe.[permission_name]										AS [PermissionName]
		,srp.[is_disabled]											AS [IsDisabled]
		,IS_SRVROLEMEMBER('sysadmin',srp.[name])					AS [IsSysAdmin]
		,CASE 
			WHEN IS_SRVROLEMEMBER('sysadmin',srp.[name]) = 1 THEN 0
			ELSE IS_SRVROLEMEMBER('dbcreator',srp.[name])		
		END															AS [IsDbCreator]
		,CASE 
			WHEN IS_SRVROLEMEMBER('sysadmin',srp.[name]) = 1 THEN 0
			ELSE IS_SRVROLEMEMBER('bulkadmin',srp.[name])		
		END															AS [IsBulkAdmin]
		,CASE 
			WHEN IS_SRVROLEMEMBER('sysadmin',srp.[name]) = 1 THEN 0
			ELSE IS_SRVROLEMEMBER('diskadmin',srp.[name])		
		END															AS [IsDiskAdmin]
		,CASE 
			WHEN IS_SRVROLEMEMBER('sysadmin',srp.[name]) = 1 THEN 0
			ELSE IS_SRVROLEMEMBER('processadmin',srp.[name])	
		END															AS [IsProcessAdmin]
		,CASE 
			WHEN IS_SRVROLEMEMBER('sysadmin',srp.[name]) = 1 THEN 0
			ELSE IS_SRVROLEMEMBER('serveradmin',srp.[name])		
		END															AS [IsServerAdmin]
		,CASE 
			WHEN IS_SRVROLEMEMBER('sysadmin',srp.[name]) = 1 THEN 0
			ELSE IS_SRVROLEMEMBER('setupadmin',srp.[name])		
		END															AS [IsSetupAdmin]
		,CASE 
			WHEN IS_SRVROLEMEMBER('sysadmin',srp.[name]) = 1 THEN 0
			ELSE IS_SRVROLEMEMBER('securityadmin',srp.[name])	
		END															AS [IsSecurityAdmin]
		,srp.[create_date]											AS [DateCreated]
		,srp.[modify_date]											AS [ModifiedDate]
	FROM [sys].[server_principals] srp
	LEFT JOIN [sys].[server_permissions] spe
		ON spe.grantee_principal_id = srp.principal_id
	LEFT JOIN [dbo].[Split_fn](@LoginName,',') ln
		ON ln.[item] = srp.[name] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@LoginNameExclude,',') lne
		ON lne.[item] = srp.[name] COLLATE DATABASE_DEFAULT
	WHERE srp.[type] IN ('S', 'U', 'G')													-- SQL Login, Windows Login, Windows Group
	AND ((ln.[item] IS NOT NULL AND @LoginName IS NOT NULL) OR @LoginName IS NULL)		-- Specified login(s), or all
	AND lne.[item] IS NULL																-- All but excluded login(s)
	ORDER BY [LoginName], [PermissionState]
END

--Object permissions
IF @ShowUsers = 1
BEGIN
	SELECT DISTINCT
		COALESCE(DB_NAME(dpe.[database_id]),DB_NAME(dpr.[database_id]),DB_NAME(so.[database_id]))	AS [DBName]
		,spr.[name]							AS [Login]
		,CASE
			WHEN spr.[name] IS NULL AND spr.[sid] IS NULL THEN 0
			ELSE 1
		END									AS [IsUser]
		,CASE
			WHEN spr.[name] IS NOT NULL AND spr.[sid] IS NOT NULL THEN 0
			ELSE 1
		END									AS [IsRole]
		,dpr.[name]							AS [Name]
		,dpr.[type_desc]					AS [Type]
		,dpe.[state_desc]					AS [PermissionState]
		,dpe.[permission_name]				AS [PermissionName]
		,OBJECT_SCHEMA_NAME(dpe.[major_id], dpe.[database_id]) + '.' + OBJECT_NAME(dpe.[major_id], dpe.[database_id])	AS [ObjectName]
		,dbo.[sysobjecttype_fn](so.[type])	AS [ObjectType]
		,CASE
			WHEN sc.[name] IS NULL AND so.[type] IN ('TF','U','V') THEN '<All>'	-- User/role has permission to all columns
			ELSE sc.[name]							
		END									AS [ColumnName]
		,dpr.[create_date]					AS [DateCreated]
		,dpr.[modify_date]					AS [ModifiedDate]
	FROM #sysdatabasepermissions dpe
	LEFT JOIN #sysdatabaseprincipals dpr
		ON dpr.[database_id] = dpe.[database_id]
		AND dpr.[principal_id] = dpe.[grantee_principal_id] 
	LEFT JOIN #sysdatabaserolemembers drm
		ON drm.[database_id] = dpr.[database_id]
		AND drm.role_principal_id = dpr.principal_id
	LEFT JOIN #sysdatabaseprincipals dpr2
		ON dpr2.[database_id] = drm.[database_id]
		AND dpr2.principal_id = drm.member_principal_id
	LEFT JOIN [sys].[server_principals] spr
		ON spr.[sid] = dpr.[sid]
	LEFT JOIN [sys].[server_role_members] srm 
		ON srm.[role_principal_id] = spr.[principal_id] 
	LEFT JOIN #sysobjects so
		ON so.[database_id] = COALESCE(dpe.[database_id], dpr.[database_id])
		AND so.[object_id] = dpe.[major_id]
	LEFT JOIN #syscolumns sc
		ON sc.[database_id] = COALESCE(dpe.[database_id], dpr.[database_id], so.[database_id])
		AND sc.[object_id] = dpe.[major_id] 
		AND sc.[column_id] = dpe.[minor_id]
	LEFT JOIN [dbo].[Split_fn](@LoginName,',') ln
		ON ln.[item] = spr.[name]
	LEFT JOIN [dbo].[Split_fn](@LoginNameExclude,',') lne
		ON lne.[item] = spr.[name]
	LEFT JOIN [dbo].[Split_fn](@UserName,',') un
		ON un.[item] = dpr.[name]
	LEFT JOIN [dbo].[Split_fn](@UserNameExclude,',') une
		ON une.[item] = dpr.[name]
	LEFT JOIN [dbo].[Split_fn](@RoleName,',') rn
		ON rn.[item] = dpr.[name]
	LEFT JOIN [dbo].[Split_fn](@RoleNameExclude,',') rne
		ON rne.[item] = dpr.[name]
	LEFT JOIN [dbo].[Split_fn](@ObjectName,',') obj
		ON obj.[item] = so.[name]
	LEFT JOIN [dbo].[Split_fn](@ObjectType,',') ot
		ON ot.[item] = so.[type] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@ObjectTypeExclude,',') ote
		ON ote.[item] = so.[type] COLLATE DATABASE_DEFAULT
	WHERE ((@ShowPublic = 0 AND dpr.[name] != 'public') OR @ShowPublic = 1)				-- Show or exclude public permissions
	AND ((ln.[item] IS NOT NULL AND @LoginName IS NOT NULL) OR @LoginName IS NULL)		-- Specified login(s), or all
	AND ((un.[item] IS NOT NULL AND @UserName IS NOT NULL) OR @UserName IS NULL)		-- Specified user(s), or all
	AND ((rn.[item] IS NOT NULL AND @RoleName IS NOT NULL) OR @RoleName IS NULL)		-- Specified role(s), or all
	AND ((obj.[item] IS NOT NULL AND @ObjectName IS NOT NULL) OR @ObjectName IS NULL)	-- Specified role(s), or all
	AND ((ot.[item] IS NOT NULL AND @ObjectType IS NOT NULL) OR @ObjectType IS NULL)	-- Specified object type(s), or all
	AND lne.[item] IS NULL	-- All but excluded login(s)
	AND une.[item] IS NULL	-- All but excluded users(s)
	AND rne.[item] IS NULL	-- All but excluded roles(s)
	AND ote.[item] IS NULL	-- All but excluded object type(s)
	ORDER BY [DBName], [IsRole] DESC, [Login]
END

-- Role membership
IF @ShowRoles = 1
BEGIN
	SELECT
		DB_NAME(dpr.[database_id])	AS [DBName]
		--,CASE 
		--	WHEN dpr.[is_fixed_role] = 1 THEN '<Fixed_Database_Role>'
		--	ELSE dpr2.[name]
		--END							AS [User]
		,dpr2.[name]				AS [User]
		,dpr.[name]					AS [Role]
		--,*
	FROM #sysdatabaseprincipals dpr
	LEFT JOIN #sysdatabaserolemembers drm
		ON drm.[database_id] = dpr.[database_id]
		AND drm.[role_principal_id] = dpr.[principal_id]
	LEFT JOIN #sysdatabaseprincipals dpr2
		ON dpr2.[database_id] = drm.[database_id]
		AND dpr2.[principal_id] = drm.[member_principal_id]
	LEFT JOIN [dbo].[Split_fn](@UserName,',') un
		ON un.[item] = dpr2.[name]
	LEFT JOIN [dbo].[Split_fn](@UserNameExclude,',') une
		ON une.[item] = dpr2.[name]
	LEFT JOIN [dbo].[Split_fn](@RoleName,',') rn
		ON rn.[item] = dpr.[name]
	LEFT JOIN [dbo].[Split_fn](@RoleNameExclude,',') rne
		ON rne.[item] = dpr.[name]
	WHERE dpr.[type] = 'R'																-- Database roles only
	AND ((@ShowPublic = 0 AND dpr.[name] != 'public') OR @ShowPublic = 1)				-- Show or exclude public permissions
	AND ((un.[item] IS NOT NULL AND @UserName IS NOT NULL) OR @UserName IS NULL)		-- Specified user(s), or all
	AND ((rn.[item] IS NOT NULL AND @RoleName IS NOT NULL) OR @RoleName IS NULL)		-- Specified role(s), or all
	AND une.[item] IS NULL	-- All but excluded users(s)
	AND rne.[item] IS NULL	-- All but excluded roles(s)
	ORDER BY [DBName],[User]
END

SET QUOTED_IDENTIFIER OFF
SET NOCOUNT OFF
