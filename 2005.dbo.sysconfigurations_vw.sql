IF OBJECT_ID('[dbo].[sysconfigurations_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[sysconfigurations_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.sysconfigurations_vw
**	Desc:			Compatibility view for consistency between 2000, 2005 and 2008
**					Used like sys.configurations in '05 and 08
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2009.07.22
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE VIEW [dbo].[sysconfigurations_vw]

AS

SELECT 
	CAST([configuration_id] AS INT)			AS [configuration_id]
	,CAST([name] AS NVARCHAR(35))			AS [name]
	,CAST([value] AS SQL_VARIANT)			AS [value]
	,CAST([minimum] AS SQL_VARIANT)			AS [minimum]
	,CAST([maximum] AS SQL_VARIANT)			AS [maximum]
	,CAST([value_in_use] AS SQL_VARIANT)	AS [value_in_use]
	,CAST([description] AS NVARCHAR(255))	AS [description]
	,CAST([is_dynamic] AS BIT)				AS [is_dynamic]
	,CAST([is_advanced] AS BIT)				AS [is_advanced]
FROM sys.configurations
