IF OBJECT_ID('[dbo].[Maintenance_GetCommandID]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[Maintenance_GetCommandID]
GO

/******************************************************************************
* Name
	[dbo].[Maintenance_GetCommandID]

* Author
	Adam Bean
	
* Date
	2011.09.01
	
* Synopsis
	Maintenance routine to retrieve ID for command issued.
	
* Description
	Used for all maintenance routines, this procedure will retrieve an existing ID or insert a new record
	and retrieve the appropriate ID. This data is then used to log into a logging table per maintenance routine.

* Examples
	Do not call this procedure directly, it will be utilized within the maintenance routines.

* Dependencies
	n/a
	
* Parameters
	@Command	- Command which was issued from the calling maintenance routine
	@CommandID	- CommandID that will be returned to the calling maintenance routine

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [dbo].[Maintenance_GetCommandID]
(
	@Command		VARCHAR(2048)
	,@CommandID		INT OUTPUT
)

AS

SET NOCOUNT ON

SELECT 
	@CommandID = [CommandID]
FROM [dbo].[Maintenance_CommandIDs]
WHERE [Command] = @Command

IF @CommandID IS NULL
BEGIN
	INSERT INTO [dbo].[Maintenance_CommandIDs] ([Command]) 
	VALUES (@Command)

	SET @CommandID = SCOPE_IDENTITY()
END

SET NOCOUNT OFF