IF OBJECT_ID('dbo.IndexInfo','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[IndexInfo]
GO

/******************************************************************************
* Name
	[dbo].[IndexInfo]

* Author
	Adam Bean
	
* Date
	2008.03.13
	
* Synopsis
	Retrieves all relevant index information
	
* Description
	Displays pretty much all supporting information for any/all indexing

* Examples
	EXEC IndexInfo @DBNameExclude = 'admin', @PrimaryKeys = 0, @Rank='2,3,4'
	EXEC IndexInfo @DBName = 'X', @MISearch = 1

* Dependencies
	dbo.GetManSection_fn

* Parameters
	@DBName = (optional) Database to be queried, CSV supported, NULL for all
	@DBNameExclude = (optional) Databases to be excluded (useful when wanting all but one or two)
	@TableName = (optional) retrieve index information for specified table(s), CSV supported, NULL for all
	@IndexName = (optional) retrieve index information for specified index(es), CSV supported, NULL for all
	@ScanType = LIMITED, SAMPLED, or DETAILED (http://msdn.microsoft.com/en-us/library/ms188917.aspx)
	@OrderBy1, 2, 3 = Columns to order output by
	@Rank = Rank of index(es) to display, CSV supported
	@PrimaryKeys = Exclude primary keys from the output
	@MISearch = Ability to search for indexes that were created as a result of MissingIndexes_vw

* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20080821	Adam Bean		Added index columns
	20080822	Adam Bean		Added index create statements
	20080826	Adam Bean		Combined index usage procedure
								Added schema name into create/drop statements
								Added case for primary key on create
								Added additional fields for create statement
								Added index key size
	20080903	Matt Stanford	Moved index key size to CTE
	20080308	Adam Bean		Fixed duplicates
	20080408	Adam Bean		Changed JOIN on dm_db_index_usage_stats to left join
	20081021	Adam Bean		Removed * 2 on nchar, nvarchar, ntext as value is stored double already
	20090713	Adam Bean		Changed index size math to properly match Microsofts sp_spaceused
								Added OrderBy arguments
	20090723	Adam Bean		Added CSV support for @DBName, @Tablename, @IndexName
								Added @ScanType
								Added ranking system and reorganized columns
	20090727	Adam Bean		Resolved issue with 80 compatibility databases by passing in @DBID
								Fixed bad join in create index cursor
								Added @PrimaryKeys and @DBNameExclude
	20090728	Adam Bean		Wrapped ISNULL around reads/writes/forwardedrecordcount
								Removed ObjectID and IndexId from select on return and changed defaulted OrderBy's
								Changed LastStatsUpdate to DATETIME
	20090915	Matt Stanford	Changed @DBID to an INT (was TINYINT)
	20090915	Adam Bean		Changed @DBID to VARCHAR
	20090929	Adam Bean		Global header formatting & general clean up
	20091006	Adam Bean		Added proper syntax for clustered & unique indexes
								Fixed collation issues on SYSNAME
	20100506	Adam Bean		Added @MISearch to only analyze IX_MI_ indexed added from MissingIndexes_vw
	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
	20101027	Matt Stanford	Added the NONE @ScanType, also changed default @ScanType to LIMITED (the actual system default)
	20120507	Adam Bean		Added _dta_index_ to the @MISearch (the default naming convention for database tuning advisor)
	20130325	Adam Bean		Added support for COLUMNSTORE indexing, new header
	20170210	Mike Wolfe		Set PageSize=8, and added column names instead of position numbers
	20170419	Adam Bean		Changed @IndexFillFactor data type to fix bug
******************************************************************************/

CREATE PROCEDURE [dbo].[IndexInfo]
(
	@DBName				NVARCHAR(512)	= NULL		-- Database names (CSV supported), NULL for all
	,@DBNameExclude		NVARCHAR(512)	= NULL		-- Database names to exclude (CSV supported), NULL for all
	,@TableName			NVARCHAR(512)	= NULL		-- Table name, one or many, NULL for all
	,@IndexName			NVARCHAR(512)	= NULL		-- Index name, one or many, NULL for all
	,@ScanType			NVARCHAR(128)	= 'LIMITED'	-- LIMITED, SAMPLED, DETAILED, or NONE
	,@OrderBy1			TINYINT			= 1			-- DatabaseName
	,@OrderBy2			TINYINT			= 2			-- Rank
	,@OrderBy3			TINYINT			= 3			-- TableName
	,@OrderBy4			TINYINT			= 5			-- IndexName
	,@Rank				VARCHAR(16)		= NULL		-- 0, 1, 2, 3, 4 (or any combination of, CSV supported), NULL for all
	,@PrimaryKeys		BIT				= NULL		-- 0 to exclude, 1 to show only primary keys, NULL for all
	,@MISearch			BIT				= 0			-- 0 to exclude, 1 to show indexes with an IX_MI_ or _dta_index_ prefix
)

AS

SET NOCOUNT ON
SET DEADLOCK_PRIORITY HIGH

DECLARE 
	@DBID				VARCHAR(4)
	,@IdxCols			VARCHAR(MAX)
	,@IdxInclCols		VARCHAR(MAX)
	,@ObjectID			INT
	,@IndexID			INT
	,@PageSize			TINYINT
	,@ColNames			VARCHAR(MAX)
	,@ColIncludes		VARCHAR(MAX)
	,@SchemaName		sysname
	,@IdxName			sysname
	,@TblName			sysname
	,@IdxType			VARCHAR(32)
	,@IsUnique			BIT
	,@Padded			BIT
	,@IgnoreDupKey		BIT
	,@AllowRowLocks		BIT
	,@AllowPageLocks	BIT
	,@IndexFillFactor	INT
	,@FileGroup			sysname
	
-- Retrieve page size
--SELECT @PageSize = ([low] / 1024) FROM [master].[dbo].[spt_values] WHERE [number] = 1 AND [type] = 'E'
-- This value is always 8K for SQL Server
SELECT @PageSize = 8

-- Get all of the catalog views
-- Drop the tables if they exist
IF OBJECT_ID('tempdb.dbo.#sysindexes') IS NOT NULL
   DROP TABLE #sysindexes
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#syspartitions') IS NOT NULL
   DROP TABLE #syspartitions
IF OBJECT_ID('tempdb.dbo.#sysschemas') IS NOT NULL
   DROP TABLE #sysschemas
IF OBJECT_ID('tempdb.dbo.#syscolumns') IS NOT NULL
   DROP TABLE #syscolumns
IF OBJECT_ID('tempdb.dbo.#sysindex_columns') IS NOT NULL
   DROP TABLE #sysindex_columns
IF OBJECT_ID('tempdb.dbo.#sysfilegroups') IS NOT NULL
   DROP TABLE #sysfilegroups
IF OBJECT_ID('tempdb.dbo.#sysallocation_units') IS NOT NULL
   DROP TABLE #sysallocation_units
IF OBJECT_ID('tempdb.dbo.#PhysStats') IS NOT NULL
   DROP TABLE #PhysStats
IF OBJECT_ID('tempdb.dbo.#IndexInfo') IS NOT NULL
	DROP TABLE #IndexInfo

-- Create the tables
SELECT TOP 0 *, [statsdate] = CAST(NULL AS DATETIME), [database_id] = CONVERT(INT,NULL) INTO #sysindexes FROM [sys].[indexes]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysobjects FROM [sys].[objects]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #syspartitions FROM [sys].[partitions]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysschemas FROM [sys].[schemas]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #syscolumns FROM [sys].[columns]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysindex_columns  FROM [sys].[index_columns]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysallocation_units FROM [sys].[allocation_units]
SELECT TOP 0 *, [database_id] = CONVERT(INT,NULL) INTO #sysfilegroups  FROM [sys].[filegroups]
SELECT TOP 0 * INTO #PhysStats FROM [sys].[dm_db_index_physical_stats] (1, NULL, NULL, NULL, NULL)

-- Populate the tables
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
	,[database_id]
FROM [sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName, @DBID
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 
			
		-- sys.indexes
		INSERT INTO #sysindexes
		SELECT i.*, STATS_DATE(i.[object_id], i.[index_id]), ' + @DBID + ' 
		FROM [sys].[indexes] i 
		JOIN [sys].[tables] t 
			ON t.[object_id] = i.[object_id] 
		WHERE (LEFT(i.[name], 6) = ''IX_MI_'' OR LEFT(i.[name], 11) = ''_dta_index_'') 
		OR ' + @MISearch + ' = 0

		-- sys.objects
		INSERT INTO #sysobjects
		SELECT *, ' + @DBID + ' FROM [sys].[objects]

		-- sys.partitions 
		INSERT INTO #syspartitions
		SELECT *, ' + @DBID + ' FROM [sys].[partitions]

		-- sys.schemas 
		INSERT INTO #sysschemas
		SELECT *, ' + @DBID + ' FROM [sys].[schemas]

		-- sys.columns 
		INSERT INTO #syscolumns
		SELECT *, ' + @DBID + ' FROM [sys].[columns]

		-- sys.index_columns 
		INSERT INTO #sysindex_columns 
		SELECT *, ' + @DBID + ' FROM [sys].[index_columns]

		-- sys.filegroups
		INSERT INTO #sysfilegroups 
		SELECT *, ' + @DBID + ' FROM [sys].[filegroups]

		-- sys.allocation_units
		INSERT INTO #sysallocation_units
		SELECT *, ' + @DBID + ' FROM [sys].[allocation_units]
	')
	
	-- sys.dm_db_index_physical_stats
	IF ISNULL(UPPER(@ScanType),'LIMITED') IN ('LIMITED','SAMPLED','DETAILED')
	BEGIN
		INSERT INTO #PhysStats
		SELECT * FROM [sys].[dm_db_index_physical_stats] (@DBID, NULL, NULL , NULL, @ScanType)
	END
	
FETCH NEXT FROM #dbs INTO @DBName, @DBID
END
CLOSE #dbs
DEALLOCATE #dbs

-- CTE for index key size
;WITH KeySize([database_id], [ObjectID], [IndexID], [Size])
AS
(
	SELECT 
		o.[database_id]
		,o.[object_id]
		,i.[index_id]
		,SUM(CAST(
		CASE WHEN (c.[system_type_id] = 108 OR c.[system_type_id] = 106) AND c.[scale] >0 AND c.[precision] >= 1 AND c.[precision] <= 9 --numeric OR decimal
			THEN c.[max_length] + 5 + 1 
		WHEN (c.[system_type_id] = 108 OR c.[system_type_id] = 106) AND c.[scale] >0 AND c.[precision] >= 10 AND c.[precision] <= 19 --numeric OR decimal
			THEN c.[max_length] + 9 + 1
		WHEN (c.[system_type_id] = 108 OR c.[system_type_id] = 106) AND c.[scale] >0 AND c.[precision] >= 20 AND c.[precision] <= 28 --numeric OR decimal
			THEN c.[max_length] + 13 + 1
		WHEN (c.[system_type_id] = 108 OR c.[system_type_id] = 106) AND c.[scale] >0 AND c.[precision] >= 29 AND c.[precision] <= 38 --numeric OR decimal
			THEN c.[max_length] + 17 + 1
		WHEN (c.[system_type_id] = 108 OR c.[system_type_id] = 106) AND c.[scale] !>0 AND c.[precision] >= 1 AND c.[precision] <= 9 --numeric OR decimal
			THEN c.[max_length] + 5
		WHEN (c.[system_type_id] = 108 OR c.[system_type_id] = 106) AND c.[scale] !>0 AND c.[precision] >= 10 AND c.[precision] <= 19 --numeric OR decimal
			THEN c.[max_length] + 9
		WHEN (c.[system_type_id] = 108 OR c.[system_type_id] = 106) AND c.[scale] !>0 AND c.[precision] >= 20 AND c.[precision] <= 28 --numeric OR decimal
			THEN c.[max_length] + 13
		WHEN (c.[system_type_id] = 108 OR c.[system_type_id] = 106) AND c.[scale] !>0 AND c.[precision] >= 29 AND c.[precision] <= 38 --numeric OR decimal
			THEN c.[max_length] + 17 
		WHEN c.[system_type_id] = 36	--uniqueidentifier
			THEN c.[max_length]
		WHEN c.[system_type_id] = 167	--varchar
			THEN c.[max_length] + 2
		WHEN c.[system_type_id] = 231	--nvarchar 
			THEN c.[max_length] + 2
		WHEN c.[system_type_id] = 175	--char
			THEN c.[max_length]
		WHEN c.[system_type_id] = 239	--nchar
			THEN c.[max_length]
		WHEN c.[system_type_id] = 58	--smalldatetime 
			THEN 8
		WHEN c.[system_type_id] = 61	--datetime
			THEN 8
		WHEN c.[system_type_id] = 189	--timestamp 
			THEN 8
		WHEN c.[system_type_id] = 35	--text 
			THEN 2 * c.[max_length] 
		WHEN c.[system_type_id] = 99	--ntext 
			THEN c.[max_length] 
		WHEN c.[system_type_id] = 104	--bit 
			THEN 2
		WHEN c.[system_type_id] = 62	--float 
			THEN c.[max_length]
		WHEN c.[system_type_id] = 59	--real 
			THEN 4
		WHEN c.[system_type_id] = 165	--varbinary 
			THEN c.[max_length] + 2
		WHEN c.[system_type_id] = 173	--binary 
			THEN c.[max_length]
		WHEN c.[system_type_id] = 60	--money 
			THEN 8
		WHEN c.[system_type_id] = 34	--image 
			THEN 2 * c.[max_length]
		WHEN c.[system_type_id] = 98	--sql_variant 
			THEN 8016
		WHEN c.[system_type_id] = 241	--xml
			THEN 2 * 1024 * 1024			
		WHEN c.[system_type_id] = 56	--int
			THEN 4
		WHEN c.[system_type_id] = 127	--bigint
			THEN 8
		WHEN c.[system_type_id] = 48	--tinyint
			THEN 1
		WHEN c.[system_type_id] = 52	--smallint 
			THEN 2
		ELSE 0
		END AS BIGINT)) AS [cnt]
	FROM #sysindexes i
	JOIN #sysindex_columns ic 
		ON i.[database_id] = ic.[database_id]
		AND i.[object_id] = ic.[object_id] 
		AND i.[index_id] = ic.[index_id]
	JOIN #sysobjects o
		ON i.[database_id] = o.[database_id]
		AND i.[object_id] = o.[object_id]
	JOIN #syscolumns c
		ON i.[database_id] = c.[database_id]
		AND o.[object_id] = c.[object_id]
		AND c.[column_id] = ic.[column_id]
	WHERE o.[is_ms_shipped] = 0
	AND i.[index_id] > 0
	GROUP BY o.[database_id], o.[object_id], i.[index_id]
) 

-- Populate temp table with all the index information
SELECT 
	DB_NAME(i.[database_id])				AS [DBName]
	,CASE
		WHEN ISNULL((u.[user_seeks] + u.[user_scans] + u.[user_lookups]),0) = 0 
		AND ISNULL(u.[user_updates],0) = 0
			THEN 0 -- No reads and no writes
		WHEN (u.[user_seeks] + u.[user_scans] + u.[user_lookups]) > u.[user_updates] 
			THEN 1	-- More reads than writes
		WHEN ISNULL((u.[user_seeks] + u.[user_scans] + u.[user_lookups]),0) = 0 
		AND u.[user_updates] > 50
			THEN 2	-- No reads, with more than 50 writes
		WHEN ISNULL((u.[user_seeks] + u.[user_scans] + u.[user_lookups]),0) != 0 
		AND ROUND(CAST(u.[user_updates] AS DECIMAL(20,2)) / CAST((u.[user_seeks] + u.[user_scans] + u.[user_lookups]) AS DECIMAL(20,2)),0) > 20 
			THEN 3	-- More than 20% writes than reads
		ELSE 4		-- Everything else
	END										AS [Rank]
	,o.[object_id]							AS [ObjectID]
	,o.[name]								AS [TableName]
	,s.[name]								AS [SchemaName]
	,i.[index_id]							AS [IndexID]
	,i.[name]								AS [IndexName]
	,i.[type_desc]							AS [IndexType]
	,g.[name]								AS [FileGroup]
	,@PageSize * (a.[used_pages] - 
		CASE 
			WHEN a.[type] != 1 THEN a.[used_pages] 
			WHEN p.[index_id] < 2 THEN a.[data_pages]
			ELSE 0 
		END)								AS [IndexSizeKB]
	,ks.[Size]								AS [KeySizeKB]
	,CAST(NULL AS VARCHAR(MAX))				AS [KeyColumns]
	,CAST(NULL AS VARCHAR(MAX))				AS [IncludedColumns]
	,CAST(NULL AS VARCHAR(MAX))				AS [CreateIndexStatement]
	,CASE
		WHEN i.[is_primary_key] = 0 THEN 'USE [' + DB_NAME(i.[database_id]) + '] DROP INDEX [' + s.[name] + '].[' + o.[name] + '].[' + i.[name] + ']'
		ELSE 'USE [' + DB_NAME(i.[database_id]) + '] ALTER TABLE [' + s.[name] + '].[' + o.[name] + '] DROP CONSTRAINT [' + i.[name] + ']'
	END										AS [DropIndexStatement]
	,ISNULL((u.[user_seeks] + u.[user_scans] + u.[user_lookups]),0)	AS [Reads]
	,ISNULL(u.[user_updates],0)				AS [Writes]
	,u.[last_user_seek]						AS [LastSeek]
	,u.[last_user_scan]						AS [LastScan]
	,u.[last_user_lookup]					AS [LastLookup]
	,u.[last_user_update]					AS [LastUpdate]
	,o.[create_date]						AS [DateCreated]
	,o.[modify_date]						AS [DateModified]
	,i.[statsdate]							AS [LastStatsUpdate]
	,i.[is_primary_key]						AS [IsPrimaryKey]
	,i.[is_unique]							AS [IsUnique]
	,i.[is_padded]							AS [IsPadded]
	,i.[ignore_dup_key]						AS [IgnoreDupKey]
	,i.[allow_row_locks]					AS [AllowRowLocks]
	,i.[allow_page_locks]					AS [AllowPageLocks]
	,i.[fill_factor]						AS [FillFactor]
	,(SELECT COUNT(*) FROM #syspartitions p WHERE p.[object_id] = o.[object_id] AND p.[index_id] = i.[index_id]) AS [PartitionCount]
	,ps.[avg_fragmentation_in_percent]		AS [AvgFragmentationInPercent]
	,ps.[fragment_count]					AS [FragmentCount]
	,ps.[avg_fragment_size_in_pages]		AS [AvgFragmentationSizeInPages]
	,ps.[page_count]						AS [PageCount]
	,ps.[avg_page_space_used_in_percent]	AS [AvgPageSpaceUsedInPercent]
	,ps.[record_count]						AS [RecordCount]
	,ps.[ghost_record_count]				AS [GhostRecordCount]
	,ps.[version_ghost_record_count]		AS [VersionGhostRecordCount]
	,ps.[min_record_size_in_bytes]			AS [MinRecordSizeInBytes]
	,ps.[max_record_size_in_bytes]			AS [MaxRecordSizeInBytes]
	,ps.[avg_record_size_in_bytes]			AS [AvgRecordSizeInBytes]
	,CASE 
		WHEN ps.[database_id] IS NULL THEN NULL
		ELSE ISNULL(ps.[forwarded_record_count],0)
	END										AS [ForwardedRecordCount]
INTO #IndexInfo
FROM #sysindexes i
JOIN #sysobjects o
	ON o.[database_id] = i.[database_id]
	AND o.[object_id] = i.[object_id]
JOIN #sysschemas s
	ON s.[database_id] = i.[database_id]
	AND s.[schema_id] = o.[schema_id]
JOIN #syspartitions p 
	ON p.[database_id] = i.[database_id]
	AND p.[object_id] = i.[object_id]
	AND p.[index_id] = i.[index_id]
JOIN #sysallocation_units a 
	ON a.[database_id] = i.[database_id]
	AND  a.[container_id] = p.[partition_id]
JOIN #sysfilegroups g
	ON g.[database_id] = i.[database_id]
	AND g.[data_space_id] = i.[data_space_id]
LEFT JOIN #PhysStats ps
	ON i.[database_id] = ps.[database_id]
	AND i.[object_id] = ps.[object_id]
	AND i.[index_id] = ps.[index_id]
LEFT JOIN [sys].[dm_db_index_usage_stats] u
	ON u.[database_id] = i.[database_id]
	AND u.[object_id] = i.[object_id]
	AND u.[index_id] = i.[index_id]
LEFT JOIN [KeySize] ks
	ON ks.[database_id] = i.[database_id]
	AND ks.[ObjectID] = o.[object_id]
	AND ks.[IndexID] = i.[index_id]
LEFT JOIN [dbo].[Split_fn](@TableName,',') tn
	ON o.[name] = tn.[item]
LEFT JOIN [dbo].[Split_fn](@IndexName,',') ix
	ON i.[name] = ix.[item]
WHERE ((tn.[item] IS NOT NULL AND @TableName IS NOT NULL) OR @TableName IS NULL) -- Specified Table names, or all
AND ((ix.[item] IS NOT NULL AND @IndexName IS NOT NULL) OR @IndexName IS NULL) -- Specified Index names, or all
AND (i.[is_primary_key]= @PrimaryKeys OR @PrimaryKeys IS NULL) -- Exclude PK's if desired
AND i.[index_id] > 0 -- Non Heaps
AND a.[type_desc] = 'IN_ROW_DATA'
AND ISNULL(ps.[alloc_unit_type_desc],'IN_ROW_DATA') = 'IN_ROW_DATA'
AND o.[is_ms_shipped] = 0 -- Exclude microsoft objects
ORDER BY [DBName], [TableName], [IndexName]

-- Get the column names per index
DECLARE #IdxCols CURSOR LOCAL STATIC FOR
SELECT 
	DB_ID(DBName)
	,[DBName]
	,[ObjectID]
	,[IndexID] 
FROM #IndexInfo  

OPEN #IdxCols
FETCH NEXT FROM #IdxCols INTO @DBID, @DBName, @ObjectID, @IndexID
WHILE @@FETCH_STATUS = 0
BEGIN

	SET @IdxCols = ''
	SELECT @IdxCols = @IdxCols + ISNULL('[' + sc.[name] + ']','') + ',' 
	FROM #sysindexes i
	JOIN #sysindex_columns ic
		ON i.[database_id] = ic.[database_id]
		AND i.[object_id] = ic.[object_id] 
		AND i.[index_id] = ic.[index_id]
	JOIN #syscolumns sc
		ON ic.[database_id] = sc.[database_id]
		AND ic.[object_id] = sc.[object_id] 
		AND ic.[column_id] = sc.[column_id]
	WHERE i.[database_id] = @DBID
	AND i.[object_id] = @ObjectID 
	AND i.[index_id] = @IndexID
	AND ic.[is_included_column] = 0
	ORDER BY sc.[column_id]
	
	SET @IdxInclCols = ''
	SELECT @IdxInclCols = @IdxInclCols + ISNULL('[' + sc.[name] + ']','') + ',' 
	FROM #sysindexes i
	JOIN #sysindex_columns ic
		ON i.[database_id] = ic.[database_id]
		AND i.[object_id] = ic.[object_id] 
		AND i.[index_id] = ic.[index_id]
	JOIN #syscolumns sc
		ON ic.[database_id] = sc.[database_id]
		AND ic.[object_id] = sc.[object_id] 
		AND ic.[column_id] = sc.[column_id]
	WHERE i.[database_id] = @DBID
	AND i.[object_id] = @ObjectID 
	AND i.[index_id] = @IndexID
	AND ic.[is_included_column] = 1
	ORDER BY sc.[column_id]

	UPDATE #IndexInfo 
		SET [KeyColumns] = @IdxCols
	WHERE [DBName] = @DBName
	AND [ObjectID] = @ObjectID 
	AND [IndexID] = @IndexID
	
	UPDATE #IndexInfo 
		SET [IncludedColumns] = @IdxInclCols 
	WHERE [DBName] = @DBName
	AND [ObjectID] = @ObjectID 
	AND [IndexID] = @IndexID

FETCH NEXT FROM #IdxCols INTO @DBID, @DBName, @ObjectID, @IndexID
END
CLOSE #IdxCols
DEALLOCATE #IdxCols

-- Remove trailing comma, or set to NULL if COLUMNSTORE
UPDATE #IndexInfo 
SET [KeyColumns] = 
	CASE
		WHEN [IndexType] = 'NONCLUSTERED COLUMNSTORE' THEN NULL
		ELSE LEFT([KeyColumns],LEN([KeyColumns]) -1)
	END
	,[IncludedColumns] = LEFT([IncludedColumns],CASE WHEN LEN([IncludedColumns]) > 0 THEN LEN([IncludedColumns]) -1 ELSE 0 END)

-- Populate create index statements
DECLARE #CreateIndex CURSOR LOCAL STATIC FOR
SELECT 
	[DBName]
	,[SchemaName]
	,[IndexName]
	,[TableName]
	,[IndexType]
	,[IsUnique]
	,[KeyColumns]
	,[IncludedColumns]
	,[IsPadded]
	,[IgnoreDupKey]
	,[AllowRowLocks]
	,[AllowPageLocks]
	,[FillFactor]
	,[FileGroup]
FROM #IndexInfo

OPEN #CreateIndex
FETCH NEXT FROM #CreateIndex INTO @DBName, @SchemaName, @IdxName, @TblName, @IdxType, @IsUnique, @ColNames, @ColIncludes, @Padded, @IgnoreDupKey, @AllowRowLocks, @AllowPageLocks, @IndexFillFactor, @FileGroup
WHILE @@FETCH_STATUS = 0
BEGIN

UPDATE #IndexInfo 
SET [CreateIndexStatement] = 
	'USE [' + @DBName + ']'
		+ ' CREATE'
		+ CASE
			WHEN @IsUnique = 1 THEN ' UNIQUE'
			ELSE ''
		END
		+ CASE
			WHEN @IdxType = 'CLUSTERED' THEN ' CLUSTERED'
			WHEN @IdxType = 'NONCLUSTERED COLUMNSTORE' THEN ' NONCLUSTERED COLUMNSTORE'
			ELSE ' NONCLUSTERED'
		END 
		+ ' INDEX [' + @IdxName + ']' 
		+ ' ON [' + @SchemaName + '].[' + @TblName + '] '
		+ CASE 
			WHEN @IdxType = 'NONCLUSTERED COLUMNSTORE' THEN '(' + [IncludedColumns] + ')' 
			ELSE '(' + @ColNames + ')' 
		END
		+ CASE
			WHEN [IncludedColumns] = '' THEN ''
			ELSE ' INCLUDE (' + [IncludedColumns] + ')'
		END
		+ ' WITH ('
		+ CASE
			WHEN @Padded = 0 THEN 'PAD_INDEX  = OFF,'
			ELSE 'PAD_INDEX  = ON,'
		END
		+ ' STATISTICS_NORECOMPUTE  = OFF,'
		+ ' SORT_IN_TEMPDB = OFF,'
		+ CASE
			WHEN @IgnoreDupKey = 0 THEN ' IGNORE_DUP_KEY = OFF,'
			ELSE ' IGNORE_DUP_KEY = ON,'
		END
		+ ' DROP_EXISTING = OFF,'
		+ CASE
			WHEN CAST(SERVERPROPERTY('EDITION') AS VARCHAR) NOT LIKE 'Enterprise%' THEN ' ONLINE = OFF,'
			ELSE ' ONLINE = ON,'
		END
		+ CASE
			WHEN @AllowRowLocks = 0 THEN ' ALLOW_ROW_LOCKS = OFF,'
			ELSE ' ALLOW_ROW_LOCKS = ON,'
		END
		+ CASE
			WHEN @AllowPageLocks = 0 THEN ' ALLOW_PAGE_LOCKS = OFF,'
			ELSE ' ALLOW_PAGE_LOCKS = ON,'
		END
		+ CASE 
			WHEN @IndexFillFactor = 0 THEN 'FILLFACTOR = 100'
			ELSE ' FILLFACTOR = ' + CAST(@IndexFillFactor AS VARCHAR)
		END 
		+ ' )'
		+ ' ON [' + @FileGroup + ']' 
	FROM #IndexInfo
WHERE [DBName] = @DBName
AND [TableName] = @TblName
AND [IndexName] = @IdxName

FETCH NEXT FROM #CreateIndex INTO @DBName, @SchemaName, @IdxName, @TblName, @IdxType, @IsUnique, @ColNames, @ColIncludes, @Padded, @IgnoreDupKey, @AllowRowLocks, @AllowPageLocks, @IndexFillFactor, @FileGroup
END
CLOSE #CreateIndex
DEALLOCATE #CreateIndex

-- Updates for COLUMNSTORE indexing. 
-- The columns are stored as included columns, not key columns; therefore we have to remove everything after include
-- This also will remove all the WITH options as those are also not supported 
UPDATE #IndexInfo
SET [CreateIndexStatement] = LEFT([CreateIndexStatement],CHARINDEX(' INCLUDE (',[CreateIndexStatement])-1)
WHERE [IndexType] = 'NONCLUSTERED COLUMNSTORE'

-- Return results
-- If @Rank was left NULL, set to all (haven't found a better way to do this yet because of the dynamic SQL (for ordering))
IF @Rank IS NULL
	SET @Rank = '0,1,2,3,4'
	
EXEC('
		SELECT 
			[DBName]
			,[Rank]
			,[TableName]
			,[SchemaName]
			,[IndexName]
			,[IndexType]
			,[FileGroup]
			,[IndexSizeKB]
			,[KeySizeKB]
			,[KeyColumns]
			,[IncludedColumns]
			,[CreateIndexStatement]
			,[DropIndexStatement]
			,[Reads]
			,[Writes]
			,[LastSeek]
			,[LastScan]
			,[LastLookup]
			,[LastUpdate]
			,[DateCreated]
			,[DateModified]
			,[LastStatsUpdate]
			,[IsPrimaryKey]
			,[IsUnique]
			,[IsPadded]
			,[IgnoreDupKey]
			,[AllowRowLocks]
			,[AllowPageLocks]
			,[FillFactor]
			,[PartitionCount]
			,[AvgFragmentationInPercent]
			,[FragmentCount]
			,[AvgFragmentationSizeInPages]
			,[PageCount]
			,[AvgPageSpaceUsedInPercent]
			,[RecordCount]
			,[GhostRecordCount]
			,[VersionGhostRecordCount]
			,[MinRecordSizeInBytes]
			,[MaxRecordSizeInBytes]
			,[AvgRecordSizeInBytes]
			,[ForwardedRecordCount]
		FROM #IndexInfo
		WHERE [Rank] IN (' + @Rank + ')
		ORDER BY ' + @OrderBy1 + ', ' + @OrderBy2 + ', ' + @OrderBy3 + ', ' + @OrderBy4 + '
	')
	
SET NOCOUNT OFF
SET QUOTED_IDENTIFIER OFF