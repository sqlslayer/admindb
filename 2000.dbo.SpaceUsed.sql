IF OBJECT_ID('[dbo].[SpaceUsed]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[SpaceUsed]
GO

/*******************************************************************************************************
**	Name:			dbo.SpaceUsed
**	Desc:			Procedure to retrieve data and log file sizes, used/Unused space and growth rates
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	Split_fn, sysdatabase_vw
**  Parameters:		@DBName = CSV list of dbnames, or null for all
					@DBNameExclude = CSV list of dbnames, or null for all
					@DataOnly = 1 to only return data files
					@LogsOnly = 1 to only return log files 
					@ExcludeSystem = 0 to exclude, 1 to include system databases
					@Summary = Display multiple recordsets showing overall totals
					1 = Overall totals for specified db's, or all
					2 = Summary of used/Unused by database
					3 = Summary of disk space per drive and database utilization of said drives
**  Notes:			Dynamic SQL is used to support the FILEPROPERTY function
					Also because sysaltfiles is not always accurate
**	Date:			2008.06.27
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090504	Matt Stanford	Moved the conversion of data types from INT to NUMERIC(10,2) to the admin db, so that we avoid ARITHABORT errors
**	20090504	Adam Bean		Added @Summary paramenter, added _GB into totals
**	20090519	Matt Stanford	Added @Summary = 2 option for data collection
**	20090608	Matt Stanford	Fixed the offline bug, also fixed some terrible math errors in @Summary = 1
**  20090622	Adam Bean		Added Unused to @Summary 2
**	20090630	Matt Stanford	Added Next Growth Size - Fixed Growth_MB math error in 2000 version
**	20090706	Matt Stanford	Fixed huge join error in @Summary = 2
**	20090706	Adam Bean		Removed underscores from column names in favor of camel case
								Reformatted @Summary = 1
**	20090714	Matt Stanford	Added @Summary = 3 for Drive rollup summary
**  20090728	Adam Bean		Fixed for case sensitive collation
**	20090911	Adam Bean		Added @DBNameExclude
**	20090929	Adam Bean		Global header formatting & general clean up
**  20091006	Adam Bean		Added @ExcludeSystem to add ability to remove system databases
								Added @DataOnly, @LogsOnly to be able to return only data or logs
********************************************************************************************************/

CREATE PROCEDURE [dbo].[SpaceUsed]
(
	@DBName				NVARCHAR(512)	= NULL
	,@DBNameExclude		NVARCHAR(512)	= NULL
	,@DataOnly			BIT				= 0	
	,@LogsOnly			BIT				= 0	
	,@ExcludeSystem		BIT				= 0	
	,@Summary			TINYINT			= 0
)

AS

SET NOCOUNT ON

DECLARE
	@DatabaseName		sysname
	,@SQL				VARCHAR(4000)
	
IF OBJECT_ID ('tempdb.dbo.#DBSizes') IS NOT NULL
	DROP TABLE #DBSizes
	
IF OBJECT_ID ('tempdb.dbo.#FixedDrives') IS NOT NULL
	DROP TABLE #FixedDrives

CREATE TABLE #DBSizes
(
	[DBID]				INT
	,[DBName]			sysname
	,[Type]				CHAR(4)
	,[LogicalName]		sysname
	,[FileGroup]		sysname NULL
	,[PhysicalName]		sysname
	,[Size]				INT
	,[SizeMB]			AS CAST(ROUND([Size]/128.0,2) AS NUMERIC(10,2))
	,[Used]				INT
	,[UsedMB]			AS CAST(ROUND([Used]/128.0,2) AS NUMERIC(10,2))
	,[Unused]			INT
	,[UnusedMB]			AS CAST(ROUND([Unused]/128.0,2) AS NUMERIC(10,2))
	,[MaxSize]			INT NULL
	,[MaxSizeMB]		AS CAST(ROUND([MaxSize]/128.0,2) AS NUMERIC(10,2))
	,[GrowthPercent]	INT NULL
	,[GrowthSize]		INT NULL
	,[GrowthMB]			AS CAST(ROUND([GrowthSize]/128.0,2) AS NUMERIC(10,2))
)

DECLARE #sizes CURSOR FOR
SELECT 
	[name]
FROM [sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item]
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item]
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[source_database_id] IS NULL
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database(s), or all
AND de.[item] IS NULL -- All but excluded databases
AND (s.[database_id] > 4 OR @ExcludeSystem = 0) -- Exclude system databases if specified
ORDER BY 1

OPEN #sizes
FETCH NEXT FROM #sizes INTO @DatabaseName
WHILE @@FETCH_STATUS = 0
BEGIN

SET @SQL = 
'
	USE [' + @DatabaseName + ']
	INSERT INTO #DBSizes
	([DBID], [DBName], [Type], [LogicalName], [FileGroup], [PhysicalName], [Size], [Used], [Unused], [MaxSize], [GrowthPercent], [GrowthSize])
	SELECT 
		DB_ID()																				AS [DBID]
		,DB_NAME(DB_ID())																	AS [DBName]
		,CASE
			WHEN ([status] & 64 <> 0) THEN ''Log''
			ELSE ''Data''
		END																					AS [Type]
		,[name]																				AS [LogicalName]
		,FILEGROUP_NAME([groupid])															AS [FileGroup]
		,[filename]																			AS [PhysicalName]
		,[size]																				AS [Size]
		,FILEPROPERTY([name],''SpaceUsed'')													AS [Used]
		,([size]-FILEPROPERTY([name],''SpaceUsed''))										AS [Unused]
		,CASE 
			WHEN [maxsize] >= 0 THEN [maxsize]
			ELSE NULL
		END																					AS [MaxSize]
		,CASE 
			WHEN ([status] & 0x100000 != 0) THEN [growth]
		END																					AS [Growth_Percent]         
		,CASE 
			WHEN ([status] & 0x100000  = 0) THEN [growth]
		END																					AS [GrowthSize]         
	FROM [sysfiles]
'
EXEC (@SQL)

FETCH NEXT FROM #sizes INTO @DatabaseName
END
CLOSE #sizes
DEALLOCATE #sizes

IF @Summary IN (0,1)
BEGIN
	-- Return results
	SELECT 
		[DBID]
		,[DBName]
		,[Type]
		,[LogicalName]
		,[FileGroup]
		,[PhysicalName]
		,[SizeMB]
		,[UsedMB]
		,[UnusedMB]
		,[MaxSizeMB]
		,[GrowthPercent]
		,[GrowthMB]
		,CAST(ROUND(CASE 
			WHEN [SizeMB] > 0
				THEN [UsedMB] / [SizeMB] * 100.
			ELSE 0 
		END,1) AS NUMERIC (4,1)) AS [PercentUsed]
		,CAST(ROUND(CASE 
			WHEN [GrowthPercent] IS NOT NULL 
				THEN [SizeMB] * [GrowthPercent] / 100.
			WHEN [GrowthMB] IS NOT NULL
				THEN [GrowthMB]
			ELSE 0
		END,2) AS NUMERIC(10,2)) AS [NextGrowthMB]
	FROM #DBSizes
	WHERE (([Type] = 'Data' AND @DataOnly = 1) OR @DataOnly = 0) -- Only show data files
	AND (([Type] = 'Log' AND @LogsOnly  = 1) OR @LogsOnly = 0) -- Only show log files
	ORDER BY 2, 3
END

IF @Summary = 1
BEGIN
	-- Return total by type
	SELECT
		SUM(d.[DataSizeMB])					AS [TotalDataMB]
		,SUM(d.[DataSizeMB]) / 1024			AS [TotalDataGB]
		,SUM(d.[DataSizeUsedMB])			AS [TotalDataUsedMB]
		,SUM(d.[DataSizeUsedMB]) / 1024		AS [TotalDataUsedGB]
		,SUM(d.[DataSizeUnusedMB])			AS [TotalDataUnusedMB]
		,SUM(d.[DataSizeUnusedMB]) / 1024	AS [TotalDataUnusedGB]
		,SUM(l.[LogSizeMB])					AS [TotalLogMB]
		,SUM(l.[LogSizeMB]) / 1024			AS [TotalLogGB]
		,SUM(l.[LogSizeUsedMB])				AS [TotalLogUsedMB]
		,SUM(l.[LogSizeUsedMB]) / 1024		AS [TotalLogUsedGB]
		,SUM(l.[LogSizeUnusedMB])			AS [TotalLogUnusedMB]
		,SUM(l.[LogSizeUnusedMB]) / 1024	AS [TotalLogUnusedGB]
	FROM 
	(
		SELECT 
			t.[DBName]
			,SUM(t.[SizeMB])				AS [DataSizeMB]
			,SUM(t.[UsedMB])				AS [DataSizeUsedMB]
			,SUM(t.[UnusedMB])				AS [DataSizeUnusedMB]
		FROM #DBSizes t
		WHERE t.[Type] = 'Data'
		GROUP BY t.[DBName]
	) d
	JOIN 
	(
		SELECT 
			t.[DBName]
			,SUM(t.[SizeMB])				AS [LogSizeMB]
			,SUM(t.[UsedMB])				AS [LogSizeUsedMB]
			,SUM(t.[UnusedMB])				AS [LogSizeUnusedMB]
		FROM #DBSizes t
		WHERE t.[Type] = 'Log'
		GROUP BY t.[DBName]
	) l
		ON d.[DBName] = l.[DBName]

	-- Return overal totals
	SELECT
		SUM([SizeMB])						AS [TotalMB]
		,SUM([SizeMB]) / 1024				AS [TotalGB]
		,SUM([UsedMB])						AS [TotalUsedMB]
		,SUM([UsedMB]) / 1024				AS [TotalUsedGB]
		,SUM([UnusedMB])					AS [TotalUnusedMB]
		,SUM([UnusedMB]) / 1024				AS [TotalUnusedGB]
	FROM #DBSizes
	WHERE (([Type] = 'Data' AND @DataOnly = 1) OR @DataOnly = 0) -- Only show data files
	AND (([Type] = 'Log' AND @LogsOnly  = 1) OR @LogsOnly = 0) -- Only show log files		
END

IF @Summary = 2
BEGIN
	SELECT
		d.[DBName]
		,d.[DataSizeMB]
		,d.[DataSizeUnusedMB]
		,l.[LogSizeMB]
		,l.[LogSizeUnusedMB]
	FROM 
	(
		SELECT 
			t.[DBName]
			,SUM(CAST(ROUND(t.[SizeMB],0) AS INT))		AS [DataSizeMB]
			,SUM(CAST(ROUND(t.[UnusedMB],0) AS INT))	AS [DataSizeUnusedMB]
		FROM #DBSizes t
		WHERE t.[Type] = 'Data'
		GROUP BY t.[DBName]
	) d
	JOIN 
	(
		SELECT 
			t.[DBName]
			,SUM(CAST(ROUND(t.[SizeMB],0) AS INT))		AS [LogSizeMB]
			,SUM(CAST(ROUND(t.[UnusedMB],0) AS INT))	AS [LogSizeUnusedMB]
		FROM #DBSizes t
		WHERE t.[Type] = 'Log'
		GROUP BY t.[DBName]
	) l
		ON d.[DBName] = l.[DBName]
END

IF @Summary = 3
BEGIN

	CREATE TABLE #FixedDrives 
	(
		[drive]				VARCHAR(10)
		,[MBfree]			BIGINT
	)

	INSERT INTO #FixedDrives
	EXEC [master].[dbo].[xp_fixeddrives]

	SELECT 
		LEFT([PhysicalName],1)							AS [DriveName]
		,COUNT(*)										AS [FileCount]
		,SUM([SizeMB])									AS [SQLTotalSizeMB]
		,CAST(ROUND(CASE
			WHEN SUM([SizeMB]) > 0
				THEN SUM([UsedMB]) / SUM([SizeMB]) * 100 
			ELSE 0
		END,1) AS NUMERIC(4,1))							AS [SQLPercentUsed]
		,CAST(ROUND(CASE
			WHEN SUM([SizeMB]) > 0
				THEN SUM([UnUsedMB]) / SUM([SizeMB]) * 100 
			ELSE 0
		END,1) AS NUMERIC(4,1))							AS [SQLPercentUnused]
		,MAX(d.[MBfree])								AS [DiskMBUnused]
	FROM #DBSizes o
	INNER JOIN #FixedDrives d
		ON d.[drive] = LEFT([PhysicalName],1)
	WHERE (([Type] = 'Data' AND @DataOnly = 1) OR @DataOnly = 0) -- Only show data files
	AND (([Type] = 'Log' AND @LogsOnly  = 1) OR @LogsOnly = 0) -- Only show log files
	GROUP BY LEFT([PhysicalName],1)
	ORDER BY 1
END

SET NOCOUNT OFF

