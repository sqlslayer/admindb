IF OBJECT_ID('[dbo].[SnapshotDB]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[SnapshotDB]
GO

/******************************************************************************
* Name
	[dbo].[SnapshotDB]

* Author
	Matt Stanford (SQLSlayer.com)
	
* Date
	2008.01.24
	
* Synopsis
	Creates a snapshot of a specified database
	
* Description
	Creates a snapshot of a specified database

* Examples
	EXEC [dbo].[SnapshotDB] @DBName = 'admin'
	EXEC [dbo].[SnapshotDB] @DBName = 'admin', @SnapPrefix = 'Maintenance'

* Dependencies
	n/a

* Parameters
	@DBName			= Database to be snapshotted
	@SnapShotName	= Name of new snapshot, if not passed, one will be generated
	@DropSnapshots	= Pass a 1 to drop previous snapshots of this database
	@Debug			= Pass a 1 to see what would be run, no actions will be taken

* Notes
	Add the extended property "CHECKDB_CHECK" to any CHECK option that you want to override the logic of this procedure on
	Add the extended property "CHECKDB_WITH" to any WITH option that you want to override the logic of this procedure on
	For the value of these properties, simply add any options csv supported: 
	Example how to add: 
		EXEC [DBName].sys.sp_addextendedproperty @name=N'CHECKDB_CHECK', @value=N'NOINDEX' 
		EXEC [DBName].sys.sp_addextendedproperty @name=N'CHECKDB_WITH', @value=N'TABLOCK,EXTENDED_LOGICAL_CHECKS' 
*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20080221	Adam Bean		Added ability to drop previous snapshots
	20090929	Adam Bean		Global header formatting & general clean up
	20140224	Adam Bean		Added @UseDateSuffix and @SnapSuffix, reworked how the name is created by default
	20140506	Adam Bean		Added @SnapPrefix and defaulted to SNAPSHOT
******************************************************************************/

CREATE PROCEDURE [dbo].[SnapshotDB] 
(
	@DBName				VARCHAR(256)
	,@SnapshotName		VARCHAR(512)	= NULL OUTPUT
	,@SnapPrefix		VARCHAR(50)		= 'SNAPSHOT'
	,@SnapSuffix		VARCHAR(50)		= NULL
	,@UseDateSuffix		BIT				= 1
	,@DropSnapshots		TINYINT			= 0
	,@SupressOutput		BIT				= 0
	,@Debug				TINYINT			= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@TimeStamp			VARCHAR(16)
	,@DropDBName		VARCHAR(256)
	,@SQLCmd			VARCHAR(MAX)
	,@DropCmd			VARCHAR(500)
	,@FileID			INT
	,@tmpCmd			VARCHAR(4000)
	,@Comma				VARCHAR(2)
	,@DestLogPath		VARCHAR(512)
	,@DestDataPath		VARCHAR(512)

IF @Debug = 1 
	PRINT ('/* Debug Mode */')

SET @TimeStamp = (SELECT REPLACE(CONVERT(VARCHAR(10),getdate(),111),'/','') + '_' + REPLACE(CONVERT(VARCHAR(10),getdate(),114),':',''))

-- Set the snapshot name
IF @SnapshotName IS NULL
BEGIN
	-- Add the prefix and suffix
	SET @SnapshotName = @SnapPrefix + '_' + @DBName + '_' + ISNULL(@SnapSuffix,'')	
	-- Add the time stamp
	IF @UseDateSuffix = 1
		SET @SnapshotName = @SnapshotName + '_' + @TimeStamp
END

-- Drop the previous snapshots if desired
IF @DropSnapshots = 1
BEGIN
	DECLARE #DropSnapshotDB CURSOR FAST_FORWARD FOR
	SELECT [name] FROM [dbo].[sysdatabases_vw]
	WHERE [source_database_id] = DB_ID(@DBName)

	OPEN #DropSnapshotDB
	FETCH NEXT FROM #DropSnapshotDB INTO @DropDBName
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @DropCmd = 'DROP DATABASE [' + @DropDBName + ']'
	
		IF @Debug=1
		BEGIN
			PRINT(@DropCmd)
		END
		ELSE
		BEGIN
			EXEC(@DropCmd)
			IF @SupressOutput = 0
				PRINT 'DROPPED SnapShot ' + @DropDBName + ''
		END

		FETCH NEXT FROM #DropSnapshotDB INTO @DropDBName
	END
	CLOSE #DropSnapshotDB
	DEALLOCATE #DropSnapshotDB
END
ELSE
BEGIN
-- Make sure there isn't already a snapshot with the same name, if so, drop it
	SET @DropCmd = 
		'
			IF EXISTS(SELECT * FROM master.sys.databases 
			WHERE name = ''' + @SnapshotName + ''' 
			AND source_database_id IS NOT NULL)
			BEGIN
				IF ' + CAST(@SupressOutput AS VARCHAR) + ' = 0
					PRINT (''Previous Snapshot Detected...'')

				DROP DATABASE [' + @SnapshotName + ']

				IF ' + CAST(@SupressOutput AS VARCHAR) + ' = 0
					PRINT ''DROPPED SnapShot ' + @SnapshotName + '''
			END
		'
	IF @Debug=1
	BEGIN
		PRINT(@DropCmd)
	END
	ELSE
	BEGIN
		EXEC(@DropCmd)
	END
END

-- Setup table to hold database info
DECLARE	@Files TABLE 
	(
		[FileID]		[INT] IDENTITY,
		[Processed]		[TINYINT] DEFAULT 0,
		[IsLogFile]		[VARCHAR](10),
		[LogicalName]	[VARCHAR](512),
		[OrigFileName]	[VARCHAR](512),
		[NewFileName]	[VARCHAR](512),
		[CMD]			AS '(NAME = ' + LogicalName + ', FILENAME = ''' + NewFileName + ''')'
	) 

-- Insert data for specified database
INSERT INTO @Files 
(IsLogFile, LogicalName, OrigFileName)
SELECT 
	type
	,name
	,physical_name
FROM 
	master.sys.master_files
WHERE database_id = DB_ID(@DBName)

-- Find the directories to put the files in
SELECT
	 @DestLogPath	= SUBSTRING(OrigFileName,0,LEN(OrigFileName)-CHARINDEX('\',REVERSE(OrigFileName))+2)
FROM 
	@Files 
WHERE IsLogFile = 1

SELECT TOP 1
	 @DestDataPath	= SUBSTRING(OrigFileName,0,LEN(OrigFileName)-CHARINDEX('\',REVERSE(OrigFileName))+2)
FROM 
	@Files 
WHERE IsLogFile = 0

-- Create the new file names
UPDATE 
	@Files 
SET	
	Processed		= 1
	,NewFileName	= REPLACE(OrigFileName,@DBName,@SnapshotName)
	--,NewFileName	= REPLACE(OrigFileName,@DBName,@SnapFilePrefix + @SnapshotName)
WHERE CHARINDEX(@DBName,OrigFileName) > 0

UPDATE
	@Files
SET
	Processed		= 1
	,NewFileName	= '\' + @DBName + '_' + @SnapshotName + '_' + LTRIM(STR(FileID)) + SUBSTRING(OrigFileName,LEN(OrigFileName) - CHARINDEX('.',REVERSE(OrigFileName)) + 1,100)
WHERE Processed = 0

-- Set the paths for the new file names
UPDATE 
	@Files
SET
	NewFileName		= @DestDataPath + RIGHT(NewFileName,CHARINDEX('\',REVERSE(NewFileName)) - 1)
WHERE IsLogFile = 0

UPDATE 
	@Files
SET
	NewFileName		= @DestLogPath + RIGHT(NewFileName,CHARINDEX('\',REVERSE(NewFileName)) - 1)
WHERE IsLogFile = 1

-- Build the create string
Set @FileID = (SELECT MIN(FileID) FROM @Files WHERE IsLogFile = 0)
SET @SQLCmd = ''
WHILE @FileID <= (SELECT MAX(FileID) from @Files WHERE IsLogFile = 0)
BEGIN

	IF @FileID = (SELECT MIN(FileID) FROM @Files WHERE IsLogFile = 0)
		SET @Comma = CHAR(10)
	ELSE
		SET @Comma = ',' + CHAR(10)

	IF (SELECT CMD FROM @Files WHERE FileID = @FileID AND IsLogFile = 0) IS NOT NULL
		SET @SQLCmd = @SQLCmd + @Comma + (SELECT CMD FROM @Files where FileID = @FileID AND IsLogFile = 0)

	SET @FileID = @FileID + 1

END

SET @SQLCmd = 'CREATE DATABASE [' + @SnapshotName + ']' + CHAR(10) + 'ON' + @SQLCmd
SET @SQLCmd = @SQLCmd + CHAR(10) + 'AS SNAPSHOT OF ' + @DBName

IF @Debug = 1
	PRINT (@SQLCmd)
ELSE
	EXEC (@SQLCmd)
	IF @SupressOutput = 0
		PRINT 'CREATED Snapshot ' + @SnapshotName