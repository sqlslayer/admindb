IF OBJECT_ID('[dbo].[DecryptDB]','P') IS NOT NULL 
       DROP PROCEDURE [dbo].[DecryptDB]
GO

/******************************************************************************
* Name
	[dbo].[DecryptDB]

* Author
	Jeff Gogel (SQLSlayer.com)
	
* Date
	2012.09.20
	
* Synopsis
	Remove TDE encryption from databases.
	
* Description
	Procedure to remove TDE encryption.  @DropDEK is enabled by default, so the database encryption keys will also be dropped.

* Examples
	EXEC dbo.DecryptDB @DBName = 'TestDB'

* Dependencies
	dbo.Split_fn
	dbo.sysdatabases_vw
	dbo.EncryptedDBs_vw

* Parameters
	@DBName = Database name(s) to run code against (CSV supported, NULL for all)
	@DBNameExclude = Database name(s) to exclude from code (CSV supported)
	@DropDEK = Drops database encryption key after turning encryption off
	@Debug = Show, but don't execute work to be done
	
* Notes


*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
	12272016	Jeff Gogel		Removed ORDER BY ordinal column number
******************************************************************************/

CREATE PROCEDURE [dbo].[DecryptDB]
(
       @DBName             NVARCHAR(2048)   = NULL
       ,@DBNameExclude		NVARCHAR(2048)	= NULL
	   ,@DropDEK			BIT				= 1
       ,@Debug              BIT				= 0
)

AS

DECLARE @SQLVersion			INT
		,@SQLEdition		VARCHAR(32)
		,@EncryptionState	VARCHAR(64)
		,@SQL				NVARCHAR(MAX)
				


SET @SQLVersion = @@MICROSOFTVERSION / 0x01000000
SET @SQLEdition = (SELECT CAST(SERVERPROPERTY('EDITION') AS VARCHAR))

-- check requirements
IF (@SQLVersion < 10)
BEGIN
	PRINT 'Transparent Data Encryption is only supported on SQL Server 2008 or later.'
	RETURN
END
IF (@SQLEdition NOT LIKE 'Developer%' AND @SQLEdition NOT LIKE 'Enterprise%')
BEGIN
	PRINT 'Transparent Data Encryption is only supported on Developer and Enterprise edition.'
	RETURN
END
IF NOT  EXISTS (SELECT name FROM [master].[sys].[symmetric_keys] WHERE name = '##MS_DatabaseMasterKey##')
BEGIN
	PRINT 'There is no database master key for the master database therefore no databases are encrypted.'
	RETURN
END

-- decrypt the db's
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [admin].[dbo].[sysdatabases_vw] s
LEFT JOIN [admin].[dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [admin].[dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

IF (EXISTS (SELECT DBName FROM [admin].[dbo].[EncryptedDBs_vw] WHERE DBName = @DBName))
BEGIN

	SELECT @EncryptionState = EncryptionState FROM [admin].[dbo].[EncryptedDBs_vw] WHERE DBName = @DBName
	SET @SQL = ''

	IF (@EncryptionState <> 'Encrypted' AND @EncryptionState <> 'Unencrypted')
	BEGIN
		PRINT @DBName + ' cannot be decrypted because the current encryption state is ''' + @EncryptionState + '''.'
	END
	ELSE
		BEGIN TRY
			IF (@EncryptionState = 'Encrypted')
			BEGIN
				SET @SQL = '

USE [master];
ALTER DATABASE [' + @DBName + '] SET ENCRYPTION OFF WITH ROLLBACK IMMEDIATE;'
			END

			IF (@Debug=1)
				PRINT @SQL
			ELSE
				EXEC (@SQL)

			IF (@DropDEK = 1)
			BEGIN
				-- Wait for decryption, otherwise you can't drop the key
				IF (@Debug <> 1)
				BEGIN
					WHILE (SELECT EncryptionState FROM [admin].[dbo].[EncryptedDBs_vw] WHERE DBName = @DBName) <> 'Unencrypted'
					BEGIN
						WAITFOR DELAY '00:00:00:20';
					END
				END

				SET @SQL = '

USE [' + @DBName + '];
DROP DATABASE ENCRYPTION KEY;'
				
				IF (@Debug=1)
					PRINT @SQL
				ELSE
					EXEC (@SQL)

				PRINT @DBName + ' is now decrypted.'
			END
		END TRY
		BEGIN CATCH
			PRINT 'Error Number ' + CAST(ERROR_NUMBER() AS VARCHAR) + ': (' + @DBName + ') ' + ERROR_MESSAGE()
		END CATCH
END

FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs