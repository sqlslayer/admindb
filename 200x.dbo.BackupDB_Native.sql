﻿IF OBJECT_ID('[dbo].[BackUpDB_Native]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[BackUpDB_Native]
GO

/*******************************************************************************************************
**	Name:			dbo.BackUpDB_Native
**	Desc:			Runs full SQL Server database backups with Native support
**	Auth:			Adam Bean (SQLSlayer.com)
**  Parameters:		Inherited from BackupDB
**	Notes:			Should not be run as a stand alone procedure. Use BackupDB.
**	Date:			2008.10.13
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090820	Adam Bean		Fixed for case sensitive collation
**	20090929	Adam Bean		Global header formatting & general clean up
**  20110216    John DelNostro  Added @CopyOnly Option
**	20150803	Jeff Gogel		Added @Compression = 0 option to include NO_COMPRESSION parameter
**	20160307	Jeff Gogel		Added @FileCount support for backup to multiple files 
********************************************************************************************************/

CREATE PROCEDURE [dbo].[BackUpDB_Native] 
(
	@DBName					sysname
	,@BackupType			CHAR(1)
	,@BackupLocation		VARCHAR(512)
	,@BackupName			VARCHAR(256)
	,@File					VARCHAR(4000)	
	,@FileGroup				VARCHAR(4000)
	,@Init					BIT				
	,@RetainDays			TINYINT
	,@Stats					TINYINT
	,@Description			VARCHAR(512)		
	,@Verify				BIT
	,@Password				VARCHAR(4000)
	,@Compression			TINYINT
	,@CopyOnly	            BIT
	,@FileCount				INT					= 1
	,@Debug					BIT				
)

AS

SET NOCOUNT ON

DECLARE
	@Comma					VARCHAR(2)
	,@Tab					VARCHAR(8)
	,@NewLine				VARCHAR(8)
	,@InitValue				VARCHAR(8)
	,@FileCountdown			INT
	,@MultiFileExtension	VARCHAR(8)
	,@SQL					VARCHAR(4000)

-- Populate some variables
SET @Comma = CHAR(44)
SET @Tab = CHAR(9)
SET @NewLine = CHAR(10)
SET @FileCountdown = @FileCount

-- Test inputs
IF @Init = 1
	SET @InitValue = 'INIT'
ELSE
	SET @InitValue = 'NOINIT'
	
-- Build our backup command
SET @SQL = @NewLine
SET @SQL = @SQL + 'BACKUP'
IF @BackupType = 'L' 
	SET @SQL = @SQL + ' LOG'
ELSE 
	SET @SQL = @SQL + ' DATABASE'
SET @SQL = @SQL + ' [' + @DBName + ']'
IF @BackupType = 'F'
	SET @SQL = @SQL + ' FILE = ''' + @File + ''''
ELSE IF @BackupType = 'G'
	SET @SQL = @SQL + ' FILEGROUP = ''' + @FileGroup + '''  '
IF (@FileCount = 1)
	SET @SQL = @SQL + ' TO DISK = ''' + @BackupName + '''' + @NewLine
ELSE -- Backup to mutiple files
BEGIN
	SET @MultiFileExtension = '_File' + CAST(@FileCountdown AS VARCHAR) + '.'
	SET @SQL = @SQL + ' TO DISK = ''' + REPLACE(@BackupName,'.',@MultiFileExtension) + '''' + @NewLine
	SET @FileCountdown = @FileCountdown - 1

	WHILE (@FileCountdown > 0)
	BEGIN
		SET @MultiFileExtension = '_File' + CAST(@FileCountdown AS VARCHAR) + '.'
		SET @SQL = @SQL + ', DISK = ''' + REPLACE(@BackupName,'.',@MultiFileExtension) + '''' + @NewLine
		SET @FileCountdown = @FileCountdown - 1
	END
END
SET @SQL = @SQL + @Tab + 'WITH ' + @InitValue + @NewLine
IF @Stats IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + 'STATS = ' + CAST(@Stats AS VARCHAR) + '' + @NewLine
IF @BackupType = 'I'
	SET @SQL = @SQL + @Tab + @Comma + 'DIFFERENTIAL' + @NewLine
IF @Password IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + 'PASSWORD  = ''' + @Password + '''' + @NewLine
IF @Compression = 1
	SET @SQL = @SQL + @Tab + @Comma + 'COMPRESSION' + @NewLine
ELSE IF @Compression = 0
	SET @SQL = @SQL + @Tab + @Comma + 'NO_COMPRESSION' + @NewLine
IF @CopyOnly = 1
	SET @SQL = @SQL + @Tab + @Comma + 'COPY_ONLY' + @NewLine
IF @RetainDays IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + 'RETAINDAYS = ' + CAST(@RetainDays AS VARCHAR) + '' + @NewLine
IF @Description IS NOT NULL
	SET @SQL = @SQL + @Tab + @Comma + 'DESCRIPTION = ''' + @Description + '''' + @NewLine
IF @Verify = 1
BEGIN
	IF @Password IS NULL
		SET @SQL = @SQL + @NewLine + 'RESTORE VERIFYONLY FROM DISK = ''' + @BackupName + ''''		
	ELSE
		SET @SQL = @SQL + @NewLine + 'RESTORE VERIFYONLY FROM DISK = ''' + @BackupName + ''' WITH PASSWORD = ''' + @Password + ''''
END

IF @Debug = 1
	PRINT (@SQL)
ELSE
	EXEC (@SQL)
	
SET NOCOUNT OFF