IF OBJECT_ID('[dbo].[SQLServerJobLogDir_fn]','FN') IS NOT NULL 
	DROP FUNCTION [dbo].[SQLServerJobLogDir_fn]
GO

/*******************************************************************************************************
**	Name:			dbo.SQLServerJobLogDir_fn()
**	Desc:			Return the SQL Server Agent log directory
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2009.11.01
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.	
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**
********************************************************************************************************/

CREATE FUNCTION [dbo].[SQLServerJobLogDir_fn]()
RETURNS NVARCHAR(4000)

AS

BEGIN

	DECLARE 
		@dir NVARCHAR(4000)

	EXEC [master].[dbo].[xp_instance_regread] 'HKEY_LOCAL_MACHINE','Software\Microsoft\MSSQLServer\SQLServerAgent','WorkingDirectory', @dir OUTPUT, 'no_output'

	RETURN @dir

END