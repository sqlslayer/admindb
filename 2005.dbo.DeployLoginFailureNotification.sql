IF OBJECT_ID('[dbo].[DeployLoginFailureNotification]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[DeployLoginFailureNotification]
GO


/******************************************************************************
* Name
	[dbo].[DeployLoginFailureNotification]

* Author
	John DelNostro
	
* Date
	2012.01.15
	
* Synopsis
	Wrapper procedure to deploy the LoginFailureNotification procedure
	
* Description
	Wrapper procedure is required as the procedure can not accept email recipients as a parameter because
	a queue can not call a procedure with a parameter. 

* Examples
	EXEC [dbo].[DeployLoginFailureNotification] @Recipients = 'you@domain.com'

* Dependencies
	n/a

* Parameters
	@Recipients			- Email recipients of the notification
	@Debug				- Show the procedure to be deployed but do not execute
	
* Notes
	This procedure will also setup the queue, service and event notification.

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [dbo].[DeployLoginFailureNotification]
(
	@Recipients		VARCHAR(1024)
	,@Debug			BIT	= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@SQL		VARCHAR(MAX)
	,@DBName	NVARCHAR(128)

-- Replace commas with semi-colons to work properly for database mail
SET @Recipients = REPLACE(@Recipients, ',', ';')

-- Find our current database
SET @DBName = (SELECT DB_NAME())

IF @Debug = 0
	BEGIN
	-- Setup service broker
	IF NOT EXISTS
	(
		SELECT 
			[name] 
		FROM [dbo].[sysdatabases_vw] 
		WHERE [is_broker_enabled] = 1
		AND [name] = @DBName
	)
	BEGIN
		EXEC('ALTER DATABASE [' + @DBName + '] SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE')
	END

	-- Setup queue
	IF NOT EXISTS (SELECT [name] FROM [sys].[service_queues] WHERE [name] = N'LoginFailureNotificationQueue')
	CREATE QUEUE [dbo].[LoginFailureNotificationQueue] 
		WITH STATUS = OFF
		,RETENTION = OFF
		,ACTIVATION (STATUS = OFF, PROCEDURE_NAME = [dbo].[LoginFailureNotification]
		,MAX_QUEUE_READERS = 5
		,EXECUTE AS OWNER)
	ON [PRIMARY] 

	-- Setup service
	IF NOT EXISTS (SELECT [name] FROM [sys].[services] WHERE [name] = N'LoginFailureNotificationService')
	CREATE SERVICE [LoginFailureNotificationService] AUTHORIZATION [dbo] 
	ON QUEUE [dbo].[LoginFailureNotificationQueue] ([http://schemas.microsoft.com/SQL/Notifications/PostEventNotification])

	-- Setup event notification
	IF NOT EXISTS (SELECT [name] FROM [sys].[server_event_notifications] WHERE [name] = 'LoginNotification')
	CREATE EVENT NOTIFICATION LoginFailureNotification
		ON SERVER
		FOR AUDIT_LOGIN_FAILED
		TO SERVICE 'LoginFailureNotificationService', 'current database'

		
END

-- Create the procedure
SET @SQL = 
('
IF OBJECT_ID(''admin.[dbo].[LoginFailureNotification]'',''P'') IS NOT NULL
	DROP PROCEDURE [dbo].[LoginFailureNotification]
	
EXEC
(''

/******************************************************************************
* Name
	[dbo].[LoginFailureNotification]

* Author
	John DelNostro
	
* Date
	2012.06.19
	
* Synopsis
	Procedure to notify recipients of Login failures.
	
* Description
	Procedure which will send an HTML formatted email showing the last three states of logins on said instance.
	The procedure is fired via service broker.

* Examples
	EXEC [dbo].[LoginFailureNotification]

* Dependencies
	[dbo].[DeployLoginFailureNotification]

* Parameters
	n/a
	
* Notes
	To only be created from the wrapper procedure: [dbo].[DeployLoginFailureNotification]

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	
******************************************************************************/

CREATE PROCEDURE [dbo].[LoginFailureNotification]
WITH EXECUTE AS OWNER
AS 

SET NOCOUNT ON

DECLARE 
	@message_body		XML
	,@email_message		NVARCHAR(MAX)
	,@Subject			VARCHAR(150)
	,@EmailHeader		VARCHAR(2048)
	,@EmailBody			VARCHAR(MAX)
	,@EmailCaption		VARCHAR(128)
	,@EmailFooter		VARCHAR(2048)
	,@Recipients		VARCHAR(MAX)
	,@Count				INT
	,@html				NVARCHAR(MAX)


SET @Recipients	= ''''' + @Recipients + '''''
SET @Subject	= ''''Login Audit Notification - Failed Login On '''' + @@SERVERNAME 

-- Parse XML Into Temp Table
DECLARE @EventTable TABLE
(   
	[EventType]			NVARCHAR(128)
	,[EventPostTime]	NVARCHAR(25)
	,[DatabaseName]		NVARCHAR(256)
	,[LoginName]		NVARCHAR(100)
	,[LoginType]		NVARCHAR(50)
	,[NTUserName]		NVARCHAR(100)
	,[NTDomainName]		NVARCHAR(100)
	,[SessionLoginName]	NVARCHAR(100)
	,[ApplicationName]	NVARCHAR(250)
	,[HostName]			NVARCHAR(50)
	,[ClientProcessID]	NVARCHAR(50)
	,[ErrorMessage]		NVARCHAR(MAX)
)

WHILE (1 = 1)
BEGIN
	BEGIN TRANSACTION
	-- Receive the next available message FROM the queue
	WAITFOR 
	(
		RECEIVE TOP (1) CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/EventType)[1]'''',''''NVARCHAR(128)'''')			AS [EventType]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/PostTime)[1]'''',''''NVARCHAR(25)'''')				AS [EventPostTime]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/DatabaseName)[1]'''',''''NVARCHAR(256)'''')		AS [DatabaseName]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/LoginName)[1]'''',''''NVARCHAR(100)'''')			AS [LoginName]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/LoginType)[1]'''',''''NVARCHAR(50)'''')			AS [LoginType]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/NTUserName)[1]'''',''''NVARCHAR(100)'''')			AS [NTUserName]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/NTDomainName)[1]'''',''''NVARCHAR(100)'''')		AS [NTDomainName]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/SessionLoginName)[1]'''',''''NVARCHAR(100)'''')	AS [SessionLoginName]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/ApplicationName)[1]'''',''''NVARCHAR(250)'''')		AS [ApplicationName]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/HostName)[1]'''',''''NVARCHAR(50)'''')				AS [HostName]
					   ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/ClientProcessID)[1]'''',''''NVARCHAR(256)'''')		AS [ClientProcessID]
		               ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/TextData)[1]'''',''''NVARCHAR(MAX)'''')			AS [ErrorMessage]
		FROM [dbo].[LoginFailureNotificationQueue]		
		INTO @EventTable
	), TIMEOUT 5000
	-- If we didn''''t get anything, bail out
	IF (@@ROWCOUNT = 0)
	BEGIN
		ROLLBACK TRANSACTION
		BREAK
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
	END
END	

-- Build the email
SET @EmailCaption = ''''SQL Server Audit Notification For '''' + @@SERVERNAME 

-- Setup table to build body
DECLARE @EmailBodyContents TABLE 
(
	[html]			VARCHAR(MAX)
)

-- Setup table to build email
DECLARE @Email TABLE 
(
	[ID]			INT IDENTITY
   ,[html]			VARCHAR(MAX)
)

-- Build the email header
SELECT @EmailHeader = 
''''
<HTML>
<DIV ALIGN="CENTER">
<TABLE BORDER="1" CELLSPACING="5" CELLPADDING="1" ALIGN="CENTER">
<CAPTION>
	<H2>'''' + @EmailCaption + ''''</H2>
	<EM><i>The following Login Failures have occured on :'''' + @@SERVERNAME + ''''</i></EM>
</CAPTION>
<TR BGCOLOR="#C0C0C0">
	<TH>EventType</TH>
	<TH>EventPostTime</TH>
	<TH>DatabaseName</TH>
	<TH>LoginName</TH>
	<TH>LoginType</TH>
	<TH>NTUserName</TH>
	<TH>NTDomainName</TH>
	<TH>SessionLoginName</TH>
	<TH>ApplicationName</TH>
	<TH>HostName</TH>
	<TH>ClientProcessID</TH>
	<TH>ErrorMessage</TH>
</TR>
''''

-- Build the email footer
SELECT @EmailFooter =
''''
</TABLE>
</DIV>
</HTML>
''''

-- Build the email body
INSERT INTO @EmailBodyContents
SELECT
	''''<TR>
		<TD>'''' + E.[EventType]		+ ''''</TD>
		<TD>'''' + E.[EventPostTime]	+ ''''</TD>
		<TD>'''' + E.[DatabaseName]		+ ''''</TD>
		<TD>'''' + E.[LoginName]		+ ''''</TD>
		<TD>'''' + E.[LoginType]		+ ''''</TD>	
		<TD>'''' + E.[NTUserName]		+ ''''</TD>	
		<TD>'''' + E.[NTDomainName]		+ ''''</TD>	
		<TD>'''' + E.[SessionLoginName]	+ ''''</TD>	
		<TD>'''' + E.[ApplicationName]	+ ''''</TD>	
		<TD>'''' + E.[HostName]			+ ''''</TD>	
		<TD>'''' + E.[ClientProcessID]	+ ''''</TD>	
		<TD>'''' + E.[ErrorMessage]		+ ''''</TD>	

	</TR>
	'''' 
FROM ( 
		SELECT      
			A.[EventType]
			,REPLACE(REPLACE(A.[EventPostTime],''''T'''','''' ''''),''''.'''','''''''')		AS [EventPostTime]
			,A.[DatabaseName]
			,A.[LoginName]
			,ISNULL(A.[LoginType], ''''SQL Login'''')						AS LoginType
			,A.[NTUserName]
			,A.[NTDomainName]
			,A.[SessionLoginName]
			,A.[ApplicationName]
			,A.[HostName]
			,A.[ClientProcessID]
			,A.[ErrorMessage]
			,ROW_NUMBER() OVER (PARTITION BY [LoginName] ORDER BY [EventPostTime] DESC)	AS Row 
		FROM @EventTable A
      ) E

-- Build the email
INSERT INTO @Email
SELECT @EmailHeader
UNION ALL
SELECT *
FROM @EmailBodyContents
UNION ALL
SELECT @EmailFooter

-- Put it all together
SET @Count = 1
SET @EmailBody = ''''''''
WHILE @Count <= (SELECT COUNT(*) FROM @Email)
BEGIN
	SET @EmailBody = @EmailBody + (SELECT [html] FROM @Email WHERE [ID] = @Count)
	SET @Count = @Count + 1
END

-- Only send email if there is data
IF @Count > 3
BEGIN
	-- Send mail
	EXEC [msdb].[dbo].[sp_send_dbmail]
		@Recipients		= @Recipients
		,@Subject		= @Subject
		,@Body			= @EmailBody
		,@Body_format	= ''''HTML''''
END
'')
')

IF @Debug = 1
BEGIN
	PRINT '/*** DEBUG ENABLED ****/'
	PRINT (@SQL)
END
ELSE
BEGIN
	EXEC(@SQL)
	IF EXISTS (SELECT [name] FROM [sys].[objects] WHERE [name] = '[LoginFailureNotification]')
	BEGIN
		PRINT 'Successfully deployed [dbo].[LoginFailureNotification] with ''' + @Recipients + ''' as the recipient(s).'
	END
END