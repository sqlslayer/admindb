IF OBJECT_ID('dbo.whoLoggedIn_vw','V') IS NOT NULL 
	DROP VIEW [dbo].[whoLoggedIn_vw]
GO

/******************************************************************************
**	Name:			[dbo].[whoLoggedIn_vw]
**	Desc:			Query default trace and find last login times
**	Auth:			Adam Bean (SQLSlayer.com) [Original idea from Jack Corbett [http://www.ntm.org/wp/jack_corbett/]]
**	Dependancies:	dbo.DefaultTrace_vw
**	Date:			2010.02.07
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20100419	Adam Bean		Modified to point at DefaultTrace_vw
********************************************************************************************************/

CREATE VIEW [dbo].[whoLoggedIn_vw]

AS

SELECT 
	[LoginName]
	,[SessionLoginName]
	,[PrincipalName]
	,[NTUserName]
	,[DatabaseName]
	,MIN([StartTime])		AS [FirstUsed]
	,MAX([StartTime])		AS [LastUsed]
FROM [dbo].[DefaultTrace_vw]
GROUP BY 
	[LoginName]
	,[SessionLoginName]
	,[PrincipalName]
	,[NTUserName]
	,[DatabaseName]
