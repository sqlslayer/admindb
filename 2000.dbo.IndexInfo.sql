IF OBJECT_ID('[dbo].[IndexInfo]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[IndexInfo]
GO

SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
**	Name:			dbo.IndexInfo
**	Desc:			Retrieves all relevant index information 
**	Auth:			Adam Bean (SQLSlayer.com)
**  Parameters:		@DBName = Database to be queried
**					@TableName = (optional) retrieve index sizes for all indexes in specified tabled
**					@IndexSize = (optional) retrieve index sizes for specified index
**					@OrderBy1, 2, 3 = Columns to order output by
**	Date:			2008.03.12
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090713	Adam Bean		Added OrderBy arguments
								General cleanup
**	20090821	Adam Bean		Fixed default ordering and added AND i.[indid] < 255
								Resolved collation issues
**	20090929	Adam Bean		Global header formatting & general clean up
**	20100712	Adam Bean		Resolved collation issues for Latin1_General_BIN
********************************************************************************************************/

CREATE PROCEDURE [dbo].[IndexInfo] 
(
	@DBName				NVARCHAR(128)
	,@TableName			NVARCHAR(128)	= NULL
	,@IndexName			NVARCHAR(128)	= NULL
	,@OrderBy1			TINYINT = 1		-- TableName
	,@OrderBy2			TINYINT = 3		-- IndexName
	,@OrderBy3			TINYINT = 5		-- IndexSize
)

AS

SET NOCOUNT ON

DECLARE 
	@ObjectID		VARCHAR(1024)
	,@PageSize		INT

SELECT @PageSize = [low] / 1024 FROM [master].[dbo].[spt_values] WHERE [number] = 1 AND [type] = 'E'

-- Get all of the catalog views
-- Drop the tables if they exist
IF OBJECT_ID('tempdb.dbo.#sysindexes') IS NOT NULL
   DROP TABLE #sysindexes
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#showcontig') IS NOT NULL
	DROP TABLE #showcontig
IF OBJECT_ID('tempdb.dbo.#IndexInfo') IS NOT NULL
	DROP TABLE #IndexInfo

-- Create the tables
-- sysindexes
SELECT TOP 0 *, [statsdate] = CONVERT([VARCHAR](48),NULL) INTO #sysindexes FROM [dbo].[sysindexes]
INSERT INTO #sysindexes
EXEC('USE [' + @DBName + '] SELECT *,STATS_DATE([id],[indid]) FROM [dbo].[sysindexes]')

-- sysobjects
SELECT TOP 0 *, [schemaname] = CONVERT([VARCHAR](48),NULL) INTO #sysobjects FROM [dbo].[sysobjects]
INSERT INTO #sysobjects
EXEC('USE [' + @DBName + '] SELECT *, USER_NAME(uid) FROM [dbo].[sysobjects] WHERE type = ''U''')

-- Create table to capture SHOWCONTIG output
CREATE TABLE #showcontig
(
	[ObjectName]		VARCHAR (512)
	,[ObjectId]			INT
	,[IndexName]		VARCHAR (512)
	,[IndexId]			INT
	,[Lvl]				INT
	,[Pages]			INT
	,[Rows]				INT
	,[MinRecSize]		INT
	,[MaxRecSize]		INT
	,[AvgRecSize]		INT
	,[ForRecCount]		INT
	,[Extents]			INT
	,[ExtentSwitches]	INT
	,[AvgFreeBytes]		INT
	,[AvgPageDensity]	INT
	,[ScanDensity]		DECIMAL
	,[BestCount]		INT
	,[ActualCount]		INT
	,[LogicalFrag]		DECIMAL
	,[ExtentFrag]		DECIMAL
)

-- Capture the index statistics
DECLARE #IndexFrag CURSOR FAST_FORWARD FOR
SELECT [id] FROM #sysobjects

OPEN #IndexFrag
FETCH NEXT FROM #IndexFrag INTO @ObjectID
WHILE @@FETCH_STATUS = 0
BEGIN
	
	EXEC
	('
		USE [' + @DBName + ']
		INSERT INTO #showcontig
		EXEC(''DBCC SHOWCONTIG (' + @ObjectID + ') WITH ALL_INDEXES, TABLERESULTS, NO_INFOMSGS'')
	')

FETCH NEXT FROM #IndexFrag INTO @ObjectID
END
CLOSE #IndexFrag
DEALLOCATE #IndexFrag

-- Populate temp table with all the index information
SELECT 
	c.[ObjectName]		AS [TableName]
	,o.[schemaname]		AS [SchemaName]
	,c.[IndexName]
	,CASE i.[indid]
		WHEN 0 THEN 'HEAP'
		WHEN 1 THEN 'CLUSTERED'
		ELSE		'NON CLUSTERED'
	END					AS [IndexType]
	,CASE 
		WHEN i.[indid] > 1 AND i.[indid] < 255 THEN i.[used] * @PageSize
		WHEN i.[indid] = 1 THEN i.[dpages] * @PageSize
	END					AS [IndexSize]
	,i.[OrigFillFactor]	AS [IndexFillFactor]
	,i.[statsdate]		AS [LastStatsUpdate]
	,c.[Pages]
	,c.[Rows]
	,c.[MinRecSize]
	,c.[MaxRecSize]
	,c.[AvgRecSize]
	,c.[ForRecCount]
	,c.[ExtentSwitches]
	,c.[AvgFreeBytes]
	,c.[AvgPageDensity]
	,c.[ScanDensity]
	,c.[BestCount]
	,c.[ActualCount]
	,c.[LogicalFrag]
	,c.[ExtentFrag]
INTO #IndexInfo
FROM #sysobjects o 
JOIN #showcontig c
	ON c.[ObjectId] = o.[id]
JOIN #sysindexes i
	ON i.[id] = o.[id]
	AND i.[name] = c.[IndexName] COLLATE DATABASE_DEFAULT
WHERE (o.[name] = @TableName OR @TableName IS NULL)
AND (i.[name] = @IndexName OR @IndexName IS NULL)
AND i.[indid] > 0
AND i.[indid] < 255

-- Return results
EXEC('
		SELECT * FROM #IndexInfo
		ORDER BY ' + @OrderBy1 + ', ' + @OrderBy2 + ', ' + @OrderBy3 + '
	')

SET QUOTED_IDENTIFIER OFF
SET NOCOUNT OFF

-- Return status
IF @@ERROR <> 0
	RETURN 2
ELSE
	RETURN 0

