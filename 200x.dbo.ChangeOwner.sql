IF OBJECT_ID('[dbo].[ChangeOwner]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[ChangeOwner]
GO

/*******************************************************************************************************
**	Name:			dbo.ChangeOwner
**	Desc:			Change database(s) or job(s) owner(s)
**	Auth:			Adam Bean (SQLSlayer.com)
**  Dependancies:	dbo.Split_fn, dbo.sysdatabases_vw
**  Parameters:		@Type = Database (d & db will work as well) or Job (j will work as well)
					@DBName = Database name(s) to change ownership of (CSV supported, NULL for all)
					@DBNameExclude = Database name(s) to exclude change ownership of (CSV supported)
					@JobName = Database name(s) to change ownership of (CSV supported, NULL for all)
					@JobNameExclude = Database name(s) to exclude change ownership of (CSV supported)
					@NewOwner = New owner to claim ownership of specified database(s) or job(s)
					@Debug = Show, but don't execute work to be done and return current owners
**	Date:			2009.11.08
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20091113	Adam Bean		Fixed problem with showing that database owner changed
**  20091208	Adam Bean		Replaced single quotes in job name
**	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
**	20120523	Adam Bean		Updated both job and database sections to only update those that don't match the @NewOwner
********************************************************************************************************/

CREATE PROCEDURE [dbo].[ChangeOwner]
(
	@Type				VARCHAR(8)
	,@DBName			NVARCHAR(512)	= NULL
	,@DBNameExclude		NVARCHAR(512)	= NULL
	,@JobName			NVARCHAR(128)	= NULL
	,@JobNameExclude	NVARCHAR(128)	= NULL
	,@NewOwner			NVARCHAR(128)	= NULL
	,@Debug				TINYINT			= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@SQL				VARCHAR(512)
	,@DBOwner			NVARCHAR(128)
	,@JobOwner			NVARCHAR(128)	

-- Setup a temp working table to show before and after owners
IF OBJECT_ID('tempdb.dbo.#owners') IS NOT NULL
	DROP TABLE #owners
	
CREATE TABLE #owners
(
	[Type]				CHAR(8)
	,[Name]				NVARCHAR(128)
	,[Owner]			NVARCHAR(128)
	,[NewOwner]			NVARCHAR(128)
	,[Renamed]			BIT
)

-- If @NewOwner was passed in, check to make sure it's a valid login
IF @NewOwner IS NOT NULL
BEGIN
	IF NOT EXISTS(SELECT [name] FROM [master].[dbo].[syslogins] WHERE [name] = @NewOwner)
	BEGIN
		RAISERROR ('Passed in owner name is not a valid login.',16,1) WITH NOWAIT
		RETURN 2
	END
END

-- If @NewOwner was not passed in, default to sysadmin
IF @NewOwner IS NULL
	SET @NewOwner = [dbo].[WhatIsGodsName_fn]()

-- Update owners for either database(s) or job(s)
-- Database(s)
IF @Type IN('D','DB','Database')
BEGIN
	DECLARE #dbs CURSOR LOCAL STATIC FOR
	SELECT 
		[name]
		,SUSER_SNAME([owner_sid])	AS [DBOwner]
	FROM [sysdatabases_vw] s
	LEFT JOIN [dbo].[Split_fn](@DBName,',') d
		ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
		ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
	WHERE s.[state_desc] = 'ONLINE'
	AND s.[user_access_desc] = 'MULTI_USER'
	AND s.[source_database_id] IS NULL
	AND [database_id] > 4
	AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified database(s), or all
	AND de.[item] IS NULL -- All but excluded databases
	AND SUSER_SNAME([owner_sid]) != @NewOwner -- Only update those that need to be
	ORDER BY 1

	OPEN #dbs
	FETCH NEXT FROM #dbs INTO @DBName, @DBOwner
	WHILE @@FETCH_STATUS = 0
	BEGIN

		-- Populate working table with current owner
		INSERT INTO #owners
		VALUES
		('Database', @DBName, @DBOwner, @NewOwner, 0)
		
		SET @SQL = 
		'
		USE [' + @DBName + ']
		EXEC [sp_changedbowner] @loginame = ''' + @NewOwner + '''
		'
		
		IF @Debug = 1
			PRINT (@SQL)
		ELSE
		BEGIN
			EXEC (@SQL)
			-- Update working table to show that database was renamed
			UPDATE #owners
			SET [Renamed] = 1
			WHERE [Name] = @DBName
			AND [Owner] = @DBOwner
		END

FETCH NEXT FROM #dbs INTO @DBName, @DBOwner
END
CLOSE #dbs
DEALLOCATE #dbs
END
-- Job(s)
ELSE IF @Type IN ('J','Job')
BEGIN
	DECLARE #jobs CURSOR LOCAL STATIC FOR
	SELECT 
		REPLACE([name],'''','''''')		AS [JobName] 
		,SUSER_SNAME([owner_sid])		AS [JobOwner]
	FROM [msdb].[dbo].[sysjobs] s
	LEFT JOIN [dbo].[Split_fn](@JobName,',') j
		ON s.[name] = j.[item] COLLATE DATABASE_DEFAULT
	LEFT JOIN [dbo].[Split_fn](@JobNameExclude,',') je
		ON s.[name] = je.[item] COLLATE DATABASE_DEFAULT
	WHERE ((j.[item] IS NOT NULL AND @JobName IS NOT NULL) OR @JobName IS NULL) -- Specified job(s), or all
	AND je.[item] IS NULL -- All but excluded jobs
	AND SUSER_SNAME([owner_sid]) != @NewOwner -- Only update those that need to be

	OPEN #jobs
	FETCH NEXT FROM #jobs INTO @JobName, @JobOwner
	WHILE @@FETCH_STATUS = 0
	BEGIN

		-- Populate working table with current owner
		INSERT INTO #owners
		VALUES
		('Job', @JobName, @JobOwner, @NewOwner, 0)
		
		SET @SQL = 
		'
		EXEC [msdb].[dbo].[sp_update_job]
			@job_name = ''' + @JobName + '''
			,@owner_login_name = ''' + @NewOwner + '''
		'
		
		IF @Debug = 1
			PRINT (@SQL)
		ELSE
		BEGIN
			EXEC (@SQL)
			-- Update working table to show that database was renamed
			UPDATE #owners
			SET [Renamed] = 1
			WHERE [Name] = @JobName
			AND [Owner] = @JobOwner
		END

	FETCH NEXT FROM #jobs INTO @JobName, @JobOwner
	END
	CLOSE #jobs
	DEALLOCATE #jobs
END

-- Return work done
SELECT * FROM #owners
ORDER BY [Name]

SET NOCOUNT OFF
