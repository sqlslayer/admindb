IF OBJECT_ID('[dbo].[Maintenance_CheckDB_Logger]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[Maintenance_CheckDB_Logger]
GO

/******************************************************************************
* Name
	[dbo].[Maintenance_CheckDB_Logger]

* Author
	Adam Bean
	
* Date
	2011.09.08
	
* Synopsis
	Logging procedure which supports CHECKDB.
	
* Description
	Procedure to normalize and log all appropriate data surrounding the CHECKDB maintenance procedure.

* Examples
	Do not call this procedure directly, it will be utilized within the maintenance routines.

* Dependencies
	[dbo].[Maintenance_GetDatabaseID] 
	[dbo].[Maintenance_GetRunID] 
	[dbo].[Maintenance_CheckDB_Master]
	[dbo].[Maintenance_CheckDB]
	[dbo].[CheckDB_vw]

* Parameters
	@DBName = Database name that had CHECKDB run on it
	@Command = The CHECK and WITH options run with CHECKDB
	@Override = Specifies that an override via extended properties was utilized
	
* Notes
	[NewDbId] is in place so that we can use our normalized DatabaseID vs. the system database_id in order
	to support changing database_id's (restores/drops/creates)

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20140223	Adam Bean		Changed [DateCreated] to [DateStart]
								Added @DateStart and @DateEnd
								Removed @RunID logic from this procedure as it's now done in the calling Reindex procedure
	20140302	Adam Bean		Moved error handling logic into CheckDB
******************************************************************************/

CREATE PROCEDURE [dbo].[Maintenance_CheckDB_Logger]
(
	@DBName			NVARCHAR(128)
	,@Command		VARCHAR(256)
	,@Override		BIT
	,@DateStart		DATETIME
	,@DateEnd		DATETIME
	,@RunID			INT
)

AS

SET NOCOUNT ON

DECLARE 	
	@DatabaseID		INT
	,@SchemaID		INT
	,@TableID		INT
	,@CommandID		INT

-- Get the database ID
EXEC [dbo].[Maintenance_GetDatabaseID] 
	@DBName = @DBName
	,@DatabaseID = @DatabaseID OUTPUT

-- Get the command ID
EXEC [dbo].[Maintenance_GetCommandID] 
	@Command = @Command
	,@CommandID = @CommandID OUTPUT

-- Update the run ID in Maintenance_CheckDB_Master
UPDATE [dbo].[Maintenance_CheckDB_Master]
SET [RunID] = @RunID
WHERE [DbId] = DB_ID(@DBName)
AND [RunID] IS NULL

-- Update the NewDbId in Maintenance_CheckDB_Master to reflect the new database ID
UPDATE [dbo].[Maintenance_CheckDB_Master]
SET [NewDbId] = @DatabaseID
WHERE [DbId] = DB_ID(@DBName)
AND [RunID] = @RunID

-- Insert the values
INSERT INTO [dbo].[Maintenance_CheckDB]
([DatabaseID], [CommandID], [IsOverride], [RunID], [DateStart], [DateEnd])
SELECT
	@DatabaseID
	,@CommandID
	,@Override
	,@RunID
	,@DateStart
	,@DateEnd

SET NOCOUNT OFF