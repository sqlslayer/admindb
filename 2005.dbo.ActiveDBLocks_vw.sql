IF OBJECT_ID('[dbo].[ActiveDBLocks_vw]','V') IS NOT NULL 
	DROP VIEW [dbo].[ActiveDBLocks_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.ActiveDBLocks_vw
**	Desc:			View to display locks held per spid (SQL2005)
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2008.06.17
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090730	Adam Bean		Added dbid
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE VIEW [dbo].[ActiveDBLocks_vw]

AS

SELECT 
	t.*
	,s.[login_name]						AS [Login]
	,s.[host_name]						AS [HostName]
	,s.[program_name]					AS [ProgramName]
FROM 
	(
		SELECT
			d.[name]					AS [DBName]
			,d.[database_id]			AS [DBID]
			,l.[request_session_id]		AS [SPID]
			,COUNT(*)					AS [LockCount]
			,l.[resource_type]			AS [LockType]
			,l.[request_status]			AS [LockStatus]
			,CASE l.[request_mode]	
				WHEN 'Sch-S' THEN [request_mode] + ' (Schema stability)'
				WHEN 'Sch-M' THEN [request_mode] + ' (Schema modification)'
				WHEN 'S' THEN [request_mode] + ' (Shared)'
				WHEN 'U' THEN [request_mode] + ' (Update)'
				WHEN 'X' THEN [request_mode] + ' (Exclusive)'
				WHEN 'IS' THEN [request_mode] + ' (Intent Shared)'
				WHEN 'IU' THEN [request_mode] + ' (Intent Update)'
				WHEN 'IX' THEN [request_mode] + ' (Intent Exclusive)'
				WHEN 'SIU' THEN [request_mode] + ' (Shared Intent Update)'
				WHEN 'SIX' THEN [request_mode] + ' (Shared Intent Exclusive)'
				WHEN 'UIX' THEN [request_mode] + ' (Update Intent Exclusive)'
				WHEN 'BU' THEN [request_mode] + ' (Bulk Operation)'
				WHEN 'RangeS_S' THEN [request_mode] + ' (Shared Key-Range and Shared Resource lock)'
				WHEN 'RangeS_U' THEN [request_mode] + ' (Shared Key-Range and Update Resource lock)'
				WHEN 'RangeI_N' THEN [request_mode] + ' (Insert Key-Range and Null Resource lock)'
				WHEN 'RangeI_S' THEN [request_mode] + ' (Key-Range Conversion lock)'
				WHEN 'RangeI_U' THEN [request_mode] + ' (Key-Range Conversion lock)'
				WHEN 'RangeI_X' THEN [request_mode] + ' (Key-Range Conversion lock)'
				WHEN 'RangeX_S' THEN [request_mode] + ' (Key-Range Conversion lock)'
				WHEN 'RangeX_U' THEN [request_mode] + ' (Key-Range Conversion lock)'
				WHEN 'RangeX_X' THEN [request_mode] + ' (Exclusive Key-Range and Exclusive Resource lock)'
			ELSE l.[request_mode]
			END							AS [LockMode]
		FROM [sys].[dm_tran_locks] l
		LEFT JOIN [master].[sys].[databases] d
			ON d.[database_id] = l.[resource_database_id]
		GROUP BY d.[name], d.[database_id], l.[request_session_id], l.[resource_type], l.[request_status], l.[request_mode]
	) t
LEFT JOIN [sys].[dm_exec_sessions] s 
	ON t.[SPID] = s.[session_id]