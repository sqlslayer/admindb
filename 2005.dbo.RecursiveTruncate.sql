IF OBJECT_ID('dbo.RecursiveTruncate','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[RecursiveTruncate]
GO

/******************************************************************************
* Name
	[dbo].[RecursiveTruncate]

* Author
	Matt Stanford
	
* Date
	2011.09.23
	
* Synopsis
	A procedure that will quickly truncate a table and all dependent tables.
	
* Description
	Following the chain of foreign key dependencies, all involved tables are stripped of their
	foreign keys, truncated and then the foreign keys are re-created.  

* Examples
	EXEC [dbo].[RecursiveTruncate] 'SampleDB', 'dbo', 'SampleTable';

* Parameters
	@DBName				- Database Name
	@SchemaName			- Schema Name
	@TableName			- Table Name
	@Debug				- If @Debug = 1 then the commands are printed, not executed.

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
******************************************************************************/

CREATE PROCEDURE [dbo].[RecursiveTruncate] (
	@DBName			NVARCHAR(128)
	,@SchemaName	NVARCHAR(128)
	,@TableName		NVARCHAR(128)
	,@Debug			BIT = 0
)
AS

SET XACT_ABORT ON
SET NOCOUNT ON

BEGIN TRANSACTION

DECLARE
	@SQL			NVARCHAR(4000)
	,@RowCt			INT
	
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#sysschemas') IS NOT NULL
   DROP TABLE #sysschemas
IF OBJECT_ID('tempdb.dbo.#syscolumns') IS NOT NULL
   DROP TABLE #syscolumns
IF OBJECT_ID('tempdb.dbo.#sysforeign_keys') IS NOT NULL
   DROP TABLE #sysforeign_keys
IF OBJECT_ID('tempdb.dbo.#sysforeign_key_columns') IS NOT NULL
   DROP TABLE #sysforeign_key_columns
IF OBJECT_ID('tempdb.dbo.#KillTables') IS NOT NULL
	DROP TABLE #KillTables
IF OBJECT_ID('tempdb.dbo.#KillFkeys') IS NOT NULL
	DROP TABLE #KillFkeys

SELECT TOP 0 * INTO #sysobjects FROM [sys].[objects]
SELECT TOP 0 * INTO #sysschemas FROM [sys].[schemas]
SELECT TOP 0 * INTO #syscolumns FROM [sys].[columns]
SELECT TOP 0 * INTO #sysforeign_keys  FROM [sys].[foreign_keys]
SELECT TOP 0 * INTO #sysforeign_key_columns  FROM [sys].[foreign_key_columns]

CREATE TABLE #KillTables (
	SchemaName		NVARCHAR(128) NOT NULL
	,TableName		NVARCHAR(128) NOT NULL
	,ObjectID		INT NOT NULL
)

CREATE TABLE #KillFkeys (
	FKeyName		NVARCHAR(128) NOT NULL
	,ObjectID		INT NOT NULL
	,RefObjID		INT NOT NULL
	,ParentObjID	INT NOT NULL
)

SET @SQL = '
	USE [' + @DBName + '] 
		
	-- sys.objects
	INSERT INTO #sysobjects
	SELECT * FROM [sys].[objects]

	-- sys.schemas 
	INSERT INTO #sysschemas
	SELECT * FROM [sys].[schemas]

	-- sys.columns 
	INSERT INTO #syscolumns
	SELECT * FROM [sys].[columns]

	-- sys.foreign_keys 
	INSERT INTO #sysforeign_keys
	SELECT * FROM [sys].[foreign_keys]
	
	-- sys.foreign_key_columns 
	INSERT INTO #sysforeign_key_columns
	SELECT * FROM [sys].[foreign_key_columns]
'

EXEC (@SQL)

-- Prime the list with the initial table
INSERT INTO #KillTables( [SchemaName], [TableName], [ObjectID] )
SELECT
	s.[name]
	,o.[name]
	,o.[object_id]
FROM #sysobjects o
INNER JOIN #sysschemas s
	ON o.[schema_id] = s.[schema_id]
WHERE s.[name] = @SchemaName
AND o.[name] = @TableName

SET @RowCt = 1

-- Collect all of the tables that are going to need to be killed
WHILE @RowCt > 0
BEGIN

	INSERT INTO #KillTables( [SchemaName], [TableName], [ObjectID] )
	SELECT DISTINCT
		s.[name]
		,o.[name]
		,o.[object_id]
	FROM #sysforeign_keys fk
	INNER JOIN #sysobjects o
		ON fk.[parent_object_id] = o.[object_id]
	INNER JOIN #sysschemas s
		ON o.[schema_id] = s.[schema_id]
	INNER JOIN #sysobjects ro
		ON fk.[referenced_object_id] = ro.[object_id]
	INNER JOIN #sysschemas rs
		ON ro.[schema_id] = rs.[schema_id]
	INNER JOIN #KillTables rk
		ON ro.[object_id] = rk.[ObjectID]
	LEFT OUTER JOIN #KillTables ok
		ON o.[object_id] = ok.[ObjectID]
	WHERE ok.[ObjectID] IS NULL
	
	SET @RowCt = @@ROWCOUNT

END

-- Now collect the names of all of the FKeys that reference any of the killed tables
INSERT INTO #KillFkeys([FKeyName],[ObjectID],[RefObjID],[ParentObjID])
SELECT DISTINCT
	fk.[name]
	,fk.[object_id]
	,fk.[referenced_object_id]
	,fk.[parent_object_id]
FROM #sysforeign_keys fk
INNER JOIN #KillTables kt
	ON fk.[referenced_object_id] = kt.[ObjectID]
	
-- Remove the foreign keys
DECLARE #c CURSOR LOCAL STATIC FOR
SELECT
	'USE [' + @DBName + ']; ALTER TABLE [' + s.[name] + '].[' + o.[name] + '] DROP CONSTRAINT [' + fk.[FKeyName] + '];'
FROM #KillFkeys fk
INNER JOIN #sysobjects o
	ON fk.[ParentObjID] = o.[object_id]
INNER JOIN #sysschemas s
	ON o.[schema_id] = s.[schema_id]

OPEN #c
FETCH NEXT FROM #c INTO @SQL

WHILE @@FETCH_STATUS = 0
BEGIN

	IF @Debug = 0
		EXEC(@SQL)
	ELSE
		PRINT(@SQL)
		
	FETCH NEXT FROM #c INTO @SQL
END
CLOSE #c
DEALLOCATE #c

-- Truncate those poor tables
DECLARE #c CURSOR LOCAL STATIC FOR
SELECT
	'USE [' + @DBName + ']; TRUNCATE TABLE [' + o.[SchemaName] + '].[' + o.[TableName] + '];'
FROM #KillTables o

OPEN #c
FETCH NEXT FROM #c INTO @SQL

WHILE @@FETCH_STATUS = 0
BEGIN

	IF @Debug = 0
		EXEC(@SQL)
	ELSE
		PRINT(@SQL)
		
	FETCH NEXT FROM #c INTO @SQL
END
CLOSE #c
DEALLOCATE #c

-- Re-create the foreign keys
DECLARE #c CURSOR LOCAL STATIC FOR
SELECT
	'USE [' + @DBName + ']; ALTER TABLE [' + s.[name] + '].[' + o.[name] + '] ADD CONSTRAINT [' + fk.[FKeyName] + '] FOREIGN KEY (' + cols.[ParentCols] + ') REFERENCES [' + rs.[name] + '].[' + ro.[name] + '] (' + cols.[RefCols] + ');'
FROM #KillFkeys fk
INNER JOIN #sysobjects o
	ON fk.[ParentObjID] = o.[object_id]
INNER JOIN #sysschemas s
	ON o.[schema_id] = s.[schema_id]
INNER JOIN #sysobjects ro
	ON fk.[RefObjID] = ro.[object_id]
INNER JOIN #sysschemas rs
	ON ro.[schema_id] = rs.[schema_id]
CROSS APPLY (
	SELECT SUBSTRING(
		(SELECT 
			', [' + c.[name] + ']'
		FROM #sysforeign_key_columns fkc
		INNER JOIN #syscolumns c
			ON fkc.[parent_object_id] = c.[object_id]
			AND fkc.[parent_column_id] = c.[column_id]
		WHERE fkc.[constraint_object_id] = fk.[ObjectID]
		ORDER BY fkc.[constraint_column_id]
		FOR XML PATH(''))
	,3,4000) AS [ParentCols]
	,SUBSTRING(
		(SELECT 
			', [' + c.[name] + ']'
		FROM #sysforeign_key_columns fkc
		INNER JOIN #syscolumns c
			ON fkc.[referenced_object_id] = c.[object_id]
			AND fkc.[referenced_column_id] = c.[column_id]
		WHERE fkc.[constraint_object_id] = fk.[ObjectID]
		ORDER BY fkc.[constraint_column_id]
		FOR XML PATH(''))
	,3,4000) AS [RefCols]
) cols([ParentCols],[RefCols])

OPEN #c
FETCH NEXT FROM #c INTO @SQL

WHILE @@FETCH_STATUS = 0
BEGIN

	IF @Debug = 0
		EXEC(@SQL)
	ELSE
		PRINT(@SQL)
		
	FETCH NEXT FROM #c INTO @SQL
END
CLOSE #c
DEALLOCATE #c

COMMIT
