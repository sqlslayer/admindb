IF OBJECT_ID('[dbo].[SQLAgentStart]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[SQLAgentStart]
GO

/*******************************************************************************************************
**	Name:			dbo.SQLAgentStart
**	Desc:			Run with a SQLAgent startup job to send notification that instance/agent was restarted
					Will notify of current node if instanced
**	Auth:			Adam Bean (SQLSlayer.com)
**	Dependancies:	Either xp_sendmail or xp_smtp_sendmail
**  Parameters:		@Recipients = Email recipients
					@EmailServer = Email server, only needed if using xp_smtp_sendmail
					@FromEmail = From email, only needed if using xp_smtp_sendmail
**	Date:			2009.05.15
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20090708	Adam Bean		Changed @@SERVERNAME to SERVERPROPERTY('ServerName')
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE PROCEDURE [dbo].[SQLAgentStart]
(
	@Recipients		VARCHAR(1024)
	-- Set these if you're using xp_smtp_sendmail
	,@EmailServer	VARCHAR(32)	= NULL 
	,@FromEmail		VARCHAR(32)	= NULL
)

AS

SET NOCOUNT ON

DECLARE 
	@ServerName		VARCHAR(64)
	,@IsInstanced	BIT
	,@StartTime		DATETIME
	,@Subject		VARCHAR(64)
	,@Body			VARCHAR(128)
	,@ComputerName	VARCHAR(64)

SET @ServerName		= (SELECT CAST(SERVERPROPERTY('ServerName') AS VARCHAR(64)))
SET @IsInstanced	= (SELECT CAST(SERVERPROPERTY('IsClustered') AS BIT))
SET @StartTime		= (SELECT [login_time] FROM [master].[dbo].[sysprocesses] WHERE [spid] = 1)
SET @Subject		= 'SQL Agent started on ' + @ServerName + ' @ ' + CONVERT(VARCHAR(20), GETDATE(), 100) + ''
SET @Body			= 'SQL has been running since ' + CAST(@StartTime AS VARCHAR) + '.'

IF @IsInstanced = 1
BEGIN
	-- Pre run cleanup	
	IF OBJECT_ID('tempdb.dbo.#tt') IS NOT NULL
		DROP TABLE #tt

	-- Create staging table to hold current server name if instanced	
	CREATE TABLE #tt
	(
		[Value]	VARCHAR(16)
		,[Data]	VARCHAR(16)
	)

	-- Retrieve current node name
	INSERT INTO #tt
	EXEC [master].[dbo].[xp_regread] 'HKEY_LOCAL_Machine','SYSTEM\CurrentControlSet\Control\ComputerName\ComputerName\','ComputerName'

	SET @ComputerName	= (SELECT [Data] FROM #tt)
	SET @Body			= @Body + CHAR(10) + CHAR(10) + 'SQL is currently running on ' + @ComputerName + '.'
END

-- Determine which mail format to use
IF EXISTS (SELECT * FROM [master].[dbo].[sysobjects] WHERE [name] = 'xp_smtp_sendmail' AND [type] = 'X')
BEGIN
-- Send mail using xp_smtp_sendmail
	EXEC [master].[dbo].[xp_smtp_sendmail]
		@From       = @FromEmail
		,@To        = @Recipients
		,@Subject   = @Subject
		,@Message   = @Body
		,@Type      = 'text/html'
		,@Server	= @EmailServer
END
ELSE
BEGIN
-- Send mail using xp_sendmail
	EXEC [master].[dbo].[xp_sendmail]
		@Recipients	= @Recipients
		,@Subject	= @Subject
		,@Message   = @Body
END