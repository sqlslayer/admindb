IF OBJECT_ID('[dbo].[sysmasterfiles_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[sysmasterfiles_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.sysmasterfiles_vw
**	Desc:			Standard view (similar to sys.master_files) for all versions of SQL
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2009.06.09
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090702	Matt Stanford	Because sysaltfiles does not allow you to query for growth percent
								I had to modify the percent detection to make it ghetto fabulous
**  20090729	Adam Bean		Fixed for case sensitive collation
**  20090730	Adam Bean		Added LTRIM(RTRIM to physical name
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE VIEW [dbo].[sysmasterfiles_vw]

AS

SELECT
	DB_NAME(f.[dbid])									AS [database_name]
	,f.[dbid]											AS [database_id]
	,f.[fileid]											AS [file_id]
	,CAST(NULL AS UNIQUEIDENTIFIER)						AS [file_guid]
	,CAST(
		CASE 
			WHEN f.[groupid] = 0 THEN 1
			ELSE 0
		END 
	AS TINYINT)											AS [type]
	,CAST(
		CASE
			WHEN f.[groupid] = 0 THEN 'LOG'
			ELSE 'ROWS'
		END
	AS NVARCHAR(60))									AS [type_desc]
	,CAST(f.[groupid] AS INT)							AS [data_space_id]
	,CAST(f.[name] AS sysname)							AS [name]
	,LTRIM(RTRIM(CAST(f.[filename] AS NVARCHAR(260))))	AS [physical_name]
	,CAST(0 AS TINYINT)									AS [state]
	,CAST('ONLINE' AS NVARCHAR(60))						AS [state_desc]
	,CAST(f.[size] AS INT)								AS [size]
	,CAST(f.[size] * 8 / 1024. AS DECIMAL(12,2))		AS [size_MB]
	,CAST(f.[maxsize] AS INT)							AS [max_size]
	,CAST(
		CASE
			WHEN f.[growth] % 8 <> 0				-- Totally ghetto way to do this - not always accurate
				THEN f.[size] * 8 / 1024. * f.[growth] / 100
			ELSE f.[growth] * 8 / 1024.
		END AS DECIMAL(12,2))							AS [next_growth_MB]
	,CAST(f.[growth] AS INT)							AS [growth]
	,CAST(0 AS BIT)										AS [is_media_read_only]
	,CAST(CASE 
		WHEN f.[status] & 4096 = 0 THEN 0
		ELSE 1
	END	AS BIT)											AS [is_read_only]
	,CAST(0 AS BIT)										AS [is_sparse]
	,CAST(CASE 
		WHEN f.[growth] % 8 = 0 THEN 0				-- Totally ghetto way to do this - not always accurate
		ELSE 1
	END	AS BIT)											AS [is_percent_growth]
	,CAST(0 AS BIT)										AS [is_name_reserved]
FROM [master].[dbo].[sysaltfiles] f