IF OBJECT_ID('[dbo].[GetErrorInfo_fn]', 'IF') IS NOT NULL 
    DROP FUNCTION [dbo].[GetErrorInfo_fn]
GO

/*******************************************************************************************************
**	Name:			dbo.GetErrorInfo_fn
**	Desc:			Helper function used in TRY/CATCH blocks
**	Auth:			Adam Bean 
**	Date:			2009.06.30
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE FUNCTION [dbo].[GetErrorInfo_fn]()
RETURNS TABLE 

AS

RETURN
(
	SELECT
		ERROR_NUMBER()		AS [ErrorNumber]
		,ERROR_SEVERITY()	AS [ErrorSeverity]
		,ERROR_STATE()		AS [ErrorState]
		,ERROR_PROCEDURE()	AS [ErrorProcedure]
		,ERROR_LINE()		AS [ErrorLine]
		,ERROR_MESSAGE()	AS [ErrorMessage]
)	
