IF OBJECT_ID('MissingIndexes_vw','V') IS NOT NULL
	DROP VIEW [dbo].[MissingIndexes_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.MissingIndexes_vw
**	Desc:			Discover missing indexes
**	Auth:			Adam Bean (SQLSlayer.com)
**	Date:			2008.04.04
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20080408	Adam Bean		Cleaned up. Added table name.
**	20080627	Adam Bean		Switched to view
**	20090319	Adam Bean		Seperated columns to be indexed
**	20090929	Adam Bean		Global header formatting & general clean up
**	20100708	Adam Bean		Added drop statement, added online for enterprise edition, cleanup
**	20101021	Matt Stanford	Added Schema Name
********************************************************************************************************/

CREATE VIEW [dbo].[MissingIndexes_vw] 

AS

SELECT
	DB_NAME(mid.[database_id])	AS [DBName]
	,OBJECT_SCHEMA_NAME(mid.[object_id], mid.[database_id]) AS [SchemaName]
	,OBJECT_NAME(mid.[object_id], mid.[database_id]) AS [TableName]
	,CONVERT (DECIMAL (28,1), migs.[avg_total_user_cost] * migs.[avg_user_impact] * (migs.[user_seeks] + migs.[user_scans])) AS [ImprovementMeasure]
	,CASE
		WHEN mid.[equality_columns] IS NOT NULL AND mid.[inequality_columns] IS NOT NULL 
			THEN mid.[equality_columns] + ',' + mid.[inequality_columns]
		WHEN mid.[equality_columns] IS NOT NULL AND mid.[inequality_columns] IS NULL 
			THEN mid.[equality_columns]
		WHEN mid.[equality_columns] IS NULL AND mid.[inequality_columns] IS NOT NULL 
			THEN mid.[inequality_columns]
		ELSE '' 
	END AS [Columns]
	,mid.[included_columns] AS [IncludedColumns]
	,'CREATE INDEX [IX_MI_' + OBJECT_NAME(mid.[object_id], mid.[database_id]) + '_' 
		+ CONVERT([VARCHAR], mig.[index_group_handle]) 
		+ '_' + CONVERT([VARCHAR], mid.[index_handle]) + ']'
		+ ' ON ' + mid.[statement]
		+ ' (' + ISNULL (mid.[equality_columns],'') 
		+ CASE 
			WHEN mid.[equality_columns] IS NOT NULL AND mid.[inequality_columns] IS NOT NULL 
				THEN ',' 
				ELSE '' 
			END + ISNULL (mid.[inequality_columns], '')
		+ ')' 
		+ ISNULL (' INCLUDE (' + mid.[included_columns] + ')', '')	
		+ CASE
			WHEN CAST(SERVERPROPERTY('EDITION') AS VARCHAR(24)) LIKE 'Enterprise%'
				THEN ' WITH (ONLINE = ON)'
				ELSE ''
			END														AS [CreateIndexStatement]
	,'USE ' + DB_NAME(mid.[database_id]) 
		+ ' DROP INDEX ' + REPLACE(mid.[statement], SUBSTRING(mid.[statement], 0, CHARINDEX('.', mid.[statement]) + 1), '') 
		+ '.[IX_MI_' + OBJECT_NAME(mid.[object_id], mid.[database_id])
		+ '_' + CONVERT([VARCHAR], mig.[index_group_handle]) 
		+ '_' + CONVERT([VARCHAR], mid.[index_handle]) + ']'		AS [DropIndexStatement]
	,migs.*
FROM [sys].[dm_db_missing_index_groups] mig
JOIN [sys].[dm_db_missing_index_group_stats] migs 
	ON migs.[group_handle] = mig.[index_group_handle]
JOIN [sys].[dm_db_missing_index_details] mid 
	ON mig.[index_handle] = mid.[index_handle]
WHERE CONVERT(DECIMAL (28,1), migs.[avg_total_user_cost] * migs.[avg_user_impact] * (migs.[user_seeks] + migs.[user_scans])) > 10
