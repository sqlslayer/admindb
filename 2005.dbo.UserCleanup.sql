IF OBJECT_ID('[dbo].[UserCleanup]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[UserCleanup]
GO

/******************************************************************************
* Name
	[dbo].[UserCleanup]

* Author
	Adam Bean
	
* Date
	2008.07.01
	
* Synopsis
	Repair or remove orphaned database users
	
* Description
	Procedure to analyze user ownership at a schema and object level to determine 
	if a user can be removed or repaired within a given database.

* Examples
	EXEC [dbo].[UserCleanup] 
	EXEC [dbo].[UserCleanup] @DBName = 'admin', @Type = 'Repair'
	EXEC [dbo].[UserCleanup] @DBName = 'admin', @UserName = 'temp'

* Dependencies
	dbo.Split_fn
	dbo.sysdatabases_vw

* Parameters
	@DBName = Database(s) to be analyzed (CSV supported, NULL for all)
	@DBNameExclude = Database(s) to excluded (CSV supported, NULL for all)
	@UserName = User name(s) to be analyzed (CSV supported, NULL for all)
	@UserNameExclude = User name(s) to be analyzed (CSV supported, NULL for all)
	@Type = Repair (sp_change_users_login), Remove, or Both to run a repair first, then a remove on non repaired users
	@Debug	= Display but don't run queries 
	
* Notes
	n/a

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20080709	Adam Bean		Added databasename to both outputs
	20080916	Adam Bean		Added new case for orphaned user that owns schema (8)
								Fixed Processed flag to properly display repair or removed at end
	20081001	Adam Bean		Fixed case for repair
	20081206	Adam Bean		Added exclusion for system database roles
	20090929	Adam Bean		Global header formatting & general clean up
	20130918	Adam Bean		Added multi-db support, added @UserName, @UserNameExclude
								Fixed bug with incorrectly reporting on owned objects
								General code cleanup and new header
	20140331	Adam Bean		Fixed collation errors on MatchFound & LoginAlreadyMapped
******************************************************************************/

CREATE PROCEDURE [dbo].[UserCleanup]
(
	@DBName						NVARCHAR(2048)	= NULL
	,@DBNameExclude				NVARCHAR(2048)	= NULL
	,@UserName					NVARCHAR(2048)	= NULL
	,@UserNameExclude			NVARCHAR(2048)	= NULL    
	,@Type						CHAR(8)			= 'Repair' -- Options are 'Repair', 'Remove', or 'Both' 
	,@Debug						BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE
	@DBID			INT
	,@RepairSQL		VARCHAR(MAX)
	,@DropSQL		VARCHAR(MAX)
	,@DropSchema	VARCHAR(MAX)
	,@User			NVARCHAR(128)
	,@Status		TINYINT
	,@SchemasOwned	VARCHAR(128)
	,@LineBreak		CHAR(2)

-- Populate our working variables
SET @LineBreak = CHAR(13) + CHAR(10)

IF @Debug = 1
	PRINT '/**** DEBUG ENABLED ****/'

IF @Type NOT IN ('Repair','Remove','Both')
	PRINT '/**** @Type must be either ''Repair'', ''Remove'' or ''Both'' ****/ '

-- Get all of the catalog views
-- Drop the tables if they exist
IF OBJECT_ID('tempdb.dbo.#sysdatabase_permissions') IS NOT NULL
   DROP TABLE #sysdatabase_permissions
IF OBJECT_ID('tempdb.dbo.#sysdatabase_principals') IS NOT NULL
   DROP TABLE #sysdatabase_principals
IF OBJECT_ID('tempdb.dbo.#sysschemas') IS NOT NULL
   DROP TABLE #sysschemas
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects

-- Create the tables
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysdatabase_permissions FROM [sys].[database_permissions]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysdatabase_principals FROM [sys].[database_principals]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysschemas FROM [sys].[schemas]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysobjects FROM [sys].[objects]

-- Retrieve data per database
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
	,[database_id]
FROM [dbo].[sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[is_read_only] = 0
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY 1

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName, @DBID
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 

		-- sys.database_permissions
		INSERT INTO #sysdatabase_permissions
		SELECT *, ' + @DBID + ' FROM [sys].[database_permissions]

		-- sys.database_principals
		INSERT INTO #sysdatabase_principals
		SELECT *, ' + @DBID + ' FROM [sys].[database_principals]

		-- sys.schemas
		INSERT INTO #sysschemas
		SELECT *, ' + @DBID + ' FROM [sys].[schemas]
					
		-- sys.objects
		INSERT INTO #sysobjects
		SELECT *, ' + @DBID + ' FROM [sys].[objects]

	')

FETCH NEXT FROM #dbs INTO @DBName, @DBID
END
CLOSE #dbs
DEALLOCATE #dbs

-- @OrphanedUsers
DECLARE @OrphanedUsers TABLE 
(
	[DatabaseName]			NVARCHAR(128)
	,[OrphanedUserName]		NVARCHAR(128)
	,[Type]					VARCHAR(64)
	,[RepairResults]		VARCHAR(256) 
	,[DropResults]			VARCHAR(256) 
	,[OwnsSchemas]			TINYINT
	,[SchemasOwned]			VARCHAR(512)
	,[OwnsObjects]			TINYINT
	,[ObjectsOwned]			NVARCHAR(128)	NULL
	,[MatchFound]			TINYINT
	,[LoginAlreadyMapped]	TINYINT
	,[Status]				AS (
									CASE
										-- Cases where repair will happen without user interaction
										WHEN [OwnsSchemas] = 0 AND [OwnsObjects] = 0 AND [MatchFound] = 1 AND [LoginAlreadyMapped] = 0 THEN 1 -- Orphaned user that can be fixed
										WHEN [OwnsSchemas] = 1 AND [OwnsObjects] = 0 AND [MatchFound] = 1 AND [LoginAlreadyMapped] = 0 THEN 2 -- Orphaned user and schema
										WHEN [OwnsSchemas] = 1 AND [OwnsObjects] = 1 AND [MatchFound] = 1 AND [LoginAlreadyMapped] = 0 THEN 3 -- Schema owns objects
										-- Cases where repair can happen, after manual user interaction
										WHEN [OwnsSchemas] = 0 AND [OwnsObjects] = 0 AND [MatchFound] = 1 AND [LoginAlreadyMapped] = 1 THEN 4 -- Login is already mapped
										WHEN [OwnsSchemas] = 1 AND [OwnsObjects] = 0 AND [MatchFound] = 1 AND [LoginAlreadyMapped] = 1 THEN 5 -- Login is already mapped and user owns schema
										WHEN [OwnsSchemas] = 0 AND [OwnsObjects] = 1 AND [MatchFound] = 1 AND [LoginAlreadyMapped] = 1 THEN 6 -- Login is already mapped and schema owns objects
										WHEN [OwnsSchemas] = 1 AND [OwnsObjects] = 1 AND [MatchFound] = 1 AND [LoginAlreadyMapped] = 1 THEN 7 -- Login is already mapped and user owns schema and objects
										-- Cases of no repair, user can be dropped
										WHEN [OwnsSchemas] = 1 AND [OwnsObjects] = 0 AND [MatchFound] = 0 AND [LoginAlreadyMapped] = 0 THEN 8 -- User owns a schema, schema and user can be dropped
										-- Cases of no repair, manual intervention for drop
										WHEN [OwnsSchemas] = 1 AND [OwnsObjects] = 1 AND [MatchFound] = 0 AND [LoginAlreadyMapped] = 0 THEN 10 -- User will have to drop objects or change schema owner before dropping orphan user
										ELSE 0 -- Orphaned user
									END
								)
	,[Processed]			TINYINT DEFAULT (0)
)

-- Find our orphaned users
INSERT INTO @OrphanedUsers
([DatabaseName], [OrphanedUserName],[Type],[OwnsSchemas],[SchemasOwned],[OwnsObjects],[ObjectsOwned],[MatchFound],[LoginAlreadyMapped])
SELECT 
	@DBName				AS [DatabaseName]
	,dp.[name]			AS [OrphanedUserName] -- Orphaned user
	,CASE dp.[type] -- Type of user
		WHEN 'S' THEN '(' + dp.[type] + ')' + ' SQL user'
		WHEN 'U' THEN '(' + dp.[type] + ')' + ' Windows User'
		WHEN 'G' THEN '(' + dp.[type] + ')' + ' Windows Group'
		WHEN 'A' THEN '(' + dp.[type] + ')' + ' Application role'
		WHEN 'C' THEN '(' + dp.[type] + ')' + ' User mapped to a certificate'
		WHEN 'K' THEN '(' + dp.[type] + ')' + ' User mapped to an asymmetric key'
		ELSE dp.[type] 
	END AS [Type]
	,CASE -- Does user own schemas?
		WHEN (SELECT COUNT(*) FROM #sysschemas WHERE dp.[principal_id] = ss.[principal_id]) > 0 THEN 1
		ELSE 0
	END					AS [OwnsSchemas]
	,ss.[name]			AS [SchemasOwned] -- Schema name owned by user
	,CASE -- Does user own objects?
		WHEN (SELECT COUNT(*) FROM #sysobjects WHERE dp.[principal_id] = so.[schema_id]) > 0 THEN 1
		ELSE 0
	END					AS [OwnsObjects]
	,so.[name]			AS [ObjectsOwned] -- Objects owned by schema
	,CASE -- Determine if the user can be repaired by finding a matching login, only SQL users can be repaired
		WHEN (SELECT COUNT(*) FROM [sys].[server_principals] t WHERE t.[name] = dp.[name] COLLATE DATABASE_DEFAULT AND t.[type] = 'S') > 0 THEN 1
		ELSE 0
	END AS [MatchFound]
	,CASE -- Determine if the matched user is already mapped to a different user
		WHEN (SELECT COUNT(*) FROM [sys].[server_principals] t WHERE t.[name] = dp.[name] COLLATE DATABASE_DEFAULT AND t.[type] = 'S' AND t.[sid] IN (SELECT [sid] FROM #sysdatabase_principals)) > 0 THEN 1
		ELSE 0
	END AS [LoginAlreadyMapped]
FROM #sysdatabase_principals dp
LEFT JOIN [sys].[server_principals] sp
	ON dp.[sid] = sp.[sid] 
LEFT JOIN #sysschemas ss
	ON dp.[principal_id] = ss.[principal_id]
	AND dp.[database_id] = ss.[database_id]
LEFT JOIN #sysobjects so
	--ON dp.[principal_id] = so.[principal_id]
	ON ss.[schema_id] = so.[schema_id] 
	AND dp.[database_id] = so.[database_id]
LEFT JOIN [dbo].[Split_fn](@UserName,',') u
	ON dp.[name] = u.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@UserNameExclude,',') ue
	ON dp.[name] = ue.[item] COLLATE DATABASE_DEFAULT
WHERE sp.[name] IS NULL -- User not have a matching login
AND dp.[type] <> 'R' -- Database roles
AND dp.[name] NOT IN ('public', 'dbo', 'guest', 'INFORMATION_SCHEMA', 'sys') 
AND ((u.[item] IS NOT NULL AND @UserName IS NOT NULL) OR @UserName IS NULL) -- Specified users, or all
AND ue.[item] IS NULL -- All but excluded users

-- Update results columns with a summary of work to be done
UPDATE @OrphanedUsers
	SET 
		[RepairResults] = (
							CASE
								WHEN [Status] = 0 THEN 'No match found. User can not be repaired.'
								WHEN [Status] = 1 THEN 'Match found. User can be repaired.'
								WHEN [Status] = 2 THEN 'Match found. User owns schema. User can be repaired.'
								WHEN [Status] = 3 THEN 'Match found. Schema owns objects. User can be repaired.'
								WHEN [Status] = 4 THEN 'Match found. Login already mapped to different user. User can not be repaired.'
								WHEN [Status] = 5 THEN 'Match found. Login already mapped to different user, user owns schema. User can not be repaired.'
								WHEN [Status] = 6 THEN 'Match found. Login already mapped to different user, schema owns objects. User can not be repaired.'
								WHEN [Status] = 7 THEN 'Match found. Login already mapped to different user, user owns schema, schema owns objects. User can not be repaired.'
								WHEN [Status] = 8 THEN 'No match found, user owns schema. User can not be repaired.'
								WHEN [Status] = 10 THEN 'No match found, user owns schema, schema owns objects. User can not be repaired.'
							END
						)
		,[DropResults] = (
							CASE
								WHEN [Status] = 0 THEN 'User can be dropped.'
								WHEN [Status] = 1 THEN 'User can be dropped.'
								WHEN [Status] = 2 THEN 'User owns schema. Schema will be automatically dropped. User can be dropped.'
								WHEN [Status] = 3 THEN 'Schema owns objects. User can only be dropped after giving ownership of objects to new owner, or drop objects.'
								WHEN [Status] = 4 THEN 'User can be dropped.'
								WHEN [Status] = 5 THEN 'User owns schema. Schema will be automatically dropped. User can be dropped.'
								WHEN [Status] = 6 THEN 'Schema owns objects. User can only be dropped after giving ownership of objects to new owner, or drop objects.'
								WHEN [Status] = 7 THEN 'User owns schema. Schema owns objects. User can only be dropped after giving ownership of schema and objects to new owner, or drop schema and objects.'
								WHEN [Status] = 8 THEN 'User owns schema. User and schema can be dropped.'
								WHEN [Status] = 10 THEN 'User owns schema. Schema owns objects. User can only be dropped after giving ownership of schema and objects to new owner, or drop schema and objects.'
							END
						)

-- Return results 
SELECT * FROM @OrphanedUsers
ORDER BY 1, 2


-- Do the work
-- Repair
IF @Type = 'Repair' OR @Type = 'Both'
BEGIN
	DECLARE #repairusers CURSOR FAST_FORWARD FOR 
	SELECT DISTINCT
		[OrphanedUserName] 
	FROM @OrphanedUsers
	WHERE [Status] IN (1,2,3)

	OPEN #repairusers
	FETCH NEXT FROM #repairusers INTO @User
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
			SET @RepairSQL = 'USE [' + @DBName + ']' + @LineBreak + 'EXEC dbo.[sp_change_users_login] ''update_one'', ' + @User + ', ' + @User + ''

		IF @Debug = 1
			PRINT (@RepairSQL)
		ELSE
		BEGIN
			EXEC (@RepairSQL)
			-- Update processed flag for repairs
			UPDATE @OrphanedUsers
			SET [Processed] = 1
			WHERE [OrphanedUserName] = @User
		END


	FETCH NEXT FROM #repairusers INTO @User
	END     
	CLOSE #repairusers
	DEALLOCATE #repairusers
END

-- Remove
IF @Type = 'Remove' OR @Type = 'Both'
BEGIN
	DECLARE #dropusers CURSOR FAST_FORWARD FOR 
	SELECT DISTINCT
		[OrphanedUserName] 
		,[Status]
		,[SchemasOwned]
	FROM @OrphanedUsers
	WHERE [Status] IN (0,1,2,4,5,8)
	AND [Processed] = 0 -- Don't remove repaired users when @Type = 'Both'

	OPEN #dropusers
	FETCH NEXT FROM #dropusers INTO @User, @Status, @SchemasOwned
	WHILE @@FETCH_STATUS = 0
	BEGIN

		IF @Status IN (2,5,8) -- Orphan Schema
		BEGIN
			SET @DropSchema = 'USE [' + @DBName + ']' + @LineBreak + 'DROP SCHEMA [' + @SchemasOwned + ']'
		END
			SET @DropSQL = 'USE [' + @DBName + ']' + @LineBreak + 'DROP USER [' + @User + ']'
		IF @Debug = 1
		BEGIN
			PRINT (@DropSchema)
			PRINT (@DropSQL)
		END
		ELSE
		BEGIN
			EXEC (@DropSchema)
			EXEC (@DropSQL)
			-- Update processed flag for removes
			UPDATE @OrphanedUsers
			SET [Processed] = 2
			WHERE [OrphanedUserName] = @User
		END

	FETCH NEXT FROM #dropusers INTO @User, @Status, @SchemasOwned
	END     
	CLOSE #dropusers
	DEALLOCATE #dropusers
END

-- Display results after work is done
IF @Debug = 0
BEGIN
	SELECT
		@DBName	AS [DatabaseName]
		,[OrphanedUserName] 
		,CASE [Processed]
			WHEN 1 THEN 'Repaired'
			WHEN 2 THEN 'Removed'
			ELSE 'Nothing done'
		END AS [Status]
		,[Processed]
	FROM @OrphanedUsers
END
ELSE
	SELECT 'Debug Enabled, see messages for additional information' AS [Debug]

SET NOCOUNT OFF