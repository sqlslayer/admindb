IF OBJECT_ID('[dbo].[Trace_Blocking]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[Trace_Blocking]
GO

/*******************************************************************************************************
**	Name:			dbo.Trace_Blocking
**	Desc:			Procedure to create profiler definition for blocking
**	Auth:			Adam Bean (SQLSlayer.com)
**  Parameters:		@MaxFileSize = Maximum file size
					@FileLocation = Destination for file output
					@FileName = File prefix name (prior to numbering)
					@FileSuffix = Leave null to use yyyy.mm.dd timestamp
					@StopTime = Time in which to stop the trace
					@FileCount = Max amount of files to keep
**  Usage:			DECLARE @StopTime DATETIME
					SET @StopTime = (SELECT DATEADD(hh, 6, GETDATE()))
					EXEC [admin].[dbo].[Trace_Blocking]
						@FileLocation = 'D:\Production\SQLBlocking\Profiler\'
						,@StopTime = @StopTime
**  Notes:			In order to capture the blocked process report with profiler, the blocked process threshold has to be set
					sp_configure 'show advanced options',1
					RECONFIGURE
					GO 
					sp_configure 'blocked process threshold',5 -- 5 seconds
					RECONFIGURE
					GO
**	Date:			2008.12.23
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
**	20091203	Adam Bean		Fixed @FileLocation backslash, Added servername to trace file
********************************************************************************************************/

CREATE PROCEDURE [dbo].[Trace_Blocking]
(
	@MaxFileSize	BIGINT			= 1024
	,@FileLocation	NVARCHAR(256)
	,@FileName		NVARCHAR(256)   = 'BlockTrace'
	,@FileSuffix	NVARCHAR(256)	= NULL
	,@StopTime		DATETIME		= NULL
	,@FileCount		INT				= 10
)

AS

SET NOCOUNT ON

-- Create a Queue
DECLARE 
	@rc				INT
	,@TraceID		INT
	,@FullFileName	NVARCHAR(256)
	,@on			BIT
	,@ServerName	NVARCHAR(128)
	,@InstanceName	NVARCHAR(128)

-- Populate some working variables
SET @on = 1
SET @ServerName = (SELECT CAST(SERVERPROPERTY('ServerName') AS NVARCHAR(128)))
SET @InstanceName = (SELECT CAST(SERVERPROPERTY('InstanceName') AS VARCHAR(128)))

-- Build servername based on instance name
IF @InstanceName IS NOT NULL
	SET @ServerName = LEFT(@ServerName,CHARINDEX('\',@ServerName) - 1) + '_' + @InstanceName

-- Add a time stamp to the file name
IF @FileSuffix IS NULL
	SET @FileSuffix = CAST(DATEPART(yy, GETDATE()) AS VARCHAR) + RIGHT('0' + CAST(DATEPART(mm, GETDATE()) AS VARCHAR),2) + RIGHT('0' + CAST(DATEPART(dd, GETDATE()) AS VARCHAR),2) + '_'

-- Check for trailing back slash
IF RIGHT(@FileLocation,1) != '\'
	SET @FileLocation = @FileLocation + '\'

-- Build the fulle file name
SET @FullFileName = @FileLocation + @ServerName + '_' + @FileName + '_' + @FileSuffix

-- Create the trace
EXEC @rc = sp_trace_create 
		@TraceID = @TraceID OUTPUT
		,@Options = 2 --TRACE_FILE_ROLLOVER
		,@TraceFile = @FullFileName
		,@MaxFileSize = @MaxFileSize
		,@StopTime = @StopTime 
		,@FileCount = @FileCount

-- If trace doesn't create, return
IF @rc != 0
BEGIN
	SELECT ErrorCode = @rc
	RETURN
END

-- Set the events
EXEC sp_trace_setevent @TraceID, 137, 3, @on
EXEC sp_trace_setevent @TraceID, 137, 15, @on
EXEC sp_trace_setevent @TraceID, 137, 51, @on
EXEC sp_trace_setevent @TraceID, 137, 4, @on
EXEC sp_trace_setevent @TraceID, 137, 24, @on
EXEC sp_trace_setevent @TraceID, 137, 32, @on
EXEC sp_trace_setevent @TraceID, 137, 60, @on
EXEC sp_trace_setevent @TraceID, 137, 64, @on
EXEC sp_trace_setevent @TraceID, 137, 1, @on
EXEC sp_trace_setevent @TraceID, 137, 13, @on
EXEC sp_trace_setevent @TraceID, 137, 41, @on
EXEC sp_trace_setevent @TraceID, 137, 22, @on
EXEC sp_trace_setevent @TraceID, 137, 26, @on

-- Set the trace status to start
EXEC sp_trace_setstatus @TraceID, 1

-- Display trace id for future references
SELECT 
	'TraceStarted'	AS [Status]
	,@TraceID		AS [TraceID]
	,'See Messages'	AS [Additional_Info]

PRINT '/*** Additional Info ***/'
PRINT 'SELECT * FROM [sys].[traces] WHERE id = ' + CAST(@TraceID AS VARCHAR) + ''
PRINT 'To stop the trace: EXEC sp_trace_setstatus @traceid = ' + CAST(@TraceID AS VARCHAR) + ', @status = 0'
PRINT 'To remove trace definition: EXEC sp_trace_setstatus @traceid = ' + CAST(@TraceID AS VARCHAR) + ', @status = 2'

IF @@ERROR != 0
	RETURN 2
ELSE
	RETURN 0

SET NOCOUNT OFF