﻿IF OBJECT_ID('dbo.DeployStartupTraceFlags','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[DeployStartupTraceFlags]
GO

/******************************************************************************
* Name
	[dbo].[DeployStartupTraceFlags]

* Author
	Jeff Gogel
	
* Date
	2016.10.06
	
* Synopsis
	Procedure to add start up trace flags through T-SQL as opposed to using SQL Server Configuration Manager.
	
* Description
	Adds trace flags to the registry.  Requres SQL Restart before trace flags take effect.

* Examples
	EXEC [dbo].[DeployStartupTraceFlags] 								-- Deploy standard flags.
	EXEC [dbo].[DeployStartupTraceFlags] @TraceFlags = '-T1222,-T1117'	-- Deploy specified trace flags.

* Dependencies
	dbo.Split_fn

* Parameters
	@TraceFlags			- Specify trace flags to deploy.

	
* Notes
	This procedure edits the registry.
	Original Code Source:  http://beyondrelational.com/modules/2/blogs/657/posts/15442/how-to-use-tsql-to-add-trace-flag-to-sql-server-service-startup-parameters.aspx

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	10212016	Jeff Gogel		Added logic to remove T1117 and T1118 for SQL 2016+.  
								The trace flags are no longer applicable.
******************************************************************************/

CREATE PROCEDURE [dbo].[DeployStartupTraceFlags]
(
	@TraceFlags				VARCHAR(2048) = '-T1222,-T3226,-T1117,-T1118,-T4199'
	,@ShutDownSQLServer		BIT			= 0
)

AS

SET NOCOUNT ON;

DECLARE @InstanceName		NVARCHAR(2048) 
DECLARE @ProductVersion		sysname
DECLARE @RegKeyPrefix		NVARCHAR(16)
DECLARE @InstanceRegKey		NVARCHAR(128)
DECLARE @CurrentFlagToAdd	NVARCHAR(16)
DECLARE @NextParamCount		INT
DECLARE @NextValueName		NVARCHAR(16)
DECLARE @FlagAddCount		INT = 0

-- Figure out what the instance looks like in the registry
SELECT @InstanceName = CAST(ISNULL(SERVERPROPERTY('InstanceName'), @@SERVICENAME) AS NVARCHAR(MAX))
SELECT @ProductVersion = CONVERT(sysname, SERVERPROPERTY('ProductVersion'))
SELECT @RegKeyPrefix = CASE 
							WHEN @ProductVersion LIKE '10.00.%' THEN '10'
							WHEN @ProductVersion LIKE '10.50.%' THEN '10_50'
							WHEN @ProductVersion LIKE '11.0%' THEN '11'
							WHEN @ProductVersion LIKE '12.0%' THEN '12'
							WHEN @ProductVersion LIKE '13.0%' THEN '13'
							ELSE 'Unknown'
						END

IF (@RegKeyPrefix = 'Unknown')
	raiserror('Unknown SQL Server Version.', 20, -1) with log

-- Remove T1117/T1118 for SQL 2016+.  The flags no longer do anything.
IF (CONVERT(INT,@RegKeyPrefix) >= 13)
BEGIN
	SET @TraceFlags = REPLACE(@TraceFlags,',-T1117','')
	SET @TraceFlags = REPLACE(@TraceFlags,',-T1118','')
END

SET @InstanceRegKey = 'MSSQL' + @RegKeyPrefix + '.' + @InstanceName


DECLARE @instance_name NVARCHAR(2000),
        @subkey NVARCHAR(2000),
        @traceflag_to_add NVARCHAR(20),
        @tmp_int INT,
        @my_value_name NVARCHAR(4000),
        @my_value NVARCHAR(4000);

SELECT @traceflag_to_add = '1222'; -- <------- Change this

SELECT @my_value = '-T ' + CAST(@traceflag_to_add AS NVARCHAR(10));

SELECT  @instance_name = CAST(ISNULL(SERVERPROPERTY('InstanceName'), @@SERVICENAME) AS NVARCHAR(MAX));


-- Build subkey
SELECT @subkey = N'SOFTWARE\Microsoft\\Microsoft SQL Server\\' + @InstanceRegKey + '\\MSSQLServer\Parameters';

-- Create a temp table to store the paramters that have already been added
IF OBJECT_ID('tempdb..#SQLParameters') IS NOT NULL DROP TABLE #SQLParameters;
CREATE TABLE #SQLParameters
(
    value_name VARCHAR(255),
    value VARCHAR(255)
)


-- Import current parameters from the registry
INSERT #SQLParameters EXEC master..xp_instance_regenumvalues @rootkey = N'HKEY_LOCAL_MACHINE', @key = @subkey;

PRINT 'Loaded parameters from ' + 'HKEY_LOCAL_MACHINE' + @subkey

-- Iterate through flags to add
DECLARE #FlagsToAdd CURSOR LOCAL STATIC FOR
SELECT item AS [Flag]
FROM dbo.Split_fn(@TraceFlags,',')

OPEN #FlagsToAdd
FETCH NEXT FROM #FlagsToAdd INTO @CurrentFlagToAdd
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Add flags that do not exist
	IF (NOT EXISTS (SELECT [value] FROM #SQLParameters WHERE [value] = @CurrentFlagToAdd))
	BEGIN
		-- Figure out new param name
		SELECT @NextParamCount = (COUNT(0)) FROM #SQLParameters
		SELECT @NextValueName = 'SQLArg' + CAST(@NextParamCount AS VARCHAR)
		
		PRINT 'Adding ' + @NextValueName + ' for flag ' + @CurrentFlagToAdd
		EXEC master.dbo.xp_instance_regwrite @rootkey = 'HKEY_LOCAL_MACHINE',
												@key = @subkey,
												@value_name = @NextValueName,
												@type = 'REG_SZ',
												@value = @CurrentFlagToAdd;

		-- Increment flag added count
		SET @FlagAddCount = @FlagAddCount + 1

		-- Add the flag to our master list
		INSERT INTO #SQLParameters ( value_name, value ) VALUES  (@NextValueName,@CurrentFlagToAdd)
	END

FETCH NEXT FROM #FlagsToAdd INTO @CurrentFlagToAdd
END
CLOSE #FlagsToAdd
DEALLOCATE #FlagsToAdd

PRINT CAST(@FlagAddCount AS VARCHAR) + ' Startup Trace Flags Added'

IF (@FlagAddCount > 0)
	PRINT '*** Remember to restart the SQL Server Engine ***'

SET NOCOUNT OFF