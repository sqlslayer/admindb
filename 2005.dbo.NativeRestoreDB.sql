IF OBJECT_ID('[dbo].[NativeRestoreDB]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[NativeRestoreDB]
GO

/*******************************************************************************************************
**	Name:			dbo.NativeRestoreDB
**	Desc:			Restore database based on file name
**	Auth:			Matt Stanford (SQLSlayer.com)
**  Dependancies:	dbo.SQLServerLogDir_fn, dbo.SQLServerDataDir_fn, dbo.KillProcess
**	Debug:			EXEC dbo.NativeRestoreDB 'E:\Trace90.bak', 3, NULL, NULL, NULL, 1
**	Date:			2007.05.14
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20070604	Matt Stanford	Changed the 90 version to use the DBO schema
**	20071203	Matt Stanford	Added some logic to account for offline/restoring DBs
**	20080917	Adam Bean		Cleaned up code
								Added 2008 support
**  20081120	Adam Bean		Added password argument
**	20090415	Matt Stanford	Split up the logic for finding data and log directories
**	20090610	Matt Stanford	Merged in Bean's changes
**  20090625	Adam Bean		Added @ChangeDBOwner & @NewDBOwner
**	20090626	Matt Stanford	Added @DropDB to KillProcess call
**	20090629	Matt Stanford	Added @ShowProcesses parameter to KillProcess call
**  20090728	Adam Bean		Fixed for case sensitive collation
**	20090929	Adam Bean		Global header formatting & general clean up
**	20091014	Adam Bean		Resolved issue with changing owner if database was backed up in single user/read only
**	20100316	John DelNostro	Added User Cleanup options
**  20110616	John DelNostro	Added Restore Restart Options (2005, 2008 only)
**  20110616	John DelNostro	Added option for adding standard nt group permissions (useful for new db restores)
**	20111006	Kathy Toth		Changed FileID from TINYINT to INT in #Files to support backups with full text files
**	20120201	Adam Bean		Added @DeleteHistory for KillProcesses call
**	20120305	Adam Bean		Expanded @SQL to MAX for backups with many file groups, added @Recovery option
**	20120315	Adam Bean		Change @SQL to NVARCHAR to support if the database already exists based on current logic
**	20120406	Adam Bean		Added 2012 support until rewrite is done
**  20130521    pDale Campbell  Added option for stats, made debug mode more effective
**  20130605	Jeff Gogel		Fixed logic that determines SQL version to prevent casting errors
**	20130605	Adam Bean		Fixed new @Debug changes 
**	20130624	Adam Bean		More fixing for 2012 stuff that broke due to previous changes
**	20160322	Jeff Gogel		Added columns for 2014 support
	20161222	Jeff Gogel		Added columns for 2016 support
**	20161227	Jeff Gogel	Removed references to old master.sys.sysdatabases
********************************************************************************************************/

CREATE PROCEDURE [dbo].[NativeRestoreDB]
(
	@filename					VARCHAR(512)
	,@RestoreFreshnessHours		TINYINT			= 0
	,@NewDBName					VARCHAR(512)	= NULL
	,@DestDataPath				VARCHAR(512)	= NULL
	,@DestLogPath				VARCHAR(512)	= NULL
	,@Recovery					BIT				= 1
	,@Password					VARCHAR(512)	= NULL
	,@ChangeDBOwner				BIT				= 1
	,@NewDBOwner				VARCHAR(48)		= NULL
	,@CleanupUsers				BIT				= 0
	,@RestartRestore			BIT				= 0
	,@AddStandardPermissions	BIT				= 0
	,@Domain					NVARCHAR(25)    = NULL
	,@DeleteHistory				BIT				= 0
	,@Debug						BIT				= 0
    ,@Stats                     INT             = 0
)

AS

SET NOCOUNT ON

DECLARE
	 @DBName					VARCHAR(512)
	,@OrigDBName				VARCHAR(512)
	,@SQL						NVARCHAR(MAX)
	,@tempSQL					VARCHAR(1000)
	,@killSQL					VARCHAR(1000)
	,@sort						INT
	,@didDelete					TINYINT
	,@didRestore				TINYINT
	,@scriptKickoff				DATETIME
	,@LastRestoreDate			DATETIME
	,@IntentionallySkipped		TINYINT
	,@IsRenamingDB				TINYINT
	,@CompatLevel				TINYINT
	,@Recover					VARCHAR(10)

SET @RestoreFreshnessHours = ISNULL(@RestoreFreshnessHours,0)
SET @scriptKickoff = GETDATE()
SET @didDelete = 0
SET @didRestore = 0
SET @IntentionallySkipped = 0

IF @Recovery = 1
	SET @Recover = 'RECOVERY'
ELSE 
	SET @Recover = 'NORECOVERY'

IF (@@MICROSOFTVERSION / 0x01000000) < 9
	RAISERROR('NativeRestoreDB is only compatible with version 9.0 of SQL Server or higher',20,1) WITH LOG

DECLARE @cmds TABLE
(
	[sort]				INT
	,[SQL]				VARCHAR(MAX)
)

DECLARE @PhysFiles TABLE
(
	[FileID]			INT IDENTITY
	,[OrigFileName]		VARCHAR(512)
	,[NewFileName]		VARCHAR(512)	
	,[LogicalName]		VARCHAR(512)
	,[ContainsBUName]	TINYINT DEFAULT 0
	,[IsLogFile]		TINYINT
)

IF OBJECT_ID('tempdb.dbo.#files') IS NOT NULL
	DROP TABLE #files

CREATE TABLE #files (
	[LogicalName]		VARCHAR(256)
	,[PhysicalName]		VARCHAR(512)
	,[Type]				VARCHAR(10)
	,[FileGroupName]	VARCHAR(100)
	,[Size]				BIGINT
	,[MaxSize]			BIGINT
	,[FileID]			INT
	,[CreateLSN]		VARCHAR(100)
	,[DropLSN]			VARCHAR(100)
	,[UniqueID]			VARCHAR(100)
	,[ReadOnlyLSN]		VARCHAR(100)
	,[ReadWriteLSN]		VARCHAR(100)
	,[BackupSize]		BIGINT
	,[SourceBlkSize]	BIGINT
	,[FileGroupID]		TINYINT
	,[LogGroupGUID]		VARCHAR(100)
	,[DiffBaseLSN]		VARCHAR(100)
	,[DiffBaseGUID]		VARCHAR(100)
	,[IsReadOnly]		TINYINT
	,[IsPresent]		TINYINT
)

IF OBJECT_ID('tempdb.dbo.#header') IS NOT NULL
	DROP TABLE #header

CREATE TABLE #header 
(
	[bacnam]			NVARCHAR(100)
	,[bacdes]			NVARCHAR(100)
	,[bactyp]			NVARCHAR(100)
	,[expdat]			DATETIME
	,[compre]			NVARCHAR(100)
	,[positi]			NVARCHAR(100)
	,[devtyp]			NVARCHAR(100)
	,[usenam]			NVARCHAR(100)
	,[sernam]			NVARCHAR(100)
	,[datnam]			NVARCHAR(100) -- 10
	,[datver]			NVARCHAR(100)
	,[datcre]			DATETIME
	,[bacsiz]			NVARCHAR(100) -- 13
	,[firlsn]			NVARCHAR(100)
	,[laslsn]			NVARCHAR(100)
	,[chelsn]			NVARCHAR(100)
	,[difbas]			NVARCHAR(100)
	,[bacsta]			DATETIME
	,[bacfin]			DATETIME
	,[sorord]			NVARCHAR(100) -- 20
	,[codpag]			NVARCHAR(100)
	,[uniloc]			NVARCHAR(100)
	,[unicom]			NVARCHAR(100)
	,[comlev]			NVARCHAR(100)
	,[sofven]			NVARCHAR(100)
	,[sofvma]			NVARCHAR(100)
	,[sofvmi]			NVARCHAR(100)
	,[sofvbu]			NVARCHAR(100)
	,[macnam]			NVARCHAR(100)
	,[flags]			NVARCHAR(100) -- 30
	,[bindin]			NVARCHAR(100)
	,[recfor]			NVARCHAR(100)
	,[collat]			NVARCHAR(100)
	,[famgui]			NVARCHAR(100)
	,[hasbul]			TINYINT
	,[issnap]			TINYINT
	,[isro]				TINYINT
	,[issu]				TINYINT
	,[HasChecksum]		TINYINT
	,[IsDamaged]		TINYINT
	,[BeginsLogChain]	TINYINT
	,[IncompleteMeta]	TINYINT
	,[IsForceOffline]	TINYINT
	,[IsCopyOnly]		TINYINT
	,[FirstRecovForkID]	NVARCHAR(100)
	,[ForkPoINTLSN]		NVARCHAR(100)
	,[RecoveryModel]	NVARCHAR(100)
	,[DiffBaseLSN]		NVARCHAR(100)
	,[DiffBaseGUID]		NVARCHAR(100)
	,[BackupTypeDesc]	NVARCHAR(100)
	,[BackupSETGUID]	NVARCHAR(100)
)

--Alter tables for 2008
IF (@@MICROSOFTVERSION / 0x01000000) = 10
BEGIN
       ALTER TABLE #files ADD [TDEThumbprint] VARCHAR(100)
       ALTER TABLE #header ADD [CompressedBackupSize] NVARCHAR(100)
END

--Alter tables for 2012
IF (@@MICROSOFTVERSION / 0x01000000) = 11
BEGIN
       ALTER TABLE #files ADD [TDEThumbprint] VARCHAR(100)
       ALTER TABLE #header ADD [CompressedBackupSize] NVARCHAR(100)
       ALTER TABLE #header ADD [Containment] BIT
END

--Alter tables for 2014
IF (@@MICROSOFTVERSION / 0x01000000) = 12
BEGIN
	ALTER TABLE #header ADD [CompressedBackupSize]	BIGINT
	ALTER TABLE #header ADD [Containment]			TINYINT
	ALTER TABLE #header ADD [KeyAlgorithm]			NVARCHAR(100)
	ALTER TABLE #header ADD [EncryptorThumbprint]	VARBINARY(100)
	ALTER TABLE #header ADD [EncryptorType]			NVARCHAR(100)
	ALTER TABLE #files ADD [TDEThumbprint]			VARBINARY(100)
END

--Alter tables for 2016
IF (@@MICROSOFTVERSION / 0x01000000) = 13
BEGIN
	ALTER TABLE #header ADD [CompressedBackupSize]	BIGINT
	ALTER TABLE #header ADD [Containment]			TINYINT
	ALTER TABLE #header ADD [KeyAlgorithm]			NVARCHAR(100)
	ALTER TABLE #header ADD [EncryptorThumbprint]	VARBINARY(100)
	ALTER TABLE #header ADD [EncryptorType]			NVARCHAR(100)
	ALTER TABLE #files ADD [TDEThumbprint]			VARBINARY(100)
	ALTER TABLE #files ADD [SnapshotURL]			NVARCHAR(360)
END

-- Get the Backup file header info
SET @SQL = 'RESTORE HEADERONLY FROM DISK = ''' + @filename + ''''
-- Add password
IF @Password IS NOT NULL
	SET @SQL = @SQL + 'WITH PASSWORD = ''' + @Password + ''''
	
INSERT INTO #header EXEC (@SQL)

SELECT 
	@OrigDBName		= [datnam],
	@CompatLevel	= CAST([comlev] AS TINYINT) 
FROM #header 
WHERE [bactyp] = 1 
AND [bacfin] = (SELECT MAX([bacfin]) FROM #header)

-- Restore DB as name
IF (@NewDBName IS NOT NULL)
BEGIN
	SET @DBName = @NewDBName
	SET @IsRenamingDB = 1
END
ELSE
BEGIN
	SET @DBName = @OrigDBName
	SET @IsRenamingDB = 0
END

-- Test to see if the backup will process OK
--IF (@CompatLevel > CAST(LEFT(CAST(SERVERPROPERTY('PRODUCTVERSION') AS VARCHAR(10)),1) + '0' AS TINYINT)) OR (@CompatLevel IS NULL)
--	RAISERROR('Backup type or compatibility level mismatch',20,1) WITH LOG

-- Get the list of files in the backup
SET @SQL = 'RESTORE FILELISTONLY FROM DISK = ''' + @filename + ''''
-- Add password
IF @Password IS NOT NULL
	SET @SQL = @SQL + 'WITH PASSWORD = ''' + @Password + ''''
	
INSERT INTO #files EXEC (@SQL)

-- Rename the files as necessary (if the restore changes names)
INSERT INTO @PhysFiles ([OrigFileName],[IsLogFile],[LogicalName])
SELECT 
	[PhysicalName]
	,CASE [Type]
		WHEN 'L' THEN 1
		ELSE 0
	END AS IsLogFile
	,[LogicalName]
FROM #files
	
-- Find where the file names contain the backup name
UPDATE @PhysFiles 
SET	[ContainsBUName] = 1
	,[NewFileName] = REPLACE([OrigFileName],@OrigDBName,@DBName)
WHERE CHARINDEX(@OrigDBName,[OrigFileName]) > 0

-- Change the file names of any files that don't have the DB name in them
UPDATE @PhysFiles
SET [ContainsBUName] = 0
	,[NewFileName] = '\' + @OrigDBName + '_' + @NewDBName + '_' + LTRIM(STR([FileID])) + SUBSTRING([OrigFileName],LEN([OrigFileName]) - CHARINDEX('.',REVERSE(OrigFileName)) + 1,100)
WHERE CHARINDEX(@OrigDBName,OrigFileName) <= 0

-- Revert the changes if we're not renaming this thing
IF @IsRenamingDB = 0
	UPDATE @PhysFiles SET [NewFileName] = [OrigFileName]

-- Base restore command
INSERT INTO @cmds ([sort],SQL) VALUES (1,'RESTORE DATABASE [' + @DBName + ']')
INSERT INTO @cmds ([sort],SQL) VALUES (2, 'FROM DISK = ''' + @filename + '''')
INSERT INTO @cmds ([sort],SQL) VALUES (9, 'WITH REPLACE')
INSERT INTO @cmds ([sort],SQL) VALUES (10, ',' + @Recover + '')


-- Add Restore Restart For Interrupted Restores
IF @RestartRestore = 1
INSERT INTO @cmds ([sort],SQL) VALUES (15, ',WITH RESTART')

-- Add Status info
IF @Stats <> 0
  BEGIN
    DECLARE @StatOpt VARCHAR(64);
    SELECT @StatOpt= ',STATS='+convert(VARCHAR(6),@Stats);
    INSERT INTO @cmds ([sort],SQL) VALUES (16, @StatOpt)
  END

-- Add password
IF @Password IS NOT NULL
	INSERT INTO @cmds (sort,SQL) VALUES (11,',PASSWORD = ''' + @Password + '''')
	
--- DETERMINE THE DATA/LOG DIRECTORIES
IF (@DestLogPath IS NULL) 
BEGIN
	-- Find existing database, use existing locations
	IF EXISTS(SELECT [name] FROM [dbo].[sysdatabases_vw] WHERE [name] = @DBName)
	BEGIN
		-- Get the log file directory
		SET @SQL = 'SELECT @in_dir = SUBSTRING([filename],0,LEN(filename)-CHARINDEX(''\'',REVERSE(REPLACE(filename,'' '','''')))+2) FROM [' + @DBName + '].[dbo].[sysfiles] WHERE [fileid] = 2'
		EXEC sp_executesql @SQL, N'@in_dir VARCHAR(512) OUTPUT', @in_dir = @DestLogPath OUTPUT

	END
	ELSE
	BEGIN
		-- Get the Default log directory
		SELECT @DestLogPath = [dbo].[SQLServerLogDir_fn]()
	END
END

IF (@DestDataPath IS NULL)
BEGIN

	-- Find existing database, use existing locations
	IF EXISTS(SELECT [name] FROM [dbo].[sysdatabases_vw] WHERE [name] = @DBName)
	BEGIN
		-- Get the data file directory
		SET @SQL = 'SELECT @in_dir = SUBSTRING(filename,0,LEN(filename)-CHARINDEX(''\'',REVERSE(REPLACE(filename,'' '','''')))+2) FROM [' + @DBName + '].[dbo].[sysfiles] WHERE [fileid] = 1'
		EXEC sp_executesql @SQL, N'@in_dir VARCHAR(512) OUTPUT', @in_dir = @DestDataPath OUTPUT
	END
	ELSE
	BEGIN
		-- Get the Default data directory
		SELECT @DestDataPath = [dbo].[SQLServerDataDir_fn]()
	END
END


-- Make sure the Data/Log paths contain a trailing "\"
IF RIGHT(@DestLogPath,1) <> '\'
	SET @DestLogPath = @DestLogPath + '\'

IF RIGHT(@DestDataPath,1) <> '\'
	SET @DestDataPath = @DestDataPath + '\'

-- ADD the MOVE commands to the command list
INSERT INTO @cmds ([sort],[SQL])
SELECT 
	10
	,CASE [IsLogFile]
		WHEN 1 
		THEN ',MOVE ''' + LogicalName + ''' TO ''' + @DestLogPath + SUBSTRING([NewFileName],LEN([NewFileName]) - CHARINDEX('\',REVERSE([NewFileName])) + 2,100) + ''''
	ELSE ',MOVE ''' + LogicalName + ''' TO ''' + @DestDataPath + SUBSTRING([NewFileName],LEN([NewFileName]) - CHARINDEX('\',REVERSE([NewFileName])) + 2,100) + ''''
	END AS [MoveCommand]
FROM @PhysFiles

DROP TABLE #files
DROP TABLE #header

PRINT ('Successfully read backup information for ' + @DBName + char(10) + '     Kicking off restore logic now')

-- Check to make sure that we didn't just restore this guy
SET @LastRestoreDate = ISNULL((SELECT MAX([restore_date]) FROM [msdb].[dbo].[restorehistory] WHERE [destination_database_name] = @DBName),DATEADD(YEAR,-1,GETDATE()))

IF (DATEADD(HOUR,@RestoreFreshnessHours,@LastRestoreDate) < @scriptKickoff) OR @RestoreFreshnessHours = 0
BEGIN
	PRINT('Last restore was more than ' + CAST(@RestoreFreshnessHours AS VARCHAR(3)) + ' hours ago.  We''re doing a restore')

	-------------------------------------------------------
	-- OK, the commands have been built, now throw them all together
	-------------------------------------------------------
	DECLARE curs1 CURSOR LOCAL STATIC FOR
	SELECT 
		[sort]
		,[SQL] 
	FROM @cmds 
	ORDER BY sort ASC
	OPEN curs1
	FETCH NEXT FROM curs1 INTO @sort,@tempSQL

	SET @SQL = ''
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @SQL = @SQL + @tempSQL
		SET @SQL = @SQL + char(10) + char(9)

	FETCH NEXT FROM curs1 INTO @sort,@tempSQL
	END
	CLOSE curs1
	DEALLOCATE curs1

	SET @killSQL = 'PRINT(''Killing the database'')
	EXEC [dbo].[KillProcess] @DBName = ''' + @DBName + ''', @DropDB = 1, @ShowProcesses = 0, @DeleteHistory = ' + CAST(@DeleteHistory AS VARCHAR) + '
	PRINT(''    Killing done'')
	PRINT(''Restoring ' + @DBName + ''' + char(10))' + char(10) + char(10) 

	-- Check to see if the database exists
	IF EXISTS(SELECT * FROM [dbo].[sysdatabases_vw] WHERE [name] = @DBName)
	BEGIN
		IF @Debug = 1
			PRINT (@killSQL)
		ELSE
			EXEC (@killSQL)

		IF (SELECT COUNT(*) FROM [dbo].[sysdatabases_vw] WHERE [name] = @DBName) = 0
			SET @didDelete = 1
	END

	IF @Debug = 1
		PRINT (@SQL)
	ELSE
		EXEC (@SQL)

	SET @LastRestoreDate = ISNULL((SELECT MAX([restore_date]) FROM [msdb].[dbo].[restorehistory] WHERE [destination_database_name] = @DBName),DATEADD(YEAR,-1,GETDATE()))

	-- Test to see if the restore worked
	IF @LastRestoreDate >= @scriptKickoff
		SET @didRestore = 1
END
ELSE
BEGIN
	PRINT ('Did not restore anything... the last restore was pretty recent')
	SET @IntentionallySkipped = 1
END

IF @Recovery = 1
BEGIN
	IF @ChangeDBOwner = 1
	BEGIN
		-- If new owner was specified, use that, if not, determine primary sa account
		IF @NewDBOwner IS NULL
			SET @NewDBOwner = (SELECT dbo.WhatIsGodsName_fn())
			
		-- Make sure the database isn't in single user or read only
        IF @Debug = 0
          BEGIN
    		IF (SELECT [user_access_desc] FROM [sysdatabases_vw] WHERE [name] = @DBName) = 'SINGLE_USER'
    			EXEC('ALTER DATABASE [' + @DBName + '] SET MULTI_USER')
    		IF (SELECT [is_read_only] FROM [sysdatabases_vw] WHERE [name] = @DBName) = 1
    			EXEC('ALTER DATABASE [' + @DBName + '] SET READ_WRITE')
          END
				
		-- Change owner
		SET @SQL = 'USE [' + @DBName + '] EXEC [sp_changedbowner] @loginame = ''' + @NewDBOwner + ''''
		IF @Debug = 1
			PRINT (@SQL)
		ELSE
			EXEC (@SQL)

		IF @CleanupUsers = 1
          BEGIN
		    IF @Debug = 1
		        PRINT ('EXEC [dbo].[UserCleanup] @DBName = ''' + @DBName + '''')
            ELSE
		        EXEC [dbo].[UserCleanup] @DBName = @DBName 
          END
	END
END

-- Add back standard nt permissions
IF @AddStandardPermissions = 1 AND @Domain IS NOT NULL
  BEGIN
    IF @Debug = 1
		PRINT ('EXEC [dbo].[StandardGroupPermissions] @DBName = ''' + @DBName + ''', @Domain = ''' + @Domain + '''')
    ELSE
        EXEC [dbo].[StandardGroupPermissions] @DBName = @DBName, @Domain = @Domain
  END
       
    
SELECT 
	@didDelete				AS [DidDelete]
	,@didRestore			AS [DidRestore]
	,@IntentionallySkipped	AS [IntentionallySkipped]
	,@LastRestoreDate		AS [LastRestoreDateTime]

SET NOCOUNT OFF