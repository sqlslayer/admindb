﻿IF OBJECT_ID('[dbo].[SecurityRequest]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[SecurityRequest]
GO

/******************************************************************************
* Name
	[dbo].[SecurityRequest]

* Author
	Jeff Gogel
	
* Date
	2012.12.01
	
* Synopsis
	Scripts out security in the form of a request.
	
* Description
	Uses a bunch of nasty cursors to output the results of dbo.GetSQLPermissions in the form of an access request.

* Examples
	EXEC [dbo].[SecurityRequest]

* Dependencies
	dbo.GetSQLPermissions

* Parameters
	@LoginName						= Logins to include (CSV supported, NULL for all)
	@LoginNameExclude				= Logins to include (CSV supported, NULL for all)
	
* Notes
	A handful of certificates, default accounts, etc. are excluded automatically.

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------

******************************************************************************/

CREATE PROCEDURE [dbo].[SecurityRequest]
(
	@LoginName VARCHAR(8000) = NULL
	,@LoginNameExclude VARCHAR(8000) = NULL
)
AS

SET NOCOUNT ON

DECLARE @SQL			VARCHAR(8000)
DECLARE @CurrentLoginName		VARCHAR(1000)
DECLARE @DBName			VARCHAR(1000)
DECLARE @RoleName		VARCHAR(1000)

IF OBJECT_ID('tempdb.dbo.#SecurityRequests') IS NOT NULL
	DROP TABLE #SecurityRequests

IF OBJECT_ID('tempdb.dbo.#SQLPerms') IS NOT NULL
	DROP TABLE #SQLPerms

CREATE TABLE #SecurityRequests
(
		[RequestDetail] VARCHAR(8000)
)

CREATE TABLE #SQLPerms
(
	    DBMSInstanceID INT
	,   InstanceName VARCHAR(100) 
	,	LoginType VARCHAR(60) 
	,	LoginName VARCHAR(128) 
	,	UserName VARCHAR(100) 
	,	RoleType VARCHAR(10) 
	,	RoleName VARCHAR(100) 
	,	DatabaseName VARCHAR(100) 
	,	SID VARBINARY(85) 
	,	CreateDate VARCHAR(50) 
	,	ModifyDate VARCHAR(50) 
	,	Password_hash VARBINARY(256)
	,   PasswordPolicyChecked BIT
	,   PasswordExpirationChecked BIT
	,   SQLLoginDisabled BIT  
)


-- Load permissions
INSERT INTO #SQLPerms EXEC dbo.GetSQLPermissions


-- Header
INSERT INTO #SecurityRequests VALUES ('SQL Request - Please forward to the SQL Team:')
INSERT INTO #SecurityRequests VALUES ('')
SET @SQL = 'Grant the following access on ' + @@SERVERNAME + '...'
INSERT INTO #SecurityRequests VALUES (@SQL)
INSERT INTO #SecurityRequests VALUES ('')


-- Loop through logins
DECLARE #Logins CURSOR LOCAL STATIC FOR
SELECT DISTINCT LoginName
FROM #SQLPerms
WHERE LoginName NOT IN
('sa','##MS_SQLResourceSigningCertificate##','##MS_SQLReplicationSigningCertificate##','##MS_SQLAuthenticatorCertificate##','##MS_PolicySigningCertificate##','##MS_SmoExtendedSigningCertificate##','##MS_PolicyTsqlExecutionLogin##','UHHS\svcSQLAdm1','UHHS\SQLDBAALL','UHHS\IT&S Data Protection','UHHS\svcSQLSCOM','NT SERVICE\SQLWriter','NT SERVICE\Winmgmt','NT SERVICE\ClusSvc','NT AUTHORITY\SYSTEM','UHHS\svcSQLMonitor','##MS_PolicyEventProcessingLogin##','##MS_AgentSigningCertificate##')
AND (@LoginName IS NULL OR LoginName IN (@LoginName))
AND (@LoginNameExclude IS NULL OR LoginName NOT IN (@LoginNameExclude))
ORDER BY LoginName
OPEN #Logins
FETCH NEXT FROM #Logins INTO @CurrentLoginName
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Check for server level access
	IF ((SELECT COUNT(0) FROM #SQLPerms WHERE LoginName = @CurrentLoginName AND RoleType = 'Server') > 0)
	BEGIN
		
		PRINT @CurrentLoginName

		SET @SQL = 'Grant "' + @CurrentLoginName + '" the '

		-- Loop through each server role
		DECLARE #ServerRoles CURSOR LOCAL STATIC FOR
		SELECT RoleName
		FROM #SQLPerms
		WHERE LoginName = @CurrentLoginName AND RoleType = 'Server'
		OPEN #ServerRoles
		FETCH NEXT FROM #ServerRoles INTO @RoleName
		WHILE @@FETCH_STATUS = 0
		BEGIN

			-- Add role names
			SET @SQL = @SQL + @RoleName + ', '
		

		FETCH NEXT FROM #ServerRoles INTO @RoleName
		END
		CLOSE #ServerRoles
		DEALLOCATE #ServerRoles

		IF RIGHT(@SQL,2) = ', ' 
			SET @SQL = SUBSTRING(@SQL,1,len(@SQL)-1)

		SET @SQL = @SQL + ' server level access. '

		-- Insert record details
		INSERT INTO #SecurityRequests VALUES (@SQL)

	END

	-- Loop through each DB
	DECLARE #Databases CURSOR LOCAL STATIC FOR
	SELECT DISTINCT DatabaseName
	FROM #SQLPerms
	WHERE LoginName = @CurrentLoginName AND RoleType = 'Database'
	OPEN #Databases
	FETCH NEXT FROM #Databases INTO @DBName
	WHILE @@FETCH_STATUS = 0
	BEGIN

			-- Start statement
			SET @SQL = 'Grant "' + @CurrentLoginName + '" '

			-- Loop through each role
			DECLARE #DBPerms CURSOR LOCAL STATIC FOR
			SELECT RoleName
			FROM #SQLPerms
			WHERE LoginName = @CurrentLoginName AND RoleType = 'Database' AND DatabaseName = @DBName
			OPEN #DBPerms
			FETCH NEXT FROM #DBPerms INTO @RoleName
			WHILE @@FETCH_STATUS = 0
			BEGIN

				-- Add role names
				SET @SQL = @SQL + @RoleName + ', '

			FETCH NEXT FROM #DBPerms INTO @RoleName
			END
			CLOSE #DBPerms
			DEALLOCATE #DBPerms

			-- Finish statement
			IF RIGHT(@SQL,2) = ', ' 
				SET @SQL = SUBSTRING(@SQL,1,len(@SQL)-1)

			SET @SQL = @SQL + ' access to the ' + @DBName + ' database. '

			
			-- Insert database access request details
			INSERT INTO #SecurityRequests VALUES (@SQL)
	FETCH NEXT FROM #Databases INTO @DBName
	END
	CLOSE #Databases
	DEALLOCATE #Databases


FETCH NEXT FROM #Logins INTO @CurrentLoginName
END
CLOSE #Logins
DEALLOCATE #Logins


SELECT * FROM #SecurityRequests

SET NOCOUNT OFF