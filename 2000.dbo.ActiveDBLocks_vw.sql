IF OBJECT_ID('[dbo].[ActiveDBLocks_vw]','V') IS NOT NULL 
	DROP VIEW [dbo].[ActiveDBLocks_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.ActiveDBLocks_vw
**	Desc:			View to display locks held per spid
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2007.05.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20080613	Adam Bean		Cleaned up code, renamed to ActiveDBLocks_vw
**  20080617	Adam Bean		Added locktype, lockstatus, lockmode
**  20090728	Adam Bean		Fixed for case sensitive collation
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE VIEW [dbo].[ActiveDBLocks_vw]

AS

SELECT 
	t.*
	,p.[loginame]				AS [Login]
	,p.[hostname]				AS [HostName]
	,p.[program_name]			AS [ProgramName]
FROM 
	(
		SELECT 
			d.[name]			AS [DBName]
			,DB_ID(d.[name])	AS [dbid]
			,l.[req_spid]		AS [spid]
			,COUNT(*)			AS [LockCount]
			,CASE l.[rsc_type]
				WHEN 2 THEN 'Database'
				WHEN 3 THEN 'File'
				WHEN 4 THEN 'Index'
				WHEN 5 THEN 'Table'
				WHEN 6 THEN 'Page'
				WHEN 7 THEN 'Key'
				WHEN 8 THEN 'Extent'
				WHEN 9 THEN 'RID (Row ID)'
				WHEN 10 THEN 'Application'
			END				AS [LockType]
			,CASE l.[req_status]
				WHEN 1 THEN 'Granted'
				WHEN 2 THEN 'Converting'
				WHEN 3 THEN 'Waiting'
			END				AS [LockStatus]
			,CASE l.[req_mode]
				WHEN 1 THEN 'Sch-S'
				WHEN 2 THEN 'Sch-M'
				WHEN 3 THEN 'S'
				WHEN 4 THEN 'U'
				WHEN 5 THEN 'X'
				WHEN 6 THEN 'IS'
				WHEN 7 THEN 'IU'
				WHEN 8 THEN 'IX'
				WHEN 9 THEN 'SIU'
				WHEN 10 THEN 'SIX'
				WHEN 11 THEN 'UIX'
				WHEN 12 THEN 'BU'
				WHEN 13 THEN 'RangeS_S'
				WHEN 14 THEN 'RangeS_U'
				WHEN 15 THEN 'RangeI_N'
				WHEN 16 THEN 'RangeI_S'
				WHEN 17 THEN 'RangeI_U'
				WHEN 18 THEN 'RangeI_X'
				WHEN 19 THEN 'RangeX_S'
				WHEN 20 THEN 'RangeX_U'
				WHEN 21 THEN 'RangeX_X'
			END				AS [LockMode]
		FROM [master].[dbo].[syslockinfo] l
		LEFT OUTER JOIN [master].[dbo].[sysdatabases] d
			ON d.[dbid] = l.[rsc_dbid]
		GROUP BY l.[req_spid], l.[rsc_type], l.[req_status], l.[req_mode], d.[name]
	) t
LEFT OUTER JOIN	[master].[dbo].[sysprocesses] p
	ON p.[spid] = t.[spid]