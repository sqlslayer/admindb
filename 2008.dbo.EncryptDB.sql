IF OBJECT_ID('[dbo].[EncryptDB]','P') IS NOT NULL 
       DROP PROCEDURE [dbo].[EncryptDB]
GO

/******************************************************************************
* Name
	[dbo].[EncryptDB]

* Author
	Jeff Gogel (SQLSlayer.com)
	
* Date
	2012.09.20
	
* Synopsis
	Encrypt databases with TDE.
	
* Description
	Use this SP to encrypt databases.  @BackupDirectory and @Password are required if you're using a certificate that has not been created.

* Examples
	EXEC dbo.EncryptDB @DBName = 'TestDB'

* Dependencies
	dbo.Split_fn
	dbo.sysdatabases_vw
	dbo.EncryptedDBs_vw

* Parameters
	@DBName = Database name(s) to run code against (CSV supported, NULL for all)
	@DBNameExclude = Database name(s) to exclude from code (CSV supported)
	@CertName = Name of certificate to use.  A certificate with the default naming convention will be created/used if a cert name is not specified
	@CertSubject = Subject/descrption of certificate to use.  Only used if a new certificate is being created
	@BackupDirectory = Directory for certificate and private key backups if not already created
	@Password = Password for private key backup if certificate has not been created
	@Debug = Show, but don't execute work to be done
	
* Notes


*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	YYYYDDMM	Full Name	
	12272016	Jeff Gogel		Removed ORDER BY ordinal column number
******************************************************************************/

CREATE PROCEDURE [dbo].[EncryptDB]
(
       @DBName             NVARCHAR(2048)   = NULL
       ,@DBNameExclude		NVARCHAR(2048)	= NULL
	   ,@CertName			NVARCHAR(256)	= NULL
	   ,@CertSubject		NVARCHAR(256)	= NULL
       ,@BackupDirectory	NVARCHAR(256)	= NULL
	   ,@Password			NVARCHAR(256)	= NULL
       ,@Debug              BIT				= 0
)

AS

DECLARE @SQLVersion		INT
		,@SQLEdition	VARCHAR(32)
		,@ServerName	NVARCHAR(128)
		,@InstanceName	NVARCHAR(128)
		,@SQL			NVARCHAR(MAX)


SET @SQLVersion = @@MICROSOFTVERSION / 0x01000000
SET @SQLEdition = (SELECT CAST(SERVERPROPERTY('EDITION') AS VARCHAR))
SET @ServerName = (SELECT CAST(SERVERPROPERTY('MachineName') AS VARCHAR(128)))
SET @InstanceName = ISNULL((SELECT CAST(SERVERPROPERTY('InstanceName') AS VARCHAR(128))),'DEFAULT')

IF @CertName IS NULL
	SET @CertName = CAST((SELECT 'CERT_TDE_' + @ServerName + '_' + @InstanceName) AS VARCHAR(256))

IF @CertSubject IS NULL
BEGIN
	IF @InstanceName = 'DEFAULT'
		SET @CertSubject = 'Certificate source server: ' + @ServerName
	ELSE
		SET @CertSubject = 'Certificate source server: ' + @ServerName + '\' + @InstanceName
END

IF RIGHT(@BackupDirectory,1) <> '\'
	SET @BackupDirectory = @BackupDirectory + '\'

-- check requirements
IF (@SQLVersion < 10)
BEGIN
	PRINT 'Transparent Data Encryption is only supported on SQL Server 2008 or later.'
	RETURN
END
IF (@SQLEdition NOT LIKE 'Developer%' AND @SQLEdition NOT LIKE 'Enterprise%')
BEGIN
	PRINT 'Transparent Data Encryption is only supported on Developer and Enterprise edition.'
	RETURN
END
IF NOT  EXISTS (SELECT name FROM [master].[sys].[symmetric_keys] WHERE name = '##MS_DatabaseMasterKey##')
BEGIN
	PRINT '
--There is no database master key for the master database.  You must create one before continuing.
USE [master]
GO

CREATE MASTER KEY ENCRYPTION BY PASSWORD = ''YourNewPasswordHere''
GO

/***** OPTIONAL *****/
BACKUP MASTER KEY TO FILE = ''C:\NewMasterKeyBackup.dmk'' ENCRYPTION BY PASSWORD = ''YourBackupPasswordHere''
GO

ALTER MASTER KEY ADD ENCRYPTION BY SERVICE MASTER KEY
GO

CLOSE MASTER KEY
GO'
	RETURN
END

-- create certificate if needed
IF (NOT EXISTS (SELECT name FROM [master].[sys].[certificates] WHERE name = @CertName))
BEGIN
	IF ((@BackupDirectory IS NULL) OR (@Password IS NULL))
	BEGIN
		PRINT 'Provide values for @BackupDirectory and @Password so the certificate can be backed up after creation.'
		RETURN
	END

	BEGIN TRY
		SET @SQL = '

USE [master];
CREATE CERTIFICATE [' + @CertName + '] WITH SUBJECT = ''' + @CertSubject +  ''';

BACKUP CERTIFICATE [' + @CertName + '] TO FILE = ''' + @BackupDirectory + @CertName + '.cer'' 
	WITH PRIVATE KEY (FILE = ''' + @BackupDirectory + 'PK_' + @CertName + '.pvk'', ENCRYPTION BY PASSWORD = ''' + @Password + ''');'

		IF (@Debug=1)
			PRINT @SQL
		ELSE
			EXEC (@SQL)

	END TRY
	BEGIN CATCH
		PRINT 'Error Number ' + CAST(ERROR_NUMBER() AS VARCHAR) + ': ' + ERROR_MESSAGE()
		RETURN
	END CATCH
END

-- encrypt the db's
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [admin].[dbo].[sysdatabases_vw] s
LEFT JOIN [admin].[dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [admin].[dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
AND database_id > 4 -- Exclude system databases
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

IF (EXISTS (SELECT DBName FROM [admin].[dbo].[EncryptedDBs_vw] WHERE DBName = @DBName AND EncryptionState = 'Encrypted'))
	PRINT @DBName + ' was already encrypted.'
ELSE IF (EXISTS (SELECT DBName FROM [admin].[dbo].[EncryptedDBs_vw] WHERE DBName = @DBName AND EncryptionState <> 'Encrypted'))
BEGIN
	BEGIN TRY
		SET @SQL = '
ALTER DATABASE [' + @DBName + '] SET ENCRYPTION ON;'-- turn on encryption

		IF (@Debug=1)
			PRINT @SQL
		ELSE
		BEGIN
			EXEC (@SQL)
			PRINT @DBName + ' is now encrypted.'
		END
	END TRY
	BEGIN CATCH
		PRINT 'Error Number ' + CAST(ERROR_NUMBER() AS VARCHAR) + ': (' + @DBName + ') ' + ERROR_MESSAGE()
	END CATCH
END
ELSE
BEGIN
	BEGIN TRY

		SET @SQL = '

USE [' + @DBName + '];
CREATE DATABASE ENCRYPTION KEY WITH ALGORITHM = AES_256 ENCRYPTION BY SERVER CERTIFICATE [' + @CertName + '];
ALTER DATABASE [' + @DBName + '] SET ENCRYPTION ON;'

		IF (@Debug=1)
			PRINT @SQL
		ELSE
		BEGIN
			EXEC (@SQL)
			PRINT @DBName + ' is now encrypted.'
		END
			
	END TRY
	BEGIN CATCH
		PRINT 'Error Number ' + CAST(ERROR_NUMBER() AS VARCHAR) + ': (' + @DBName + ') ' + ERROR_MESSAGE()
	END CATCH
END

FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs
