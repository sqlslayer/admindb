IF OBJECT_ID('[dbo].[UpdateStats]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[UpdateStats]
GO

/******************************************************************************
* Name
	[dbo].[UpdateStats]

* Author
	Adam Bean
	
* Date
	2008.02.28
	
* Synopsis
	Updates statistics against specified database/table/index or all
	
* Description
	Maintenance routine which has the ability to dynamically update statistics with default samping, fullscan or sample on any or all specified
	databases/tables based on the last time stats were updated. Robust options provide granular control of minimum thresholds of what to maintain.

* Examples
	EXEC [dbo].[UpdateStats]
	EXEC [dbo].[UpdateStats] @MinRowCount = 100
	EXEC [dbo].[UpdateStats] @DBName = 'admin', @FullScan = 1
	EXEC [dbo].[UpdateStats] @DBName = 'admin', @UpdateAll = 1, @FullScan = 1
	EXEC [dbo].[UpdateStats] @StatsOlderThan = 48
	EXEC [dbo].[UpdateStats] @AutoSample = 1

* Dependencies
	dbo.Split_fn
	dbo.sysdatabases_vw

* Parameters
	@DBName					= Databas(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@DBNameExclude			= Databas(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@TableName				= Table(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@TableNameExclude		= Table(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@IndexName				= Indexes(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@IndexNameExclude		= Index(es) to excluded (CSV supported, NULL for all, accepts % for searching)
	@AutoSample				= Built in logic that will dynamically determine sample rates (off by default)
	@FullScan				= Force a FULLSCAN (override all other logic)
	@SampleRatePercent		= Sample percent to UPDATE with
	@Resample				= Update each statistic using its most recent sample rate.
	@Norecompute			= Disable the automatic statistics update option
	@StatsOlderThan			= In hours, filter based on the last time stats were updated older than this value
	@MinRowsChanged			= Minimum level of data row change
	@MinRowCount			= Minimum level of data row count
	@MinPctChange			= Minimum level of data percentage change (change / count) 
	@UpdateAll				= This will ignore any filtering threshold logic
	@Logging				= Log the state of the statistics prior to executing
	@Debug					= Display what will be maintained and the supporting queries
	
* Notes
	*** Statistics will always have a 0 for RowCount - meaning that if you use @MinPctChange and/or @MinRowCount, you will omit all statistics (not indexing though)
			
	Add the extended property "UpdateStats" to any table that you want to override the logic of this procedure on
	For the value of this proeprty, simply add any options after the WITH statement, csv supported: SAMPLE X PERCENT/ROWS, NORECOMPUTE
	Example how to add: 
		EXEC sys.sp_addextendedproperty 
			@name=N'UpdateStats'
			,@value=N'SAMPLE 10000 ROWS, NORECOMPUTE' 
			,@level0type=N'SCHEMA'
			,@level0name=N'dbo'
			,@level1type=N'TABLE'
			,@level1name=N'TableNameGoesHere'
*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20090511	Adam Bean		Added ARITHABORT setting
	20090706	Adam Bean		Added logging
	20090929	Adam Bean		Global header formatting & general clean up
	20110825	Adam Bean		Starting over
	20111212	Adam Bean		Added exclusion for read only databases
	20140216	Adam Bean		Changed logic to work at the stats level vs. table - old logic available via @AutoSample
								Added @IndexName, @IndexNameExclude and added them to search capabilities
								Removed STATS_DATE from the staging table as found that it was causing dupes
								Added @RowsChanged to the logging
								Added @MinPctChange, @MinRowCount, @MinRowsChanged
	20140222	Adam Bean		Added @DateStart, @DateEnd to record start and end times of the statistic maintenance per object
								Brought in RunID from logger to here so that this is recorded for the whole run vs. by object
	20140301	Adam Bean		Added LastStatsUpdate into debug and logging
								Added @StatsOlderThan to allow for filtering based on LastStatsUpdated
								Changed logic to check temporary tables from the table to the table + stat
								Added @UpdateAll to allow for overriding OF the default thresholds (primarily @MinRowsChanged = 1)
	20140511	Adam Bean		Excluded AlwaysOn replicas 
	20161227	Paul Popovich		Update order by to names from numbers per the 2016 upgrade advisor
******************************************************************************/

CREATE PROCEDURE [dbo].[UpdateStats] 
(
	@DBName						NVARCHAR(2048)	= NULL		
	,@DBNameExclude				NVARCHAR(2048)	= NULL		
	,@TableName					NVARCHAR(2048)	= NULL		
	,@TableNameExclude			NVARCHAR(2048)	= NULL
	,@IndexName					NVARCHAR(2048)	= NULL		
	,@IndexNameExclude			NVARCHAR(2048)	= NULL	
	,@AutoSample				BIT				= 0
	,@FullScan					BIT				= 0
	,@SampleRatePercent			TINYINT			= NULL
	,@Resample					BIT				= 0
	,@Norecompute				BIT				= 0
	,@StatsOlderThan			INT				= NULL
	,@MinRowsChanged			INT				= 1
	,@MinRowCount				INT				= NULL
	,@MinPctChange				INT				= NULL
	,@Logging					BIT				= 1
	,@UpdateAll					BIT				= 0
	,@Debug						BIT				= 0
)

AS

SET DEADLOCK_PRIORITY HIGH
SET ARITHABORT ON
SET QUOTED_IDENTIFIER ON
SET NOCOUNT ON

DECLARE 
	@TableNameSearch				NVARCHAR(512)
	,@TableNameSearchExclude		NVARCHAR(512)
	,@IndexNameSearch				NVARCHAR(512)
	,@IndexNameSearchExclude		NVARCHAR(512)
	,@SQL							NVARCHAR(2048)
	,@DBID							INT
	,@RunID							INT
	,@DateStart						DATETIME
	,@DateEnd						DATETIME
	,@SchemaName					VARCHAR(128)
	,@AutoUpdateStats				BIT
	,@OverrideValue					VARCHAR(32)
	,@Override						BIT
	,@StatName						NVARCHAR(128)
	,@UpdateCommand					VARCHAR(2048)
	,@UpdateCommandBase				VARCHAR(512)
	,@UpdateCommandSample			VARCHAR(24)
	,@UpdateCommandReSample			VARCHAR(16)
	,@UpdateCommandFull				VARCHAR(8)
	,@UpdateCommandNoRecompute		VARCHAR(16)
	,@Command						VARCHAR(256)
	,@PctChange						DECIMAL
	,@RowCount						BIGINT
	,@RowsChanged					BIGINT
	,@LastStatsUpdate				DATETIME
	,@ObjectExists					BIT

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'

-- Initialize variables
SET @UpdateCommandReSample		= 'RESAMPLE'
SET @UpdateCommandFull			= 'FULLSCAN'
SET @UpdateCommandNoRecompute	= 'NORECOMPUTE'

-- Get the run ID
IF @Debug = 0
	EXEC [dbo].[Maintenance_GetRunID] @RunID = @RunID OUTPUT

-- Determine if we're doing search or a match for database, table or index
IF CHARINDEX('%',@DBName) > 0
	SET @DBName = (SELECT [dbo].[DBSearch_fn](@DBName))
IF CHARINDEX('%',@DBNameExclude) > 0
	SET @DBNameExclude = (SELECT [dbo].[DBSearch_fn](@DBNameExclude))
IF CHARINDEX('%',@TableName) > 0
BEGIN
	SET @TableNameSearch = @TableName
	SET @TableName = NULL
END
IF CHARINDEX('%',@TableNameExclude) > 0
BEGIN
	SET @TableNameSearchExclude = @TableNameExclude
	SET @TableNameExclude = NULL
END
IF CHARINDEX('%',@IndexName) > 0
BEGIN
	SET @IndexNameSearch = @IndexName
	SET @IndexName = NULL
END
IF CHARINDEX('%',@IndexNameExclude) > 0
BEGIN
	SET @IndexNameSearchExclude = @IndexNameExclude
	SET @IndexNameExclude = NULL
END

-- Get all of the catalog views
-- Drop the tables if they exist
IF OBJECT_ID('tempdb.dbo.#sysstats') IS NOT NULL
   DROP TABLE #sysstats
IF OBJECT_ID('tempdb.dbo.#sysindexes') IS NOT NULL
   DROP TABLE #sysindexes
IF OBJECT_ID('tempdb.dbo.#systables') IS NOT NULL
   DROP TABLE #systables
IF OBJECT_ID('tempdb.dbo.#sysschemas') IS NOT NULL
   DROP TABLE #sysschemas
IF OBJECT_ID('tempdb.dbo.#sysextendedproperties') IS NOT NULL
   DROP TABLE #sysextendedproperties
IF OBJECT_ID('tempdb.dbo.#UpdateStats') IS NOT NULL
   DROP TABLE #UpdateStats

-- Create the tables
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT), [LastStatsUpdate] = CAST(NULL AS DATETIME), [rowcnt] = CAST(NULL AS BIGINT), [rowmodctr] = CAST(NULL AS BIGINT) INTO #sysstats FROM [sys].[stats]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysindexes FROM [sys].[indexes]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #systables FROM [sys].[tables]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT) INTO #sysschemas FROM [sys].[schemas]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT), [TableName] = CAST(NULL AS NVARCHAR(128)) INTO #sysextendedproperties FROM [sys].[extended_properties]

-- Retrieve data per database
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
	,[database_id]
FROM [dbo].[sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[is_read_only] = 0
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND s.[replica_id] IS NULL
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName, @DBID
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 
		
		-- sys.stats
		INSERT INTO #sysstats
		SELECT 
			s.*
			,' + @DBID + '
			,STATS_DATE(s.[object_id],s.[stats_id]) 
			,i.[rowcnt]
			,i.[rowmodctr]
		FROM [sys].[stats] s
		JOIN [dbo].[sysindexes] i -- To get rowcnt and rowmodctr
			ON i.[id] = s.[object_id]
			AND i.[name] = s.[name]
		WHERE i.[indid] != 0

		-- sys.indexes
		INSERT INTO #sysindexes
		SELECT 
			*
			,' + @DBID + ' 
		FROM [sys].[indexes]
		WHERE [index_id] != 0 -- Exclude heaps

		-- sys.tables
		INSERT INTO #systables
		SELECT 
			*
			,' + @DBID + ' 
		FROM [sys].[tables]

		-- sys.schemas 
		INSERT INTO #sysschemas
		SELECT 
			*
			,' + @DBID + ' 
		FROM [sys].[schemas]

		-- sys.extended_properties 
		INSERT INTO #sysextendedproperties
		SELECT 
			*
			,' + @DBID + '
			,OBJECT_NAME([major_id]) 
		FROM [sys].[extended_properties]
		WHERE [name] = ''UpdateStats''
	')

FETCH NEXT FROM #dbs INTO @DBName, @DBID
END
CLOSE #dbs
DEALLOCATE #dbs

-- Populate temp table with statistic information
SELECT DISTINCT
	DB_NAME(s.[database_id])																			AS [DBName]
	,t.[name]																							AS [TableName]
	,s.[name]																							AS [SchemaName]
	,a.[name]																							AS [StatName]
	,d.[is_auto_update_stats_on]																		AS [AutoUpdateStats]
	,a.[rowcnt]																							AS [RowCount]
	,a.[rowmodctr]																						AS [RowsChanged]
	,CAST(1.00 * CAST(a.[rowmodctr] AS BIGINT) / NULLIF(a.[rowcnt],0) * 100 AS DECIMAL(14,2))			AS [PctChange]
	,a.[LastStatsUpdate]																				AS [LastStatsUpdate]
	,CAST(e.[value] AS VARCHAR(32))																		AS [OverrideValue]
	,[Command] = CONVERT(VARCHAR(48),NULL)
	,[UpdateStatement] = CONVERT(VARCHAR(2048),NULL)
INTO #UpdateStats
FROM #systables t
JOIN [dbo].[sysdatabases_vw] d
	ON d.[database_id] = t.[database_id]
JOIN #sysstats a
	ON a.[database_id] = t.[database_id]
	AND a.[object_id] = t.[object_id]
JOIN #sysindexes i 
	ON i.[database_id] = t.[database_id] 
	AND i.[object_id] = t.[object_id]
JOIN #sysschemas s
	ON s.[database_id] = t.[database_id]
	AND s.[schema_id] = t.[schema_id]
LEFT JOIN #sysextendedproperties e
	ON e.[database_id] = t.[database_id]
	AND e.[major_id] = t.[object_id]
LEFT JOIN [dbo].[Split_fn](@TableName,',') tbl
	ON t.[name] = tbl.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@TableNameExclude,',') tble
	ON t.[name] = tble.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@IndexName,',') idx
	ON i.[name] = idx.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@IndexNameExclude,',') idxe
	ON i.[name] = idxe.[item] COLLATE DATABASE_DEFAULT
WHERE
(
	(t.[name] LIKE @TableNameSearch) -- Target specified table names based on a search
	OR (i.[name] LIKE @IndexNameSearch) -- Target specified index names based on a search
	OR (t.[name] NOT LIKE @TableNameSearchExclude) -- Exclude specified table names based on a search
	OR (i.[name] NOT LIKE @IndexNameSearchExclude) -- Exclude specified index names based on a search
	OR (@TableNameSearch IS NULL AND @IndexNameSearch IS NULL AND @TableNameSearchExclude IS NULL AND @IndexNameSearchExclude IS NULL)
)	
AND
(
	((tbl.[item] IS NOT NULL AND @TableName IS NOT NULL) OR @TableName IS NULL) -- Specified table(s), or all
	AND ((idx.[item] IS NOT NULL AND @IndexName IS NOT NULL) OR @IndexName IS NULL) -- Specified index(s), or all
	AND tble.[item] IS NULL -- All but excluded tables
	AND idxe.[item] IS NULL -- All but excluded indexes
)
AND (DATEDIFF(HOUR,[LastStatsUpdate],GETDATE()) > @StatsOlderThan OR @StatsOlderThan IS NULL) -- Only retrieve stats older than @StatsOlderThan 

-- Cursor through table names and update statistics
DECLARE #StatsCursor CURSOR STATIC LOCAL FOR
SELECT 
	[DBName]
	,[SchemaName]
	,[TableName]
	,[StatName]
	,[AutoUpdateStats]
	,[OverrideValue]
	,[PctChange]
	,[RowCount]
	,[RowsChanged]
	,[LastStatsUpdate]
FROM #UpdateStats
WHERE
(
	ISNULL([RowsChanged],0) >= ISNULL(@MinRowsChanged,0)	-- Only retrieve stats where the data change exceeds the parameter value
	AND ISNULL([RowCount],0) >= ISNULL(@MinRowCount,0)		-- Only retrieve stats where the data-level rowcount (stats will always be 0) exceeds the parameter value 
	AND ISNULL([PctChange],0) >= ISNULL(@MinPctChange,0)	-- Only retrieve stats where the percentage of change [change / count] (stats will always be 0) exceeds the parameter value
)
OR @UpdateAll = 1 -- Update everything (ignoring levels)
ORDER BY [DBName]
	,[SchemaName]
	,[TableName]
	,[StatName]

OPEN #StatsCursor
FETCH NEXT FROM #StatsCursor INTO @DBName, @SchemaName, @TableName, @StatName, @AutoUpdateStats, @OverrideValue, @PctChange, @RowCount, @RowsChanged, @LastStatsUpdate
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Initialize variables
	SET @UpdateCommandBase		= 'UPDATE STATISTICS [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '] [' + @StatName + '] WITH '
	SET @UpdateCommandSample	= 'SAMPLE # PERCENT'

	-- Reset @Override (for logging)
	SET @Override = 0

	-- Time to figure out what options will be issued for the update 
	-- If @FullScan is passed, then perform a FULLSCAN on everything
	IF @FullScan = 1
		SET @UpdateCommand = @UpdateCommandBase + @UpdateCommandFull
	-- If @SampleRatePercent is passed, use whatever percent is desired
	ELSE IF @SampleRatePercent IS NOT NULL
		SET @UpdateCommand = @UpdateCommandBase + REPLACE(@UpdateCommandSample,'#', CAST(@SampleRatePercent AS VARCHAR))
	-- If @Resample is passed, use appropriately
	ELSE IF @Resample = 1
		SET @UpdateCommand = @UpdateCommandBase + @UpdateCommandReSample
	-- By default, simply process an UPDATE STATS with no SAMPLE set
	ELSE IF @AutoSample = 1
	BEGIN
		-- The following logic will determine what SAMPLE percent rate to use based on the percent of changed rows
		-- If the percent of change is less than 10%, do nothing if AutoUpdateStats is enabled as AutoUpdateStats will maintain these
		IF @PctChange < 10 AND @AutoUpdateStats = 1
			SET @UpdateCommand = NULL
		-- If the percent of change is less than 10%, run a SAMPLE with 10% if AutoUpdateStats is not enabled
		ELSE IF @PctChange < 10 AND @AutoUpdateStats = 0
			SET @UpdateCommand = @UpdateCommandBase + REPLACE(@UpdateCommandSample,'#', 10)
		-- If the percent of change is greater than 95%, run a FULLSCAN
		ELSE IF @PctChange > 95
			SET @UpdateCommand = @UpdateCommandBase + @UpdateCommandFull
		-- For all others, take the @PctChange and add 5% buffer
		ELSE
			SET @UpdateCommand = @UpdateCommandBase + REPLACE(@UpdateCommandSample,'#', CAST(ROUND(@PctChange,0) AS INT) + 5)
	END
	ELSE
	BEGIN
		-- Remove the WITH command and process a default update
		SET @UpdateCommand = REPLACE(@UpdateCommandBase,' WITH ','') 
	END

	-- Add any additional options
	IF @Norecompute = 1
	BEGIN
		IF @UpdateCommand IS NOT NULL
		SET @UpdateCommand = @UpdateCommand + ', ' + @UpdateCommandNoRecompute + ''
	END

	-- If @OverrideValue has a value, run the contents of @OverrideValue instead (see notes in the header)
	IF @OverrideValue IS NOT NULL
	BEGIN
		SET @UpdateCommand = @UpdateCommandBase + @OverrideValue
		SET @Override = 1
	END
	
	-- Strip out the command issued for logging
	IF @AutoSample = 0 AND (@FullScan = 0 AND @SampleRatePercent IS NULL AND @Resample = 0 AND @Norecompute = 0 AND @Override = 0)
		SET @Command = 'DEFAULT'
	ELSE
		SET @Command = REPLACE(@UpdateCommand,@UpdateCommandBase,'')

	-- Update our temp table with the command to be issued and update statement
	UPDATE #UpdateStats
	SET 
		[UpdateStatement] = @UpdateCommand
		,[Command] = @Command
	WHERE [DBName] = @DBName
	AND [SchemaName] = @SchemaName
	AND [TableName] = @TableName
	AND [StatName] = @StatName
	AND @UpdateCommand IS NOT NULL

	-- Let's make sure the object still exists (avoid temporary real tables)
	SET @ObjectExists = 0
	SET @SQL = N'USE [' + @DBName + '] IF EXISTS(SELECT [name] FROM [sys].[stats] WHERE OBJECT_ID(''' + @TableName + ''') = [object_id] AND [name] = ''' + @StatName + ''') SET @ObjectExists = 1'
	EXEC sp_executesql @SQL, N'@ObjectExists BIT OUTPUT', @ObjectExists = @ObjectExists OUTPUT

	-- Check for object existance and remove those with nothing to do and run or execute
	IF @ObjectExists = 1 AND @UpdateCommand IS NOT NULL
	BEGIN	
		-- Print or execute
		IF @Debug = 1
			PRINT(@UpdateCommand)
		ELSE
		BEGIN
			PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - UpdateStats started on [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '] [' + @StatName + ']'
			-- Execute
			SET @DateStart = (SELECT GETDATE())
			EXEC(@UpdateCommand)
			SET @DateEnd = (SELECT GETDATE())

			-- Log the data if desired
			IF @Logging = 1
				EXEC [dbo].[Maintenance_UpdateStats_Logger]
					@DBName = @DBName
					,@SchemaName = @SchemaName
					,@TableName = @TableName
					,@StatName = @StatName
					,@PctChange = @PctChange
					,@RowCount = @RowCount
					,@RowsChanged = @RowsChanged
					,@LastStatsUpdate = @LastStatsUpdate
					,@Command = @Command
					,@Override = @Override
					,@DateStart = @DateStart
					,@DateEnd = @DateEnd
					,@RunID = @RunID
			PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - UpdateStats finished on [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '] [' + @StatName + ']'
		END
	END
		
FETCH NEXT FROM #StatsCursor INTO @DBName, @SchemaName, @TableName, @StatName, @AutoUpdateStats, @OverrideValue, @PctChange, @RowCount, @RowsChanged, @LastStatsUpdate
END
CLOSE #StatsCursor
DEALLOCATE #StatsCursor

-- Wrap up time. Return data if debugging or update our end time
IF @Debug = 1
BEGIN
	UPDATE #UpdateStats SET [Command] = 'No maintenance required.'
	WHERE [Command] IS NULL

	SELECT * FROM #UpdateStats
ORDER BY 
	DB_NAME(s.[database_id])  
	,t.[name] 
	,s.[name] 
	,a.[name]
END
ELSE
BEGIN
	-- Update our Run table with an end time
	UPDATE dbo.[Maintenance_RunIDs]
	SET [DateEnd] = (SELECT GETDATE())
	WHERE [RunID] = @RunID
END

SET ARITHABORT OFF
SET QUOTED_IDENTIFIER OFF
SET NOCOUNT OFF