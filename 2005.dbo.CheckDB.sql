IF OBJECT_ID('[dbo].[CheckDB]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[CheckDB]
GO

/******************************************************************************
* Name
	[dbo].[CheckDB]

* Author
	Adam Bean
	
* Date
	2008.02.28
	
* Synopsis
	Runs DBCC CHECKDB against specified database(s)
	
* Description
	Maintenance routine that will run CHECKDB against specified, excluded or all databases. Will run options by default but can also
	be overridden (check notes).

* Examples
	EXEC [dbo].[CheckDB]

* Dependencies
	dbo.Split_fn
	dbo.sysdatabases_vw
	dbo.SnapshotDB

* Parameters
	@DBName						= Database to be checked (Database names (CSV supported), NULL for all)
	@DBNameExclude				= Database names to exclude (CSV supported), NULL for all)
	@Logging					= Log the state of the statistics prior to executing
	@UseSnapshots				= Only use existing database snapshots
	@CreateSnapshots			= Create snapshots of the specified database(s) during this execution, check those and then drop
	@NOINDEX					= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@REPAIR_ALLOW_DATA_LOSS	BIT = see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@REPAIR_FAST				= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@REPAIR_REBUILD				= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@ALL_ERRORMSGS				= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@EXTENDED_LOGICAL_CHECKS	= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@NO_INFOMSGS				= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@TABLOCK					= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@ESTIMATEONLY				= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@PHYSICAL_ONLY				= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@DATA_PURITY				= see BOL (http://msdn.microsoft.com/en-us/library/ms176064.aspx)
	@TABLERESULTS				= display the output of CHECKDB in a table output vs. print
	@YesIReallyWantToRepair		= If you want to run one of the REPAIR options, you must also pass a 1 for this parameter
	@Debug						= Display but don't run queries 
	
* Notes
	Add the extended property "CHECKDB_CHECK" to any CHECK option that you want to override the logic of this procedure on
	Add the extended property "CHECKDB_WITH" to any WITH option that you want to override the logic of this procedure on
	For the value of these properties, simply add any options csv supported: 
	Example how to add: 
		EXEC [DBName].sys.sp_addextendedproperty @name=N'CHECKDB_CHECK', @value=N'NOINDEX' 
		EXEC [DBName].sys.sp_addextendedproperty @name=N'CHECKDB_WITH', @value=N'TABLOCK,EXTENDED_LOGICAL_CHECKS' 
*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20090331	Matt Stanford	Added KeepHistoryDays parameter, added DBID check on the SELECT MAX(CheckDBID) query
	20090511	Adam Bean		Added ARITHABORT setting
	20090615	Adam Bean		Improved logging
								Removed ordering on CheckDBId in favor of datecreated for the error checking
	20090615	Matt Stanford	Improved error detection
	20090619	Adam Bean		Added ALL_ERRORMSGS
	20090929	Adam Bean		Global header formatting & general clean up
	20091130	Adam Bean		Removed 2 part OBJECT_NAME in error checking to be compatible prior to SP2
	20100112	Matt Stanford	Added DBCC UPDATEUSAGE for SQL 2000 DBs as an option
	20100307	Matt Stanford	Modified to not attempt UPDATEUSAGE if the database is read only
	20101201	Adam Bean		Change sysname to NVARCHAR(128). Fixed logging issue.
	20110502	Adam Bean		Collation fixes
	20110908	Adam Bean		Starting over
	20140223	Adam Bean		Added @UseSnapshots & @CreateSnapshots 
								Brought in RunID from logger to here so that this is recorded for the whole run vs. by object
	20140302	Adam Bean		Fixed logic for @CreateSnapshots and @UseSnapshots in the main query to determine databases
								Moved error handling logic into this procedure from the Logger
								Got rid of the 2012 specifc version and instead simply have a check for version in here
	20140407	Adam Bean		Added a check to do a DBCC UPDATEUSAGE if the database was restored from 2005 into a 2012 or later version
	20161227	Paul Popovich Jr	Swapped order by from numbers to column names, as noted by running the upgrade advisor for sql16
******************************************************************************/

CREATE PROCEDURE [dbo].[CheckDB]
(
	@DBName						NVARCHAR(2048)	= NULL		
	,@DBNameExclude				NVARCHAR(2048)	= NULL		
	,@Logging					BIT				= 1
	,@UseSnapshots				BIT				= 0
	,@CreateSnapshots			BIT				= 0
	,@NOINDEX					BIT				= 0
	,@REPAIR_ALLOW_DATA_LOSS	BIT				= 0
	,@REPAIR_FAST				BIT				= 0
	,@REPAIR_REBUILD			BIT				= 0
	,@ALL_ERRORMSGS				BIT				= 1
	,@EXTENDED_LOGICAL_CHECKS	BIT				= 0
	,@NO_INFOMSGS				BIT				= 1
	,@TABLOCK					BIT				= 0
	,@ESTIMATEONLY				BIT				= 0
	,@PHYSICAL_ONLY				BIT				= 0
	,@DATA_PURITY				BIT				= 0
	,@TABLERESULTS				BIT				= 1
	,@YesIReallyWantToRepair	BIT				= 0
	,@Debug						BIT				= 0
)

AS

SET NOCOUNT ON
SET DEADLOCK_PRIORITY HIGH
SET ARITHABORT ON 

DECLARE 
	@Comma						VARCHAR(2)
	,@RunID						INT
	,@ThisDB					NVARCHAR(128)
	,@SnapshotName				NVARCHAR(128)
	,@CheckDBCommand			VARCHAR(2048)
	,@CheckOption				VARCHAR(32)
	,@WithOptions				VARCHAR(512)
	,@CheckOptionOverride		VARCHAR(128)
	,@WithOptionsOverride		VARCHAR(128)
	,@DateStart					DATETIME
	,@DateEnd					DATETIME
	,@Override					BIT
	,@Command					VARCHAR(256)
	,@UpdateUsage				BIT
	,@CheckDBFailureError		NVARCHAR(256)
	,@SQL						VARCHAR(1024)

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'

-- Get the run ID
IF @Debug = 0
	EXEC [dbo].[Maintenance_GetRunID] @RunID = @RunID OUTPUT

-- Initialize variables
SET @ThisDB = (SELECT DB_NAME())
SET @WithOptions = ''
SET @Comma = CHAR(44)
SET @CheckDBFailureError = CONVERT(VARCHAR, GETDATE(), 120) + ' - CheckDB failed. Investigate the failures by running SELECT * FROM [' + @ThisDB + '].[dbo].[CheckDB_vw] WHERE [RunID] = ' + CAST(@RunID AS VARCHAR) + ''

-- Determine if we're doing search or a match
IF CHARINDEX('%',@DBName) > 0
	SET @DBName = (SELECT [dbo].[DBSearch_fn](@DBName))
IF CHARINDEX('%',@DBNameExclude) > 0
	SET @DBNameExclude = (SELECT [dbo].[DBSearch_fn](@DBNameExclude))
	
-- Test parameters
IF (@REPAIR_ALLOW_DATA_LOSS = 1 OR @REPAIR_FAST = 1 OR @REPAIR_REBUILD = 1) AND @YesIReallyWantToRepair = 0 AND @Debug = 0
BEGIN
	PRINT 'You have specified one of the @REPAIR options, if you''re sure you want to do this, you must pass @YesIReallyWantToRepair = 1 with the appropriate @REPAIR command'
	PRINT 'The specified database also must be in single user mode (ALTER DATABASE [DBName] SET SINGLE_USER)'
	RETURN
END
-- @TABLERESULTS has to be enabled if logging is desired
IF (@TABLERESULTS = 0 AND @Logging = 1)
	SET @TABLERESULTS = 1

-- Create our working table
IF OBJECT_ID('tempdb.dbo.#CheckDB') IS NOT NULL
   DROP TABLE #CheckDB
   
CREATE TABLE #CheckDB
(
	[DBName]					NVARCHAR(128)
	,[CheckOption]				VARCHAR(128)
	,[WithOptions]				VARCHAR(128)
	,[UpdateUsage]				BIT DEFAULT 0
)

-- Retrieve any Override values
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
FROM [dbo].[sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
AND
(
	( -- Standard database
		@UseSnapshots = 0
		AND @CreateSnapshots = 0
		AND s.[state_desc] = 'ONLINE' 
		AND s.[source_database_id] IS NULL 
	)
	OR -- Only use existing snapshots 
	(
		@UseSnapshots = 1 
		AND @CreateSnapshots = 0
		AND s.[source_database_id] IS NOT NULL
	) 
	OR -- Create new snapshots 
	(
		@CreateSnapshots = 1 
		AND @UseSnapshots = 0
		AND s.[source_database_id] IS NULL
	) 
)
ORDER BY [name]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName
WHILE @@FETCH_STATUS = 0
BEGIN

	-- If we're creating snapshots, do that now and return the name
	IF @CreateSnapshots = 1
	BEGIN
		-- Reset the output param
		SET @SnapshotName = NULL 

		-- Can't snap master or model
		IF @DBName NOT IN ('master','model')
		EXEC dbo.[SnapshotDB] 
			@DBName = @DBName
			,@SnapshotName = @SnapshotName OUTPUT
			,@SnapSuffix = 'SNAPSHOT_CHECKDB'
			,@UseDateSuffix = 0
			,@SupressOutput = 1
		
		-- Replace the original name with the new snapshot name
		IF @DBName NOT IN ('master','model')
			SET @DBName = @SnapshotName
	END

	-- Grab the database names 
	INSERT INTO #CheckDB
	([DBName])
	SELECT @DBName

	-- Check for Override values
	EXEC
	(
		'
		-- Find CHECK options
		UPDATE #CheckDB
		SET [CheckOption] = 
		(
			SELECT 
				CAST([value] AS NVARCHAR(128))
			FROM [' + @DBName + '].[sys].[extended_properties]
			WHERE [class_desc] = ''DATABASE''
			AND [name] = ''CHECKDB_CHECK''
		)
		WHERE [DBName] = ''' + @DBName + '''
		
		-- Find WITH options
		UPDATE #CheckDB
		SET [WithOptions] = 
		(
			SELECT 
				CAST([value] AS NVARCHAR(128))
			FROM [' + @DBName + '].[sys].[extended_properties]
			WHERE [class_desc] = ''DATABASE''
			AND [name] = ''CHECKDB_WITH''
		)
		WHERE [DBName] = ''' + @DBName + '''
		'
	)
		
FETCH NEXT FROM #dbs INTO @DBName
END
CLOSE #dbs
DEALLOCATE #dbs

-- Build the @CheckOption
IF @NOINDEX = 1
	SET @CheckOption = 'NOINDEX'
IF @REPAIR_ALLOW_DATA_LOSS = 1
	SET @CheckOption = 'REPAIR_ALLOW_DATA_LOSS'
IF @REPAIR_FAST = 1
	SET @CheckOption = 'REPAIR_FAST'
IF @REPAIR_REBUILD = 1
	SET @CheckOption = 'REPAIR_REBUILD'

-- Build the @WithOptions
-- In order to return TABLERESULTS, ESTIMATEONLY can not be enabled
IF (@TABLERESULTS = 1 AND @ESTIMATEONLY = 0)
	SET @WithOptions = @WithOptions + ' TABLERESULTS' + @Comma 
IF @ALL_ERRORMSGS = 1
	SET @WithOptions = @WithOptions + ' ALL_ERRORMSGS' + @Comma 
IF @EXTENDED_LOGICAL_CHECKS = 1
	SET @WithOptions = @WithOptions + ' EXTENDED_LOGICAL_CHECKS' + @Comma 
IF @NO_INFOMSGS = 1
	SET @WithOptions = @WithOptions + ' NO_INFOMSGS' + @Comma 
IF @TABLOCK = 1
	SET @WithOptions = @WithOptions + ' TABLOCK' + @Comma 
IF @ESTIMATEONLY = 1
	SET @WithOptions = @WithOptions + ' ESTIMATEONLY' + @Comma 
IF @PHYSICAL_ONLY = 1
	SET @WithOptions = @WithOptions + ' PHYSICAL_ONLY' + @Comma 
IF @DATA_PURITY = 1
	SET @WithOptions = @WithOptions + ' DATA_PURITY' + @Comma 

-- Reset @WithOptions to NULL if no options are specified or remove the trailing comma
IF @WithOptions = ''
	SET @WithOptions = NULL
ELSE
	SET @WithOptions = LEFT(@WithOptions,LEN(@WithOptions) -1)

-- Determine if we should account for a 2000 restore and do a DBCC updateusage
UPDATE c
SET [UpdateUsage] = 1
FROM #CheckDB c
JOIN
(
	SELECT 
		r.[destination_database_name]	AS [DBName]
		,MAX(r.[restore_date])			AS [restore_date]
	FROM [dbo].[sysdatabases_vw] s
	JOIN [msdb].[dbo].[restorehistory] r
		ON r.[destination_database_name] = s.[name]
	LEFT JOIN [dbo].[CheckDB_vw] c
		ON r.[destination_database_name] = c.[DBName] COLLATE DATABASE_DEFAULT
	WHERE s.[compatibility_level] = 80
	AND r.[restore_date] >= ISNULL(c.[DateStart],r.[restore_date])
	GROUP BY r.[destination_database_name]
) rh
	ON c.[DBName] = rh.[DBName]
WHERE c.[DBName] = rh.[DBName]

-- Determine if we should account for a 2005 database restored to 2012 or later and do a DBCC updateusage
IF @@MICROSOFTVERSION / 0x01000000 >= 11
BEGIN
	UPDATE c
	SET [UpdateUsage] = 1
	FROM #CheckDB c
	JOIN
	(
		SELECT 
			r.[destination_database_name]	AS [DBName]
			,MAX(r.[restore_date])			AS [restore_date]
		FROM [dbo].[sysdatabases_vw] s
		JOIN [msdb].[dbo].[restorehistory] r
			ON r.[destination_database_name] = s.[name]
		LEFT JOIN [dbo].[CheckDB_vw] c
			ON r.[destination_database_name] = c.[DBName] COLLATE DATABASE_DEFAULT
		WHERE s.[compatibility_level] = 90
		AND r.[restore_date] >= ISNULL(c.[DateStart],r.[restore_date])
		GROUP BY r.[destination_database_name]
	) rh
		ON c.[DBName] = rh.[DBName]
	WHERE c.[DBName] = rh.[DBName]
END

-- Run CheckDB
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[DBName]
	,[CheckOption]
	,[WithOptions]
	,[UpdateUsage]
FROM #CheckDB
ORDER BY [DBName]

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName, @CheckOptionOverride, @WithOptionsOverride, @UpdateUsage
WHILE @@FETCH_STATUS = 0
BEGIN

	-- If overrides were found and logging is desired, we have to add TABLERESULTS appropriately
	-- If ESTIMATEONLY is enabled, logging will break so check for that as well
	IF (@ESTIMATEONLY = 0 OR @WithOptionsOverride NOT LIKE '%ESTIMATEONLY%')
	BEGIN
		IF (@WithOptionsOverride IS NOT NULL OR @CheckOptionOverride IS NOT NULL) AND (@TABLERESULTS = 1 AND @Logging = 1)
			SET @WithOptionsOverride = ' TABLERESULTS' + ISNULL(', ' + @WithOptionsOverride,'')
	END
	ELSE
		SET @Logging = 0
	
	-- Build the CHECKDB statement
	SET @CheckDBCommand = COALESCE
	(
		'''DBCC CHECKDB ([' + @DBName + '],' + ISNULL(@CheckOptionOverride, @CheckOption) + ') WITH' + ISNULL(@WithOptionsOverride, @WithOptions) + ''''
		,'''DBCC CHECKDB ([' + @DBName + '],' + ISNULL(@CheckOptionOverride, @CheckOption) + ')'''
		,'''DBCC CHECKDB ([' + @DBName + ']) WITH' + ISNULL(@WithOptionsOverride, @WithOptions) + ''''
		,'''DBCC CHECKDB ([' + @DBName + '])'''
	)

	-- Build @Command for logging purposes
	SET @Command = COALESCE
	(
		@CheckOptionOverride + ',' + @WithOptionsOverride
		,@CheckOptionOverride
		,@WithOptionsOverride
		,@CheckOption + ',' + @WithOptions
		,@CheckOption
		,@WithOptions
	)
	
	-- If we performed an override, it will be logged
	IF (@CheckOptionOverride IS NOT NULL OR @WithOptionsOverride IS NOT NULL)
		SET @Override = 1
	ELSE
		SET @Override = 0
		
	-- Log the data if desired
	IF @Logging = 1
	BEGIN
		-- In SQL2012 additional columns were added into the output of CHECKDB
		IF (@@MICROSOFTVERSION / 0x01000000) < 11
		BEGIN
			SET @SQL =
			'
			INSERT INTO [dbo].[Maintenance_CheckDB_Master]
			([Error],[Level],[State],[MessageText],[RepairLevel],[Status],[DbId],[ObjectID],[IndexID],[PartitionID],[AllocUnitID],[File],[Page],[Slot],[RefFile],[RefPage],[RefSlot],[Allocation])
			EXEC(' + @CheckDBCommand + ')
			'
		END
		ELSE
		BEGIN
			SET @SQL =
			'
			INSERT INTO [dbo].[Maintenance_CheckDB_Master]
			([Error],[Level],[State],[MessageText],[RepairLevel],[Status],[DbId],[DbFragId],[ObjectID],[IndexID],[PartitionID],[AllocUnitID],[RidDbId],[RidPruId],[File],[Page],[Slot],[RefDbId],[RefPruId],[RefFile],[RefPage],[RefSlot],[Allocation])
			EXEC(' + @CheckDBCommand + ')
			'
		END
	END
	ELSE
		SET @SQL = 'EXEC(' + @CheckDBCommand + ')'

	-- If we need to perform an UPDATEUSAGE add the command appropriately
	IF @UpdateUsage = 1
		SET @SQL = 'DBCC UPDATEUSAGE([' + @DBName + '])' + @SQL

	-- Print or execute
	IF @Debug = 1
		PRINT(@SQL)
	ELSE
	BEGIN
		PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - DBCC CHECKDB started on ' + @DBName + '.'
		BEGIN TRY 
			-- Execute
			SET @DateStart = (SELECT GETDATE())
			EXEC(@SQL)
			SET @DateEnd = (SELECT GETDATE())

			-- Log the data if desired
			IF @Logging = 1
			BEGIN
				EXEC [dbo].[Maintenance_CheckDB_Logger]
					@DBName = @DBName
					,@Command = @Command
					,@Override = @Override
					,@DateStart = @DateStart
					,@DateEnd = @DateEnd
					,@RunID = @RunID
			END
		PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - DBCC CHECKDB finished on ' + @DBName + '.'
		END TRY
		BEGIN CATCH
			-- Dump the error to the output
			PRINT 
				'Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER()) +
				',Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY()) +
				',State ' + CONVERT(VARCHAR(5), ERROR_STATE()) + 
				',Line ' + CONVERT(VARCHAR(5), ERROR_LINE())
			PRINT ERROR_MESSAGE()
		END CATCH    
	END
	
	-- If we created snapshots, drop them
	IF @CreateSnapshots = 1
	BEGIN
		-- No master or model
		IF @DBName NOT IN ('master','model')
		BEGIN
			-- Let's just play it safe and make sure we're only hitting snapshots here
			IF EXISTS (SELECT [name] FROM dbo.[sysdatabases_vw] WHERE [name] = @DBName AND [source_database_id] IS NOT NULL)
			BEGIN
				EXEC('DROP DATABASE [' + @DBName + ']')
			END
		END
	END

FETCH NEXT FROM #dbs INTO @DBName, @CheckOptionOverride, @WithOptionsOverride, @UpdateUsage
END
CLOSE #dbs
DEALLOCATE #dbs

-- Wrap up time. Return data if debugging or check for and report on failures
IF @Debug = 1
BEGIN
	-- Return the staged data
	SELECT * FROM #CheckDB 
END
ELSE
BEGIN
	-- Update our Run table with an end time
	UPDATE dbo.[Maintenance_RunIDs]
	SET [DateEnd] = (SELECT GETDATE())
	WHERE [RunID] = @RunID

	-- Report failure if any processing failed
	IF 
		(
			SELECT COUNT(*) FROM [dbo].[CheckDB_vw]
			WHERE [RunID] = @RunID
			AND [Error] IS NOT NULL
			AND [State] > 2
			AND [Level] > 10
		) > 0 
	BEGIN
		PRINT (@CheckDBFailureError)
		RAISERROR (@CheckDBFailureError, 16, 2) WITH LOG
		RETURN
	END
END