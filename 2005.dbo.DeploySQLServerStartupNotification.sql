IF OBJECT_ID('[dbo].[DeploySQLServerStartupNotification]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[DeploySQLServerStartupNotification]
GO

/*******************************************************************************************************
**	Name:			dbo.DeploySQLServerStartupNotification
**	Desc:			This procedure is a wrapper procedure to SQLServerStartupNotification to deploy the object in the master
					database and mark it as a startup procedure, with the parameter of @Recipients.
**	Auth:			Adam Bean (SQLSlayer.com)
**	Dependancies:	Database mail configured
**  Parameters:		@Recipients = Email recipients (comma or semi-colon seperated - ie. dba@company.com;joe@company.com)
					Database mail only allows for semi-colons, but if you pass in commas, they'll be replaced properly
**  Notes:			In order to allow the procedure to be created in the master database without a hardcoded 
					@Recipients parameter and to use with the admin update utility, dynamic SQL is used to 
					create SQLServerStartupNotification in the master database from this procedure.
					Will mark SQLServerStartupNotification as a startup procedure
**	Date:			2009.10.07
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20091112	Adam Bean		Changed from sp to xp_readerrorlog to change the ordering of the log file
**	20100108	Matt Stanford	Fixed bug that reports failure if you have multiple system startup procedures
********************************************************************************************************/

CREATE PROCEDURE [dbo].[DeploySQLServerStartupNotification]
(
	@Recipients		VARCHAR(1024)
	,@Debug			BIT	= 0
)

AS 

SET NOCOUNT ON

DECLARE 
	@SQL			VARCHAR(MAX)

-- Replace commas with semi-colons to work properly for database mail
SET @Recipients = REPLACE(@Recipients, ',', ';')

-- Deploy SQLServerStartupNotification to master database and mark as system started procedure
SET @SQL = 
('
USE [master]
IF OBJECT_ID(''[dbo].[SQLServerStartupNotification]'',''P'') IS NOT NULL
	DROP PROCEDURE [dbo].[SQLServerStartupNotification]
	
EXEC
(''

/*******************************************************************************************************
**	Name:			dbo.SQLServerStartupNotification
**	Desc:			Sends email to notify that SQL Server was restarted
					Will notify of current node if instanced
**	Auth:			Adam Bean (SQLSlayer.com)
**	Dependancies:	Database mail configured
**	Notes:			This procedure should only be created from the dbo.DeploySQLServerStartupNotification wrapper
**	Date:			2009.10.07
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  20091112	Adam Bean		Changed from sp to xp_readerrorlog to change the ordering of the log file
********************************************************************************************************/

CREATE PROCEDURE [dbo].[SQLServerStartupNotification]

AS 

SET NOCOUNT ON

DECLARE 
	@ServerName		VARCHAR(64)
	,@IsInstanced	BIT
	,@StartTime		DATETIME
	,@ComputerName	VARCHAR(64)
	,@Recipients	VARCHAR(1024)
	,@EmailSubject	VARCHAR(64)
	,@EmailHeader	VARCHAR(2048)
	,@EmailBody		VARCHAR(MAX)
	,@EmailCaption	VARCHAR(128)
	,@EmailFooter	VARCHAR(2048)
	,@Count			INT

SET @Recipients		= ''''' + @Recipients + '''''
SET @ServerName		= (SELECT CAST(SERVERPROPERTY(''''ServerName'''') AS VARCHAR(64)))
SET @IsInstanced	= (SELECT CAST(SERVERPROPERTY(''''IsClustered'''') AS BIT))
SET @StartTime		= (SELECT [login_time] FROM [master].[sys].[dm_exec_sessions] WHERE [session_id] = 1)
SET @EmailSubject	= ''''SQL Server started on '''' + @ServerName + '''' @ '''' + CONVERT(VARCHAR(20), GETDATE(), 120) + ''''''''
SET @EmailCaption	= ''''SQL has been running since '''' + CONVERT(VARCHAR(20), @StartTime, 120) + ''''.''''

IF @IsInstanced = 1
BEGIN
	SET @ComputerName	= (SELECT CAST(SERVERPROPERTY(''''ComputerNamePhysicalNetBIOS'''') AS VARCHAR(64)))
	SET @EmailCaption	= @EmailCaption + CHAR(10) + CHAR(10) + ''''SQL is currently running on '''' + @ComputerName + ''''.''''
END

-- Get the tail end of the previous error log
DECLARE @ErrorLog TABLE
(
	[LogDate]		DATETIME
	,[ProcessInfo]	VARCHAR(24)
	,[Text]			VARCHAR(MAX)
)

INSERT INTO @ErrorLog
EXEC [master].[dbo].[xp_readerrorlog] 1, 1, NULL, NULL, NULL, NULL, ''''desc''''

-- Setup table to build body
DECLARE @EmailBodyContents TABLE 
(
	[html]			VARCHAR(MAX)
	
)

-- Setup table to build email
DECLARE @Email TABLE 
(
	[ID]			INT IDENTITY
	,[html]			VARCHAR(MAX)
)

-- Build the email header
SELECT @EmailHeader = 
''''
<HTML>
<DIV ALIGN="CENTER">
<TABLE BORDER="5" CELLSPACING="5" CELLPADDING="5" ALIGN="CENTER">
<CAPTION>
	<H2>'''' + @EmailCaption + ''''</H2>
	<EM>Last 20 entries in error log before SQL was restarted on '''' + @ServerName  + ''''</EM>
</CAPTION>
<TR BGCOLOR="#C0C0C0">
	<TH>LogDate</TH>
	<TH>ProcessInfo</TH>
	<TH>Text</TH>
</TR>
''''

-- Build the email footer
SELECT @EmailFooter =
''''
</TABLE>
</DIV>
</HTML>
''''

-- Build the email body
INSERT INTO @EmailBodyContents
SELECT TOP 20 ''''
		<TR>
			<TD>'''' + CONVERT(VARCHAR(24), [LogDate], 120) + ''''</TD>
			<TD>'''' + [ProcessInfo] + ''''</TD>
			<TD>'''' + [Text] + ''''</TD>
		</TR>'''' AS [html]
FROM @ErrorLog

-- Build the email
INSERT INTO @Email
SELECT @EmailHeader
UNION ALL
SELECT * FROM @EmailBodyContents
UNION ALL
SELECT @EmailFooter

-- Put it all together
SET @Count = 1
SET @EmailBody = ''''''''
WHILE @Count <= (SELECT COUNT(*) FROM @Email)
BEGIN
	SET @EmailBody = @EmailBody + (SELECT [html] FROM @Email WHERE [ID] = @Count)
	SET @Count = @Count + 1
END

-- Send mail
EXEC [msdb].[dbo].[sp_send_dbmail]
	@Recipients		= @Recipients
	,@Subject		= @EmailSubject
	,@Body			= @EmailBody
	,@Body_format	= ''''HTML''''
'')
')

IF @Debug = 1
BEGIN
	PRINT '/*** DEBUG ENABLED ****/'
	PRINT (@SQL)
END
ELSE
BEGIN
	EXEC(@SQL)
	-- Set object as a startup
	EXEC [master].[sys].[sp_procoption] 'dbo.SQLServerStartupNotification', 'startup', 'true' 
	-- Query to ensure it's a startup object
	DECLARE @ObjectExists TABLE
	(
		[ObjectName]	sysname
	)
	
	INSERT INTO @ObjectExists
	EXEC
	('
		USE [master]
		SELECT 
			[name]
		FROM [sys].[objects]
		WHERE OBJECTPROPERTY(OBJECT_ID, ''ExecIsStartup'') = 1
	')
	
	IF EXISTS (SELECT [ObjectName] FROM @ObjectExists WHERE [ObjectName] = 'SQLServerStartupNotification')
	BEGIN
		PRINT 'Successfully deployed dbo.SQLServerStartupNotification to master database with ''' + @Recipients + ''' as the recipient(s).'
		PRINT 'Successfully marked dbo.SQLServerStartupNotification as a system startup procedure'
	END
	ELSE
	BEGIN
		PRINT 'Failed to deploy dbo.SQLServerStartupNotification to master database'
		PRINT 'Failed to mark dbo.SQLServerStartupNotification as a system startup procedure'
	END
END
	
SET NOCOUNT OFF