IF OBJECT_ID('[dbo].[KillProcess]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[KillProcess]
GO

/******************************************************************************
* Name
	dbo.KillProcess

* Author
	Adam Bean (SQLSlayer.com)
	
* Date
	2008.02.14
	
* Synopsis
	Robust support procedure to kill processes
	
* Description
	Kills a connection based on login (with exclusions), database name (with exclusions), or both. 
	Optionally puts database into single user, offline, drops database, logs locally or sends email

* Examples
	EXEC dbo.KillProcess @dbname='admin',@SetSingleUser=0,@sendemail=1,@Recipients='you@me.com', @debug=1
	EXEC dbo.KillProcess @dbname='admin',@LoginExclude='SQLAccount,domain\ServiceAccount',@debug=1
	EXEC dbo.KillProcess @Login='user',@DropDB=1,@dbname='admin',@sendemail=1,@Recipients='you@me.com', @debug=1

* Dependencies
	Database Mail configured
	dbo.who_vw
	dbo.Split_fn
	dbo.GetErrorInfo_fn

* Parameters
	@DBName				= Database(s) to kill connections against
	@DBNameExclude		= Database(s) to exclude
	@Login				= Login(s) to kill connections against
	@LoginExclude		= Login(s) to exclude from the kill list
	@HostName			= Host Name(s) to kill connections against
	@HostNameExclude	= Host Name(s) to exclude
	@ProgramName		= Program Name(s) to kill connections against
	@ProgramNameExclude	= Program Name(s) to exclude
	@LastBatchMinutes	= Time in minutes to filter connections on (connections older than @LastBatchMinutes compared to GETDATE())
	@SetSingleUser		= Put database into single user mode
	@SetOffline			= Put database into offline mode
	@DropDB				= Drop database 
	@TableLogging		= If you want to log the results of the kill locally to this server
	@SendEmail			= Send an email with the results of what was killed
	@Recipients			= Email recipients if @SendEmail is enabled
	@EmailSubject		= Override the default subject with your own
	@ShowProcesses		= Display results of connections to be killed
	@Debug				= Shows but does not kill connections

* Notes
	You can use any combination of the 8 main parameters ... if left null, all will be populated
	If you're sending email, make sure you have databasemail setup properly

*******************************************************************************
* License
*******************************************************************************
	Copyright © SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20080215	Adam Bean		Fixed email debug, Added nolocks, Omitted system databases, 
								Added ability to drop database, Test for work to do,
								Verify database and login
	20080327	Adam Bean		Added ability to pass in exclusions for logins
								Added table logging
	20080824	Adam Bean		Added offline
	20090629	Matt Stanford	Added @ShowProcesses parameter
	20090630	Adam Bean		Updated to use admin support objects, added TRY/CATCH for KILL
								Added @DBNameExclude, @KillEverything, Removed verification checks, added enhanced CSV support
	20090702	Matt Stanford	Automatically drop the snapshots if @DropDB is set to 1
	20090702	Adam Bean		Added support to set single/offline/drop for multiple databases (excluding system)
								Added support to kill processes in system databases (SPID > 50)
								Moved KillProcessesLog to external script								
								Changed @ConnectionsKilled from TINYINT to INT
	20090708	Adam Bean		Changed @@SERVERNAME to SERVERPROPERTY('ServerName')
	20090709	Adam Bean		Switched from temp table insert into to temp variable to support empty table if no connections
								Populate staging table with values from @DBName if no active connections
	200907016	Adam Bean		Added @HostName and @HostNameExclude
	20090728	Adam Bean		Fixed for case sensitive collation
	20090922	Adam Bean		Added @DeleteHistory to remove backup history if the database is dropped
	20090929	Adam Bean		Global header formatting & general clean up
	20091025	Matt Stanford	Changed the @SQL variable to be NVARCHAR(MAX), was VARCHAR(128)
	20100701	Adam Bean		Resolved collation issues (COLLATE DATABASE_DEFAULT)
	20140212	Adam Bean		Added @ProgramName, @ProgramNameExclude, @LastBatchMinutes, changed @Subject to @EmailSubject and changed to a parameter
								Added RAISERROR functionality
******************************************************************************/

CREATE PROCEDURE [dbo].[KillProcess] 
(
	@Login					VARCHAR(512)	= NULL
	,@LoginExclude			VARCHAR(512)	= NULL
	,@DBName				VARCHAR(512)	= NULL
	,@DBNameExclude			VARCHAR(512)	= NULL
	,@HostName				VARCHAR(512)	= NULL
	,@HostNameExclude		VARCHAR(512)	= NULL
	,@ProgramName			VARCHAR(512)	= NULL
	,@ProgramNameExclude	VARCHAR(512)	= NULL
	,@LastBatchMinutes		INT				= NULL
	,@KillEverything		BIT				= 0
	,@SetSingleUser			BIT				= 0
	,@SetOffline			BIT				= 0
	,@DropDB				BIT				= 0
	,@DeleteHistory			BIT				= 1
	,@TableLogging			BIT				= 0
	,@SendEmail				BIT				= 0
	,@Recipients			VARCHAR(1024)	= NULL
	,@EmailSubject			VARCHAR(256)	= NULL
	,@ShowProcesses			BIT				= 1
	,@Debug					BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE
	@RAISERROR				VARCHAR(128)
	,@ThisDB				NVARCHAR(128)
	,@Body					VARCHAR(MAX)
	,@EmailHeader			VARCHAR(2048)
	,@EmailFooter			VARCHAR(2048)
	,@EmailBody				VARCHAR(2048)
	,@SPID					INT
	,@SQL					NVARCHAR(MAX)
	,@Count					INT
	,@ConnectionsKilled		INT
	,@ServerName			VARCHAR(64)

SET NOCOUNT ON

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'

-- Populate working variables
SET @ThisDB = (SELECT DB_NAME())
SET @ServerName = (SELECT CAST(SERVERPROPERTY('ServerName') AS VARCHAR(64)))

-- Test variables
IF @SendEmail = 1 AND @Recipients IS NULL
BEGIN
	PRINT '@Recipients must be passed when attempting to send an email.'
	RETURN
END

-- Sanity check ... If all the main parameters are NULL, everything minus system db spids will be killed 
-- More than likely that's not what was intended, but just in case, the user has to pass in @KillEverything = 1
IF 
(
	@Login IS NULL 
	AND @LoginExclude IS NULL 
	AND @DBName IS NULL 
	AND @DBNameExclude IS NULL
	AND @HostName IS NULL 
	AND @HostNameExclude IS NULL
	AND @ProgramName IS NULL 
	AND @ProgramNameExclude IS NULL
	AND @Debug = 0
	AND @KillEverything = 0
)
BEGIN
	PRINT 'Pass in at least one value for any of the main parameters: @Login, @LoginExclude, @DBName, @DBNameExclude, @HostName, @HostNameExclude, @ProgramName, @ProgramNameExclude'
	PRINT 'Use @Debug = 1 to view connections'
	PRINT 'If you REALLY want to kill all non system processes, than rerun with @KillEverything = 1'
	RETURN
END

-- Setup staging table
DECLARE @KillProcesses TABLE
(
	[Action]		VARCHAR(32)
	,[SPID]			INT
	,[DBName]		VARCHAR(128)
	,[Login]		VARCHAR(128)
	,[HostName]		VARCHAR(128)
	,[ProgramName]	VARCHAR(128)
	,[Command]		VARCHAR(32)
	,[Status]		VARCHAR(32)
	,[LoginTime]	DATETIME
	,[LastBatch]	DATETIME
	,[Query]		NVARCHAR(MAX) 
)

-- Populate staging table
INSERT INTO @KillProcesses
SELECT DISTINCT
	'Connection To Be Killed ->'	AS [Action]
	,[SPID]
	,[DBName]
	,[Login]
	,[HostName]
	,[ProgramName]
	,[Command]
	,[Status]
	,[LoginTime]
	,[LastBatch]
	,[Query]
FROM [dbo].[who_vw] w
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON w.[DBName] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON w.[DBName] = de.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@Login,',') l
	ON w.[Login] = l.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@LoginExclude,',') le
	ON w.[Login] = le.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@HostName,',') h
	ON w.[HostName] = h.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@HostNameExclude,',') he
	ON w.[HostName] = he.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@ProgramName,',') p
	ON w.[ProgramName] = p.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@ProgramNameExclude,',') pe
	ON w.[ProgramName] = pe.[item] COLLATE DATABASE_DEFAULT
WHERE w.[SPID] > 50 -- Exclude system processes
AND w.[SPID] != @@SPID -- Exclude your process
AND ((DATEADD(MINUTE, -@LastBatchMinutes, GETDATE()) > w.[LastBatch]) OR @LastBatchMinutes IS NULL) -- Only retrieve processes newer than the last @LastBatchMinutes
AND ((l.[item] IS NOT NULL AND @Login IS NOT NULL) OR @Login IS NULL) -- Specified logins, or all
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND ((h.[item] IS NOT NULL AND @HostName IS NOT NULL) OR @HostName IS NULL) -- Specified hosts, or all
AND ((p.[item] IS NOT NULL AND @ProgramName IS NOT NULL) OR @ProgramName IS NULL) -- Specified programs, or all
AND le.[item] IS NULL -- All but excluded logins
AND de.[item] IS NULL -- All but excluded databases
AND he.[item] IS NULL -- All but excluded hosts
AND pe.[item] IS NULL -- All but excluded programs
ORDER BY [DBName], [Login], [HostName], [Status]

-- Get a total of the connections to be killed
SELECT @ConnectionsKilled = COUNT(*) FROM @KillProcesses

-- If no active connections, populate staging table with database names (required for other operations in procedure)
IF @ConnectionsKilled = 0
BEGIN
	INSERT INTO @KillProcesses
	([DBName])
	SELECT [item] AS [DBName] FROM [dbo].[Split_fn](@DBName,',')
END

-- Log connections to be killed
IF @Debug = 0 AND @TableLogging = 1
BEGIN
	-- Log the data
	INSERT INTO [KillProcessesLog]
	([SPID],[DBName],[Login],[HostName],[ProgramName],[Command],[Status],[LoginTime],[LastBatch],[Query])
	SELECT 
		[SPID]
		,[DBName]
		,[Login]
		,[HostName]
		,[ProgramName]
		,[Command]
		,[Status]
		,[LoginTime]
		,[LastBatch]
		,[Query] 
	FROM @KillProcesses
END
ELSE IF @Debug = 1 AND @TableLogging = 1
	PRINT 'Debug enabled, nothing will be logged'

-- Kill the connection(s) 
IF @ConnectionsKilled > 0
BEGIN
	DECLARE #killspids CURSOR LOCAL STATIC FOR
	SELECT [SPID] FROM @KillProcesses

	OPEN #killspids
	FETCH NEXT FROM #killspids INTO @SPID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		-- Reset our working variables
		SET @SQL = 'KILL ' + CAST(@SPID AS VARCHAR(5)) + ''
		SET @RAISERROR = 'Process ID ' + CAST(@SPID AS VARCHAR(5)) + ' was killed via ' + @ThisDB + '.dbo.KillProcess'

		IF @Debug = 0
		BEGIN
			BEGIN TRY
				-- Execute
				EXEC(@SQL)
				-- Leave a message in the print
				PRINT 'KILLED SPID ' + CAST(@SPID AS VARCHAR(5)) + ''
				-- Leave a message in the error log
				RAISERROR (@RAISERROR, 1, 1) WITH LOG
				-- Update our working table
				UPDATE @KillProcesses
				SET [Action] = 'Killed SPID ->'
				WHERE [SPID] = @SPID
			END TRY
			BEGIN CATCH
				SELECT * FROM [dbo].[GetErrorInfo_fn]()
			END CATCH
		END	
		ELSE
		BEGIN	
			PRINT (@SQL)
		END

	FETCH NEXT FROM #killspids INTO @SPID
	END
	CLOSE #killspids
	DEALLOCATE #killspids
END

-- Return results after kill
IF @ShowProcesses = 1
BEGIN
	IF @ConnectionsKilled > 0
		SELECT * FROM @KillProcesses
	ELSE
	BEGIN
		SELECT 'No active connections to kill' AS [ActiveConnections]
		PRINT 'No active connections to kill'
	END
END
ELSE IF (@ShowProcesses = 0 AND @ConnectionsKilled = 0 AND @Debug = 1)
	PRINT 'No active connections to kill'

-- Set database to single user 
IF @SetSingleUser = 1
BEGIN
	DECLARE #SetSingleUser CURSOR FOR
	SELECT DISTINCT [DBName] FROM @KillProcesses
	WHERE DB_ID([DBName]) > 4 
	AND [DBName] != 'admin'
	
	OPEN #SetSingleUser
	FETCH NEXT FROM #SetSingleUser INTO @DBName
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		SET @SQL = 'ALTER DATABASE [' + @DBName + '] SET SINGLE_USER WITH ROLLBACK IMMEDIATE'
		IF @Debug = 0
		BEGIN
			EXEC(@SQL)
			PRINT @DBName + ' has been put into single user mode.'
			PRINT 'To put ' + @DBName + ' back into multi user mode, issue: ALTER DATABASE [' + @DBName + '] SET MULTI_USER WITH NO_WAIT'
		END
		ELSE
			PRINT(@SQL)
	
	FETCH NEXT FROM #SetSingleUser INTO @DBName
	END
	CLOSE #SetSingleUser
	DEALLOCATE #SetSingleUser
END

-- Set database to offline
IF @SetOffline = 1
BEGIN
	DECLARE #SetOffline CURSOR FOR
	SELECT DISTINCT [DBName] FROM @KillProcesses
	WHERE DB_ID([DBName]) > 4 
	AND [DBName] != 'admin'
	
	OPEN #SetOffline
	FETCH NEXT FROM #SetOffline INTO @DBName
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		SET @SQL = 'ALTER DATABASE [' + @DBName + '] SET OFFLINE WITH ROLLBACK IMMEDIATE'
		IF @Debug = 0
		BEGIN
			EXEC(@SQL)
			PRINT @DBName + ' has been put into offline mode.'
			PRINT 'To put ' + @DBName + ' back into multi user mode, issue: ALTER DATABASE [' + @DBName + '] SET ONLINE WITH NO_WAIT'
		END
		ELSE
			PRINT(@SQL)
		
	FETCH NEXT FROM #SetOffline INTO @DBName
	END
	CLOSE #SetOffline
	DEALLOCATE #SetOffline
END

-- Drop database 
IF @DropDB = 1
BEGIN
	DECLARE #DropDB CURSOR FOR
	SELECT DISTINCT [DBName] FROM @KillProcesses
	WHERE DB_ID([DBName]) > 4 
	AND [DBName] != 'admin'
		
	OPEN #DropDB
	FETCH NEXT FROM #DropDB INTO @DBName
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @SQL = ''

		IF @DeleteHistory = 1
			SET @SQL = 'EXEC [msdb].[dbo].[sp_delete_database_backuphistory] @database_name = ''' + @DBName + ''''
					
		SELECT
			@SQL = @SQL + CHAR(10) + 'DROP DATABASE [' + name + '] -- Dropping snapshot'
		FROM [dbo].[sysdatabases_vw]
		WHERE [source_database_id] IS NOT NULL
		AND DB_NAME([source_database_id]) = @DBName

		SET @SQL = @SQL + CHAR(10) + 'DROP DATABASE [' + @DBName + ']'
		IF @Debug = 0
		BEGIN
			EXEC(@SQL)
			PRINT @DBName + ' has been dropped.'
		END
		ELSE
			PRINT(@SQL)
		
	FETCH NEXT FROM #DropDB INTO @DBName
	END
	CLOSE #DropDB
	DEALLOCATE #DropDB
END

-- Build email
IF @SendEmail = 1
BEGIN
	-- Only send email if connections were killed
	IF @ConnectionsKilled > 0 
	BEGIN
		-- Setup table to build body
		DECLARE @BodyContents TABLE 
		(
			[html]			VARCHAR(MAX)
		)

		-- Setup table to build email
		DECLARE @Email TABLE 
		(
			[ID]			INT IDENTITY
			,[html]			VARCHAR(MAX)
		)
				
		-- Set subject
		IF @EmailSubject IS NULL
			SET @EmailSubject = 'Connections terminated on ' + @ServerName  + ''

		-- Build the email header
		SELECT @EmailHeader = 
		'
		<HTML>
		<DIV ALIGN="CENTER">
		<TABLE BORDER="5" CELLSPACING="5" CELLPADDING="5" ALIGN="CENTER">
		<CAPTION>
			<H2>Terminated connections on ' + @ServerName  + '</H2>
			<EM>Message sent from ' + @ServerName  + ' @ ' + CAST(GETDATE() AS VARCHAR(24)) + '.</EM>
		</CAPTION>
		<TR>
			<TD COLSPAN="9" ALIGN="CENTER">Total connections killed: ' + CAST(@ConnectionsKilled AS VARCHAR(8)) + '
			</TD>
		</TR>
		<TR BGCOLOR="#C0C0C0">
			<TH>Database</TH>
			<TH>Login</TH>
			<TH>HostName</TH>
			<TH>ProgramName</TH>
			<TH>Query</TH>
			<TH>Command</TH>
			<TH>Status</TH>
			<TH>LoginTime</TH>
			<TH>LastBatch</TH>
		</TR>
		'

		-- Build the email footer
		SELECT @EmailFooter =
		'
		</TABLE>
		</DIV>
		</HTML>
		'

		-- Build the email body
		INSERT INTO @BodyContents
		SELECT 
		'
		<TR>
			<TD>' + ISNULL([DBName],'Unknown') + '</TD>
			<TD>' + ISNULL([Login],'Unknown') + '</TD>
			<TD>' + ISNULL([HostName],'Unknown') + '</TD>
			<TD>' + ISNULL([ProgramName],'Unknown') + '</TD>
			<TD>' + ISNULL([Query],'Unknown') + '</TD>
			<TD>' + ISNULL([Command],'Unknown') + '</TD>
			<TD>' + ISNULL([Status],'Unknown') + '</TD>
			<TD>' + CONVERT(VARCHAR(24), [LoginTime], 100) + '</TD>
			<TD>' + CONVERT(VARCHAR(24), [LastBatch], 100) + '</TD>
		</TR>
		' AS [html]
		FROM @KillProcesses

		-- Build the email
		INSERT INTO @Email
		SELECT @EmailHeader
		UNION ALL
		SELECT * FROM @BodyContents
		UNION ALL
		SELECT @EmailFooter

		-- Put it all together
		SET @Count = 1
		SET @Body = ''
		WHILE @Count <= (SELECT COUNT(*) FROM @Email)
		BEGIN
			SET @Body = @Body + (SELECT [html] FROM @Email WHERE [ID] = @Count)
			SET @Count = @Count + 1
		END

		-- Send it!
		IF @Debug = 1
			SELECT * FROM @Email
		ELSE
			EXEC [msdb].[dbo].[sp_send_dbmail]
				@Recipients		= @Recipients
				,@Subject		= @EmailSubject
				,@Body			= @Body
				,@Body_format	= 'HTML'
	END
END
SET NOCOUNT OFF