IF OBJECT_ID('[dbo].[Reindex]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[Reindex]
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************
* Name
	[dbo].[Reindex]

* Author
	Adam Bean
	
* Date
	2008.02.28
	
* Synopsis
	Rebuilds/reorganizes indexes
	
* Description
	Maintenance routine that will dynamically determine how to maintain indexing. It will determine based on
	several thresholds whether or not to rebuild or reorganize.

* Examples
	EXEC [dbo].[Reindex]

* Dependencies
	dbo.DBSearch_fn
	dbo.Split_fn
	dbo.sysdatabases_vw
	dbo.Maintenance_Reindex_Logger

* Parameters
	@DBName = Databas(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@DBNameExclude = Databas(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@TableName = Table(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@TableNameExclude = Table(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	@IndexName = Indexes(s) to be reindexed (CSV supported, NULL for all, accepts % for searching)
	@IndexNameExclude = Index(es) to excluded (CSV supported, NULL for all, accepts % for searching)
	@ReindexType = Three options: FULL (ignore @MinFragLevel), REBUILDALL (REBUILD all indexes) and ONLINEONLY (only process those that can be rebuilt online or reorganized)
	@MinFragLevel = The minimum fragmentation level of the indexing to ignore
	@RebuildFragLevel = The fragmentation level in which a rebuild will occur vs. a reorganize
	@MinPageCount = The minimum page count of the indexing to ignore
	@MinRecordCount = The minimum record count of the indexing to ignore
	@Logging = Log the state of the statistics prior to executing
	@Debug	= Display but don't run queries 
	
* Notes

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20080409	Adam Bean		Added TableName parameter
	20090511	Adam Bean		Added ARITHABORT setting
	20090629	Adam Bean		Removed brackets from INDEXDEFRAG statement as it was causing objects with periods to break
	20090801	Adam Bean		Added brackets to database index name on INDEXDEFRAG statement
	20090929	Adam Bean		Global header formatting & general clean up
	20091006	Adam Bean		Removed brackets from data types
	20100425	Adam Bean		Fixed collation issues
	20110928	Adam Bean		Started over
	20111212	Adam Bean		Added exclusion for read only databases
	20120611	Adam Bean		Added check for object existence prior to running DBCC SHOWCONTIG
    20140222	Adam Bean		Added @DateStart, @DateEnd to record start and end times of the index maintenance per object
								Brought in RunID from logger to here so that this is recorded for the whole run vs. by object
******************************************************************************/

CREATE PROCEDURE [dbo].[Reindex]
(
	@DBName						NVARCHAR(2048)	= NULL		
	,@DBNameExclude				NVARCHAR(2048)	= NULL		
	,@TableName					NVARCHAR(2048)	= NULL		
	,@TableNameExclude			NVARCHAR(2048)	= NULL
	,@IndexName					NVARCHAR(2048)	= NULL		
	,@IndexNameExclude			NVARCHAR(2048)	= NULL	
	,@ReindexType				VARCHAR(16)		= NULL
	,@MinFragLevel				TINYINT			= 10
	,@RebuildFragLevel			TINYINT			= 30
	,@MinPageCount				TINYINT			= 5
	,@MinRecordCount			INT				= 1000
	,@Logging					BIT				= 1
	,@Debug						BIT				= 0
)

AS

SET NOCOUNT ON
SET ARITHABORT ON

DECLARE
	@TableNameSearch			NVARCHAR(512)
	,@TableNameSearchExclude	NVARCHAR(512)
	,@IndexNameSearch			NVARCHAR(512)
	,@IndexNameSearchExclude	NVARCHAR(512)
	,@DBID						INT
	,@TblName					NVARCHAR(512)
	,@ObjectID					INT
	,@Comma						VARCHAR(1)
	,@RunID						INT
	,@DateStart					DATETIME
	,@DateEnd					DATETIME
	,@SQL						NVARCHAR(2048)
	,@ReindexCommand			NVARCHAR(4000)
	,@Command					VARCHAR(3072)
	,@ThisDB					NVARCHAR(128)
	,@ReindexFailureError		NVARCHAR(256)
	,@SchemaName				NVARCHAR(128)
	,@IndexFillFactor			INT
	,@AvgFrag					FLOAT
	,@PageSpaceUsed				FLOAT
	,@RecordSize				FLOAT
	,@Type						VARCHAR(16)
	,@ObjectExists				BIT

IF @Debug = 1
	PRINT '/*** DEBUG ENABLED ****/'

-- Get the run ID
IF @Debug = 0
	EXEC [dbo].[Maintenance_GetRunID] @RunID = @RunID OUTPUT

-- Initialize variables
SET @ThisDB = (SELECT DB_NAME())
SET @ReindexFailureError = CONVERT(VARCHAR, GETDATE(), 120) + ' - Reindexing failed. Investigate the failures by running SELECT [Error], * FROM [' + @ThisDB + '].[dbo].[Maintenance_Reindex_Stage] WHERE [Processed] = 0'

-- Determine if we're doing search or a match for database, table or index
IF CHARINDEX('%',@DBName) > 0
	SET @DBName = (SELECT [dbo].[DBSearch_fn](@DBName))
IF CHARINDEX('%',@DBNameExclude) > 0
	SET @DBNameExclude = (SELECT [dbo].[DBSearch_fn](@DBNameExclude))
IF CHARINDEX('%',@TableName) > 0
BEGIN
	SET @TableNameSearch = @TableName
	SET @TableName = NULL
END
IF CHARINDEX('%',@TableNameExclude) > 0
BEGIN
	SET @TableNameSearchExclude = @TableNameExclude
	SET @TableNameExclude = NULL
END
IF CHARINDEX('%',@IndexName) > 0
BEGIN
	SET @IndexNameSearch = @IndexName
	SET @IndexName = NULL
END
IF CHARINDEX('%',@IndexNameExclude) > 0
BEGIN
	SET @IndexNameSearchExclude = @IndexNameExclude
	SET @IndexNameExclude = NULL
END
	
-- Get all of the catalog views
IF OBJECT_ID('tempdb.dbo.#sysindexes') IS NOT NULL
   DROP TABLE #sysindexes
IF OBJECT_ID('tempdb.dbo.#sysobjects') IS NOT NULL
   DROP TABLE #sysobjects
IF OBJECT_ID('tempdb.dbo.#showcontig') IS NOT NULL
	DROP TABLE #showcontig
IF OBJECT_ID('tempdb.dbo.#Indexes') IS NOT NULL
   DROP TABLE #Indexes

-- Create the tables
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT), [SchemaName] = CAST(NULL AS NVARCHAR(128)) INTO #sysobjects FROM [dbo].[sysobjects]
SELECT TOP 0 *, [database_id] = CAST(NULL AS INT), [WithOptions] = CAST(NULL AS VARCHAR(128)) INTO #sysindexes FROM [dbo].[sysindexes]

-- Create table to capture SHOWCONTIG output
CREATE TABLE #showcontig
(
	[ObjectName]		VARCHAR(512)
	,[ObjectId]			INT
	,[IndexName]		VARCHAR(512)
	,[IndexId]			INT
	,[Lvl]				INT
	,[Pages]			INT
	,[Rows]				INT
	,[MinRecSize]		INT
	,[MaxRecSize]		INT
	,[AvgRecSize]		INT
	,[ForRecCount]		INT
	,[Extents]			INT
	,[ExtentSwitches]	INT
	,[AvgFreeBytes]		INT
	,[AvgPageDensity]	INT
	,[ScanDensity]		DECIMAL
	,[BestCount]		INT
	,[ActualCount]		INT
	,[LogicalFrag]		DECIMAL
	,[ExtentFrag]		DECIMAL
	,[database_id]		INT
)

-- Retrieve data per database
DECLARE #dbs CURSOR LOCAL STATIC FOR
SELECT 
	[name]
	,[database_id]
FROM [dbo].[sysdatabases_vw] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE s.[state_desc] = 'ONLINE'
AND s.[user_access_desc] = 'MULTI_USER'
AND s.[is_read_only] = 0
AND s.[source_database_id] IS NULL
AND s.[name] NOT IN ('tempdb')
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases
ORDER BY 1

OPEN #dbs
FETCH NEXT FROM #dbs INTO @DBName, @DBID
WHILE @@FETCH_STATUS = 0
BEGIN

	EXEC
	('
		USE [' + @DBName + '] 
		
		-- sysobjects
		INSERT INTO #sysobjects
		SELECT 
			*
			,' + @DBID + '
			,USER_NAME([uid])
		FROM [dbo].[sysobjects] 
		WHERE [type] = ''U''
		
		-- sysindexes
		INSERT INTO #sysindexes
		SELECT 
			*
			,' + @DBID + '
			,NULL
		FROM [dbo].[sysindexes]
		WHERE [indid] != 0
	')

FETCH NEXT FROM #dbs INTO @DBName, @DBID
END
CLOSE #dbs
DEALLOCATE #dbs

-- Capture the index statistics
DECLARE #IndexFrag CURSOR FAST_FORWARD FOR
SELECT 
	DB_NAME([database_id])
	,[database_id]
	,[id] 
FROM #sysobjects

OPEN #IndexFrag
FETCH NEXT FROM #IndexFrag INTO @DBName, @DBID, @ObjectID
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Let's make sure the object still exists (avoid temporary real tables)
	SET @ObjectExists = 0
	SET @SQL = N'IF EXISTS (SELECT * FROM [' + @DBName + '].[dbo].[sysobjects] WHERE [id] = ' + CAST(@ObjectID AS VARCHAR) + ') SET @ObjectExists = 1'
	EXEC sp_executesql @SQL, N'@ObjectExists BIT OUTPUT', @ObjectExists = @ObjectExists OUTPUT
	
	IF @ObjectExists = 1
		EXEC
		(
			'
				USE [' + @DBName + ']
			
				-- Run showcontig against each table
				INSERT INTO #showcontig
				(
					[ObjectName]
					,[ObjectId]
					,[IndexName]
					,[IndexId]
					,[Lvl]
					,[Pages]
					,[Rows]
					,[MinRecSize]
					,[MaxRecSize]
					,[AvgRecSize]
					,[ForRecCount]
					,[Extents]
					,[ExtentSwitches]
					,[AvgFreeBytes]
					,[AvgPageDensity]
					,[ScanDensity]
					,[BestCount]
					,[ActualCount]
					,[LogicalFrag]
					,[ExtentFrag]
				)
				EXEC(''DBCC SHOWCONTIG (' + @ObjectID + ') WITH ALL_INDEXES, TABLERESULTS, NO_INFOMSGS'')
			'
		)
	
	-- Capture the database_id
	UPDATE #showcontig
	SET [database_id] = @DBID
	WHERE [database_id] IS NULL

FETCH NEXT FROM #IndexFrag INTO @DBName, @DBID, @ObjectID
END
CLOSE #IndexFrag
DEALLOCATE #IndexFrag

-- Populate table with all the indexes that need rebuilt
SELECT 
	DB_NAME(o.[database_id])		AS [DBName]
	,o.[id]							AS [TableID]
	,i.[indid]						AS [IndexID]
	,c.[ObjectName]					AS [TableName]
	,o.[schemaname]					AS [SchemaName]
	,c.[IndexName]
	,CASE i.[indid]
		WHEN 0 THEN 'HEAP'
		WHEN 1 THEN 'CLUSTERED'
		ELSE 'NON CLUSTERED'
	END								AS [IndexType]
	,i.[OrigFillFactor]				AS [IndexFillFactor]
	,c.[LogicalFrag]				AS [AvgFrag]
	,c.[AvgPageDensity]				AS [PageSpaceUsed]
	,c.[AvgRecSize]					AS [RecordSize]
	,CASE
		-- If REBUILDALL is specified, REBUILD everything
		WHEN @ReindexType = 'REBUILDALL' THEN 'REBUILD'
		-- If ONLINEONLY is specified, REORGANIZE if it can't be done online
		WHEN @ReindexType = 'ONLINEONLY' THEN 'REORGANIZE'
		-- If the fragmentation is greater than @RebuildFragLevel, or space used is greater than 75%, REBUILD
		WHEN ROUND(c.[LogicalFrag],0) >= @RebuildFragLevel OR ROUND(c.[AvgPageDensity],0) > 75 THEN 'REBUILD'
		-- Everything else, REORGANIZE
		ELSE 'REORGANIZE'
	END								AS [Type]
	,CAST(NULL AS VARCHAR(3072))	AS [Command]
INTO #Indexes 
FROM #sysobjects o 
JOIN #showcontig c
	ON c.[database_id] = o.[database_id]
	AND c.[ObjectId] = o.[id]
JOIN #sysindexes i
	ON i.[database_id] = o.[database_id]
	AND i.[id] = o.[id]
	AND i.[name] = c.[IndexName] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@TableName,',') tbl
	ON o.[name] = tbl.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@TableNameExclude,',') tble
	ON o.[name] = tble.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@IndexName,',') idx
	ON i.[name] = idx.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@IndexNameExclude,',') idxe
	ON i.[name] = idxe.[item] COLLATE DATABASE_DEFAULT
WHERE 
(
	(o.[name] LIKE @TableNameSearch) -- Target specified table names based on a search
	OR (i.[name] LIKE @IndexNameSearch) -- Target specified index names based on a search
	OR (o.[name] NOT LIKE @TableNameSearchExclude) -- Exclude specified table names based on a search
	OR (i.[name] NOT LIKE @IndexNameSearchExclude) -- Exclude specified index names based on a search
	OR (@TableNameSearch IS NULL AND @IndexNameSearch IS NULL AND @TableNameSearchExclude IS NULL AND @IndexNameSearchExclude IS NULL)
)	
AND
(
	((tbl.[item] IS NOT NULL AND @TableName IS NOT NULL) OR @TableName IS NULL) -- Specified table(s), or all
	AND ((idx.[item] IS NOT NULL AND @IndexName IS NOT NULL) OR @IndexName IS NULL) -- Specified index(s), or all
	AND tble.[item] IS NULL -- All but excluded tables
	AND idxe.[item] IS NULL -- All but excluded indexes
)
AND 
(
	(
		ROUND(c.[LogicalFrag],0) >= @MinFragLevel -- Only retrieve indexes with greater than @MinFragLevel
		AND c.[Pages] > @MinPageCount -- Only retrieve indexes with greater than @MinPageCount
		AND c.[Rows] > @MinRecordCount -- Only retrieve indexes with greater than @MinRecordCount
		
	)
	OR @ReindexType IN ('FULL', 'REBUILDALL') -- Reindex everything (ignoring fragmentation levels) if the type is "FULL" or "REBUILDALL"
)

-- Now loop through our list and rebuild/reorganize as appropriate
DECLARE #Reindex CURSOR LOCAL STATIC FOR 
SELECT 
	[DBName]
	,[TableName]
	,[SchemaName]
	,[IndexName]
	,[IndexFillFactor]
	,[AvgFrag]
	,[PageSpaceUsed]
	,[RecordSize]
	,[Type]
FROM #Indexes
ORDER BY [DBName], [TableName], [IndexName]

OPEN #Reindex
FETCH NEXT FROM #Reindex INTO @DBName, @TableName, @SchemaName, @IndexName, @IndexFillFactor, @AvgFrag, @PageSpaceUsed, @RecordSize, @Type
WHILE @@FETCH_STATUS = 0
BEGIN
--select @DBName, @TableName, @SchemaName, @IndexName
	-- Build the reindex command
	IF @Type = 'REBUILD'
		SET @ReindexCommand = 'DBCC DBREINDEX(''[' + @DBName + '].[' + @SchemaName + '].[' + @TableName + ']'',''' + @IndexName + ''') WITH NO_INFOMSGS'
	ELSE
		SET @ReindexCommand = 'DBCC INDEXDEFRAG([' + @DBName + '],[' + @SchemaName + '.' + @TableName + '],[' + @IndexName + ']) WITH NO_INFOMSGS'

	-- Build our logging command and update the temp table for debugging purposes
	IF @Type = 'REBUILD'
		SET @Command = 'REBUILD'
	ELSE
		SET @Command = 'REORGANIZE'

	-- Update the temp table with the command for debugging purposes
	UPDATE #Indexes
		SET [Command] = @Command
	WHERE [DBName] = @DBName
	AND [TableName] = @TableName
	AND [SchemaName] = @SchemaName
	AND [IndexName] = @IndexName

	-- Let's make sure the object still exists (avoid temporary real tables)
	SET @ObjectExists = 0
	SET @SQL = N'IF EXISTS (SELECT * FROM [' + @DBName + '].[dbo].[sysindexes] WHERE [id] = OBJECT_ID(''[' + @SchemaName + '].[' + REPLACE(@TableName,'''','''''') + ']'') AND [name] = ''' + REPLACE(@IndexName,'''','''''') + ''') SET @ObjectExists = 1'
	EXEC sp_executesql @SQL, N'@ObjectExists BIT OUTPUT', @ObjectExists = @ObjectExists OUTPUT
	
	-- Check for object existance and remove those with nothing to do and run or execute
	IF @ObjectExists = 1 AND @ReindexCommand IS NOT NULL
	BEGIN	
		-- Print or execute
		IF @Debug = 1
			PRINT(@ReindexCommand)
		ELSE
		BEGIN
			PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - REINDEX started on [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '].[' + @IndexName + ']'
			-- Execute
			SET @DateStart = (SELECT GETDATE())
			EXEC(@ReindexCommand)
			SET @DateEnd = (SELECT GETDATE())

			-- Log the data if desired
			IF @Logging = 1
				EXEC [dbo].[Maintenance_Reindex_Logger]
					@DBName = @DBName
					,@SchemaName = @SchemaName
					,@TableName = @TableName
					,@IndexName = @IndexName
					,@IndexFillFactor = @IndexFillFactor
					,@PageSpaceUsed = @PageSpaceUsed
					,@RecordSize = @RecordSize
					,@AvgFrag = @AvgFrag
					,@Command = @Command
					,@Override = 0 -- SQL2005 and later only
					,@DateStart = @DateStart
					,@DateEnd = @DateEnd
					,@RunID = @RunID
			PRINT '' + CONVERT(VARCHAR, GETDATE(), 120) + ' - REINDEX finished on [' + @DBName + '].[' + @SchemaName + '].[' + @TableName + '].[' + @IndexName + ']'
		END
	END

FETCH NEXT FROM #Reindex INTO @DBName, @TableName, @SchemaName, @IndexName, @IndexFillFactor, @AvgFrag, @PageSpaceUsed, @RecordSize, @Type
END
CLOSE #Reindex
DEALLOCATE #Reindex

-- Wrap up time. Return data if debugging or check for and report on failures
IF @Debug = 1
BEGIN
	-- Return the staged data
	SELECT 
		* 
	FROM dbo.[Maintenance_Reindex_Stage] 
	ORDER BY [DBName], [TableName], [IndexName]
END
ELSE
BEGIN
	-- Update our Run table with an end time
	UPDATE dbo.[Maintenance_RunIDs]
	SET [DateEnd] = (SELECT GETDATE())
	WHERE [RunID] = @RunID

	-- Report failure if any processing failed
	IF (SELECT COUNT(*) FROM dbo.[Maintenance_Reindex_Stage] WHERE [Processed] = 0) > 0
	BEGIN
		PRINT (@ReindexFailureError)
		RAISERROR (@ReindexFailureError, 16, 2) WITH LOG
		RETURN
	END
END

SET NOCOUNT OFF
SET ARITHABORT ON 