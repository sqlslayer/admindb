IF OBJECT_ID('[dbo].[DeployLoginAuditNotification]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[DeployLoginAuditNotification]
GO

/******************************************************************************
* Name
	[dbo].[DeployLoginAuditNotification]

* Author
	John DelNostro
	
* Date
	2012.01.15
	
* Synopsis
	Wrapper procedure to deploy the LoginAuditNotification procedure
	
* Description
	Wrapper procedure is required as the procedure can not accept email recipients as a parameter because
	a queue can not call a procedure with a parameter. 

* Examples
	EXEC [dbo].[DeployLoginAuditNotification] @Recipients = 'you@domain.com'

* Dependencies
	dbo.Split_fn

* Parameters
	@Recipients			- Email recipients of the notification
	@Debug				- Show the procedure to be deployed but do not execute
	
* Notes
	Currently the procedure will report error stating that the object already exists when you run the procedure.
		You can run in @debug = 1 and run the output without issue. Have an issue with dynamic SQL that needs to be resolved.
	This procedure will also setup the queue, service and event notification.

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20131018	Adam Bean		Added filter to exclude standard NT groups as they''re typically rebuilt nightly
******************************************************************************/

CREATE PROCEDURE [dbo].[DeployLoginAuditNotification]
(
	@Recipients					VARCHAR(1024)
	,@LoginExclusions			VARCHAR(2048) = NULL
	,@ExcludeStandardNTGroups	BIT = 1
	,@Debug						BIT	= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@ServerName					NVARCHAR(128)
	,@InstanceName				NVARCHAR(128)
	,@StandardGroupNames		VARCHAR(1024)
	,@DropSQL					VARCHAR(512)
	,@SQL						VARCHAR(MAX)
	,@DBName					NVARCHAR(128)

-- Populate some working variables
SET @DBName = (SELECT DB_NAME())
SET @Recipients = REPLACE(@Recipients, ',', ';')
SET @ServerName = (SELECT CAST(SERVERPROPERTY('MachineName') AS VARCHAR(128)))
SET @InstanceName = (SELECT CAST(SERVERPROPERTY('InstanceName') AS VARCHAR(128)))
SET @StandardGroupNames = '%' + UPPER(@ServerName) + '_' + ISNULL(UPPER(@InstanceName),'DEFAULT') + '%'

-- Build the login exclusions
IF @LoginExclusions IS NOT NULL OR @ExcludeStandardNTGroups = 1
BEGIN

	SET @LoginExclusions = 
	(
		SELECT STUFF((SELECT ', ' + s.[name]
		FROM sys.server_principals s
		LEFT JOIN [dbo].[Split_fn](@LoginExclusions,',') le
			ON s.[name] = le.[item] COLLATE DATABASE_DEFAULT
		WHERE (s.[name] LIKE @StandardGroupNames AND @ExcludeStandardNTGroups = 1) -- Exclude standard nt groups if desired
		OR (le.[item] IS NOT NULL AND @LoginExclusions IS NOT NULL) -- Specific excluded logins
		FOR XML PATH('')),1, 2, '')
	)
END
ELSE
	SET @LoginExclusions = '' -- Until I can figure out something a bit cleaner, the dynamic SQL below to create the proc was breaking if this value was NULL

IF @Debug = 0
	BEGIN
	-- Setup service broker
	IF NOT EXISTS
	(
		SELECT 
			[name] 
		FROM [dbo].[sysdatabases_vw] 
		WHERE [is_broker_enabled] = 1
		AND [name] = @DBName
	)
	BEGIN
		EXEC('ALTER DATABASE [' + @DBName + '] SET ENABLE_BROKER WITH ROLLBACK IMMEDIATE')
	END

	-- Setup queue
	IF NOT EXISTS (SELECT [name] FROM [sys].[service_queues] WHERE [name] = N'LoginAuditNotificationQueue')
	CREATE QUEUE [dbo].[LoginAuditNotificationQueue] 
		WITH STATUS = ON
		,RETENTION = OFF
		,ACTIVATION (STATUS = OFF, PROCEDURE_NAME = [dbo].[LoginAuditNotification]
		,MAX_QUEUE_READERS = 5
		,EXECUTE AS OWNER)
	ON [PRIMARY] 

	-- Setup service
	IF NOT EXISTS (SELECT [name] FROM [sys].[services] WHERE [name] = N'LoginAuditNotificationService')
	CREATE SERVICE [LoginAuditNotificationService] AUTHORIZATION [dbo] 
	ON QUEUE [dbo].[LoginAuditNotificationQueue] ([http://schemas.microsoft.com/SQL/Notifications/PostEventNotification])

	-- Setup event notification
	IF NOT EXISTS (SELECT [name] FROM [sys].[server_event_notifications] WHERE [name] = 'LoginNotification')
	CREATE EVENT NOTIFICATION LoginAuditNotification
		ON SERVER
		FOR CREATE_LOGIN, ALTER_LOGIN, DROP_LOGIN
		TO SERVICE 'LoginAuditNotificationService', 'current database'
END

-- Create the procedure
SET @DropSQL = 
'
IF OBJECT_ID(''[dbo].[LoginAuditNotification]'',''P'') IS NOT NULL
	DROP PROCEDURE [dbo].[LoginAuditNotification]
'

SET @SQL = 
'

EXEC
(''

/******************************************************************************
* Name
	[dbo].[LoginAuditNotification]

* Author
	John DelNostro
	
* Date
	2012.01.15
	
* Synopsis
	Procedure to notify recipients of Login changes (drops, alters, creates etc.)
	
* Description
	Procedure which will send an HTML formatted email showing the last three states of logins on said instance.
	The procedure is fired via service broker.

* Examples
	EXEC [dbo].[LoginAuditNotification]

* Dependencies
	[dbo].[DeployLoginAuditNotification]

* Parameters
	n/a
	
* Notes
	To only be created from the wrapper procedure: [dbo].[DeployLoginAuditNotification]

*******************************************************************************
* License
*******************************************************************************
	Copyright � SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
	20131018	Adam Bean		Added @LoginExclusions to exclude logins passed from wrapper
******************************************************************************/

CREATE PROCEDURE [dbo].[LoginAuditNotification]
WITH EXECUTE AS OWNER
AS 

SET NOCOUNT ON

DECLARE 
	@LoginExclusions	VARCHAR(2048)
	,@message_body		XML
	,@email_message		NVARCHAR(MAX)
	,@Subject			VARCHAR(150)
	,@EmailHeader		VARCHAR(2048)
	,@EmailBody			VARCHAR(MAX)
	,@EmailCaption		VARCHAR(128)
	,@EmailFooter		VARCHAR(2048)
	,@Recipients		VARCHAR(MAX)
	,@Count				INT
	,@html				NVARCHAR(MAX)

SET @Recipients	= ''''' + @Recipients + '''''
SET @Subject	= ''''Login Audit Notification - Login Status Change On '''' + @@SERVERNAME 
SET @LoginExclusions = ''''' + @LoginExclusions + '''''

-- If no values were passed in the wrapper, an empty string had to be passed to avoid breaking the dynamic SQL
IF @LoginExclusions = '''''''' SET @LoginExclusions = NULL 

-- Parse XML Into Temp Table
DECLARE @EventTable TABLE
(   
	[EventType]			NVARCHAR(128)
	,[EventPostTime]	NVARCHAR(25)
	,[ServerName]		NVARCHAR(256)
	,[LoginName]		NVARCHAR(100)
	,[LoginType]		NVARCHAR(50)
	,[ObjectName]		NVARCHAR(256)
)


WHILE (1 = 1)
BEGIN
	BEGIN TRANSACTION
	-- Receive the next available message FROM the queue
	WAITFOR 
	(
		RECEIVE TOP (1) CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/EventType)[1]'''',''''NVARCHAR(128)'''')	AS [EventType]
		               ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/PostTime)[1]'''',''''NVARCHAR(25)'''')		AS [EventPostTime]
		               ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/ServerName)[1]'''',''''NVARCHAR(256)'''')	AS [ServerName]
		               ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/LoginName)[1]'''',''''NVARCHAR(100)'''')	AS [LoginName]
		               ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/LoginType)[1]'''',''''NVARCHAR(50)'''')	AS [LoginType]
		               ,CAST(message_body AS XML).value(''''(/EVENT_INSTANCE/ObjectName)[1]'''',''''NVARCHAR(256)'''')	AS [ObjectName]
		FROM [dbo].[LoginAuditNotificationQueue]		
		INTO @EventTable
	), TIMEOUT 5000
	-- If we didn''''t get anything, bail out
	IF (@@ROWCOUNT = 0)
	BEGIN
		ROLLBACK TRANSACTION
		BREAK
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
	END
END	

-- Build the email
SET @EmailCaption = ''''SQL Server Login Audit Notification For '''' + @@SERVERNAME 

-- Setup table to build body
DECLARE @EmailBodyContents TABLE 
(
	[html]			VARCHAR(MAX)
)

-- Setup table to build email
DECLARE @Email TABLE 
(
	[ID]			INT IDENTITY
   ,[html]			VARCHAR(MAX)
)

-- Build the email header
SELECT @EmailHeader = 
''''
<HTML>
<DIV ALIGN="CENTER">
<TABLE BORDER="1" CELLSPACING="5" CELLPADDING="1" ALIGN="CENTER">
<CAPTION>
	<H2>'''' + @EmailCaption + ''''</H2>
	<EM><i>The following Login Change Event(s) have occured on :'''' + @@SERVERNAME + ''''</i></EM>
</CAPTION>
<TR BGCOLOR="#C0C0C0">
	<TH>EventType</TH>
	<TH>EventPostTime</TH>
	<TH>ServerName</TH>
	<TH>LoginName</TH>
	<TH>Logintype</TH>
	<TH>ObjectName</TH>
</TR>
''''

-- Build the email footer
SELECT @EmailFooter =
''''
</TABLE>
</DIV>
</HTML>
''''

-- Build the email body
INSERT INTO @EmailBodyContents
SELECT
	''''
	<TR>
		<TD>'''' + E.[EventType]		+ ''''</TD>
		<TD>'''' + E.[EventPostTime]	+ ''''</TD>
		<TD>'''' + E.[ServerName]		+ ''''</TD>
		<TD>'''' + E.[LoginName]		+ ''''</TD>
		<TD>'''' + E.[LoginType]		+ ''''</TD>
		<TD>'''' + E.[ObjectName]		+ ''''</TD>

	</TR>
	'''' 
FROM ( 
		SELECT      
			A.[EventType]
			,REPLACE(REPLACE(A.[EventPostTime],''''T'''','''' ''''),''''.'''','''''''')		AS [EventPostTime]
			,A.[ServerName]
			,A.[LoginName]
			,A.[LoginType]
			,A.[ObjectName]
			,ROW_NUMBER() OVER (PARTITION BY [LoginName] ORDER BY [EventPostTime] DESC)	AS Row 
		FROM @EventTable A
		LEFT JOIN [dbo].[Split_fn](@LoginExclusions,'''','''') le
			ON A.[ObjectName] = le.[item] COLLATE DATABASE_DEFAULT
		WHERE le.[item] IS NULL -- Excluded logins
      ) E

-- Build the email
INSERT INTO @Email
SELECT @EmailHeader
UNION ALL
SELECT *
FROM @EmailBodyContents
UNION ALL
SELECT @EmailFooter

-- Put it all together
SET @Count = 1
SET @EmailBody = ''''''''
WHILE @Count <= (SELECT COUNT(*) FROM @Email)
BEGIN
	SET @EmailBody = @EmailBody + (SELECT [html] FROM @Email WHERE [ID] = @Count)
	SET @Count = @Count + 1
END

-- Only send email if there is data
IF @Count > 3
BEGIN
	-- Send mail
	EXEC [msdb].[dbo].[sp_send_dbmail]
		@Recipients		= @Recipients
		,@Subject		= @Subject
		,@Body			= @EmailBody
		,@Body_format	= ''''HTML''''
END
'')
'

IF @Debug = 1
BEGIN
	PRINT '/*** DEBUG ENABLED ****/'
	PRINT (@DropSQL)
	PRINT (@SQL)
END
ELSE
BEGIN
	EXEC (@DropSQL)
	--EXEC (@SQL)
	IF EXISTS (SELECT [name] FROM [sys].[objects] WHERE [name] = '[LoginAuditNotification]')
	BEGIN
		PRINT 'Successfully deployed [dbo].[LoginAuditNotification] with ''' + @Recipients + ''' as the recipient(s).'
	END
END