IF OBJECT_ID('[dbo].[sysserver_principals_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[sysserver_principals_vw]
GO
/*******************************************************************************************************
**	Name:			dbo.sysserver_principals_vw
**	Desc:			View to give you a common 2005 view of sys.server_principals
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2010.10.21
*******************************************************************************
**  License
*******************************************************************************
**  Copyright ? SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**  
********************************************************************************************************/
CREATE VIEW [dbo].[sysserver_principals_vw]
AS
SELECT 
	[name]
	,[principal_id]
	,[sid]
	,[type]
	,[type_desc]
	,[is_disabled]
	,[create_date]
	,[modify_date]
	,[default_database_name]
	,[default_language_name]
	,[credential_id]
FROM [sys].[server_principals]