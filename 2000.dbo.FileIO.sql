IF OBJECT_ID('[dbo].[FileIO]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[FileIO]
GO

/*******************************************************************************************************
**	Name:			dbo.FileIO
**	Desc:			Procedure to retrieve resource totals based on duration
**	Auth:			Adam Bean (SQLSlayer.com)
**  Parameters:		@SecondsToSample = Delay in seconds of time to sample
**	Date:			2009.06.02
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
**  20100215	Adam Bean		Fixed SYSNAME collation incompatibility
**	20101021	Matt Stanford	Added Disk Summary data
**	20101025	Matt Stanford	Added percentage columns
********************************************************************************************************/

CREATE PROCEDURE [dbo].[FileIO]
(
	@SecondsToSample	INT = 30
	,@DBName			sysname = NULL
	,@ShowZeros			BIT = 0
	,@ShowDiskData		BIT = 0
	,@ShowFolderData	BIT = 0
)

AS

SET NOCOUNT ON

DECLARE 
	@EndTime		DATETIME
	,@DbId			TINYINT
	,@FileId		TINYINT

SET @EndTime = DATEADD(s,@SecondsToSample,GETDATE())

IF OBJECT_ID('tempdb.dbo.#io') IS NOT NULL
DROP TABLE #io

CREATE TABLE #io (
	[database_name]					NVARCHAR(128)
	,[file_id]						SMALLINT
	,[file_type]					NVARCHAR(128)
	,[file_name]					NVARCHAR(128)
	,[sample_ms]					INT 
	,[sample_ms_post]				INT 
	,[num_of_reads]					BIGINT 
	,[num_of_reads_post]			BIGINT 
	,[num_of_bytes_read]			BIGINT 
	,[num_of_bytes_read_post]		BIGINT 
	,[io_stall_read_ms]				BIGINT 
	,[io_stall_read_ms_post]		BIGINT 
	,[num_of_writes]				BIGINT 
	,[num_of_writes_post]			BIGINT 
	,[num_of_bytes_written]			BIGINT 
	,[num_of_bytes_written_post]	BIGINT 
	,[io_stall_write_ms]			BIGINT 
	,[io_stall_write_ms_post]		BIGINT 
	,[io_stall]						BIGINT 
	,[io_stall_post]				BIGINT 
)

-- Retrieve our file stats as of right now
-- Have to cursor through each dbid and fileid to call fn_virtualfilestats
DECLARE #Files CURSOR FOR
SELECT 
	[dbid]
	,[fileid]
FROM [master].[dbo].[sysaltfiles]
WHERE (DB_NAME([dbid]) = @DBName OR @DBName IS NULL)
ORDER BY 1

OPEN #Files
FETCH NEXT FROM #Files INTO @DbId, @FileId
WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO #io 
	(
		[database_name]
		,[file_id]
		,[file_type]
		,[file_name]
		,[sample_ms]
		,[num_of_reads]
		,[num_of_bytes_read]
		,[num_of_writes]
		,[num_of_bytes_written]
		,[io_stall]
	)

	SELECT 
		DB_NAME(s.[DbId])			AS [database_name]
		,s.[FileID]
		,CASE
			WHEN ([status] & 64 <> 0) THEN 'Log'
			ELSE 'Data'
		END							AS [file_type]
		,f.[name]					AS [file_name]
		,s.[TimeStamp]
		,s.[NumberReads]
		,s.[BytesRead]
		,s.[NumberWrites]
		,s.[BytesWritten]
		,s.[IoStallMS]
	FROM :: fn_virtualfilestats(@DbId, @FileId) s
	INNER JOIN [master].[dbo].[sysaltfiles] f
		ON f.[DbId] = s.[DbId]
		AND f.[FileID] = s.[FileID]

FETCH NEXT FROM #Files INTO @DbId, @FileId
END
CLOSE #Files
DEALLOCATE #Files

-- Wait for specified delay
WAITFOR TIME @EndTime

-- Get file stats after delay
DECLARE #Files CURSOR FOR
SELECT 
	[dbid]
	,[fileid]
FROM [master].[dbo].[sysaltfiles]
WHERE (DB_NAME([dbid]) = @DBName OR @DBName IS NULL)
ORDER BY 1

OPEN #Files
FETCH NEXT FROM #Files INTO @DbId, @FileId
WHILE @@FETCH_STATUS = 0
BEGIN

	UPDATE #io
	SET [sample_ms_post]				= t.[TimeStamp]
		,[num_of_reads_post]			= t.[NumberReads]
		,[num_of_bytes_read_post]		= t.[BytesRead]
		,[num_of_writes_post]			= t.[NumberWrites]
		,[num_of_bytes_written_post]	= t.[BytesWritten]
		,[io_stall_post]				= t.[IoStallMS]
	FROM
	(
		SELECT 
			DB_NAME([DbId]) AS [database_name]
			,[FileId]
			,[TimeStamp]
			,[NumberReads]
			,[BytesRead]
			,[NumberWrites]
			,[BytesWritten]
			,[IoStallMS]
		FROM :: fn_virtualfilestats(@DbId,@FileId)
	) t
	WHERE #io.[database_name] = t.[database_name]
	AND #io.[file_id] = t.[FileId]

FETCH NEXT FROM #Files INTO @DbId, @FileId
END
CLOSE #Files
DEALLOCATE #Files

-- Save the compiled data
SELECT 
	[database_name]											AS [Database]
	,[file_id]												AS [FileID]
	,[file_type]											AS [FileType]
	,[file_name]											AS [FileName]
	,[sample_ms_post] - [sample_ms]							AS [MilliSecondsElapsed]
	,[num_of_reads_post] - [num_of_reads]					AS [NumberOfReads]
	,[num_of_bytes_read_post] - [num_of_bytes_read]			AS [BytesRead]
	,[io_stall_read_ms_post] - [io_stall_read_ms]			AS [IOStallRead(MS)]
	,[num_of_writes_post] - [num_of_writes]					AS [NumberOfWrites]
	,[num_of_bytes_written_post] - [num_of_bytes_written]	AS [BytesWritten]
	,[io_stall_write_ms_post] - [io_stall_write_ms]			AS [IOStallWrite(MS)]
	,[io_stall_post] - [io_stall]							AS [IOStall(MS)]
INTO #io2
FROM #io

-- Show comparison of file stats after delay
SELECT 
	t.[Database]
	,t.[FileID]
	,t.[FileType]
	,t.[FileName]
	,t.[MilliSecondsElapsed] / 1000							AS [SecondsElapsed]
	,t.[NumberOfReads]
	,t.[BytesRead]
	,t.[IOStallRead(MS)]
	,CAST(ROUND(((t.[IOStallRead(MS)] * 100.0)/(t.[MilliSecondsElapsed])),1) AS DECIMAL(10,1)) AS [IOStallRead(%)]
	,t.[NumberOfWrites]
	,t.[BytesWritten]
	,t.[IOStallWrite(MS)]
	,CAST(ROUND(((t.[IOStallWrite(MS)] * 100.0)/(t.[MilliSecondsElapsed])),1) AS DECIMAL(10,1)) AS [IOStallWrite(%)]
	,t.[IOStall(MS)]
	,CAST(ROUND(((t.[IOStall(MS)] * 100.0)/(t.[MilliSecondsElapsed])),1) AS DECIMAL(10,1)) AS [IOStall(%)]
	,LEFT(f.[filename],LEN(f.[filename]) - CHARINDEX('\',REVERSE(f.[filename]))) AS [Folder]
	,(f.[size] * 8)/1024									AS [FileSize(MB)]
FROM #io2 t
LEFT OUTER JOIN [master].[dbo].[sysaltfiles] f
	ON t.[database] = DB_NAME(f.[dbid])
	AND t.[fileid] = f.[fileid]
WHERE (
	@ShowZeros = 0 AND 
		(t.[NumberOfReads] > 0
		OR t.[NumberOfWrites] > 0)
) OR (@ShowZeros = 1)
ORDER BY 1, 2

-- Show the disk data summary
IF @ShowDiskData = 1
SELECT
	UPPER(LEFT(f.[filename],1))								AS [DriveLetter]
	,SUM(t.[NumberOfReads])									AS [NumberOfReads]
	,SUM(t.[BytesRead])										AS [BytesRead]
	,SUM(t.[IOStallRead(MS)])								AS [IOStallRead(MS)]
	,SUM(t.[NumberOfWrites])								AS [NumberOfWrites]
	,SUM(t.[BytesWritten])									AS [BytesWritten]
	,SUM(t.[IOStallWrite(MS)])								AS [IOStallWrite(MS)]
	,SUM(t.[IOStall(MS)])									AS [IOStall(MS)]
FROM #io2 t
LEFT OUTER JOIN [master].[dbo].[sysaltfiles] f
	ON t.[database] = DB_NAME(f.[dbid])
	AND t.[fileid] = f.[fileid]
GROUP BY LEFT(f.[filename],1)
ORDER BY 1

-- Show the folder data summary
IF @ShowFolderData = 1
SELECT 
	LEFT(f.[filename],LEN(f.[filename]) - CHARINDEX('\',REVERSE(f.[filename]))) AS [Folder]
	,SUM(t.[NumberOfReads])									AS [NumberOfReads]
	,SUM(t.[BytesRead])										AS [BytesRead]
	,SUM(t.[IOStallRead(MS)])								AS [IOStallRead(MS)]
	,SUM(t.[NumberOfWrites])								AS [NumberOfWrites]
	,SUM(t.[BytesWritten])									AS [BytesWritten]
	,SUM(t.[IOStallWrite(MS)])								AS [IOStallWrite(MS)]
	,SUM(t.[IOStall(MS)])									AS [IOStall(MS)]
FROM #io2 t
LEFT OUTER JOIN [master].[dbo].[sysaltfiles] f
	ON t.[database] = DB_NAME(f.[dbid])
	AND t.[fileid] = f.[fileid]
GROUP BY LEFT(f.[filename],LEN(f.[filename]) - CHARINDEX('\',REVERSE(f.[filename])))
ORDER BY 1

DROP TABLE #io
DROP TABLE #io2

SET NOCOUNT OFF