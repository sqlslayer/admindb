IF OBJECT_ID('[dbo].[VLFCount]','P') IS NOT NULL
	DROP PROCEDURE [dbo].[VLFCount]
GO


/******************************************************************************
* Name
	[dbo].[VLFCount]

* Author
	Jeff Gogel
	
* Date
	2013.02.04
	
* Synopsis
	Retrieves VLF information.
	
* Description
	Procedure to list the number of virtual log files for each database, or for a list of databases.

* Examples
	EXEC [dbo].[VLFCount]

* Dependencies
	dbo.DBSearch_fn
	dbo.Split_fn

* Parameters
	@DBName = Databas(s) to be get VLF information about (CSV supported, NULL for all, accepts % for searching)
	@DBNameExclude = Databas(s) to excluded (CSV supported, NULL for all, accepts % for searching)
	
*******************************************************************************
* License
*******************************************************************************
	Copyright ? SQLSlayer.com. All rights reserved.

	All objects published by SQLSlayer.com are licensed and goverened by 
	Creative Commons Attribution-Share Alike 3.0
	http://creativecommons.org/licenses/by-sa/3.0/

	For more scripts and sample code, go to http://www.SQLSlayer.com

	You may alter this code for your own *non-commercial* purposes. You may
	republish altered code as long as you give due credit.

	THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
	ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
	TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
	PARTICULAR PURPOSE.
*******************************************************************************
* Change History
*******************************************************************************
	Date:		Author:			Description:
	--------	--------		---------------------------------------
    20120204	Jeff Gogel		Initial creation.
******************************************************************************/
CREATE PROCEDURE [dbo].[VLFCount]
(
	@DBName						NVARCHAR(2048)	= NULL		
	,@DBNameExclude				NVARCHAR(2048)	= NULL
)
AS

SET NOCOUNT ON

DECLARE @Query				VARCHAR (100) 
DECLARE @DB					SYSNAME 
DECLARE @VLFs				INT 
DECLARE @Databases			TABLE ( name SYSNAME) 
DECLARE @VLFCounts			TABLE ( DBName SYSNAME,
									VLFCount INT)
DECLARE @DBCCLogInfoLegacy	TABLE (	[FileId]			TINYINT, 
									[FileSize]			BIGINT, 
									[StartOffSet]		BIGINT, 
									[FSeqNo]			INT,
									[Status]			TINYINT, 
									[Parity]			TINYINT, 
									[CreateLSN]			NUMERIC(25,0))


DECLARE @DBCCLogInfo	TABLE ( [RecoveryUnitID]	INT,
									[FileId]			TINYINT, 
									[FileSize]			BIGINT, 
									[StartOffSet]		BIGINT, 
									[FSeqNo]			INT,
									[Status]			TINYINT, 
									[Parity]			TINYINT, 
									[CreateLSN]			NUMERIC(25,0))

-- Determine if we're doing search or a match for database, table or index
IF CHARINDEX('%',@DBName) > 0
	SET @DBName = (SELECT [dbo].[DBSearch_fn](@DBName))
IF CHARINDEX('%',@DBNameExclude) > 0
	SET @DBNameExclude = (SELECT [dbo].[DBSearch_fn](@DBNameExclude))

INSERT INTO @Databases
SELECT [name] 
FROM [sys].[databases] s
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON s.[name] = d.[item] COLLATE DATABASE_DEFAULT
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON s.[name] = de.[item] COLLATE DATABASE_DEFAULT
WHERE [State] = 0 
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND de.[item] IS NULL -- All but excluded databases

WHILE EXISTS(SELECT TOP 1 [name] FROM @Databases) 
BEGIN 
    SET @DB = (SELECT TOP 1 [name] from @Databases) 
    SET @Query = 'DBCC LOGINFO (' + '''' + @DB + ''') ' 

	IF ((@@MICROSOFTVERSION / 0x01000000) < 11)
		INSERT INTO @DBCCLogInfoLegacy EXEC (@Query)
	ELSE
		INSERT INTO @DBCCLogInfo EXEC (@Query) 

    SET @VLFs = @@ROWCOUNT
    INSERT INTO @VLFCounts VALUES (@DB, @VLFs) 
 
    DELETE FROM @Databases WHERE [name] = @DB 
END 

SELECT DBName, VLFCount 
FROM @VLFCounts 
ORDER BY DBName

SET NOCOUNT OFF
