IF OBJECT_ID('[dbo].[systraces_vw]','V') IS NOT NULL
	DROP VIEW [dbo].[systraces_vw]
GO

/*******************************************************************************************************
**	Name:			dbo.systraces_vw
**	Desc:			View to give you a common 2005 view of sys.traces
**	Auth:			Matt Stanford (SQLSlayer.com)
**	Date:			2009.08.11
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE VIEW [dbo].[systraces_vw]

AS

SELECT
	CAST(t.[traceid] AS INT)					AS [id]
	,CAST(st.[value] AS INT)					AS [status]
	,CAST(f.[value] AS NVARCHAR(260))			AS [path]
	,CAST(m.[value] AS BIGINT)					AS [max_size]
	,CAST(s.[value] AS DATETIME)				AS [stop_time]
	,CAST(NULL AS INT)							AS [max_files]
	,CAST(0 AS BIT)								AS [is_rowset]
	,CAST(CASE 
		WHEN CAST(o.[value] AS INT) & 2 = 2 THEN 1
		ELSE 0
	END AS BIT)									AS [is_rollover]
	,CAST(CASE 
		WHEN CAST(o.[value] AS INT) & 4 = 4 THEN 1
		ELSE 0
	END AS BIT)									AS [is_shutdown]
	,CAST(0 AS BIT)								AS [is_default]
	,CAST(NULL AS INT)							AS [buffer_count]
	,CAST(NULL AS INT)							AS [buffer_size]
	,CAST(NULL AS BIGINT)						AS [file_position]
	,CAST(NULL AS INT)							AS [reader_spid]
	,CAST(NULL AS DATETIME)						AS [start_time]
	,CAST(NULL AS DATETIME)						AS [last_event_time]
	,CAST(NULL AS BIGINT)						AS [event_count]
	,CAST(NULL AS INT)						AS [dropped_event_count]
	,CAST(CASE 
		WHEN CAST(o.[value] AS INT) & 8 = 8 THEN 1
		ELSE 0
	END AS BIT)									AS [is_blackbox]
FROM (
	SELECT
		DISTINCT [traceid] 
	FROM ::fn_trace_getinfo(NULL)
) t
INNER JOIN ::fn_trace_getinfo(NULL) o
	ON t.[traceid] = o.[traceid]
	AND o.[property] = 1
INNER JOIN ::fn_trace_getinfo(NULL) f
	ON t.[traceid] = f.[traceid]
	AND f.[property] = 2
INNER JOIN ::fn_trace_getinfo(NULL) m
	ON t.[traceid] = m.[traceid]
	AND m.[property] = 3
INNER JOIN ::fn_trace_getinfo(NULL) s
	ON t.[traceid] = s.[traceid]
	AND s.[property] = 4
INNER JOIN ::fn_trace_getinfo(NULL) st
	ON t.[traceid] = st.[traceid]
	AND st.[property] = 5