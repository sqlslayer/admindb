IF OBJECT_ID('[dbo].[GetSecondsFromJobDuration_fn]','FN') IS NOT NULL
	DROP FUNCTION [dbo].[GetSecondsFromJobDuration_fn]
GO

/*******************************************************************************************************
**	Name:			dbo.GetSecondsFromJobDuration_fn 
**	Desc:			Translates HHMMSS format from sysjobhistory to seconds
**	Auth:			Matt Stanford (SQLSlayer.com)			
**	Date:			2007.12.18
*******************************************************************************
**  License
*******************************************************************************
**  Copyright � SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090929	Adam Bean		Global header formatting & general clean up
********************************************************************************************************/

CREATE FUNCTION [dbo].[GetSecondsFromJobDuration_fn]
(
	@duration			INT
)
RETURNS INT

AS

BEGIN

	DECLARE
		@HH			INT
		,@MM		TINYINT
		,@SS		TINYINT
		,@Seconds	INT	
		,@RAW		VARCHAR(12)

	SET @RAW = RIGHT('000000000000' + CAST(@duration AS VARCHAR(12)),12)

	SET @SS = CAST(RIGHT(@RAW,2) AS TINYINT)
	SET @MM = CAST(SUBSTRING(@RAW,9,2) AS TINYINT)
	SET @HH = CAST(LEFT(@RAW,8) AS INT)

	SET @Seconds = @SS + (@MM * 60) + (@HH * 3600)

	RETURN @Seconds

END