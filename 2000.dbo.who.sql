IF OBJECT_ID('[dbo].[who]','P') IS NOT NULL 
	DROP PROCEDURE [dbo].[who]
GO

/*******************************************************************************************************
**	Name:			dbo.who
**	Desc:			Procedure to retrieve information about sql connections (uses DBCC INPUTBUFFER)
**	Auth:			Adam Bean (SQLSlayer.com)
**	Dependancies:	dbo.ActiveDBLocks_vw & dbo.Split_fn
**	Parameters:		@OrderBy1, 2, 3 = Columns to order output by
					@Login = Login(s) to find connections for, CSV supported
					@LoginExclude = Login(s) to exclude connections for, CSV supported
					@DBName = Database(s) to find connections for, CSV supported
					@DBNameExclude = Database(s) to exclude connections for for, CSV supported
					@HostName = Host(s) to find connections for, CSV supported
					@HostNameExclude = Host(s) to exclude connections for, CSV supported
					@FilterSpid - Starting spid in which to filter on, defaulted to 50 (<50 = system)
**	Date:			2007.09.17
*******************************************************************************
**  License
*******************************************************************************
**  Copyright © SQLSlayer.com. All rights reserved.
**  
**  All objects published by SQLSlayer.com are licensed and goverened by 
**  Creative Commons Attribution-Share Alike 3.0
**  http://creativecommons.org/licenses/by-sa/3.0/
**  
**  For more scripts and sample code, go to http://www.SQLSlayer.com
**  
**  You may alter this code for your own *non-commercial* purposes. You may
**  republish altered code as long as you give due credit.
**  
**  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
**  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
**  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
**  PARTICULAR PURPOSE.
*******************************************************************************
**	Change History
*******************************************************************************
**	Date:		Author:			Description:
**	--------	--------		---------------------------------------
**	20090330	Adam Bean		Rewrote to match 2005 version of who, added logging
**  20090623	Adam Bean		Changed @OrderBy parameters to INT to remove SQL Injection
**	20090702	Matt Stanford	Expanded out the Program_Name column so that we can actually read it
								Pivoted out the lock info into separate columns
**  20090728	Adam Bean		Fixed for case sensitive collation
**  20090922	Adam Bean		Added @DBName, @LoginName and @HostName and Excludes
**	20090929	Adam Bean		Global header formatting & general clean up
**	20100117	Adam Bean		Added LTRIM/RTRIM to several columns
**	20100205	Adam Bean		Added net_transport and auth_scheme to match 2005 version
**	20100425	Adam Bean		Resolved collation issues
********************************************************************************************************/

CREATE PROCEDURE [dbo].[who] 
(
	@OrderBy1			INT				= 9 -- BlkBy
	,@OrderBy2			INT				= 1 -- Spid
	,@OrderBy3			INT				= 6 -- Host
	,@Login				VARCHAR(512)	= NULL
	,@LoginExclude		VARCHAR(512)	= NULL
	,@DBName			VARCHAR(512)	= NULL
	,@DBNameExclude		VARCHAR(512)	= NULL
	,@HostName			VARCHAR(512)	= NULL
	,@HostNameExclude	VARCHAR(512)	= NULL
	,@FilterSpid		INT				= 50
	,@Logging			BIT				= 0
)

AS

SET NOCOUNT ON

DECLARE 
	@SPID	INT
	,@SQLID INT

IF OBJECT_ID('tempdb.dbo.#Activity') IS NOT NULL
	DROP TABLE #Activity
	
IF OBJECT_ID('tempdb.dbo.#Locks') IS NOT NULL
	DROP TABLE #Locks
	
IF OBJECT_ID('tempdb.dbo.#RawLocks') IS NOT NULL
	DROP TABLE #RawLocks

IF OBJECT_ID('tempdb.dbo.#QueryCmd') IS NOT NULL
	DROP TABLE #QueryCmd

-- Setup table to hold spid info
CREATE TABLE #QueryCmd
(
	[SQLID]			INT IDENTITY
	,[spid]			INT
	,[EventType]	VARCHAR(100)
	,[Parameters]	INT
	,[Command]		VARCHAR(4000)
)

IF @Logging = 1
BEGIN
	-- Setup table to hold audit data	
	IF OBJECT_ID('dbo.Logging_ActivityMonitor') IS NULL
	BEGIN
		CREATE TABLE [dbo].[Logging_ActivityMonitor]
			(
				[spid]					SMALLINT
				,[KPID]					SMALLINT
				,[DBName]				VARCHAR(128)
				,[Query]				VARCHAR(4000)
				,[Login]				VARCHAR(128)
				,[HostName]				VARCHAR(128)
				,[Status]				VARCHAR(24)
				,[Command]				VARCHAR(32)
				,[BlkBy]				SMALLINT
				,[TranCount]			INT
				,[ReadLockCount]		INT
				,[WriteLockCount]		INT
				,[SchemaLockCount]		INT
				,[WaitType]				VARCHAR(64)
				,[PercentComplete]		REAL
				,[EstCompTime]			BIGINT
				,[CPU]					INT
				,[IO]					BIGINT
				,[Reads]				INT
				,[Writes]				INT
				,[LastRead]				SMALLDATETIME
				,[LastWrite]			SMALLDATETIME
				,[StartTime]			SMALLDATETIME
				,[LastBatch]			SMALLDATETIME
				,[NetTransport]			VARCHAR(16)
				,[AuthScheme]			VARCHAR(16)
				,[ProgramName]			VARCHAR(256) 
				,[DateCreated]			SMALLDATETIME
			)
		ALTER TABLE [dbo].[Logging_ActivityMonitor] ADD CONSTRAINT [Logging_ActivityMonitor_DateCreated]  DEFAULT (GETDATE()) FOR [DateCreated]
	END
END

-- Setup table to hold temporary data	
CREATE TABLE #Activity
(
	[spid]					SMALLINT
	,[KPID]					SMALLINT
	,[DBName]				VARCHAR(128)
	,[Query]				VARCHAR(4000)
	,[Login]				VARCHAR(128)
	,[HostName]				VARCHAR(128)
	,[Status]				VARCHAR(24)
	,[Command]				VARCHAR(32)
	,[BlkBy]				SMALLINT
	,[TranCount]			INT
	,[ReadLockCount]		INT
	,[WriteLockCount]		INT
	,[SchemaLockCount]		INT
	,[WaitType]				VARCHAR(64)
	,[PercentComplete]		REAL
	,[EstCompTime]			BIGINT
	,[CPU]					INT
	,[IO]					BIGINT
	,[Reads]				INT
	,[Writes]				INT
	,[LastRead]				SMALLDATETIME
	,[LastWrite]			SMALLDATETIME
	,[StartTime]			SMALLDATETIME
	,[LastBatch]			SMALLDATETIME
	,[NetTransport]			VARCHAR(16)
	,[AuthScheme]			VARCHAR(16)
	,[ProgramName]			VARCHAR(256) 
)

-- Table to hold lock info
CREATE TABLE #Locks
(
	[spid]					SMALLINT
	,[dbid]					SMALLINT
	,[ReadLocks]			INT
	,[WriteLocks]			INT
	,[SchemaLocks]			INT
)

-- Get lock data
SELECT
	[spid]
	,[dbid]
	,CASE 
		WHEN [LockMode] IN('S','IS','RangeS_S','RangeI_S') THEN 'Read'
		WHEN [LockMode] IN('U','X','IU','IX','SIU','SIX','UIX','BU','RangeS_U','RangeI_N','RangeI_U','RangeI_X','RangeX_S','RangeX_U','RangeX_X') THEN 'Write'
		WHEN [LockMode] IN('Sch-S','Sch-M') THEN 'Schema'
	ELSE [LockMode]
	END AS [LockMode]
	,COUNT(*) AS [LockCount]
INTO #RawLocks
FROM [dbo].[ActiveDBLocks_vw]
GROUP BY [spid], [dbid], [LockMode]

-- Populate the pivoted table with zeros
INSERT INTO #Locks ([spid],[dbid],[ReadLocks],[WriteLocks],[SchemaLocks])
SELECT DISTINCT
	[spid]
	,[dbid]
	,0
	,0
	,0
FROM #RawLocks

-- Get Read Locks
UPDATE l
	SET [ReadLocks] = rl.[LockCount]
FROM #Locks l
INNER JOIN (SELECT
	[spid]
	,[dbid]				
	,SUM([LockCount])	AS [LockCount]
FROM #RawLocks
WHERE [LockMode] = 'Read'
GROUP BY [spid], [dbid]
) rl
	ON rl.[spid] = l.[spid]
	AND rl.[dbid] = l.[dbid]
	
-- Get Write Locks
UPDATE l
	SET [WriteLocks] = rl.[LockCount]
FROM #Locks l
INNER JOIN (SELECT
	[spid]
	,[dbid]				
	,SUM([LockCount])	AS [LockCount]
FROM #RawLocks
WHERE [LockMode] = 'Write'
GROUP BY [spid], [dbid]
) rl
	ON rl.[spid] = l.[spid]
	AND rl.[dbid] = l.[dbid]
	
-- Get Schema Locks
UPDATE l
	SET [SchemaLocks] = rl.[LockCount]
FROM #Locks l
INNER JOIN (SELECT
	[spid]
	,[dbid]				
	,SUM([LockCount])	AS [LockCount]
FROM #RawLocks
WHERE [LockMode] = 'Schema'
GROUP BY [spid], [dbid]
) rl
	ON rl.[spid] = l.[spid]
	AND rl.[dbid] = l.[dbid]
	
IF OBJECT_ID('tempdb.dbo.#RawLocks') IS NOT NULL
	DROP TABLE #RawLocks

-- Insert data
INSERT INTO #Activity
([spid], [KPID], [DBName], [Login], [HostName], [Status], [Command], [BlkBy], [TranCount], [ReadLockCount], [WriteLockCount], [SchemaLockCount], [WaitType], [CPU], [IO], [StartTime], [LastBatch], [ProgramName])
SELECT
	s.[spid]						AS [spid]
	,s.[kpid]						AS [KPID]
	,DB_NAME(s.[dbid])				AS [DBName]
	,LTRIM(RTRIM(s.[loginame]))		AS [Login]
	,LTRIM(RTRIM(s.[hostname]))		AS [HostName]
	,LTRIM(RTRIM(s.[status]))		AS [Status]
	,s.[cmd]						AS [Command]
	,s.[blocked]					AS [BlkBy]
	,s.[open_tran]					AS [Trancount]
	,ISNULL(lo.[ReadLocks],0)		AS [ReadLockCount]
	,ISNULL(lo.[WriteLocks],0)		AS [WriteLockCount]
	,ISNULL(lo.[SchemaLocks],0)		AS [SchemaLockCount]
	,LTRIM(RTRIM(s.[lastwaittype]))	AS [WaitType]
	,s.[cpu]						AS [CPU]
	,s.[physical_io]				AS [IO]
	,s.[login_time]					AS [LoginTime]
	,s.[last_batch]					AS [LastBatch]
	,CASE
		WHEN s.[program_name] LIKE 'SQLAgent - TSQL JobStep%' THEN 'SQLAgent Job: ' + SUBSTRING(j.[name],1,242)
		ELSE LTRIM(RTRIM(SUBSTRING(s.[program_name],1,256)))
	END AS [ProgramName]
FROM [master].[dbo].[sysprocesses] s
LEFT JOIN [msdb].[dbo].[sysjobs] j
	ON SUBSTRING(ISNULL(s.[program_name],''),CHARINDEX('0x', ISNULL(s.[program_name],'')) + 18, 16) = SUBSTRING(REPLACE(ISNULL(j.[job_id],''), '-',''),17,16)  
LEFT JOIN #Locks lo
	ON s.[spid] = lo.[spid]
	AND s.[dbid] = lo.[dbid]
LEFT JOIN [dbo].[Split_fn](@DBName,',') d
	ON DB_NAME(s.[dbid]) = d.[item]
LEFT JOIN [dbo].[Split_fn](@DBNameExclude,',') de
	ON DB_NAME(s.[dbid]) = de.[item]
LEFT JOIN [dbo].[Split_fn](@Login,',') l
	ON s.[loginame] = l.[item]
LEFT JOIN [dbo].[Split_fn](@LoginExclude,',') le
	ON s.[loginame] = le.[item]
LEFT JOIN [dbo].[Split_fn](@HostName,',') h
	ON s.[hostname] = h.[item]
LEFT JOIN [dbo].[Split_fn](@HostNameExclude,',') he
	ON s.[hostname] = he.[item]
WHERE s.[spid] <> @@SPID -- Exclude your process
AND s.[spid] >= ISNULL(@FilterSpid,1) -- Exclude filtered spid range
AND ((d.[item] IS NOT NULL AND @DBName IS NOT NULL) OR @DBName IS NULL) -- Specified databases, or all
AND ((l.[item] IS NOT NULL AND @Login IS NOT NULL) OR @Login IS NULL) -- Specified logins, or all
AND ((h.[item] IS NOT NULL AND @HostName IS NOT NULL) OR @HostName IS NULL) -- Specified hosts, or all
AND le.[item] IS NULL -- All but excluded logins
AND de.[item] IS NULL -- All but excluded databases
AND he.[item] IS NULL -- All but excluded hosts
ORDER BY s.[BlkBy], DB_NAME(s.[dbid]), s.[cpu]

-- Get DBCC INPUTBUFFER per spid
DECLARE #spids CURSOR FAST_FORWARD FOR
SELECT [spid] FROM #Activity

OPEN #spids
FETCH NEXT FROM #spids INTO @SPID
WHILE @@FETCH_STATUS = 0
BEGIN
	
	INSERT INTO #QueryCmd 
	([EventType], [Parameters], [Command])
	EXEC('DBCC INPUTBUFFER( ' + @SPID + ') WITH NO_INFOMSGS')

	SELECT @SQLID = MAX(SQLID) FROM #QueryCmd
	UPDATE #QueryCmd 
		SET [spid] = @SPID 
	WHERE SQLID = @SQLID 	

FETCH NEXT FROM #spids INTO @SPID
END
CLOSE #spids
DEALLOCATE #spids

-- Update main table with new spid info
UPDATE a
	SET a.[Query] = q.[Command]
FROM #Activity a
JOIN #QueryCmd q
	ON a.[spid] = q.[spid]

-- Return or log results
IF @Logging = 1
BEGIN
	INSERT INTO [Logging_ActivityMonitor]
	([spid], [KPID], [DBName], [Query], [Login], [HostName], [Status], [Command], [BlkBy], [TranCount], [ReadLockCount], [WriteLockCount], [SchemaLockCount], [WaitType], [CPU], [IO], [StartTime], [LastBatch], [ProgramName])
	SELECT 
		[spid]
		,[KPID]
		,[DBName]
		,[Query]
		,[Login]
		,[HostName]
		,[Status]
		,[Command]
		,[BlkBy]
		,[TranCount]
		,[ReadLockCount]
		,[WriteLockCount]
		,[SchemaLockCount]
		,[WaitType]
		,[CPU]
		,[IO]
		,[StartTime]
		,[LastBatch]
		,[ProgramName]
	FROM #Activity
END
ELSE
BEGIN
	EXEC('
			SELECT * FROM #Activity
			ORDER BY ' + @OrderBy1 + ', ' + @OrderBy2 + ', ' + @OrderBy3 + '
		')
END

SET NOCOUNT OFF